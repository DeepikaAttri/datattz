package com.thetattz.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.aigestudio.wheelpicker.BuildConfig;
import com.aigestudio.wheelpicker.WheelPicker;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class BaseActivity extends AppCompatActivity {

    private String TAG = BaseActivity.this.getClass().getSimpleName();
    public static Dialog progressDialog;

    //Remove E from Double Value
    public static String convertEvalueToNormal(double value) //Got here 6.743240136E7 or something..
    {
        DecimalFormat formatter;

        if (value - (int) value > 0.0)
            formatter = new DecimalFormat("0.0"); // Here you can also deal with rounding if you wish..
        else
            formatter = new DecimalFormat("0.0");


        String money = formatter.format(value).replace(',', '.');

        return money;
    }

    /*
     * Get Day Number //21-04-2020//dd-MM-yyyy
     * */
    public static String convertTimeToDayNumber(String input) {
        String[] mArray = input.split("-");

        return mArray[0];
    }

    /*
     * Get Month Name //21-04-2020
     * */
    public static String convertTimeToMonthName3Digit(String input) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat inFormat = new SimpleDateFormat("dd-MM-yyyy");

        Date date = null;
        try {
            date = inFormat.parse(input);
        } catch (ParseException e) {
        }

        SimpleDateFormat outFormat = new SimpleDateFormat("MMM");
        String goal = outFormat.format(date);

        return goal;
    }

    /*
     * Get Is user login or not
     * */
    public boolean isLogin() {
        return TheTattzPrefrences.readBoolean(BaseActivity.this, TheTattzPrefrences.ISLOGIN, false);
    }

    /*
     * Get Is user id
     * */
    public String getUserID() {
        return TheTattzPrefrences.readString(BaseActivity.this, TheTattzPrefrences.USER_ID, "");
    }

    /*
     * Get Is user Email
     * */
    public String getUserEmail() {
        return TheTattzPrefrences.readString(BaseActivity.this, TheTattzPrefrences.USER_EMAIL, "");
    }

    /*
     * Get Is user Email
     * */
    public String getUserRole() {
        return TheTattzPrefrences.readString(BaseActivity.this, TheTattzPrefrences.USER_ROLE, "");
    }

    /*
     * Get Is user Name
     * */
    public String getUserName() {
        return TheTattzPrefrences.readString(BaseActivity.this, TheTattzPrefrences.USER_NAME, "");
    }

    /*
     * Get password
     * */
    public String getPassword() {
        return TheTattzPrefrences.readString(BaseActivity.this, TheTattzPrefrences.USER_PASSWORD, "");
    }

    /*
     * Get Is user Profile Pic
     * */
    public String getUserProfilePicture() {
        return TheTattzPrefrences.readString(BaseActivity.this, TheTattzPrefrences.USER_PROFILE_PIC, "");
    }

    /*
     * Get Current Latitude
     * */
    public String getCurrentLatitude() {
        return TheTattzPrefrences.readString(BaseActivity.this, TheTattzPrefrences.CURRENT_LOCATION_LATITUDE, "");
    }

    /*
     * Get Current Longitude
     * */
    public String getCurrentLongitude() {
        return TheTattzPrefrences.readString(BaseActivity.this, TheTattzPrefrences.CURRENT_LOCATION_LONGITUDE, "");
    }

    /*
     * Get getAccountDetailsID
     * */
    public String getAccountDetailsID() {
        return TheTattzPrefrences.readString(BaseActivity.this, TheTattzPrefrences.ACCOUNT_DETAIL_ID, "");
    }

    /*
     * Get PayPal Verification
     * */
    public String getPaypalVerification() {
        return TheTattzPrefrences.readString(BaseActivity.this, TheTattzPrefrences.PAYPAL_VERIFICATION, "");
    }

    /*
     * Get PayPal Email
     * */
    public String getPaypalEmail() {
        return TheTattzPrefrences.readString(BaseActivity.this, TheTattzPrefrences.PAYPAL_EMAIL_ID, "");
    }

    /*
     * Get Is subscription active or not
     * */
    public boolean isSubscriptionActive() {
        return TheTattzPrefrences.readBoolean(BaseActivity.this, TheTattzPrefrences.isSubscriptionActive, false);
    }

    /*
     * Finish the activity
     * */
    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionSlideDownExit();
    }

    /*
     * To Start the New Activity
     * */
    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionSlideUPEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    public void overridePendingTransitionSlideUPEnter() {
        overridePendingTransition(R.anim.bottom_up, 0);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    public void overridePendingTransitionSlideDownExit() {
        overridePendingTransition(0, R.anim.bottom_down);
    }

    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);

        progressDialog.show();


    }

    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /*
     * Hide Keyboard
     * */
    public boolean hideKeyBoad(Context context, View view) {
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        return true;
    }

    /*
     * Validate Email Address
     * */
    public boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    /*
     * Share Gmail Intent
     * */
    public void shareIntentGmail(Activity mActivity) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "");
        mActivity.startActivity(Intent.createChooser(sharingIntent, mActivity.getString(R.string.app_name)));
    }

    /*
     * Error Toast Message
     * */
    public void showToast(Activity mActivity, String strMessage) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     *
     * Success Alert Dialog Finish Activity
     * */
    public void showSuccessAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }

    /*
     *
     * Convert Simple Date to Formated Date
     * */
    public String getConvertedDate(String mString) {
        String[] mArray = mString.split("T");
        String formattedDate = "";
        try {
            DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd");

            // parse the date string into Date object
            Date date = srcDf.parse(mArray[0]);

            DateFormat destDf = new SimpleDateFormat("dd MMMM, yyyy");

            // format the date into another format
            formattedDate = destDf.format(date);

            System.out.println("Converted date is : " + formattedDate);
        } catch (Exception e) {
            e.toString();
        }

        return formattedDate;
    }

    /*
     * Convert Time String to Milliseconds
     * */
    public long convertTimeStringToLong(String strDate) {
        long millisecondsSinceEpoch = 0;
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
        Date parseDate = null;
        try {
            parseDate = f.parse(strDate);
            millisecondsSinceEpoch = parseDate.getTime();
            return millisecondsSinceEpoch;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return millisecondsSinceEpoch;
    }

    /*
     * Convert Time String to Milliseconds //08-04-2020 12:45 PM
     * */
    public long convertTimeStringToLongNotifications(String strDate) {
        long millisecondsSinceEpoch = 0;
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
        Date parseDate = null;
        try {
            parseDate = f.parse(strDate);
            millisecondsSinceEpoch = parseDate.getTime();
            return millisecondsSinceEpoch;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return millisecondsSinceEpoch;
    }


    /*
     * Convert Time String to Milliseconds //18/04/2020
     * */
    public long convertTimeStringToLongChat(String strDate) {
        long millisecondsSinceEpoch = 0;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        Date parseDate = null;
        try {
            parseDate = f.parse(strDate);
            millisecondsSinceEpoch = parseDate.getTime();
            return millisecondsSinceEpoch;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "**Milliseconds***" + millisecondsSinceEpoch);
        return millisecondsSinceEpoch;
    }


    /*
     * Convert Time String to Milliseconds //18/04/2020
     * */
    public long convertTimeStringToLongChatNew(String strDate) {
        long millisecondsSinceEpoch = 0;
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        Date parseDate = null;
        try {
            parseDate = f.parse(strDate);
            millisecondsSinceEpoch = parseDate.getTime();
            return millisecondsSinceEpoch;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "**Milliseconds***" + millisecondsSinceEpoch);
        return millisecondsSinceEpoch;
    }

    /*
     * Convert Time String to Milliseconds
     * */
    public long convertTimeStringToLongAppointment(String strDate) {
        long millisecondsSinceEpoch = 0;
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
        Date parseDate = null;
        try {
            parseDate = f.parse(strDate);
            millisecondsSinceEpoch = parseDate.getTime();
            return millisecondsSinceEpoch;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return millisecondsSinceEpoch;
    }

    /*
     * Convert Time Milliseconds to String
     * @Return date in specified format.
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format
     * @return String representing date in specified format
     * */
    public String convertTimeLongToString(Long mMilliseconds) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mMilliseconds);
        return formatter.format(calendar.getTime());
    }

    /*
     * Convert Time Milliseconds to String
     * @Return date in specified format.
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format
     * @return String representing date in specified format
     * */
    public String convertArtistAppointmentTimeLongToString(Long mMilliseconds) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mMilliseconds);
        return formatter.format(calendar.getTime());
    }

    /*
     * Convert Time Milliseconds to String
     * @Return date in specified format.
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format
     * @return String representing date in specified format
     * */
    public String convertTimeLongToStringMonthYear(Long mMilliseconds) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mMilliseconds);
        return formatter.format(calendar.getTime());
    }

    /*
     * Get Day Full Name
     * */
    public String convertTimeToDayName(String input) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat inFormat = new SimpleDateFormat("dd-MM-yyyy");

        Date date = null;
        try {
            date = inFormat.parse(input);
        } catch (ParseException e) {
        }

        SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
        String goal = outFormat.format(date);

        return goal;
    }

    /*
     * Get Day Full Name
     * */
    public String convertCalenderTimeToFormat(String input) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat inFormat = new SimpleDateFormat("E MMM dd hh:mm:ss z yyyy");

        Date date = null;
        try {
            date = inFormat.parse(input);
        } catch (ParseException e) {
        }

        SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd");
        String goal = outFormat.format(date);

        return goal;
    }

    /*
     *
     * function to convert imageView to Bitmap
     * */
    public Bitmap convertImageViewToBitmap(ImageView v) {

        Bitmap bm = ((BitmapDrawable) v.getDrawable()).getBitmap();

        return bm;
    }

    /*
     * Getting Screen Type: Like mobile phone, 7'' tablet, 10'' tablet
     * */
    public String getDeviceSizeType() {
        String strType = getResources().getString(R.string.screen_type);
        return strType;
    }

    /*
     * Generate String Dynamically
     * */
    public String getAlphaNumericString() {
        int n = 20;

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    @SuppressLint("NewApi")
    public String getRealPathFromURI_API19(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public String gettingMonthNameFromNumber(String strNum) {
        if (strNum.equals("1")) {
            return "JAN";
        } else if (strNum.equals("2")) {
            return "FEB";
        } else if (strNum.equals("3")) {
            return "MAR";
        } else if (strNum.equals("4")) {
            return "APR";
        } else if (strNum.equals("5")) {
            return "MAY";
        } else if (strNum.equals("6")) {
            return "JUN";
        } else if (strNum.equals("7")) {
            return "JUL";
        } else if (strNum.equals("8")) {
            return "AUG";
        } else if (strNum.equals("9")) {
            return "SEP";
        } else if (strNum.equals("10")) {
            return "OCT";
        } else if (strNum.equals("11")) {
            return "NOV";
        } else if (strNum.equals("12")) {
            return "DEC";
        } else {
            return "";
        }
    }

    /*
     * Share app links
     * */
    public void shareAppLink() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            String shareMessage = "\nLet me recommend you this application\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "Choose One"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    /*
     * Get Hours List
     * */
    public List<String> getHours() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.HOURS.length; i++) {
            mArrayList.add(Constants.HOURS[i]);
        }
        return mArrayList;
    }

    /*
     * Get Hours Text List
     * */
    public List<String> getHoursText() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.HOURS_TEXT.length; i++) {
            mArrayList.add(Constants.HOURS_TEXT[i]);
        }
        return mArrayList;
    }

    /*
     * Get Hours Time Text List
     * */
    public List<String> getHoursTimeText() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.HOURSTIME.length; i++) {
            mArrayList.add(Constants.HOURSTIME[i]);
        }
        return mArrayList;
    }

    /*
     * Get Minutes List
     * */
    public List<String> getMinutes() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.MINUTESS.length; i++) {
            mArrayList.add(Constants.MINUTESS[i]);
        }
        return mArrayList;
    }

    /*
     * Get Minutes only 0 & 30  List
     * */
    public List<String> getMinutesInLimit() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.MINUTESS_IN_LIMIT.length; i++) {
            mArrayList.add(Constants.MINUTESS_IN_LIMIT[i]);
        }
        return mArrayList;
    }

    /*
     * Get Minutes only 00 & 30  List
     * */
    public List<String> getMinutesLimit() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.MINUTESS_LIMIT.length; i++) {
            mArrayList.add(Constants.MINUTESS_LIMIT[i]);
        }
        return mArrayList;
    }

    /*
     * Get Minutes Text
     * */
    public List<String> getMinutesText() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.MINUTES_TEXT.length; i++) {
            mArrayList.add(Constants.MINUTES_TEXT[i]);
        }
        return mArrayList;
    }

    /*
     * Get AM_PM List
     * */
    public List<String> getAmPm() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.AMPM.length; i++) {
            mArrayList.add(Constants.AMPM[i]);
        }
        return mArrayList;
    }

    /*
     * Get Months List
     * */
    public List<String> getMonths() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.MONTHS_OF_YEAR.length; i++) {
            mArrayList.add(Constants.MONTHS_OF_YEAR[i]);
        }
        return mArrayList;
    }

    /*
     * Get Years List
     * */
    public List<String> getYears() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.YEARS.length; i++) {
            mArrayList.add(Constants.YEARS[i]);
        }
        return mArrayList;
    }

    /*
     *
     * Convert Month Name to Month Number
     * */
    public String getMonthNumberFromName(String strMonth) {
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[0])) {
            return "01";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[1])) {
            return "02";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[2])) {
            return "03";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[3])) {
            return "04";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[4])) {
            return "05";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[5])) {
            return "06";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[6])) {
            return "07";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[7])) {
            return "08";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[8])) {
            return "09";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[9])) {
            return "10";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[10])) {
            return "11";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[11])) {
            return "12";
        } else {
            return null;
        }

    }

    /*
     * Date Picker Dialog
     * */
    public void showDatePickerDialog(Activity mActivity, final TextView mTextView) {

        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_date_picker);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        final DatePicker mDatePickerDP = alertDialog.findViewById(R.id.mDatePickerDP);
        TextView txtCancelTV = alertDialog.findViewById(R.id.txtCancelTV);
        TextView txtDoneTV = alertDialog.findViewById(R.id.txtDoneTV);

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        txtDoneTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                if (mDatePickerDP.getDayOfMonth() < 10 && (mDatePickerDP.getMonth() + 1) < 10) {
                    mTextView.setText("0" + mDatePickerDP.getDayOfMonth() + "-" + "0" + (mDatePickerDP.getMonth() + 1) + "-" + mDatePickerDP.getYear());
                } else if (mDatePickerDP.getDayOfMonth() < 10) {
                    mTextView.setText("0" + mDatePickerDP.getDayOfMonth() + "-" + (mDatePickerDP.getMonth() + 1) + "-" + mDatePickerDP.getYear());
                } else if ((mDatePickerDP.getMonth() + 1) < 10) {
                    mTextView.setText(mDatePickerDP.getDayOfMonth() + "-" + "0" + (mDatePickerDP.getMonth() + 1) + "-" + mDatePickerDP.getYear());
                } else {
                    mTextView.setText(mDatePickerDP.getDayOfMonth() + "-" + (mDatePickerDP.getMonth() + 1) + "-" + mDatePickerDP.getYear());
                }
            }
        });
        alertDialog.show();
    }

    public String getFormatedString(String strTemp) {
        String strActual = "";

        if (strTemp.length() == 1) {
            strActual = "0" + strTemp;
        } else if (strTemp.length() == 2) {
            strActual = strTemp;
        }

        return strActual;
    }

    public void createBottomSheetDialog(final Activity mActivity, final TextView mTextView) {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        @SuppressLint("InflateParams") View sheetView = mActivity.getLayoutInflater().inflate(R.layout.dialog_bottomsheet_timer, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        Typeface mTypeface = Typeface.createFromAsset(getAssets(), "ProximaNova-Regular.ttf");

        final WheelPicker startHourWP = sheetView.findViewById(R.id.startHourWP);
        startHourWP.setTypeface(mTypeface);
        final WheelPicker startMinuteWP = sheetView.findViewById(R.id.startMinuteWP);
        startMinuteWP.setTypeface(mTypeface);
        final WheelPicker startAmPmWP = sheetView.findViewById(R.id.startAmPmWP);
        startAmPmWP.setTypeface(mTypeface);
        final WheelPicker endHourWP = sheetView.findViewById(R.id.endHourWP);
        endHourWP.setTypeface(mTypeface);
        final WheelPicker endMinuteWP = sheetView.findViewById(R.id.endMinuteWP);
        endMinuteWP.setTypeface(mTypeface);
        final WheelPicker endAmPmWP = sheetView.findViewById(R.id.endAmPmWP);
        endAmPmWP.setTypeface(mTypeface);

        TextView txtCancelTV = sheetView.findViewById(R.id.txtCancelTV);
        TextView txtSaveTV = sheetView.findViewById(R.id.txtSaveTV);


        startHourWP.setAtmospheric(true);
        startHourWP.setCyclic(false);
        startHourWP.setCurved(true);
        startHourWP.setData(getHours());

        endHourWP.setAtmospheric(true);
        endHourWP.setCyclic(false);
        endHourWP.setCurved(true);
        endHourWP.setData(getHours());


        startMinuteWP.setAtmospheric(true);
        startMinuteWP.setCyclic(false);
        startMinuteWP.setCurved(true);
        startMinuteWP.setData(getMinutes());

        endMinuteWP.setAtmospheric(true);
        endMinuteWP.setCyclic(false);
        endMinuteWP.setCurved(true);
        endMinuteWP.setData(getMinutes());

        startAmPmWP.setAtmospheric(true);
        startAmPmWP.setCyclic(false);
        startAmPmWP.setCurved(true);
        startAmPmWP.setData(getAmPm());

        endAmPmWP.setAtmospheric(true);
        endAmPmWP.setCyclic(false);
        endAmPmWP.setCurved(true);
        endAmPmWP.setData(getAmPm());

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        //11:00 PM - 04:02 PM
        txtSaveTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strStartHour = getHours().get(startHourWP.getCurrentItemPosition());
                String strStartMinute = getMinutes().get(startMinuteWP.getCurrentItemPosition());
                String strStartAmPm = getAmPm().get(startAmPmWP.getCurrentItemPosition());

                String strEndHour = getHours().get(endHourWP.getCurrentItemPosition());
                String strEndMinute = getMinutes().get(endMinuteWP.getCurrentItemPosition());
                String strEndAmPm = getAmPm().get(endAmPmWP.getCurrentItemPosition());

                String time1 = strStartHour + ":" + strStartMinute + ":00" + " " + strStartAmPm;
                String time2 = strEndHour + ":" + strEndMinute + ":00" + " " + strEndAmPm;


                if (getTimeDiffrence(time1, time2) > 0 && getTimeDiffrence(time1, time2) < 24) {
                    mTextView.setText(strStartHour + ":" + strStartMinute + " " + strStartAmPm + " - " + strEndHour + ":" + strEndMinute + " " + strEndAmPm);
                } else {
                    mTextView.setText("");
                    showToast(mActivity, getString(R.string.selecct_invalid));
                }


                mBottomSheetDialog.dismiss();
            }
        });
    }

    private long getTimeDiffrence(String time1, String time2) {
        Log.e(TAG, "Time 1 :>>" + time1);
        Log.e(TAG, "Time 2 :>>" + time2);
        SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss aaa");
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = format.parse(time1);
            date2 = format.parse(time2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diff = date2.getTime() - date1.getTime();

        Log.e(TAG, "Diffrence : " + diff);

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        Log.e(TAG, "hours: " + diffHours);

        return diffHours;
    }

    /*
     *
     * Success Alert Dialog Finish Activity
     * */
    public void showPaymentTransactionDialog(final Activity mActivity, String strTractionID, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_transaction_id);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView txtTrasactionIDTV = alertDialog.findViewById(R.id.txtTrasactionIDTV);
        TextView txtOkayTV = alertDialog.findViewById(R.id.txtOkayTV);

        txtMessageTV.setText(strMessage);
        txtTrasactionIDTV.setText("Your Transaction id is : \n\n" + strTractionID);
        txtOkayTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
//                mActivity.finish();
                startActivity(new Intent(mActivity, HomeActivity.class));
                finishAffinity();
            }
        });
        alertDialog.show();
    }

    /*
     * Get Appointment Details Time Format
     * */
    public String convertAppointmentDetialsFormat(String input) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat inFormat = new SimpleDateFormat("dd-MM-yyyy");

        Date date = null;
        try {
            date = inFormat.parse(input);
        } catch (ParseException e) {
        }

        SimpleDateFormat outFormat = new SimpleDateFormat("EEE, MMMM dd, yyyy");
        String goal = outFormat.format(date);

        return goal;
    }


    /*
     * Compare two dates for Appointment Done
     * */
    public boolean IsAppointmentComplete(String appointmentDate) {
        boolean isDateConfirm = false;
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date dateobj = new Date();
        String currentDate = df.format(dateobj);

//        Date today = new Date();
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
//        String currentDate = format.format(today);

        Log.e(TAG, "***Current Date***" + currentDate);
        Log.e(TAG, "***Appointment Date***" + appointmentDate);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date mAppointmentDate = sdf.parse(appointmentDate);
            Date mCurrentDate = sdf.parse(currentDate);

            if (mAppointmentDate.after(mCurrentDate)) {
                isDateConfirm = false;
                Log.e(TAG, "Date1 is after Date2");
            }

            if (mAppointmentDate.before(mCurrentDate)) {
                isDateConfirm = true;
                Log.e(TAG, "Date1 is before Date2");
            }

            if (mAppointmentDate.equals(mCurrentDate)) {
                isDateConfirm = true;
                Log.e(TAG, "Date1 is equal Date2");
            }
        } catch (Exception e) {
            Log.e(TAG, "****ERROR****" + e.toString());
        }


        return isDateConfirm;
    }


    /*
     * Compare two dates for Appointment Done
     * */
    public boolean IsCalenderAvailable(String appointmentDate) {
        boolean isDateConfirm = false;
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date dateobj = new Date();
        String currentDate = df.format(dateobj);

        Log.e(TAG, "***Current Date***" + currentDate);
        Log.e(TAG, "***Appointment Date***" + appointmentDate);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date mAppointmentDate = sdf.parse(appointmentDate);
            Date mCurrentDate = sdf.parse(currentDate);

            if (mAppointmentDate.after(mCurrentDate)) {
                isDateConfirm = true;
                Log.e(TAG, "Date1 is after Date2");
            }

            if (mAppointmentDate.before(mCurrentDate)) {
                isDateConfirm = false;
                Log.e(TAG, "Date1 is before Date2");
            }

            if (mAppointmentDate.equals(mCurrentDate)) {
                isDateConfirm = false;
                Log.e(TAG, "Date1 is equal Date2");
            }
        } catch (Exception e) {
            Log.e(TAG, "****ERROR****" + e.toString());
        }


        return isDateConfirm;
    }


    public void createAppointmentBottomSheetDialog(final TextView mTextView) {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        @SuppressLint("InflateParams") View sheetView = getLayoutInflater()
                .inflate(R.layout.dialog_service_duration_appoint, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        Typeface mTypeface = Typeface.createFromAsset(getAssets(), "ProximaNova-Regular.ttf");

        final WheelPicker hoursWP = sheetView.findViewById(R.id.hoursWP);
        hoursWP.setTypeface(mTypeface);

        final WheelPicker minuteWP = sheetView.findViewById(R.id.minuteWP);
        minuteWP.setTypeface(mTypeface);

        final WheelPicker amPMWP = sheetView.findViewById(R.id.amPMWP);
        amPMWP.setTypeface(mTypeface);


        TextView txtCancelTV = sheetView.findViewById(R.id.txtCancelTV);
        TextView txtSaveTV = sheetView.findViewById(R.id.txtSaveTV);


        hoursWP.setAtmospheric(true);
        hoursWP.setCyclic(false);
        hoursWP.setCurved(true);
        hoursWP.setData(getHoursTimeText());

        minuteWP.setAtmospheric(true);
        minuteWP.setCyclic(false);
        minuteWP.setCurved(true);
        minuteWP.setData(getMinutesLimit());

        amPMWP.setAtmospheric(true);
        amPMWP.setCyclic(false);
        amPMWP.setCurved(true);
        amPMWP.setData(getAmPm());


        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        //11:00 PM - 04:02 PM
        txtSaveTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strHour = getHoursTimeText().get(hoursWP.getCurrentItemPosition());
                String strMinutes = getMinutesLimit().get(minuteWP.getCurrentItemPosition());
                String strAM = getAmPm().get(amPMWP.getCurrentItemPosition());


                mTextView.setText(strHour + ":" + strMinutes + " " + strAM);


                mBottomSheetDialog.dismiss();
            }
        });
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFullName(String fullname) {
        String[] firstLastName = {};
        String FirstName = "", LastName = "";
        if (fullname != null && fullname.contains(" ")) {
            firstLastName = fullname.split(" ");
            FirstName = fullname.substring(0, fullname.lastIndexOf(' '));
            LastName = fullname.substring(fullname.lastIndexOf(" ") + 1);
        } else {
            FirstName = fullname;
        }
        String fname = "";
        String lname = "";
        if (FirstName != null && !FirstName.equals("")) {
            fname = FirstName.substring(0, 1).toUpperCase() + FirstName.substring(1).toLowerCase();
        }
        if (LastName != null && !LastName.equals("")) {
            lname = LastName.substring(0, 1).toUpperCase() + LastName.substring(1).toLowerCase();
        }
        String name = fname + " " + lname;

        return name;
    }

    //Capitalized first letter of String
    public String CapitalizedString(String myString) {
        String upperString = myString.substring(0, 1).toUpperCase() + myString.substring(1).toLowerCase();

        return upperString;
    }

    public String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 30, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    public byte[] convertBitmapToByteArrayUncompressed(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        Log.e(TAG, "value--" + byteArray);
        return byteArray;
    }

    public void HideKey() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = getCurrentFocus();
        /*
         * If no view is focused, an NPE will be thrown
         *
         * Maxim Dmitriev
         */
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /*
     * Set Up Status Bar
     * */
    public void setStatusBar() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorWhite));// set status background white
    }

    /* implement update badge count API
     * API NAME: updateBadgeCount.php
     * Method : POST (RAW)
     * Param: user_id */
    public void executeUpdateBadgeCountApi(Activity mActivity) {
        String mApiUrl = Constants.UPDATE_BADGE_COUNT;
        Log.e("TAG", "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            Log.e("TAG", "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("TAG", "**RESPONSE**" + response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("TAG", "**ERROR**" + error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }
        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }
}
