package com.thetattz.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.chat.ChatActivity;
import com.thetattz.models.AppointmentsModel;
import com.thetattz.models.ArtistAppointmentDetailsModel;
import com.thetattz.models.ArtistConfirmCancelModel;
import com.thetattz.models.RoomIdModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class UpcomingAppointDetailsActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = UpcomingAppointDetailsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = UpcomingAppointDetailsActivity.this;

    /**
     * Widgets Main Layout
     */
    @BindView(R.id.rlBackRL)
    RelativeLayout rlBackRL;
    @BindView(R.id.imgProfilePicCIV)
    CircleImageView imgProfilePicCIV;
    @BindView(R.id.txtNameTV)
    TextView txtNameTV;
    @BindView(R.id.txtStatusTV)
    TextView txtStatusTV;
    @BindView(R.id.txtDayFormatTV)
    TextView txtDayFormatTV;
    @BindView(R.id.txtMonthFormatTV)
    TextView txtMonthFormatTV;
    @BindView(R.id.txtStartEndTimeTV)
    TextView txtStartEndTimeTV;
    @BindView(R.id.layoutCancelLL)
    LinearLayout layoutCancelLL;
    @BindView(R.id.txtFullDateFormatTV)
    TextView txtFullDateFormatTV;
    @BindView(R.id.txtTimeSlotFormatTV)
    TextView txtTimeSlotFormatTV;
    @BindView(R.id.txtAddressTV)
    TextView txtAddressTV;
    @BindView(R.id.txtPriceTV)
    TextView txtPriceTV;
    @BindView(R.id.btnSendMessageB)
    Button btnSendMessageB;

    /*
     * Initialize...Objects
     * */
    AppointmentsModel mAppointmentsModel;
    String strPaymentMethod = "";
    ArtistAppointmentDetailsModel mModel;
    String PaymentMethod = "";

    //For canel Appointment Status is : 3
    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upcoming_appoint_details);
        setStatusBar();
        ButterKnife.bind(this);
        getIntentData();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            mAppointmentsModel = (AppointmentsModel) getIntent().getSerializableExtra(Constants.MODEL);
            getDetialsData();
        }
    }

    /*
     * Widget Click Listener
     * */
    @OnClick({R.id.rlBackRL, R.id.layoutCancelLL, R.id.btnSendMessageB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBackRL:
                onBackPressed();
                break;
            case R.id.layoutCancelLL:
                performCancelClick();
                break;
            case R.id.btnSendMessageB:
                performSendMessageClick();
                break;
        }
    }

    private void performSendMessageClick() {
        if (ConnectivityReceiver.isConnected()) {
            executeChatRoomID();
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    private void performCancelClick() {
        if (ConnectivityReceiver.isConnected())
            executeStatusCancelApi("3", "client");
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }


    private void getDetialsData() {
        if (ConnectivityReceiver.isConnected())
            executeApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));

    }

    private void executeApi() {
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        String mTimeZone = timeZone.getID();

        showProgressDialog(mActivity);
        final String mApiUrl = Constants.APPOINTMENT_DETATILS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("appointment_id", mAppointmentsModel.getAppointment_id());
            mParams.put("appointment_status", "2");
            mParams.put("timezone", mTimeZone);

            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        try {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.serializeNulls();
                            Gson gson = gsonBuilder.create();
                            mModel = gson.fromJson(response.toString(), ArtistAppointmentDetailsModel.class);
                            strPaymentMethod = mModel.getData().getPayment_method();
                            if (mModel.getStatus() == 1) {
                                setDataOnWidgets(mModel);
                            } else {
                                showToast(mActivity, mModel.getMessage());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "**Error**" + e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void setDataOnWidgets(ArtistAppointmentDetailsModel mModel) {
        //Profile Pic
        if (mModel.getData().getArtist_detail().getprofile_pic() != null && mModel.getData().getArtist_detail().getprofile_pic().contains("http"))
            Picasso.with(mActivity).load(mModel.getData().getArtist_detail().getprofile_pic())
                    .placeholder(R.drawable.ic_p_pp)
                    .error(R.drawable.ic_p_pp)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgProfilePicCIV);
        else
            imgProfilePicCIV.setImageResource(R.drawable.ic_p_pp);

        txtNameTV.setText(CapitalizedFullName(mModel.getData().getArtist_detail().getName()));

        txtStatusTV.setText(getString(R.string.type_confirm));

        txtDayFormatTV.setText(convertTimeToDayNumber(mAppointmentsModel.getDate()));

        txtMonthFormatTV.setText(convertTimeToMonthName3Digit(mAppointmentsModel.getDate()));

        txtStartEndTimeTV.setText(mModel.getData().getAppointment_detail().getStart_time());

        txtFullDateFormatTV.setText(mModel.getData().getAppointment_detail().getDate());
        txtTimeSlotFormatTV.setText(mModel.getData().getAppointment_detail().getStart_time() + " - " + mModel.getData().getAppointment_detail().getEnd_time()
                + "(" + mModel.getData().getAppointment_detail().getTimezoneAbr() + ")");

        //Set Address
        StringBuilder mBuilder = new StringBuilder();
        if (mModel.getData().getAppointment_detail().getShopname() != null && mModel.getData().getAppointment_detail().getShopname().length() > 0) {
            mBuilder.append(mModel.getData().getAppointment_detail().getShopname());
        }
        if (mModel.getData().getAppointment_detail().getBuilding_name() != null && mModel.getData().getAppointment_detail().getBuilding_name().length() > 0) {
            mBuilder.append(", " + mModel.getData().getAppointment_detail().getBuilding_name());
        }
        if (mModel.getData().getAppointment_detail().getStreet_address() != null && mModel.getData().getAppointment_detail().getStreet_address().length() > 0) {
            mBuilder.append(", " + mModel.getData().getAppointment_detail().getStreet_address());
        }
        if (mModel.getData().getAppointment_detail().getCity() != null && mModel.getData().getAppointment_detail().getCity().length() > 0) {
            mBuilder.append(", " + mModel.getData().getAppointment_detail().getCity());
        }
        String strAddress = mBuilder.toString();
        if (strAddress != null && strAddress.length() > 0) {
            txtAddressTV.setText(strAddress);
        } else {
            txtAddressTV.setText(getString(R.string.not_specified));
        }

        if (mModel.getData().getPayment_method().equals("1")) {
            PaymentMethod = "Mobile pay";
        } else if (mModel.getData().getPayment_method().equals("2")) {
            PaymentMethod = "Cash";
        }

        txtPriceTV.setText(mModel.getData().getAppointment_detail().getPrice() + "  " + PaymentMethod);
    }


    /*
     * Call Confirm Status Api
     * */
    private void executeStatusCancelApi(String status, String role) {
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        String mTimeZone = timeZone.getID();

        showProgressDialog(mActivity);
        final String mApiUrl = Constants.UPDATE_APPOINTMENT_STATUS_BY_ARTIST;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("appointment_id", mAppointmentsModel.getAppointment_id());
            mParams.put("appointment_status", status);
            mParams.put("role", role);
            mParams.put("timezone", mTimeZone);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        try {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.serializeNulls();
                            Gson gson = gsonBuilder.create();
                            ArtistConfirmCancelModel mModel = gson.fromJson(response.toString(), ArtistConfirmCancelModel.class);
                            if (mModel.getStatus() == 1) {
                                //Set Data:
//                                showSuccessAlertDialog(mActivity, mModel.getMessage());
                                showSuccessAlertDialog(mActivity, getString(R.string.appointment_cancelled));
                            } else {
                                showToast(mActivity, mModel.getMessage());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "**Error**" + e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };
        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }


    /*
     * Private Room ID:
     *
     * */
    private void executeChatRoomID() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.GET_MESSAGE_ROOM_ID;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("request_id", getUserID());
            mParams.put("user_id", mModel.getData().getArtist_detail().getArtistId());

            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        try {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.serializeNulls();
                            Gson gson = gsonBuilder.create();
                            RoomIdModel mRoomIdModel = gson.fromJson(response.toString(), RoomIdModel.class);
                            if (mRoomIdModel.getStatus() == 1) {
                                mModel.setmAppointmentID(mAppointmentsModel.getAppointment_id());
                                mModel.setmRoomId(mRoomIdModel.getRoomId());

                                Intent mIntent = new Intent(mActivity, ChatActivity.class);
                                mIntent.putExtra(Constants.MODEL, mModel);
                                mIntent.putExtra(Constants.TYPES, getString(R.string.type_client));
                                startActivity(mIntent);
                            } else {
                                showToast(mActivity, mModel.getMessage());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "**Error**" + e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }
}
