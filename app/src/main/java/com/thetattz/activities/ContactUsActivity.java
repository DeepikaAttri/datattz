package com.thetattz.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.fonts.TextViewBold;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ContactUsActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ContactUsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = ContactUsActivity.this;

    /**
     * Widgets Main Layout
     */
    @BindView(R.id.rlBackRL)
    RelativeLayout rlBackRL;
    @BindView(R.id.txtSaveTV)
    TextViewBold txtSaveTV;
    @BindView(R.id.imgProfilePicCIV)
    CircleImageView imgProfilePicCIV;
    @BindView(R.id.txtUserNameTV)
    TextViewBold txtUserNameTV;
    @BindView(R.id.editMessageET)
    EditText editMessageET;
    @BindView(R.id.txtFirstCapitalTV)
    TextView txtFirstCapitalTV;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        setStatusBar();
        //ButterKnife
        ButterKnife.bind(this);
        //Get User Details
        performGetUserDetails();
    }

    /*
     * Widgets click listner
     * */
    @OnClick({R.id.rlBackRL, R.id.txtSaveTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBackRL:
                onBackPressed();
                break;
            case R.id.txtSaveTV:
                perfromSaveClick();
                break;
        }
    }


    /*
     * Getting User Profile Details
     * */
    private void performGetUserDetails() {
        if (ConnectivityReceiver.isConnected())
            executeGetUserDetailsAPI();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }


    /*
     * Submit contact Us details
     * */
    private void perfromSaveClick() {
        if (isValidate()) {
            if (ConnectivityReceiver.isConnected())
                executeContactUsApi();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }


    /*
     * Set up validations
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editMessageET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_message));
            flag = false;
        }
        return flag;
    }


    /*Execute Get User Details API*/
    private void executeGetUserDetailsAPI() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.GET_USER_DETAILS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseDetailsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseDetailsResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                JSONObject mDataObject = response.getJSONObject("data");
                //Set Data On Widgets
                if (!mDataObject.isNull("name")) {
                    txtFirstCapitalTV.setText(mDataObject.getString("name"));
                    txtUserNameTV.setText("Hi, " + mDataObject.getString("name"));
                }

                if (!mDataObject.isNull("profile_pic") && mDataObject.getString("profile_pic").contains("http")){
                    imgProfilePicCIV.setVisibility(View.VISIBLE);
                    txtFirstCapitalTV.setVisibility(View.GONE);
                    Picasso.with(mActivity).load(mDataObject.getString("profile_pic").replace(" ", "%20"))
                            .placeholder(R.drawable.ic_ph_contact)
                            .error(R.drawable.ic_ph_contact)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .into(imgProfilePicCIV);
                }else{
                    imgProfilePicCIV.setVisibility(View.GONE);
                    txtFirstCapitalTV.setVisibility(View.VISIBLE);
                }




            } else {
                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }


    private void executeContactUsApi() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.CONTACT_US;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            mParams.put("content", editMessageET.getText().toString().trim());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());

                        try {
                            if (response.getString("status").equals("1")) {
                               showToast(mActivity,response.getString("message"));
                               finish();
                            } else {
                                showToast(mActivity, response.getString("message"));
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "**Exception**" + e.toString());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }
}
