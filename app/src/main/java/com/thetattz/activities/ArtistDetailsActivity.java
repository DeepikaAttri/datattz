package com.thetattz.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.thetattz.R;
import com.thetattz.fragments.ArtistInfoFragment;
import com.thetattz.fragments.ArtistRatingsFragment;
import com.thetattz.fragments.ArtistServicesFragment;
import com.thetattz.models.MyArtistModel;
import com.thetattz.models.SearchArtistModel;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzSingleton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ArtistDetailsActivity extends AppCompatActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ArtistDetailsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = ArtistDetailsActivity.this;
    /**
     * Widgets
     */
    @BindView(R.id.txtInfoTabTV)
    TextView txtInfoTabTV;
    @BindView(R.id.viewInfoTabV)
    View viewInfoTabV;
    @BindView(R.id.layoutInfoLL)
    LinearLayout layoutInfoLL;
    @BindView(R.id.ratingLL)
    LinearLayout ratingLL;
    @BindView(R.id.txtServicesTV)
    TextView txtServicesTV;
    @BindView(R.id.viewServicesTabV)
    View viewServicesTabV;
    @BindView(R.id.layoutServicesLL)
    LinearLayout layoutServicesLL;
    @BindView(R.id.containerFL)
    FrameLayout containerFL;
    @BindView(R.id.rlBackRL)
    RelativeLayout rlBackRL;
    @BindView(R.id.ratingTabV)
    View ratingTabV;

    /*
     * Initialize Objects...
     * */
    String strArtistID = "";
    MyArtistModel mMyArtistModel;
    SearchArtistModel mSearchArtistModel;
    ArtistInfoFragment fragment = new ArtistInfoFragment();
    Bundle bundle;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_details);
        ButterKnife.bind(this);
        getIntentData();
    }

    private void getIntentData() {
        if (getIntent() != null && getIntent().getSerializableExtra(Constants.MODEL) != null) {
            mMyArtistModel = (MyArtistModel) getIntent().getSerializableExtra(Constants.MODEL);
            strArtistID = mMyArtistModel.getUser_id();
            bundle = new Bundle();
            if (mMyArtistModel.getUser_id() != null && mMyArtistModel.getUser_id().length() > 0) {
                bundle.putSerializable(Constants.MODEL, mMyArtistModel);
            }

        } else if (getIntent() != null && getIntent().getSerializableExtra(Constants.MODEL_NEW) != null) {
            mSearchArtistModel = (SearchArtistModel) getIntent().getSerializableExtra(Constants.MODEL_NEW);
            strArtistID = mSearchArtistModel.getUser_id();
            bundle = new Bundle();
            if (mSearchArtistModel.getUser_id() != null && mSearchArtistModel.getUser_id().length() > 0) {
                bundle.putSerializable(Constants.MODEL_NEW, mSearchArtistModel);
            }
        }
        perfromInfoClick();
    }

    @OnClick({R.id.layoutInfoLL, R.id.layoutServicesLL, R.id.ratingLL, R.id.rlBackRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutInfoLL:
                perfromInfoClick();
                break;
            case R.id.layoutServicesLL:
                perfromServicesClick();
                break;
            case R.id.rlBackRL:
                perfromBackClick();
                break;

            case R.id.ratingLL:
                performRatingsClick();
                break;
        }
    }

    private void performRatingsClick() {
        txtInfoTabTV.setTextColor(getResources().getColor(R.color.colorTextLight));
        viewInfoTabV.setBackgroundColor(getResources().getColor(R.color.colorTrasperant));
        txtServicesTV.setTextColor(getResources().getColor(R.color.colorTextLight));
        viewServicesTabV.setBackgroundColor(getResources().getColor(R.color.colorTrasperant));
        ratingTabV.setBackgroundColor(getResources().getColor(R.color.colorWhite));

        ArtistRatingsFragment fragment = new ArtistRatingsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.ARTIST_ID, strArtistID);
        fragment.setArguments(bundle);
        switchFragment(mActivity, fragment, false, null);
    }

    private void perfromBackClick() {
        onBackPressed();
    }

    private void perfromInfoClick() {
        txtInfoTabTV.setTextColor(getResources().getColor(R.color.colorWhite));
        viewInfoTabV.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        txtServicesTV.setTextColor(getResources().getColor(R.color.colorTextLight));
        viewServicesTabV.setBackgroundColor(getResources().getColor(R.color.colorTrasperant));
        ratingTabV.setBackgroundColor(getResources().getColor(R.color.colorTrasperant));

        fragment.setArguments(bundle);
        switchFragment(mActivity, fragment, false, bundle);
    }

    private void perfromServicesClick() {
        txtInfoTabTV.setTextColor(getResources().getColor(R.color.colorWhite));
        viewInfoTabV.setBackgroundColor(getResources().getColor(R.color.colorTrasperant));
        txtServicesTV.setTextColor(getResources().getColor(R.color.colorWhite));
        viewServicesTabV.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        ratingTabV.setBackgroundColor(getResources().getColor(R.color.colorTrasperant));

        ArtistServicesFragment fragment = new ArtistServicesFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.ARTIST_ID, strArtistID);
        fragment.setArguments(bundle);
        switchFragment(mActivity, fragment, false, null);
    }

    /********
     *Replace Fragment In Activity
     **********/
    public void switchFragment(Activity activity, Fragment fragment, boolean addToStack, Bundle bundle) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.containerFL, fragment);
            if (addToStack)
                ft.addToBackStack(null);
            if (bundle != null)
                fragment.setArguments(bundle);
            ft.commitAllowingStateLoss();
        }
    }

    @Override
    public void onBackPressed() {
        TheTattzSingleton.getInstance().getSearchModel().setEmail("");
        TheTattzSingleton.getInstance().getSearchModel().setUser_id("");
        TheTattzSingleton.getInstance().getSearchModel().setStreet_address("");
        TheTattzSingleton.getInstance().getSearchModel().setBuilding_name("");
        TheTattzSingleton.getInstance().getSearchModel().setShopname("");
        TheTattzSingleton.getInstance().getSearchModel().setName("");
        TheTattzSingleton.getInstance().getSearchModel().setProfile_pic("");
        TheTattzSingleton.getInstance().getSearchModel().setCreation_time("");
        TheTattzSingleton.getInstance().getSearchModel().setEnable("");
        TheTattzSingleton.getInstance().getSearchModel().setRole("");
        TheTattzSingleton.getInstance().getSearchModel().setCity("");
        TheTattzSingleton.getInstance().getSearchModel().setPhone("");
        TheTattzSingleton.getInstance().getSearchModel().setPassword("");
        TheTattzSingleton.getInstance().getSearchModel().setRating("");

        TheTattzSingleton.getInstance().getPhotosArrayList().clear();
        TheTattzSingleton.getInstance().getWorkInfoArrayList().clear();
        TheTattzSingleton.getInstance().getServicesArrayList().clear();

        Intent mIntent = new Intent();
        setResult(555, mIntent);
        finish();
    }
}
