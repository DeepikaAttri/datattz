package com.thetattz.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aigestudio.wheelpicker.WheelPicker;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.adapters.ClientsSelectedCardsAdapter;
import com.thetattz.interfaces.SelectPaymentInterface;
import com.thetattz.models.CardDetailsModel;
import com.thetattz.utils.CardValidator;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class PaymentActivity extends BaseActivity implements SelectPaymentInterface {
    /**
     * Getting the Current Class Name
     */
    String TAG = PaymentActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intsance
     */
    Activity mActivity = PaymentActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.cardsRecyclerViewRV)
    RecyclerView cardsRecyclerViewRV;
    @BindView(R.id.rlBackRL)
    RelativeLayout rlBackRL;
    @BindView(R.id.otherCardLL)
    LinearLayout otherCardLL;
    @BindView(R.id.cardHolderNameET)
    EditText cardHolderNameET;
    @BindView(R.id.cardNumberET)
    EditText cardNumberET;
    @BindView(R.id.expiryDateTV)
    TextView expiryDateTV;
    @BindView(R.id.cvvET)
    EditText cvvET;
    @BindView(R.id.checkBoxCB)
    CheckBox checkBoxCB;
    @BindView(R.id.txtSubmitPaymentTV)
    TextView txtSubmitPaymentTV;
    @BindView(R.id.imgCardTypeIV)
    ImageView imgCardTypeIV;

    /*
     * Initialize Objects
     * */
    CardValidator mValidator = new CardValidator();

    /*
     * Payment
     * */
    Card mCard;
    Stripe mStripe;
    Gson mGson;

    /*
     * Initalize Objects
     * */
    String strAppointmentID = "";
    String strPrice = "";
    String strArtistID = "";
    String strNewPaymentToken = "";

    ClientsSelectedCardsAdapter mClientCardsAdapter;
    ArrayList<CardDetailsModel> mCardsArrayList = new ArrayList<>();
    SelectPaymentInterface mInteface;
    CardDetailsModel mCardDetailsModel = new CardDetailsModel();
    boolean isPaymentSelect = false;
    boolean isPaymentFutrueUse = false;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        setStatusBar();
        ButterKnife.bind(this);
        mInteface = this;
        getIntentData();
        setCheckboxCheck();
        setCardNumberFormatting();
    }

    private void setCheckboxCheck() {
        checkBoxCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isPaymentFutrueUse = b;
            }
        });
    }

    private void getIntentData() {
        if (getIntent() != null) {
            strAppointmentID = getIntent().getStringExtra(Constants.ID);
            strPrice = getIntent().getStringExtra(Constants.PRICE);
            strArtistID = getIntent().getStringExtra(Constants.ARTIST_ID);

            txtSubmitPaymentTV.setText("Pay $" + strPrice);
        }
    }

    /*
     * Activity Override method
     * #onResume
     * */
    @Override
    protected void onResume() {
        super.onResume();
        //Getting Cards Details
        gettingAllCardsDetails();
    }

    @OnClick({R.id.rlBackRL, R.id.expiryDateTV, R.id.txtSubmitPaymentTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBackRL:
                onBackPressed();
                break;
            case R.id.expiryDateTV:
                perfromExpireyDateClick();
                break;
            case R.id.txtSubmitPaymentTV:
                perfromPaymentClick();
                break;
        }
    }

    private void perfromPaymentClick() {
        if (isValidate()) {
            if (ConnectivityReceiver.isConnected())
                if (mCardDetailsModel.getCard_id().length() > 0) {
                    executePaymentAPI(true);
                } else {
                    executeCardTokenApi();
                }
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }


    private void gettingAllCardsDetails() {
        if (ConnectivityReceiver.isConnected())
            executeRetrivesCards();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    /*
     *
     * Execute  Card Retrives Api
     * */
    private void executeRetrivesCards() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.RETRIVE_ALL_CARDS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {
        mCardsArrayList.clear();
        try {
            if (response.getString("status").equals("1")) {
                JSONArray mJsonArray = response.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mDataObj = mJsonArray.getJSONObject(i);
                    CardDetailsModel mModel = new CardDetailsModel();
                    if (!mDataObj.isNull("card_id"))
                        mModel.setCard_id(mDataObj.getString("card_id"));
                    if (!mDataObj.isNull("brand"))
                        mModel.setBrand(mDataObj.getString("brand"));
                    if (!mDataObj.isNull("country"))
                        mModel.setCountry(mDataObj.getString("country"));
                    if (!mDataObj.isNull("exp_month"))
                        mModel.setExp_month(mDataObj.getString("exp_month"));
                    if (!mDataObj.isNull("exp_year"))
                        mModel.setExp_year(mDataObj.getString("exp_year"));
                    if (!mDataObj.isNull("last4"))
                        mModel.setLast4(mDataObj.getString("last4"));

                    mCardsArrayList.add(mModel);
                }

                //Add Other Payment Item
                CardDetailsModel mModel = new CardDetailsModel();
                mModel.setLast4("0000");
                mCardsArrayList.add(mModel);
                //Set Card Details Adpater:
                setAdapter();
            } else {
                //Add Other Payment Item
                CardDetailsModel mModel = new CardDetailsModel();
                mModel.setLast4("0000");
                mCardsArrayList.add(mModel);
                //Set Card Details Adpater:
                setAdapter();

                showToast(mActivity, getString(R.string.no_cards_added_yet));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }

    /*
     * Set Cards Adapter
     * */
    private void setAdapter() {
        mClientCardsAdapter = new ClientsSelectedCardsAdapter(mActivity, mCardsArrayList, mInteface);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity);
        cardsRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        cardsRecyclerViewRV.setAdapter(mClientCardsAdapter);
    }

    /*
     * Setting Up Interface Method
     * */
    @Override
    public void getSelectedPayment(int position, CardDetailsModel mModel) {
        mCardDetailsModel = mModel;
        int lastPositon = mCardsArrayList.size() - 1;
        isPaymentSelect = true;
        if (position == lastPositon) {
            otherCardLL.setVisibility(View.VISIBLE);
        } else {
            otherCardLL.setVisibility(View.GONE);
        }
    }

    /*
     * Set up validations for Sign Up fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (isPaymentSelect == false) {
            showAlertDialog(mActivity, getString(R.string.please_select_anyone_payment));
            flag = false;
        } else {
            if (isPaymentSelect == true && mCardDetailsModel.getCard_id().length() == 0) {
                if (cardHolderNameET.getText().toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_card_holder));
                    flag = false;
                } else if (!CardValidator.validateCardNumber(cardNumberET.getText().toString().trim())) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_valid_carnumber));
                    flag = false;
                } else if (expiryDateTV.getText().toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_expiry_date));
                    flag = false;
                } else if (cvvET.getText().toString().trim().equals("")) {
                    showAlertDialog(mActivity, getString(R.string.please_enter_cvv));
                    flag = false;
                }
            }
        }
        return flag;
    }

    private void perfromExpireyDateClick() {
        createBottomSheetDialog();
    }

    /*
     * Card Validations
     *
     * */
    private void setCardNumberFormatting() {
        cardNumberET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Remove spacing char
                String working = s.toString();
                if (working.length() == 4 && before == 0) {
                    working += "-";
                    cardNumberET.setText(working);
                    cardNumberET.setSelection(working.length());
                }
                if (working.length() == 9 && before == 0) {
                    working += "-";
                    cardNumberET.setText(working);
                    cardNumberET.setSelection(working.length());
                }
                if (working.length() == 14 && before == 0) {
                    working += "-";
                    cardNumberET.setText(working);
                    cardNumberET.setSelection(working.length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (CardValidator.getCardType(s.toString()) != null) {
                    Log.e(TAG, "afterTextChanged: " + CardValidator.getCardType(s.toString()).name());

                    if (CardValidator.getCardType(s.toString()).name().toLowerCase().equals("mastercard")) {
                        imgCardTypeIV.setImageResource(R.drawable.ic_mc);
                    } else if (CardValidator.getCardType(s.toString()).name().toLowerCase().equals("visa")) {
                        imgCardTypeIV.setImageResource(R.drawable.ic_visa);
                    } else if (CardValidator.getCardType(s.toString()).name().toLowerCase().equals("discover")) {
                        imgCardTypeIV.setImageResource(R.drawable.ic_discover);
                    } else if (CardValidator.getCardType(s.toString()).name().toLowerCase().equals("amex")) {
                        imgCardTypeIV.setImageResource(R.drawable.ic_ae);
                    } else {
                        imgCardTypeIV.setImageResource(R.drawable.ic_other);
                    }
                } else {
                    imgCardTypeIV.setImageResource(R.drawable.ic_other);
                }
            }
        });
    }


    public void createBottomSheetDialog() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        @SuppressLint("InflateParams") View sheetView = mActivity.getLayoutInflater().inflate(R.layout.dialog_card_expiry, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        Typeface mTypeface = Typeface.createFromAsset(getAssets(), "ProximaNova-Regular.ttf");

        final WheelPicker monthsWP = sheetView.findViewById(R.id.monthsWP);
        monthsWP.setTypeface(mTypeface);
        final WheelPicker yearsWP = sheetView.findViewById(R.id.yearsWP);
        yearsWP.setTypeface(mTypeface);


        TextView txtCancelTV = sheetView.findViewById(R.id.txtCancelTV);
        TextView txtSaveTV = sheetView.findViewById(R.id.txtSaveTV);


        monthsWP.setAtmospheric(true);
        monthsWP.setCyclic(false);
        monthsWP.setCurved(true);
        monthsWP.setData(getMonths());

        yearsWP.setAtmospheric(true);
        yearsWP.setCyclic(false);
        yearsWP.setCurved(true);
        yearsWP.setData(getYears());


        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        txtSaveTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expiryDateTV.setText(getMonthNumberFromName(getMonths().get(monthsWP.getCurrentItemPosition())) + "/" + getYears().get(yearsWP.getCurrentItemPosition()));
                mBottomSheetDialog.dismiss();
            }
        });
    }


    private void executeCardTokenApi() {
        try {
            mStripe = new Stripe(Constants.STRIPE_API_KEY);
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }

        String[] mMonthYearArray = expiryDateTV.getText().toString().trim().split("/");
        String exMonth = mMonthYearArray[0];
        String exyear = mMonthYearArray[1];

        Card card = new Card(cardNumberET.getText().toString().trim(),
                Integer.parseInt(exMonth),
                Integer.parseInt(exyear),
                cvvET.getText().toString().trim(), cardHolderNameET.getText().toString(), "", "", "", "", "", "", "");
        card.setCurrency("USD");

        showProgressDialog(mActivity);

        mStripe.createToken(card, Constants.STRIPE_API_KEY, new TokenCallback() {
            public void onSuccess(Token token) {
                Log.e(TAG, "**Stripe Token**" + token.getId());
                strNewPaymentToken = token.getId();
                executePaymentAPI(false);
            }

            public void onError(Exception error) {
                dismissProgressDialog();
                showToast(mActivity, error.getLocalizedMessage());
            }
        });
    }

    /*
     *
     * Execute  Card Retrives Api
     * */
    private void executePaymentAPI(boolean b) {
        if (b == true)
            showProgressDialog(mActivity);
        final String mApiUrl = Constants.CLIENT_PAY_CHANGES;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("client_id", getUserID());
            mParams.put("artist_id", strArtistID);
            mParams.put("appointment_id", strAppointmentID);

            if (mCardDetailsModel.getCard_id() != null && mCardDetailsModel.getCard_id().length() > 0) {
                mParams.put("token", mCardDetailsModel.getCard_id());
            } else {
                mParams.put("token", strNewPaymentToken);
                mParams.put("save", isPaymentFutrueUse);
            }

            mParams.put("amount", strPrice);
//            mParams.put("save", isPaymentFutrueUse);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parsePaymentResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }
        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parsePaymentResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                showPaymentTransactionDialog(mActivity, response.getString("transaction_id"), response.getString("message"));
            } else {
                showToast(mActivity, response.getString("message"));
//                showAlertDialog(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }
}
