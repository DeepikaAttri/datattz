package com.thetattz.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.messaging.FirebaseMessaging;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.artist.ArtistHomeActivity;
import com.thetattz.fonts.ButtonBold;
import com.thetattz.fonts.EditTextBold;
import com.thetattz.fonts.TextViewBold;
import com.thetattz.models.TimeZoneModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInSignUpActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        ResultCallback<LocationSettingsResult> {

    /************************
     *Fused Google Location
     **************/
    public static final int REQUEST_PERMISSION_CODE = 919;
    public String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
    public String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    protected String mLastUpdateTime;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    protected final static String KEY_LOCATION = "location";
    protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    public double mLatitude;
    public double mLongitude;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;
    protected Boolean mRequestingLocationUpdates;
    /****************************************/


    /**
     * Getting the Current Class Name
     */
    String TAG = SignInSignUpActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = SignInSignUpActivity.this;


    /**
     * Widgets Main Layout
     */
    @BindView(R.id.txtLoginTabTV)
    TextViewBold txtLoginTabTV;
    @BindView(R.id.viewLoginTabV)
    View viewLoginTabV;
    @BindView(R.id.layoutSignInLL)
    LinearLayout layoutSignInLL;
    @BindView(R.id.txtSignupTabTV)
    TextViewBold txtSignupTabTV;
    @BindView(R.id.viewSignupTabV)
    View viewSignupTabV;
    @BindView(R.id.layoutSignUpLL)
    LinearLayout layoutSignUpLL;
    @BindView(R.id.parentSignInLL)
    LinearLayout parentSignInLL;
    @BindView(R.id.parentSignUpLL)
    LinearLayout parentSignUpLL;
    @BindView(R.id.timezoneRl)
    RelativeLayout timezoneRl;
    /*
     * Widgets SignIn Layout
     * */

    @BindView(R.id.editSignInEmailET)
    EditTextBold editSignInEmailET;
    @BindView(R.id.editSignPasswordET)
    EditTextBold editSignPasswordET;
    @BindView(R.id.btnSignInLoginB)
    ButtonBold btnSignInLoginB;
    @BindView(R.id.btnSignInForgotPwdB)
    ButtonBold btnSignInForgotPwdB;

    /*
     * Widgets SignUp Layout
     * */
    @BindView(R.id.editSignUpFirstNameET)
    EditTextBold editSignUpFirstNameET;
    @BindView(R.id.editSignUpLastNameET)
    EditTextBold editSignUpLastNameET;
    @BindView(R.id.editSignUpEmailET)
    EditTextBold editSignUpEmailET;
    @BindView(R.id.editSignUpPasswordET)
    EditTextBold editSignUpPasswordET;
    @BindView(R.id.editSignUpConfirmPasswordET)
    EditTextBold editSignUpConfirmPasswordET;
    @BindView(R.id.txtSignUpNoTV)
    TextViewBold txtSignUpNoTV;
    @BindView(R.id.txtSignUpYesTV)
    TextViewBold txtSignUpYesTV;
    @BindView(R.id.btnSignUpB)
    ButtonBold btnSignUpB;
    @BindView(R.id.txtTermServicesTV)
    TextViewBold txtTermServicesTV;
    @BindView(R.id.txtPrivacyPolicyTV)
    TextViewBold txtPrivacyPolicyTV;

    @BindView(R.id.timezone_sp)
    Spinner timezone_sp;

    List<TimeZoneModel> mTimezoneArrayList = new ArrayList<>();
    String strTimezone = "";

    /*
     * Initialize Objects...
     * */
    String strRole = "0";
    String strPushToken = "";
    String mTimeZone = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    //g
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*************
         *Google Location
         **************/
        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";
        /*Update values using data stored in the Bundle.*/
        updateValuesFromBundle(savedInstanceState);
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
        if (!checkPermission()) {
            requestPermission();
        } else {
            checkLocationSettings();
        }

        updateValuesFromBundle(savedInstanceState);
        /*******************************************/

        setContentView(R.layout.activity_sign_in_sign_up);
        setStatusBar();
        //Butter Knife
        ButterKnife.bind(this);
        //Get Push Token
        getPushToken();
        //By Default Select SignIn
        performSignUpClick();
        //By Default Select NO
        performSignUpNoClick();

        /* underline services and policy text */
        txtTermServicesTV.setPaintFlags(txtTermServicesTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtPrivacyPolicyTV.setPaintFlags(txtPrivacyPolicyTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        getCurrentUserTimeZone();
        getTimezonesFromJson();
    }

    private void getTimezonesFromJson() {
        if (mTimezoneArrayList != null) {
            mTimezoneArrayList.clear();
        }

        try {
            //JSONObject mJsonObject = new JSONObject(loadJSONFromAsset());
            JSONArray mJsonArray = new JSONArray(loadJSONFromAsset());
            Log.e(TAG, "***Array Size***" + mJsonArray.length());

            for (int i = 0; i < mJsonArray.length(); i++) {
                JSONObject mDataObj = mJsonArray.getJSONObject(i);
                TimeZoneModel mModel = new TimeZoneModel();
                if (!mDataObj.isNull("timezone"))
                    mModel.setTimezone(mDataObj.getString("timezone"));
                if (!mDataObj.isNull("timezoneAbr"))
                    mModel.setTimezoneAbr(mDataObj.getString("timezoneAbr"));
                mTimezoneArrayList.add(mModel);
            }

            setTimeZoneAdapter();

            Log.e(TAG, "***Array Size***" + mTimezoneArrayList.size());
        } catch (Exception e) {
            e.toString();
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("timezones.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void getCurrentUserTimeZone() {
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        List<String> mList = Arrays.asList(TimeZone.getAvailableIDs());
        Log.e(TAG, "***TimeZoneList***" + mList);
        mTimeZone = timeZone.getID();
        Log.e(TAG, "***TimeZone***" + mTimeZone);
    }

    /*
     *  time zone adapter
     * */
    private void setTimeZoneAdapter() {
        Typeface fontStyle = Typeface.createFromAsset(mActivity.getAssets(),
                "Purissima Bold W00 Regular.ttf");
        ArrayAdapter<TimeZoneModel> mSizeAd = new ArrayAdapter<TimeZoneModel>(this, android.R.layout.simple_spinner_item, mTimezoneArrayList) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                HideKey();

                TextView itemTextView = v.findViewById(R.id.itemTV);
                String strName = mTimezoneArrayList.get(position).getTimezoneAbr() + " - " +
                        "" + mTimezoneArrayList.get(position).getTimezone();
                itemTextView.setText(strName);
                itemTextView.setTypeface(fontStyle);
                return v;
            }
        };
        timezone_sp.setAdapter(mSizeAd);
        timezone_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ((TextView) view).setTypeface(fontStyle);
                ((TextView) view).setTextSize(23);
                ((TextView) view).setPadding(0, 0, 0, 0);
                ((TextView) view).setTextColor(getResources().getColor(R.color.colorWhite));
                strTimezone = mTimezoneArrayList.get(position).getTimezoneAbr();
//                showToast(mActivity, strTimezone);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getPushToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    if (!task.isSuccessful()) {
                        //handle token error
                        Log.e(TAG, "**Get Instance Failed**", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    strPushToken = task.getResult();
                    Log.e(TAG, "**Push Token**" + strPushToken);

                });
    }

    /*
     * Widgets click listeners
     * */
    @OnClick({R.id.layoutSignInLL, R.id.layoutSignUpLL, R.id.btnSignInLoginB, R.id.btnSignInForgotPwdB, R.id.txtSignUpNoTV,
            R.id.txtSignUpYesTV, R.id.btnSignUpB, R.id.txtTermServicesTV, R.id.txtPrivacyPolicyTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutSignInLL:
                performSignInClick();
                break;
            case R.id.layoutSignUpLL:
                performSignUpClick();
                break;
            case R.id.btnSignInLoginB:
                perfromLoginClick();
                break;
            case R.id.btnSignInForgotPwdB:
                perfromForgotPwdClick();
                break;
            case R.id.txtSignUpNoTV:
                performSignUpNoClick();
                break;
            case R.id.txtSignUpYesTV:
                performSignUpYesClick();
                break;
            case R.id.btnSignUpB:
                perfromSignUpClick();
                break;
            case R.id.txtTermServicesTV:
                performTermServicesClick();
                break;
            case R.id.txtPrivacyPolicyTV:
                performPrivacyPolicyClick();
                break;
        }
    }

    private void performSignInClick() {
        txtLoginTabTV.setTextColor(getResources().getColor(R.color.colorWhite));
        viewLoginTabV.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        txtSignupTabTV.setTextColor(getResources().getColor(R.color.colorWhite));
        viewSignupTabV.setBackgroundColor(getResources().getColor(R.color.colorTrasperant));
        parentSignInLL.setVisibility(View.VISIBLE);
        parentSignUpLL.setVisibility(View.GONE);
    }

    private void performSignUpClick() {
        txtLoginTabTV.setTextColor(getResources().getColor(R.color.colorWhite));
        viewLoginTabV.setBackgroundColor(getResources().getColor(R.color.colorTrasperant));
        txtSignupTabTV.setTextColor(getResources().getColor(R.color.colorWhite));
        viewSignupTabV.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        parentSignInLL.setVisibility(View.GONE);
        parentSignUpLL.setVisibility(View.VISIBLE);
    }

    private void perfromLoginClick() {
        if (isValidateSignIn()) {
            if (ConnectivityReceiver.isConnected()) {
                executeSignIn();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }

    private void perfromForgotPwdClick() {
        startActivity(new Intent(mActivity, ForgotPasswordActivity.class));
    }

    private void performSignUpNoClick() {
        txtSignUpNoTV.setBackgroundResource(R.drawable.btn_accent_round);
        txtSignUpYesTV.setTextColor(getResources().getColor(R.color.colorWhite));
        txtSignUpYesTV.setBackgroundResource(0);
        txtSignUpNoTV.setTextColor(getResources().getColor(R.color.colorBlue));

        strRole = "0";

        timezoneRl.setVisibility(View.GONE);
    }

    private void performSignUpYesClick() {
        txtSignUpNoTV.setBackgroundResource(0);
        txtSignUpNoTV.setTextColor(getResources().getColor(R.color.colorWhite));
        txtSignUpYesTV.setBackgroundResource(R.drawable.btn_accent_round);
        txtSignUpYesTV.setTextColor(getResources().getColor(R.color.colorBlue));

        strRole = "1";

        timezoneRl.setVisibility(View.VISIBLE);
    }

    private void perfromSignUpClick() {
        if (isValidateSignUp()) {
            if (ConnectivityReceiver.isConnected()) {
                executeSignUp();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }

    private void performTermServicesClick() {
        Intent mIntent = new Intent(mActivity, InfomationActivity.class);
        mIntent.putExtra(Constants.TYPES, getString(R.string.terms_of_services));
        startActivity(mIntent);
    }

    private void performPrivacyPolicyClick() {
        Intent mIntent = new Intent(mActivity, InfomationActivity.class);
        mIntent.putExtra(Constants.TYPES, getString(R.string.privacy_policy));
        startActivity(mIntent);
    }


    /*
     * Set up validations for Sign In fields
     * */
    private boolean isValidateSignIn() {
        boolean flag = true;
        if (editSignInEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email));
            flag = false;
        } else if (!isValidEmaillId(editSignInEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email));
            flag = false;
        } else if ((editSignPasswordET.getText().toString().trim().equals("")) || (editSignPasswordET.getText().toString().trim().length() < 6)) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        }
        return flag;
    }

    /*
     * Set up validations for Sign Up fields
     * */
    private boolean isValidateSignUp() {
        boolean flag = true;
        if (editSignUpFirstNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name));
            flag = false;

        } else if (editSignUpLastNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_lastName));
            flag = false;
        } else if (editSignUpEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email));
            flag = false;
        } else if (!isValidEmaillId(editSignUpEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email));
            flag = false;
        } else if ((editSignUpPasswordET.getText().toString().trim().equals(""))) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        } else if ((editSignUpPasswordET.getText().toString().trim().length() < 8)) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password_validate));
            flag = false;
        } else if (editSignUpConfirmPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_confirm_password));
            flag = false;
        } else if (editSignUpConfirmPasswordET.getText().toString().trim().length() < 8) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password_validate));
            flag = false;
        } else if (!editSignUpPasswordET.getText().toString().trim().equals(editSignUpConfirmPasswordET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.your_password_and_match_is_wrong));
            flag = false;
        }
        return flag;
    }

    /*
     *
     * Execute  SignUp Api
     * */

    private void executeSignUp() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.SIGN_UP;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("email", editSignUpEmailET.getText().toString().trim());
            mParams.put("password", editSignUpPasswordET.getText().toString().trim());
            mParams.put("name", editSignUpFirstNameET.getText().toString().trim() + " " + editSignUpLastNameET.getText().toString().trim());
            mParams.put("role", strRole);
            mParams.put("device_token", strPushToken);
            mParams.put("device_type", "android");
            mParams.put("latitude", getCurrentLatitude());
            mParams.put("longitude", getCurrentLongitude());
            mParams.put("timezone", mTimeZone);

            if (strRole.equals("1")) {
                mParams.put("timezoneAbr", strTimezone);
            }

            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseSignUpResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseSignUpResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                showToast(mActivity, getString(R.string.account_created));
                JSONObject mDataObject = response.getJSONObject("data");
                //Collection User Data
                TheTattzPrefrences.writeBoolean(mActivity, TheTattzPrefrences.ISLOGIN, true);
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_ID, mDataObject.getString("user_id"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_NAME, mDataObject.getString("name"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_EMAIL, mDataObject.getString("email"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_ROLE, mDataObject.getString("role"));

                if (!mDataObject.isNull("profile_pic"))
                    TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_PROFILE_PIC, mDataObject.getString("profile_pic"));


                if (getUserRole().equals(Constants.ROLE_ARTIST)) {
                    //Open HomeScreen
                    Intent mIntent = new Intent(mActivity, ArtistHomeActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);
                    finish();
                } else if (getUserRole().equals(Constants.ROLE_CLIENT)) {
                    //Open HomeScreen
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);
                    finish();
                }

            } else {
                showAlertDialog(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }

    /*
     *
     * Execute  SignIn Api
     * */
    private void executeSignIn() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.SIGN_IN;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("email", editSignInEmailET.getText().toString().trim());
            mParams.put("password", editSignPasswordET.getText().toString().trim());
            mParams.put("device_token", strPushToken);
            mParams.put("device_type", "android");
            mParams.put("latitude", getCurrentLatitude());
            mParams.put("longitude", getCurrentLongitude());
            mParams.put("timezone", mTimeZone);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseSignInResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseSignInResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                showToast(mActivity, response.getString("message"));
                JSONObject mDataObject = response.getJSONObject("data");
                //Collection User Data
                TheTattzPrefrences.writeBoolean(mActivity, TheTattzPrefrences.ISLOGIN, true);
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_ID, mDataObject.getString("user_id"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_NAME, mDataObject.getString("name"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_EMAIL, mDataObject.getString("email"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_ROLE, mDataObject.getString("role"));
                if (!mDataObject.isNull("profile_pic"))
                    TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_PROFILE_PIC, mDataObject.getString("profile_pic"));


                if (getUserRole().equals(Constants.ROLE_ARTIST)) {
                    //Open HomeScreen
                    Intent mIntent = new Intent(mActivity, ArtistHomeActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);
                    finish();
                } else if (getUserRole().equals(Constants.ROLE_CLIENT)) {
                    //Open HomeScreen
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);
                    finish();
                }
            } else {
                showAlertDialog(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }




    /*
     * Locations
     * */

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     **********/
    private boolean checkPermission() {
        int mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation);
        int mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation);

        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{mAccessFineLocation, mAccessCourseLocation}, REQUEST_PERMISSION_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkLocationSettings();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //requestPermission();
                }
                return;
            }

        }
    }


    /******************
     *Google Location
     ************/
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i("", "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Uses a {@link LocationSettingsRequest.Builder} to build
     * a {@link LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
     */
    public void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    /**
     * The callback invoked when
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} is called. Examines the
     * {@link LocationSettingsResult} object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i("", "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i("", "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i("", "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i("", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
            default:
                // finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i("", "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i("", "User chose not to make required location settings changes.");
                        break;
                    default:
                        // finish();
                }
        }
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    mRequestingLocationUpdates = true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "onRestart");
    }


    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i("", "Connected to GoogleApiClient");
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        mLatitude = mCurrentLocation.getLatitude();
        mLongitude = mCurrentLocation.getLongitude();
        Log.e(TAG, "*********LATITUDE********" + mLatitude);
        Log.e(TAG, "*********LONGITUDE********" + mLongitude);
        TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.CURRENT_LOCATION_LATITUDE, "" + mLatitude);
        TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.CURRENT_LOCATION_LONGITUDE, "" + mLongitude);

        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i("", "Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }
    /*****************************************/


}
