package com.thetattz.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.savvi.rangedatepicker.SubTitle;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.adapters.ServicesAdapter;
import com.thetattz.adapters.TimeSlotsAdapter;
import com.thetattz.fonts.TextViewSemiBold;
import com.thetattz.interfaces.SelectServiceInterface;
import com.thetattz.interfaces.SelectTimeSlotInterface;
import com.thetattz.models.FreeTimeSlotsModel;
import com.thetattz.models.MyArtistModel;
import com.thetattz.models.SearchArtistModel;
import com.thetattz.models.ServicesModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class BookApointmentActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = BookApointmentActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = BookApointmentActivity.this;

    @BindView(R.id.addAppointRL)
    RelativeLayout addAppointRL;
    @BindView(R.id.rlBackRL)
    RelativeLayout rlBackRL;
    @BindView(R.id.topRL)
    RelativeLayout topRL;
    @BindView(R.id.imgProfilePicCIV)
    CircleImageView imgProfilePicCIV;
    @BindView(R.id.txtShopNameTV)
    TextViewSemiBold txtShopNameTV;
    @BindView(R.id.servicesRV)
    RecyclerView servicesRV;
    @BindView(R.id.mCalenderCV)
    CalendarView mCalenderCV;
    @BindView(R.id.headerLL)
    LinearLayout headerLL;
    @BindView(R.id.txtBook)
    TextViewSemiBold txtBook;
    @BindView(R.id.bottomRL)
    RelativeLayout bottomRL;
    @BindView(R.id.mPayRadioGroupRG)
    RadioGroup mPayRadioGroupRG;
    @BindView(R.id.rbMobilePayRB)
    RadioButton rbMobilePayRB;
    @BindView(R.id.rbInShopRB)
    RadioButton rbInShopRB;
    @BindView(R.id.mTimeSlotsRecyclerView)
    RecyclerView mTimeSlotsRecyclerView;
    @BindView(R.id.artistNotFountTV)
    TextView artistNotFountTV;
    @BindView(R.id.txtServicesPriveTV)
    TextView txtServicesPriveTV;
    @BindView(R.id.timezoneTV)
    TextView timezoneTV;
    /*
     * Initialize...Objects
     * */
    ServicesModel mServicesModel = new ServicesModel();
    MyArtistModel mMyArtistModel;
    SearchArtistModel mSearchArtistModel;

    FreeTimeSlotsModel mFreeTimeSlotsModel = new FreeTimeSlotsModel();
    ArrayList<ServicesModel> mServicesArrayList = new ArrayList<ServicesModel>();
    ArrayList<FreeTimeSlotsModel> mTimeSlotsArrayNewList = new ArrayList<>();

    ServicesAdapter mServicesAdapter;
    TimeSlotsAdapter mTimeSlotsAdapter;

    String strPaymentMethod = "";
    String strCalendarDate = "";
    String strDayDate = "";
    String artistShopStartTime = "";
    String artistShopEndTime = "";
    String mArtistID = "";

    long mTabClick = 0;

    long selectedServiceTimeMinutes = 0, selectedTimeMinutes = 0;

    /*
     * Calender
     * */
    SimpleDateFormat mSimpleDateFormat;
    Calendar mCalendar;
    SelectServiceInterface mSelectServiceInterface = new SelectServiceInterface() {
        @Override
        public void getSelectedService(ServicesModel mServicesModel11) {
            Log.e(TAG, "**Service Name**" + mServicesModel11.getService_name());
            mServicesModel = mServicesModel11;
            txtServicesPriveTV.setText("$" + mServicesModel.getPrice());

            selectedServiceTimeMinutes = Long.parseLong(mServicesModel.getMinutes());
            Log.i("======= Hours", " :: " + selectedServiceTimeMinutes);

        }
    };

    /*
     * Time Slots Click Listner
     * */
    SelectTimeSlotInterface mSelectTimeSlotInterface = new SelectTimeSlotInterface() {
        @Override
        public void getTimeSlot(FreeTimeSlotsModel mFreeTimeSlotsModel11) {
            mFreeTimeSlotsModel = mFreeTimeSlotsModel11;
            Log.e(TAG, "**Time Slot**" + mFreeTimeSlotsModel.getStartTime());

            String selectedTime = mFreeTimeSlotsModel11.getStartTime();

            FreeTimeSlotsModel lastServicesModel = mTimeSlotsArrayNewList.get(mTimeSlotsArrayNewList.size() - 1);
            String lastTime = lastServicesModel.getEndTime();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:m a");
            Date date1 = null;
            Date date2 = null;
            try {
                date1 = simpleDateFormat.parse(lastTime);
                date2 = simpleDateFormat.parse(selectedTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long millis = date1.getTime() - date2.getTime();
            long seconds = millis / 1000;
            long mins = seconds / 60;

            selectedTimeMinutes = mins;

            Log.i("======= Hours", " :: " + mins);
        }
    };

    /*
     * Activity Override method
     * #onActivityCreated
     **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_apointment);

        setStatusBar();
        //butter knife
        ButterKnife.bind(this);
        //Set Calender
        setCalendar();
//        //Get Intent Data
        getIntentData();
        //Set Payment Layout
        setPaymentLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void setPaymentLayout() {
        Typeface font = Typeface.createFromAsset(getAssets(), "ProximaNova-Semibold.ttf");
        rbMobilePayRB.setTypeface(font);
        rbInShopRB.setTypeface(font);

        mPayRadioGroupRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.rbMobilePayRB:
                        strPaymentMethod = "1";
                        Log.e(TAG, "**Mobile Pay**" + strPaymentMethod);
                        break;
                    case R.id.rbInShopRB:
                        strPaymentMethod = "2";
                        Log.e(TAG, "**Shop Pay**" + strPaymentMethod);
                        break;
                }
            }
        });
    }

    private void getIntentData() {
        if (getIntent() != null) {
            if (getIntent() != null && getIntent().getSerializableExtra(Constants.MODEL) != null) {
                mMyArtistModel = (MyArtistModel) getIntent().getSerializableExtra(Constants.MODEL);
                mArtistID = mMyArtistModel.getUser_id();
                setWidgetDataWithMyArtistModel();
            } else if (getIntent() != null && getIntent().getSerializableExtra(Constants.MODEL_NEW) != null) {
                mSearchArtistModel = (SearchArtistModel) getIntent().getSerializableExtra(Constants.MODEL_NEW);
                mArtistID = mSearchArtistModel.getUser_id();
                setWidgetDataWithSearchArtistModel();
            }
            //Get Services Data
            executeGetServices();
        }
    }

    private void setWidgetDataWithMyArtistModel() {
        if (mMyArtistModel.getProfile_pic() != null && mMyArtistModel.getProfile_pic().contains("http"))
            Picasso.with(mActivity).load(mMyArtistModel.getProfile_pic())
                    .placeholder(R.drawable.ic_p_pp)
                    .error(R.drawable.ic_p_pp)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgProfilePicCIV);
        else
            imgProfilePicCIV.setImageResource(R.drawable.ic_p_pp);


        if (mMyArtistModel.getName() != null)
            txtShopNameTV.setText(CapitalizedFullName(mMyArtistModel.getName()));

        if (mMyArtistModel.getTimezoneAbr() != null)
            timezoneTV.setText("Note: Times are displayed in " + mMyArtistModel.getTimezoneAbr() + ".");
    }

    private void setWidgetDataWithSearchArtistModel() {
        if (mSearchArtistModel.getProfile_pic() != null && mSearchArtistModel.getProfile_pic().contains("http"))
            Picasso.with(mActivity).load(mSearchArtistModel.getProfile_pic())
                    .placeholder(R.drawable.ic_p_pp)
                    .error(R.drawable.ic_p_pp)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgProfilePicCIV);
        else
            imgProfilePicCIV.setImageResource(R.drawable.ic_p_pp);

        if (mSearchArtistModel.getName() != null)
            txtShopNameTV.setText(CapitalizedFullName(mSearchArtistModel.getName()));

        if (mSearchArtistModel.getTimezoneAbr() != null)
            timezoneTV.setText("Note: Times are displayed in " + mSearchArtistModel.getTimezoneAbr() + ".");
    }

    private void setCalendar() {
        /*
         * CLICK PATICULAR DATE
         * */
        //{"artist_id":"1","date":"2020-05-11","day":"Monday"}

        mCalenderCV.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {

                // Preventing multiple clicks, using threshold of 1 second
                if (SystemClock.elapsedRealtime() - mTabClick < 1000) {
                    return;
                }
                mTabClick = SystemClock.elapsedRealtime();

                String msg = "Selected date Day: " + i2 + " Month : " + (i1 + 1) + " Year " + i;
                strCalendarDate = i2 + "-" + (i1 + 1) + "-" + i;
                Log.e(TAG, "Selected Date: " + strCalendarDate);
                strDayDate = convertTimeToDayName(strCalendarDate);
                Log.e(TAG, "Day Name: " + strDayDate);
                if (IsCalenderAvailable(strCalendarDate)) {
                    //Get Appointments time api:
                    executeGetTimeSlots();
                } else {
                    showAlertDialog(mActivity, getString(R.string.you_can_not));
                    mTimeSlotsRecyclerView.setVisibility(View.GONE);
                    artistNotFountTV.setVisibility(View.VISIBLE);
                    mCalenderCV.setDate(System.currentTimeMillis(), false, true);
                }
            }
        });
    }

    private ArrayList<SubTitle> getSubTitles() {
        final ArrayList<SubTitle> subTitles = new ArrayList<>();
        final Calendar tmrw = Calendar.getInstance();
        tmrw.add(Calendar.DAY_OF_MONTH, 4);
        subTitles.add(new SubTitle(tmrw.getTime(), "₹1000"));
        return subTitles;
    }

    @OnClick({R.id.addAppointRL, R.id.rlBackRL, R.id.txtBook})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addAppointRL:
                break;
            case R.id.rlBackRL:
                onBackPressed();
                break;
            case R.id.txtBook:
                perfromBookClick();
                break;
        }
    }

    private void perfromBookClick() {
        if (isValidate()) {
            if (ConnectivityReceiver.isConnected())
                executeBookAppointment();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    private boolean isValidate() {
        boolean flag = true;
        if (mServicesModel.getService_name().length() == 0) {
            showAlertDialog(mActivity, getString(R.string.please_select_service));
            flag = false;
        } else if (mFreeTimeSlotsModel.getStartTime() == null || mFreeTimeSlotsModel.getStartTime().length() == 0) {
            showAlertDialog(mActivity, getString(R.string.please_select_appoint));
            flag = false;
        } else if (strPaymentMethod.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_select_payment));
            flag = false;
        } else if (selectedServiceTimeMinutes > selectedTimeMinutes) {
            showAlertDialog(mActivity, getString(R.string.out_of_timings));
            flag = false;
        }
        return flag;
    }

    /*
     * Get Artist Services
     * */
    private void executeGetServices() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.GET_SERVICES_LISTING;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            if (mMyArtistModel != null && mMyArtistModel.getUser_id().length() > 0)
                mParams.put("artist_id", mMyArtistModel.getUser_id());
            else if (mSearchArtistModel != null && mSearchArtistModel.getUser_id().length() > 0)
                mParams.put("artist_id", mSearchArtistModel.getUser_id());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseServicesResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseServicesResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                if (!response.isNull("data")) {
                    JSONArray mJsonArray = response.getJSONArray("data");
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mDataObj = mJsonArray.getJSONObject(i);
                        ServicesModel mModel = new ServicesModel();
                        if (!mDataObj.isNull("id"))
                            mModel.setId(mDataObj.getString("id"));
                        if (!mDataObj.isNull("artist_id"))
                            mModel.setArtist_id(mDataObj.getString("artist_id"));
                        if (!mDataObj.isNull("service_name"))
                            mModel.setService_name(mDataObj.getString("service_name"));
                        if (!mDataObj.isNull("description"))
                            mModel.setDescription(mDataObj.getString("description"));
                        if (!mDataObj.isNull("duration"))
                            mModel.setDuration(mDataObj.getString("duration"));
                        if (!mDataObj.isNull("price"))
                            mModel.setPrice(mDataObj.getString("price"));
                        if (mDataObj.has("enable") && !mDataObj.isNull("enable"))
                            mModel.setEnable(mDataObj.getString("enable"));
                        if (mDataObj.has("disable") && !mDataObj.isNull("disable"))
                            mModel.setDisable(mDataObj.getString("disable"));
                        if (mDataObj.has("minutes") && !mDataObj.isNull("minutes"))
                            mModel.setMinutes(mDataObj.getString("minutes"));

                        mServicesArrayList.add(mModel);
                    }
                }
                //Set Up Recycler View Data
                setAdapter();

            } else {
                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    private void setAdapter() {
        if (mServicesArrayList.size() > 0)
            servicesRV.setBackgroundResource(R.drawable.bg_box_rectangle);
        servicesRV.setNestedScrollingEnabled(false);
        mServicesAdapter = new ServicesAdapter(mActivity, mServicesArrayList, mSelectServiceInterface);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity);
        servicesRV.setLayoutManager(mLayoutManagerC);
        servicesRV.setAdapter(mServicesAdapter);
    }

    /*
     * Execute Getting Time Slots:
     * */
    private void executeGetTimeSlots() {
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        String mTimeZone = timeZone.getID();

        showProgressDialog(mActivity);
        final String mApiUrl = Constants.ARTIST_BOOKED_TIME_SLOT;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            mParams.put("artist_id", mArtistID);
            mParams.put("date", strCalendarDate);
            mParams.put("day", strDayDate);
            mParams.put("timezone", mTimeZone);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseTimeSlotsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }
        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseTimeSlotsResponse(JSONObject response) {
        ArrayList<FreeTimeSlotsModel> mAlreadyBookedArrayList = new ArrayList<FreeTimeSlotsModel>();
        try {
            if (response.getString("status").equals("1")) {
                if (!response.isNull("start_time"))
                    artistShopStartTime = response.getString("start_time");
                if (!response.isNull("end_time"))
                    artistShopEndTime = response.getString("end_time");

                /*Need to Implement New things*/

                if (!response.isNull("free_slots")) {
                    JSONArray mJsonArray = response.getJSONArray("free_slots");
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mDataObj = mJsonArray.getJSONObject(i);
                        FreeTimeSlotsModel mModel = new FreeTimeSlotsModel();
                        if (!mDataObj.isNull("start_time"))
                            mModel.setStartTime(mDataObj.getString("start_time"));
                        if (!mDataObj.isNull("end_time"))
                            mModel.setEndTime(mDataObj.getString("end_time"));

                        mAlreadyBookedArrayList.add(mModel);
                    }
                }
                //Working and with Appointments
                artistNotFountTV.setVisibility(View.GONE);
                mTimeSlotsRecyclerView.setVisibility(View.VISIBLE);
                //Set Up Recycler View Data
                setTimeSlotAdapter(mAlreadyBookedArrayList);
            } else {
                //Working and with Appointments
                artistNotFountTV.setVisibility(View.VISIBLE);
                mTimeSlotsRecyclerView.setVisibility(View.GONE);
                setTimeSlotAdapter(mAlreadyBookedArrayList);
//                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    /*
     * Set Time Slot Adapter
     * */
    private void setTimeSlotAdapter(ArrayList<FreeTimeSlotsModel> mTimeSlotsArrayList) {
        mTimeSlotsArrayNewList = mTimeSlotsArrayList;
        mTimeSlotsAdapter = new TimeSlotsAdapter(mActivity, mTimeSlotsArrayList, mSelectTimeSlotInterface);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        mTimeSlotsRecyclerView.setLayoutManager(mLayoutManagerC);
        mTimeSlotsRecyclerView.setAdapter(mTimeSlotsAdapter);
    }

    /*
     * Execute Book Appointment Api
     * */
    private void executeBookAppointment() {

        Calendar calander = Calendar.getInstance();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");

        long timeInMilliseconds = 0;
        try {
            Date mDate = simpledateformat.parse(strCalendarDate + " " + mFreeTimeSlotsModel.getStartTime());
            if (mDate != null) {
                timeInMilliseconds = mDate.getTime() / 1000;
            }
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        long strTimeInMillies = convertTimeStringToLongAppointment(strCalendarDate + " " + mFreeTimeSlotsModel.getStartTime());
//        Log.e(TAG, "Time in milliseconds: " + strTimeInMillies);

        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        String mTimeZone = timeZone.getID();

        showProgressDialog(mActivity);
        final String mApiUrl = Constants.BOOK_APPOINTMENT;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            mParams.put("artist_id", mServicesModel.getArtist_id());
            mParams.put("user_id", getUserID());
            mParams.put("service_id", mServicesModel.getId());
            mParams.put("date", strCalendarDate);
            mParams.put("appointment_time", mFreeTimeSlotsModel.getStartTime());
            mParams.put("time", "" + timeInMilliseconds);
            mParams.put("payment_method", strPaymentMethod);
            mParams.put("timezone", mTimeZone);
            mParams.put("timeVal", mFreeTimeSlotsModel.getStartTime());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //* prepare the Request *//*
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseBookAppointResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }
        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseBookAppointResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                showToast(mActivity, response.getString("message"));
                if (strPaymentMethod.equals("1")) {
                    Intent mIntent = new Intent(mActivity, PaymentActivity.class);
                    mIntent.putExtra(Constants.ID, response.getString("id"));
                    mIntent.putExtra(Constants.PRICE, mServicesModel.getPrice());
                    mIntent.putExtra(Constants.ARTIST_ID, mServicesModel.getArtist_id());
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);
//                    finish();
                } else if (strPaymentMethod.equals("2")) {
//                    finish();
                    startActivity(new Intent(mActivity, HomeActivity.class));
                    finishAffinity();
                }

            } else {
                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }
}
