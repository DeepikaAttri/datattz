package com.thetattz.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.fonts.TextViewBold;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ChangePasswordActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = ChangePasswordActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.txtCancelTV)
    TextViewBold txtCancelTV;
    @BindView(R.id.txtSaveTV)
    TextViewBold txtSaveTV;
    @BindView(R.id.editCurrentPasswordET)
    EditText editCurrentPasswordET;
    @BindView(R.id.editNewPasswordET)
    EditText editNewPasswordET;
    @BindView(R.id.editConfirmPassET)
    EditText editConfirmPassET;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        setStatusBar();
        ButterKnife.bind(this);

    }

    @OnClick({R.id.txtCancelTV, R.id.txtSaveTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtCancelTV:
                performCancelClick();
                break;
            case R.id.txtSaveTV:
                performSaveClick();
                break;
        }
    }

    private void performCancelClick() {
        onBackPressed();
    }

    private void performSaveClick() {
        if (isValidate()) {
            if (ConnectivityReceiver.isConnected()) {
                executeAPI();
            } else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    /*
     * Set up validations for Sign In fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editCurrentPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_current_pass));
            flag = false;
        } else if (editNewPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_new_passowrd));
            flag = false;
        } else if ((editNewPasswordET.getText().toString().trim().length() < 8)) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password_validate));
            flag = false;
        } else if (editConfirmPassET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_new_confirm_password));
            flag = false;
        } else if (editConfirmPassET.getText().toString().trim().length() < 8) {
            showAlertDialog(mActivity, getString(R.string.please_enterconfirm_password_validate));
            flag = false;
        } else if (!editNewPasswordET.getText().toString().trim().equals(editConfirmPassET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.your_password_and_match_is_wrong));
            flag = false;
        }
        return flag;
    }


    /*
     * Execute Update Password API
     * */
    private void executeAPI() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.UPDATE_PASSWORD;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            mParams.put("current_password", editCurrentPasswordET.getText().toString().trim());
            mParams.put("new_password", editNewPasswordET.getText().toString().trim());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                showToast(mActivity, getString(R.string.passowrd_update));
                finish();
            } else if (response.getString("status").equals("2")) {
                showAlertDialog(mActivity, response.getString("message"));
            } else {
                showAlertDialog(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }
}
