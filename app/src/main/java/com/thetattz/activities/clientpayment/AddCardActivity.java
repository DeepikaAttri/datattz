package com.thetattz.activities.clientpayment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aigestudio.wheelpicker.WheelPicker;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.BaseActivity;
import com.thetattz.fonts.EditTextRegular;
import com.thetattz.fonts.TextViewBold;
import com.thetattz.fonts.TextViewRegular;
import com.thetattz.utils.CardValidator;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddCardActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = AddCardActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = AddCardActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.txtCancelTV)
    TextViewBold txtCancelTV;
    @BindView(R.id.txtSaveTV)
    TextViewBold txtSaveTV;
    @BindView(R.id.editCardNumberET)
    EditTextRegular editCardNumberET;
    @BindView(R.id.imgCardTypeIV)
    ImageView imgCardTypeIV;
    @BindView(R.id.txtExpiryDateTV)
    TextViewRegular txtExpiryDateTV;
    @BindView(R.id.editCVVNumberET)
    EditTextRegular editCVVNumberET;
    @BindView(R.id.editCardHolderNameET)
    EditTextRegular editCardHolderNameET;

    /*
     * Initialize Objects
     * */
    CardValidator mValidator = new CardValidator();

    /*
     * Payment
     * */
    Card mCard;
    Stripe mStripe;
    Gson mGson;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);
        //Butter Knife
        ButterKnife.bind(this);
        //Set CardNumber Formatting
        setCardNumberFormatting();
    }

    /*
     * Card Validations
     *
     * */
    private void setCardNumberFormatting() {

        editCardNumberET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Remove spacing char
                String working = s.toString();
                if (working.length() == 4 && before == 0) {
                    working += "-";
                    editCardNumberET.setText(working);
                    editCardNumberET.setSelection(working.length());
                }
                if (working.length() == 9 && before == 0) {
                    working += "-";
                    editCardNumberET.setText(working);
                    editCardNumberET.setSelection(working.length());
                }
                if (working.length() == 14 && before == 0) {
                    working += "-";
                    editCardNumberET.setText(working);
                    editCardNumberET.setSelection(working.length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (mValidator.getCardType(s.toString()) != null) {
                    Log.e(TAG, "afterTextChanged: " + mValidator.getCardType(s.toString()).name());

                    if (mValidator.getCardType(s.toString()).name().toLowerCase().equals("mastercard")) {
                        imgCardTypeIV.setImageResource(R.drawable.ic_mc);
                    } else if (mValidator.getCardType(s.toString()).name().toLowerCase().equals("visa")) {
                        imgCardTypeIV.setImageResource(R.drawable.ic_visa);
                    } else if (mValidator.getCardType(s.toString()).name().toLowerCase().equals("discover")) {
                        imgCardTypeIV.setImageResource(R.drawable.ic_discover);
                    } else if (mValidator.getCardType(s.toString()).name().toLowerCase().equals("amex")) {
                        imgCardTypeIV.setImageResource(R.drawable.ic_ae);
                    } else {
                        imgCardTypeIV.setImageResource(R.drawable.ic_other);
                    }
                } else {
                    imgCardTypeIV.setImageResource(R.drawable.ic_other);
                }
            }
        });
    }

    @OnClick({R.id.txtCancelTV, R.id.txtSaveTV, R.id.txtExpiryDateTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtCancelTV:
                onBackPressed();
                break;
            case R.id.txtSaveTV:
                perfromSaveClick();
                break;
            case R.id.txtExpiryDateTV:
                perfromExpireyDateClick();
                break;
        }
    }

    private void perfromSaveClick() {
        if (isValidate()) {
            if (ConnectivityReceiver.isConnected())
                executeCardTokenApi();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }


    private void perfromExpireyDateClick() {
        createBottomSheetDialog();
    }


    public void createBottomSheetDialog() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        @SuppressLint("InflateParams") View sheetView = mActivity.getLayoutInflater().inflate(R.layout.dialog_card_expiry, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        Typeface mTypeface = Typeface.createFromAsset(getAssets(), "ProximaNova-Regular.ttf");

        final WheelPicker monthsWP = sheetView.findViewById(R.id.monthsWP);
        monthsWP.setTypeface(mTypeface);
        final WheelPicker yearsWP = sheetView.findViewById(R.id.yearsWP);
        yearsWP.setTypeface(mTypeface);


        TextView txtCancelTV = sheetView.findViewById(R.id.txtCancelTV);
        TextView txtSaveTV = sheetView.findViewById(R.id.txtSaveTV);


        monthsWP.setAtmospheric(true);
        monthsWP.setCyclic(false);
        monthsWP.setCurved(true);
        monthsWP.setData(getMonths());

        yearsWP.setAtmospheric(true);
        yearsWP.setCyclic(false);
        yearsWP.setCurved(true);
        yearsWP.setData(getYears());


        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        txtSaveTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtExpiryDateTV.setText(getMonthNumberFromName(getMonths().get(monthsWP.getCurrentItemPosition())) + "/" + getYears().get(yearsWP.getCurrentItemPosition()));
                mBottomSheetDialog.dismiss();
            }
        });
    }


    /*
     * Card Validations
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editCardNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_card_number));
            flag = false;
        } else if (!CardValidator.validateCardNumber(editCardNumberET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_carnumber));
            flag = false;
        } else if (txtExpiryDateTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_expiry_date));
            flag = false;
        } else if (editCVVNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_cvv));
            flag = false;
        } else if (editCardHolderNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_card_holder));
            flag = false;
        }
        return flag;
    }


    private void executeCardTokenApi() {
        try {
            mStripe = new Stripe(Constants.STRIPE_API_KEY);
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }

        String[] mMonthYearArray = txtExpiryDateTV.getText().toString().trim().split("/");
        String exMonth = mMonthYearArray[0];
        String exyear = mMonthYearArray[1];

        Card card = new Card(editCardNumberET.getText().toString().trim(),
                Integer.parseInt(exMonth),
                Integer.parseInt(exyear),
                editCVVNumberET.getText().toString().trim());
        card.setCurrency("USD");

        showProgressDialog(mActivity);
        mStripe.createToken(card, Constants.STRIPE_API_KEY, new TokenCallback() {
            public void onSuccess(Token token) {
                Log.e(TAG, "**Stripe Token**" + token.getId());
                String strToken = token.getId();
                executeSubmitPaymentAPI(strToken);
            }

            public void onError(Exception error) {
                dismissProgressDialog();
                showToast(mActivity, error.getLocalizedMessage());
            }
        });
    }

    private void executeSubmitPaymentAPI(String strToken) {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.ADD_CARDS_FOR_PAYMENT;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            mParams.put("token", strToken);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }


    private void parseResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                showSuccessAlertDialog(mActivity, response.getString("message"));
            } else {
                showAlertDialog(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }


}
