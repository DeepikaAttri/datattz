package com.thetattz.activities.clientpayment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.BaseActivity;
import com.thetattz.adapters.ClientCardsAdapter;
import com.thetattz.fonts.TextViewBold;
import com.thetattz.models.CardDetailsModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ClientCardsListActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ClientCardsListActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = ClientCardsListActivity.this;



    /*
     * Widgets
     * */
    @BindView(R.id.txtCancelTV)
    TextViewBold txtCancelTV;
    @BindView(R.id.txtSaveTV)
    TextViewBold txtSaveTV;
    @BindView(R.id.cardsRecyclerViewRV)
    RecyclerView cardsRecyclerViewRV;
    @BindView(R.id.txtCardNumberTV)
    TextView txtCardNumberTV;


    /*
    * Initalize Objects
    * */

    ClientCardsAdapter mClientCardsAdapter;
    ArrayList<CardDetailsModel> mCardsArrayList = new ArrayList<>();


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_cards_list);
        ButterKnife.bind(this);
    }


    /*
     * Activity Override method
     * #onResume
     * */
    @Override
    protected void onResume() {
        super.onResume();
        //Getting Cards Details
        gettingAllCardsDetails();
    }

    @OnClick({R.id.txtCancelTV, R.id.txtSaveTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtCancelTV:
                onBackPressed();
                break;
            case R.id.txtSaveTV:
                perfromSaveClick();
                break;
        }
    }

    private void perfromSaveClick() {
        startActivity(new Intent(mActivity, AddCardActivity.class));
    }


    private void gettingAllCardsDetails(){
        if (ConnectivityReceiver.isConnected())
            executeRetrivesCards();
        else
            showToast(mActivity,getString(R.string.internet_connection_error));
    }

    /*
     *
     * Execute  Card Retrives Api
     * */
    private void executeRetrivesCards() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.RETRIVE_ALL_CARDS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {
        mCardsArrayList.clear();
        try {
            if (response.getString("status").equals("1")) {
                JSONArray mJsonArray = response.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++){
                    JSONObject mDataObj = mJsonArray.getJSONObject(i);
                    CardDetailsModel mModel = new CardDetailsModel();
                    if (!mDataObj.isNull("card_id"))
                        mModel.setCard_id(mDataObj.getString("card_id"));
                    if (!mDataObj.isNull("brand"))
                        mModel.setBrand(mDataObj.getString("brand"));
                    if (!mDataObj.isNull("country"))
                        mModel.setCountry(mDataObj.getString("country"));
                    if (!mDataObj.isNull("exp_month"))
                        mModel.setExp_month(mDataObj.getString("exp_month"));
                    if (!mDataObj.isNull("exp_year"))
                        mModel.setExp_year(mDataObj.getString("exp_year"));
                    if (!mDataObj.isNull("last4"))
                        mModel.setLast4(mDataObj.getString("last4"));

                    mCardsArrayList.add(mModel);
                }

                if (mCardsArrayList.size() > 0) {
                    txtCardNumberTV.setVisibility(View.GONE);
                    cardsRecyclerViewRV.setVisibility(View.VISIBLE);
                    //Set Card Details Adpater:
                    setAdapter();
                }else{
                    txtCardNumberTV.setVisibility(View.VISIBLE);
                    cardsRecyclerViewRV.setVisibility(View.GONE);
                }

            } else {
                showAlertDialog(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }

    /*
     * Set Cards Adapter
     * */
    private void setAdapter() {
        mClientCardsAdapter = new ClientCardsAdapter(mActivity, mCardsArrayList);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity);
        cardsRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        cardsRecyclerViewRV.setAdapter(mClientCardsAdapter);
    }



}
