package com.thetattz.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.fragments.AppointmentFragment;
import com.thetattz.fragments.HomeFragment;
import com.thetattz.fragments.NotificationFragment;
import com.thetattz.fragments.SearchArtistFragment;
import com.thetattz.fragments.SettingFragment;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = HomeActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = HomeActivity.this;

    /**
     * Widgets Main Layout
     */
    @BindView(R.id.containerFL)
    FrameLayout containerFL;
    @BindView(R.id.imgTab1IV)
    ImageView imgTab1IV;
    @BindView(R.id.tab1LL)
    LinearLayout tab1LL;
    @BindView(R.id.imgTab2IV)
    ImageView imgTab2IV;
    @BindView(R.id.tab2LL)
    LinearLayout tab2LL;
    @BindView(R.id.imgTab3IV)
    ImageView imgTab3IV;
    @BindView(R.id.tab3LL)
    LinearLayout tab3LL;
    @BindView(R.id.imgTab4IV)
    ImageView imgTab4IV;
    @BindView(R.id.tab4LL)
    LinearLayout tab4LL;
    @BindView(R.id.imgTab5IV)
    ImageView imgTab5IV;
    @BindView(R.id.tab5LL)
    LinearLayout tab5LL;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setStatusBar();

        //Butter Knife
        ButterKnife.bind(this);

        //By Default Selec TAB 1
        perfromTab1Click();

        if (!TheTattzPrefrences.readString(mActivity, Constants.APPOINTMENT_TYPE, "").equals("") && TheTattzPrefrences.readString(mActivity, Constants.APPOINTMENT_TYPE, "").equals(mActivity.getResources().getString(R.string.type_pending)) && TheTattzPrefrences.readString(mActivity, Constants.TYPES, "").equals(mActivity.getResources().getString(R.string.type_client))) {
            perfromTab2Click();
        } else if (!TheTattzPrefrences.readString(mActivity, Constants.APPOINTMENT_TYPE, "").equals("")) {
            perfromTab2Click();
        }

        executeUpdateBadgeCountApi(mActivity);
    }

    @OnClick({R.id.tab1LL, R.id.tab2LL, R.id.tab3LL, R.id.tab4LL, R.id.tab5LL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tab1LL:
                perfromTab1Click();
                break;
            case R.id.tab2LL:
                perfromTab2Click();
                break;
            case R.id.tab3LL:
                perfromTab3Click();
                break;
            case R.id.tab4LL:
                perfromTab4Click();
                break;
            case R.id.tab5LL:
                perfromTab5Click();
                break;
        }
    }

    private void perfromTab1Click() {
        imgTab1IV.setImageResource(R.drawable.ic_tab1_s);
        imgTab2IV.setImageResource(R.drawable.ic_tab2_us);
        imgTab3IV.setImageResource(R.drawable.ic_home_tab3_un);
        imgTab4IV.setImageResource(R.drawable.ic_tab4);
        imgTab5IV.setImageResource(R.drawable.ic_tab5_us);
        switchFragment(mActivity, new HomeFragment(), false, null);
    }

    public void perfromTab2Click() {
        imgTab1IV.setImageResource(R.drawable.ic_tab1_us);
        imgTab2IV.setImageResource(R.drawable.ic_tab2_s);
        imgTab3IV.setImageResource(R.drawable.ic_home_tab3_un);
        imgTab4IV.setImageResource(R.drawable.ic_tab4);
        imgTab5IV.setImageResource(R.drawable.ic_tab5_us);
        switchFragment(mActivity, new AppointmentFragment(), false, null);
    }

    public void perfromTab3Click() {
        imgTab1IV.setImageResource(R.drawable.ic_tab1_us);
        imgTab2IV.setImageResource(R.drawable.ic_tab2_us);
        imgTab3IV.setImageResource(R.drawable.ic_home_tab3_s);
        imgTab4IV.setImageResource(R.drawable.ic_tab4);
        imgTab5IV.setImageResource(R.drawable.ic_tab5_us);
        switchFragment(mActivity, new SearchArtistFragment(), false, null);
    }

    public void perfromTab4Click() {
        imgTab1IV.setImageResource(R.drawable.ic_tab1_us);
        imgTab2IV.setImageResource(R.drawable.ic_tab2_us);
        imgTab3IV.setImageResource(R.drawable.ic_home_tab3_un);
        imgTab4IV.setImageResource(R.drawable.ic_tab4);
        imgTab5IV.setImageResource(R.drawable.ic_tab5_us);
        switchFragment(mActivity, new NotificationFragment(), false, null);
    }

    public void perfromTab5Click() {
        imgTab1IV.setImageResource(R.drawable.ic_tab1_us);
        imgTab2IV.setImageResource(R.drawable.ic_tab2_us);
        imgTab3IV.setImageResource(R.drawable.ic_home_tab3_un);
        imgTab4IV.setImageResource(R.drawable.ic_tab4);
        imgTab5IV.setImageResource(R.drawable.ic_tab5_s);
        switchFragment(mActivity, new SettingFragment(), false, null);
    }

    /********
     *Replace Fragment In Activity
     **********/
    public void switchFragment(Activity activity, Fragment fragment, boolean addToStack, Bundle bundle) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.containerFL, fragment);
            if (addToStack)
                ft.addToBackStack(null);
            if (bundle != null)
                fragment.setArguments(bundle);
            ft.commitAllowingStateLoss();
        }
    }

    // In your activity
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    /*
     * Set Up Status Bar
     * */
    public void setStatusBar() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark
        getWindow().setStatusBarColor(ContextCompat.getColor(mActivity, R.color.colorWhite));// set status background white
    }

    private void executeGetTimeByTimezoneAPI() {
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        String mTimeZone = timeZone.getID();

        final String mApiUrl = Constants.GET_TIME_BY_TIMEZONE;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("timezone", mTimeZone);
            mParams.put("apttime", System.currentTimeMillis() / 1000);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();

//        executeGetTimeByTimezoneAPI();
    }
}
