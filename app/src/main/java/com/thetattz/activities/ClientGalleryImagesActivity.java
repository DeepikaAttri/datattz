package com.thetattz.activities;

import android.app.Activity;
import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import com.thetattz.R;
import com.thetattz.adapters.ClientGalleryImageAdapter;
import com.thetattz.models.MyTattozModel;
import com.thetattz.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class ClientGalleryImagesActivity extends BaseActivity {
    /*
     * Initlaize Activity
     * */
    Activity mActivity = ClientGalleryImagesActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = ClientGalleryImagesActivity.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.mIndicatorCI)
    CircleIndicator mIndicatorCI;
    @BindView(R.id.mViewPagerVP)
    ViewPager mViewPagerVP;

    /*
     * Initialize Objects
     * */
    ClientGalleryImageAdapter mClientGalleryImageAdapter;
    ArrayList<MyTattozModel> mMyTattozArrayList = new ArrayList();
    int mPosition = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_gallery_images);
        setStatusBar();
        //ButterKnife
        ButterKnife.bind(this);
        //Get Intent Data
        getIntentData();
        //Set Data on Widgets
        setDataOnWidgets();
    }

    /*
     * Get Intent Data
     * */
    private void getIntentData() {
        if (getIntent() != null) {
            mMyTattozArrayList = (ArrayList<MyTattozModel>) getIntent().getSerializableExtra(Constants.LIST);
            mPosition = getIntent().getIntExtra(Constants.POSITION, 0);
        }
    }

    /*
     * Set Data on Widgets
     * */
    private void setDataOnWidgets() {
        //Set Images
        mClientGalleryImageAdapter = new ClientGalleryImageAdapter(mActivity, this, mMyTattozArrayList);
        mViewPagerVP.setAdapter(mClientGalleryImageAdapter);
        mViewPagerVP.setCurrentItem(mPosition);
        mIndicatorCI.setViewPager(mViewPagerVP);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
