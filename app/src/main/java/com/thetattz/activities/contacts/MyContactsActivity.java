package com.thetattz.activities.contacts;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.tamir7.contacts.Contact;
import com.github.tamir7.contacts.Contacts;
import com.github.tamir7.contacts.PhoneNumber;
import com.thetattz.R;
import com.thetattz.activities.BaseActivity;
import com.thetattz.adapters.MyContactsAdapter;
import com.thetattz.fonts.TextViewBold;
import com.thetattz.interfaces.ContactClickInterface;
import com.thetattz.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyContactsActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = MyContactsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = MyContactsActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.txtCancelTV)
    TextViewBold txtCancelTV;
    @BindView(R.id.txtSaveTV)
    TextViewBold txtSaveTV;
    @BindView(R.id.contactsRecyclerViewRV)
    RecyclerView contactsRecyclerViewRV;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;
    @BindView(R.id.editSearchET)
    EditText editSearchET;


    /*
     * Initialize...Objects
     * */

    MyContactsAdapter myContactsAdapter;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_contacts);
        setStatusBar();
        ButterKnife.bind(this);
        setContactsAdapter();
        //Set Search
        setSearch();
    }

    private void setSearch() {
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }
        });
    }


    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<Contact> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (Contact s : Contacts.getQuery().find()) {
            //if the existing elements contains the search input
            if (s.getDisplayName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }

        //calling a method of the adapter class and passing the filtered list
        myContactsAdapter.filterList(filterdNames);
    }


    ContactClickInterface mContactClickInterface = new ContactClickInterface() {
        @Override
        public void getContactDetails(Contact mContact) {
            String mContactName = mContact.getDisplayName();
            PhoneNumber mPhoneNumber = mContact.getPhoneNumbers().get(0);
            String mContactNumber = mPhoneNumber.getNumber();


            Intent mIntent = new Intent();
            mIntent.putExtra(Constants.CONTACT_NAME,mContactName);
            mIntent.putExtra(Constants.CONTACT_NUMBER,mContactNumber);
            setResult(999, mIntent);
            finish();
        }
    };

    private void setContactsAdapter() {
        List<Contact> contactsList = Contacts.getQuery().find();
        Log.e(TAG, "**Contact**" + contactsList.size());

        if (contactsList.size() > 0) {
            contactsRecyclerViewRV.setVisibility(View.VISIBLE);
            txtNoDataFountTV.setVisibility(View.GONE);
        } else {
            contactsRecyclerViewRV.setVisibility(View.GONE);
            txtNoDataFountTV.setVisibility(View.VISIBLE);
        }


        myContactsAdapter = new MyContactsAdapter(mActivity, contactsList,mContactClickInterface);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity);
        contactsRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        contactsRecyclerViewRV.setAdapter(myContactsAdapter);
    }

    @OnClick(R.id.txtCancelTV)
    public void onViewClicked() {
        onBackPressed();
    }


}
