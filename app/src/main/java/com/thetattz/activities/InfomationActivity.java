package com.thetattz.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thetattz.R;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;
import com.thetattz.views.LollipopFixedWebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InfomationActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = InfomationActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = InfomationActivity.this;

    /**
     * Widgets Main Layout
     */
    @BindView(R.id.layoutBackRL)
    RelativeLayout layoutBackRL;

    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;

    @BindView(R.id.mWebViewWV)
    LollipopFixedWebView mWebViewWV;

    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;

    /*
     * Initialize Objects...
     * */
    String strUrl = "";
    String strType = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infomation);
        setStatusBar();
        ButterKnife.bind(this);
        //Get Intent Data
        getIntentDataSetWebView();
    }

    private void getIntentDataSetWebView() {
        if (getIntent() != null) {
            strType = getIntent().getStringExtra(Constants.TYPES);
            txtHeaderTV.setText(strType);
            if (strType.equals(getString(R.string.terms_of_services)))
                strUrl = Constants.TERMS_AND_SERVICES_URL;
            if (strType.equals(getString(R.string.privacy_policy)))
                strUrl = Constants.PRIVACY_POLICY_URL;
            if (strType.equals(getString(R.string.facebook)))
                strUrl = Constants.FACEBOOK;
            if (strType.equals(getString(R.string.instagram)))
                strUrl = Constants.INSTAGRAM;
            if (strType.equals(getString(R.string.twitter)))
                strUrl = Constants.TWITTER;
            if (strType.equals(getString(R.string.website))) {
                strUrl = Constants.WEBSITE;
                mProgressBar.setVisibility(View.GONE);
            }
            setWebView(strUrl);
        }
    }

    private void setWebView(String strUrl111) {
        if (ConnectivityReceiver.isConnected())
            loadUrlInWebView(strUrl111);
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void loadUrlInWebView(String strUrl) {
        mWebViewWV.setWebViewClient(new MyWebClient());
        mWebViewWV.getSettings().setJavaScriptEnabled(true);
        mWebViewWV.loadUrl(strUrl);
    }

    public class MyWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            mProgressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

            if (mProgressBar.isShown())
                mProgressBar.setVisibility(View.GONE);
        }
    }

    // To handle "Back" key press event for WebView to go back to previous screen.
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebViewWV.canGoBack()) {
            mWebViewWV.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /*
     * Widgets click listeners
     * */
    @OnClick({R.id.layoutBackRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutBackRL:
                onBackPressed();
                break;

        }
    }
}
