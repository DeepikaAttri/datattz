package com.thetattz.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.artist.ArtistAppointActivity;
import com.thetattz.activities.artist.ArtistHomeActivity;
import com.thetattz.chat.ChatActivity;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class SplashActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SplashActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = SplashActivity.this;

    /**
     * Splash Close Time
     */
    public final int SPLASH_TIME_OUT = 3000;

    boolean isNotification = false;
    String type = "", appointment_status = "", user_id = "", notification_id = "", artist_id = "",
            receiver_id = "", sender_id = "", room_id = "", appointment_id = "", sender_name = "", profile_pic = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setStatusBar();

        getUserProfile();

        setNotification();

    }

    private void setNotification() {
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);

                if (key.equalsIgnoreCase("type")) {
                    type = String.valueOf(value);

                    if (type.equalsIgnoreCase("appointment")) {

                        if (getUserRole().equals(Constants.ROLE_ARTIST)) {

                            TheTattzPrefrences.writeString(mActivity, Constants.TYPES, mActivity.getResources().getString(R.string.type_artist));

                            if (appointment_status.equals("1")) {
                                TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_pending));
                                mActivity.startActivity(new Intent(mActivity, ArtistAppointActivity.class)
                                        .putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH));
                                finish();
                            } else if (appointment_status.equals("2")) {
                                TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_confirm));
                                mActivity.startActivity(new Intent(mActivity, ArtistAppointActivity.class)
                                        .putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH));
                                finish();
                            } else if (appointment_status.equals("3")) {
                                TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_cancelled));
                                mActivity.startActivity(new Intent(mActivity, ArtistAppointActivity.class)
                                        .putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH));
                                finish();
                            } else if (appointment_status.equals("4")) {
                                TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_complete));
                                mActivity.startActivity(new Intent(mActivity, ArtistAppointActivity.class)
                                        .putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH));
                                finish();
                            } else if (appointment_status.equals("5")) {
                                TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_past));
                                mActivity.startActivity(new Intent(mActivity, ArtistAppointActivity.class)
                                        .putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH));
                                finish();
                            }

                        } else if (getUserRole().equals(Constants.ROLE_CLIENT)) {

                            TheTattzPrefrences.writeString(mActivity, Constants.TYPES, mActivity.getResources().getString(R.string.type_client));

                            if (appointment_status.equals("1")) {
                                mActivity.startActivity(new Intent(mActivity, HomeActivity.class)
                                        .putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH));
                                TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_pending));
                                finish();
                            } else if (appointment_status.equals("2")) {
                                TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_confirm));
                                mActivity.startActivity(new Intent(mActivity, HomeActivity.class)
                                        .putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH));
                                finish();
                            } else if (appointment_status.equals("3")) {
                                TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_cancelled));
                                mActivity.startActivity(new Intent(mActivity, HomeActivity.class)
                                        .putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH));
                                finish();
                            } else if (appointment_status.equals("4")) {
                                TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_complete));
                                mActivity.startActivity(new Intent(mActivity, HomeActivity.class)
                                        .putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH));
                                finish();
                            } else if (appointment_status.equals("5")) {
                                TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_past));
                                mActivity.startActivity(new Intent(mActivity, HomeActivity.class)
                                        .putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH));
                                finish();
                            }
                        }
                    } else if (type.equalsIgnoreCase("chat")) {
                        Intent newIntent = new Intent(mActivity, ChatActivity.class);
                        newIntent.putExtra(Constants.SENDER_ID, sender_id);
                        newIntent.putExtra(Constants.ROOM_ID, room_id);
                        newIntent.putExtra(Constants.SENDER_NAME, sender_name);
                        newIntent.putExtra(Constants.SENDER_PROFILE, profile_pic);
                        newIntent.putExtra(Constants.APPOINTMENT_ID, appointment_id);
                        if (getUserRole().equals(Constants.ROLE_ARTIST)) {
                            newIntent.putExtra(Constants.TYPES, getString(R.string.type_artist));
                        } else if (getUserRole().equals(Constants.ROLE_CLIENT)) {
                            newIntent.putExtra(Constants.TYPES, getString(R.string.type_client));
                        }
                        newIntent.putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH);
                        startActivity(newIntent);
                        finish();
                    } else if (type.equalsIgnoreCase("general_chat")) {
                        Intent newIntent = new Intent(mActivity, ChatActivity.class);
                        newIntent.putExtra(Constants.SENDER_ID, sender_id);
                        newIntent.putExtra(Constants.ROOM_ID, room_id);
                        newIntent.putExtra(Constants.SENDER_NAME, sender_name);
                        newIntent.putExtra(Constants.SENDER_PROFILE, profile_pic);
                        newIntent.putExtra(Constants.APPOINTMENT_ID, appointment_id);
                        if (getUserRole().equals(Constants.ROLE_ARTIST)) {
                            newIntent.putExtra(Constants.TYPES, getString(R.string.type_artist));
                        } else if (getUserRole().equals(Constants.ROLE_CLIENT)) {
                            newIntent.putExtra(Constants.TYPES, getString(R.string.type_client));
                        }
                        newIntent.putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH);
                        startActivity(newIntent);
                        finish();
                    }
                }
                if (key.equalsIgnoreCase("detail")) {
                    isNotification = true;

                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(String.valueOf(value));

                        if (jsonObject.has("appointment_status")) {
                            appointment_status = jsonObject.getString("appointment_status");
                        }
                        if (jsonObject.has("user_id")) {
                            user_id = jsonObject.getString("user_id");
                        }
                        if (jsonObject.has("notification_id")) {
                            notification_id = jsonObject.getString("notification_id");
                        }
                        if (jsonObject.has("artist_id")) {
                            artist_id = jsonObject.getString("artist_id");
                        }
                        if (jsonObject.has("receiver_id")) {
                            receiver_id = jsonObject.getString("receiver_id");
                        }
                        if (jsonObject.has("sender_id")) {
                            sender_id = jsonObject.getString("sender_id");
                        }
                        if (jsonObject.has("room_id")) {
                            room_id = jsonObject.getString("room_id");
                        }
                        if (jsonObject.has("appointment_id")) {
                            appointment_id = jsonObject.getString("appointment_id");
                        }
                        if (jsonObject.has("sender_name")) {
                            sender_name = jsonObject.getString("sender_name");
                        }
                        if (jsonObject.has("profile_pic")) {
                            profile_pic = jsonObject.getString("profile_pic");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (!isNotification) {
                setUpSplash();
            }
        } else {
            setUpSplash();
        }
    }

    private void getUserProfile() {
        if (isLogin()) {
            executeGetUserDetailsAPI();
            executeUpdateBadgeCountApi(mActivity);
        }
    }

    private void setUpSplash() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isLogin()) {
                    if (getUserRole().equals(Constants.ROLE_ARTIST)) {
                        //Open Artist HomeScreen
                        Intent mIntent = new Intent(mActivity, ArtistHomeActivity.class);
                        startActivity(mIntent);
                        finish();
                    } else if (getUserRole().equals(Constants.ROLE_CLIENT)) {
                        //Open Client HomeScreen
                        TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, "");
                        Intent mIntent = new Intent(mActivity, HomeActivity.class);
                        startActivity(mIntent);
                        finish();
                    }
                } else {
                    startActivity(new Intent(mActivity, SignInSignUpActivity.class));
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    /*Execute Get User Details API*/
    private void executeGetUserDetailsAPI() {
        final String mApiUrl = Constants.GET_USER_DETAILS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseDetailsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseDetailsResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                JSONObject mDataObject = response.getJSONObject("data");
                //Collection User Data
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_ID, mDataObject.getString("user_id"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_NAME, mDataObject.getString("name"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_EMAIL, mDataObject.getString("email"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_ROLE, mDataObject.getString("role"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_PROFILE_PIC, mDataObject.getString("profile_pic"));

//                if (mDataObject.has("isSubscriptionActive")) {
//                    TheTattzPrefrences.writeBoolean(mActivity, TheTattzPrefrences.isSubscriptionActive, mDataObject.getBoolean("isSubscriptionActive"));
//                }
            } else {
                Log.e(TAG, "**Error**" + response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }
}
