package com.thetattz.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.fonts.EditTextBold;
import com.thetattz.fonts.TextViewBold;
import com.thetattz.models.TimeZoneModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;
import com.thetattz.volley.AppHelper;
import com.thetattz.volley.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class EditAccountActivity extends BaseActivity {
    public final int REQUEST_CAMERA = 384;
    public final int REQUEST_GALLERY = 348;
    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    /**
     * Getting the Current Class Name
     */
    String TAG = EditAccountActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Intance
     */
    Activity mActivity = EditAccountActivity.this;
    /*
     * Widgets
     * */
    @BindView(R.id.txtCancelTV)
    TextViewBold txtCancelTV;
    @BindView(R.id.txtSaveTV)
    TextViewBold txtSaveTV;
    @BindView(R.id.txtEditTV)
    TextView txtEditTV;
    @BindView(R.id.txtEditProfilePicTV)
    TextViewBold txtEditProfilePicTV;
    @BindView(R.id.editUserNameET)
    EditTextBold editUserNameET;
    @BindView(R.id.editEmailET)
    EditTextBold editEmailET;
    @BindView(R.id.editPasswordET)
    EditTextBold editPasswordET;
    @BindView(R.id.txtChangeTV)
    TextViewBold txtChangeTV;
    @BindView(R.id.imgProfilePicCIV)
    CircleImageView imgProfilePicCIV;
    @BindView(R.id.timezone_sp)
    Spinner timezone_sp;
    @BindView(R.id.timeZoneLL)
    LinearLayout timeZoneLL;

    List<TimeZoneModel> mTimezoneArrayList = new ArrayList<>();
    String strTimezone = "", TimeZoneName = "";
    /*
     * Initialize Objects...
     *
     * */
    Bitmap mBitmapProfilePic;
    String currentPhotoPath = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);
        ButterKnife.bind(this);
        setStatusBar();
        //Set Data on widgts
        setDataOnWidgets();
        //Get User Details
        save();

        if (getIntent().getStringExtra("DETAILS").equals("DETAILS")) {
            //Get Default Layout
            performGetUserDetails();
        }
    }

    private void setDataOnWidgets() {
        //Set Name
        editUserNameET.setText(getUserName());
        editUserNameET.setSelection(getUserName().length());
        //Set Email
        editEmailET.setText(getUserEmail());
        editEmailET.setEnabled(false);
        //Set Password
        editPasswordET.setText(getPassword());
        editPasswordET.setSelection(editPasswordET.getText().toString().trim().length());
        editPasswordET.setEnabled(false);
        //Set Profile Pic
        if (getUserProfilePicture() != null && getUserProfilePicture().contains("http")) {
            String profilePic = getUserProfilePicture().replace(" ", "%20");
            Picasso.with(mActivity).load(profilePic)
                    .placeholder(R.drawable.ic_ph_contact)
                    .error(R.drawable.ic_ph_contact)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgProfilePicCIV);
        }
        Log.e(TAG, "**Profile Picture Url**" + getUserProfilePicture());

        getTimezonesFromJson();
    }

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /*
     * Widgets click Listner
     * */

    private void performGetUserDetails() {
        if (ConnectivityReceiver.isConnected())
            executeGetUserDetailsAPI();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    @OnClick({R.id.txtCancelTV, R.id.txtSaveTV, R.id.txtEditTV, R.id.txtEditProfilePicTV, R.id.txtChangeTV, R.id.imgProfilePicCIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtCancelTV:
                performCancelClick();
                break;
            case R.id.txtSaveTV:
                perfromSaveClick();
                break;
            case R.id.txtEditTV:
                perfromEditClick();
                break;
            case R.id.txtEditProfilePicTV:
                perfromChangeProfilePicClick();
                break;
            case R.id.txtChangeTV:
                performPasswordChangeClick();
                break;
            case R.id.imgProfilePicCIV:
                perfromChangeProfilePicClick();
                break;
        }
    }

    private void performCancelClick() {
        onBackPressed();
    }

    private void perfromEditClick() {
        edit();
    }

    private void edit() {
        timezone_sp.setEnabled(true);
        txtEditTV.setVisibility(View.GONE);
        txtSaveTV.setVisibility(View.VISIBLE);
        txtEditProfilePicTV.setVisibility(View.VISIBLE);
        editUserNameET.setEnabled(true);
        editUserNameET.setSelection(editUserNameET.getText().toString().trim().length());
        editEmailET.setEnabled(false);
        editPasswordET.setEnabled(false);
        imgProfilePicCIV.setEnabled(true);
    }

    private void save() {
        timezone_sp.setEnabled(false);
        txtEditTV.setVisibility(View.VISIBLE);
        txtSaveTV.setVisibility(View.GONE);
        txtEditProfilePicTV.setVisibility(View.GONE);
        editUserNameET.setEnabled(false);
        editEmailET.setEnabled(false);
        editPasswordET.setEnabled(false);
        imgProfilePicCIV.setEnabled(false);
    }

    /*
     * Set up validations for Sign In fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editUserNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_name));
            flag = false;
        }
        return flag;
    }

    private void perfromSaveClick() {
        if (isValidate()) {
            if (ConnectivityReceiver.isConnected())
                executeUploadProfileImage();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }

    }

    private void performPasswordChangeClick() {
        startActivity(new Intent(mActivity, ChangePasswordActivity.class));
    }

    /*Execute Get User Details API*/
    private void executeGetUserDetailsAPI() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.GET_USER_DETAILS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseDetailsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseDetailsResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                JSONObject mDataObject = response.getJSONObject("data");
                //Collection User Data
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_ID, mDataObject.getString("user_id"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_NAME, mDataObject.getString("name"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_EMAIL, mDataObject.getString("email"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_ROLE, mDataObject.getString("role"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_PASSWORD, mDataObject.getString("password"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_PROFILE_PIC, mDataObject.getString("profile_pic"));

                TimeZoneName = mDataObject.getString("timezone");
                strTimezone = mDataObject.getString("timezoneAbr");

                if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_ARTIST)) {
                    timeZoneLL.setVisibility(View.VISIBLE);
                } else if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_CLIENT)) {
                    timeZoneLL.setVisibility(View.GONE);
                }

                setDataOnWidgets();
            } else {
                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }

    public void onSelectImageClick(View view) {
        CropImage.startPickImageActivity(this);
    }

    private void startCropImageActivity() {
        CropImage.activity()
                .start(this);
    }

    /*
     * Add User Tattos with
     * Camera Gallery functionality
     * */
    public void perfromChangeProfilePicClick() {
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private void openCameraGalleryDialog() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_camera_gallery);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);


        // set the custom dialog components - text, image and button
        LinearLayout cameraLL = alertDialog.findViewById(R.id.cameraLL);
        LinearLayout galleryLL = alertDialog.findViewById(R.id.galleryLL);
        ImageView imgCancelIV = alertDialog.findViewById(R.id.imgCancelIV);

        imgCancelIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        cameraLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
                alertDialog.dismiss();
            }
        });

        galleryLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                alertDialog.dismiss();
            }
        });


        alertDialog.show();
    }

    private void openCamera() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getImageFile(); // 1
        Uri uri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) // 2
            uri = FileProvider.getUriForFile(mActivity, "com.thetattz.fileprovider", file);
        else
            uri = Uri.fromFile(file); // 3
        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri); // 4
        startActivityForResult(pictureIntent, REQUEST_CAMERA);
    }

    private void openGallery() {
        Intent pictureIntent = new Intent(Intent.ACTION_GET_CONTENT);
        pictureIntent.setType("image/*");  // 1
        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE);  // 2
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = new String[]{"image/jpeg", "image/png"};  // 3
            pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        startActivityForResult(Intent.createChooser(pictureIntent, "Select Picture"), REQUEST_GALLERY);  // 4
    }

    private File getImageFile() {
        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";
        File storageDir = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DCIM
                ), "Camera"
        );
        File file = null;
        try {
            file = File.createTempFile(
                    imageFileName, ".jpg", storageDir
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        currentPhotoPath = "file:" + file.getAbsolutePath();
        return file;
    }


    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
        }
    }


    /**
     * Start pick image activity with chooser.
     */
    public void onSelectImageClick() {
        CropImage.startPickImageActivity(this);
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(100, 100)
                .setMultiTouchEnabled(false)
                .start(this);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                try {

                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());

                    mBitmapProfilePic = BitmapFactory.decodeStream(imageStream);

                    imgProfilePicCIV.setImageBitmap(mBitmapProfilePic);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(mActivity, "Cropping failed: " + result.getError());
            }
        }


    }


    private void showImage(Uri imageUri) {
        String file = getRealPathFromURI_API19(mActivity, imageUri);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        mBitmapProfilePic = BitmapFactory.decodeStream(inputStream);

        imgProfilePicCIV.setImageBitmap(mBitmapProfilePic);
    }

    private void executeUploadProfileImage() {
        showProgressDialog(mActivity);
        String ApiUrl = Constants.EDIT_USER_PROFILE;
        Log.e(TAG, "Api Url::::" + ApiUrl);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                ApiUrl, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                dismissProgressDialog();
                String resultResponse = new String(response.data);
                Log.e(TAG, "onResponse: " + resultResponse);
                parseImageUploadResponse(resultResponse);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                String strBody = "";
                if (error.networkResponse != null) {
                    try {
                        strBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                        } else if (error instanceof AuthFailureError) {
                            showAlertDialog(mActivity, getString(R.string.auth_failed));
                        } else if (error instanceof ServerError) {
                            showAlertDialog(mActivity, getString(R.string.server_error));
                        } else if (error instanceof NetworkError) {
                            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                        } else if (error instanceof ParseError) {
                            showAlertDialog(mActivity, getString(R.string.parse_error));
                        } else {
                            showAlertDialog(mActivity, strBody);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                }
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", getUserID());
                params.put("name", editUserNameET.getText().toString().trim());
                if (mBitmapProfilePic == null && getUserProfilePicture() != null && getUserProfilePicture().contains("http"))
                    params.put("profile_pic", getUserProfilePicture());

                if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_ARTIST)) {
                    params.put("timezoneAbr", strTimezone);
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "multipart/form-data");
                return headers;


            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                if (mBitmapProfilePic != null)
                    params.put("profile_pic", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, mBitmapProfilePic), "image/jpeg"));
                return params;
            }
        };

        TheTattzApplication.getInstance().addToRequestQueue(multipartRequest);

    }

    private void parseImageUploadResponse(String resultResponse) {
        try {
            JSONObject mJsonObject = new JSONObject(resultResponse);
            if (mJsonObject.getString("status").equals("1")) {
                showToast(mActivity, mJsonObject.getString("message"));
                JSONObject mDataObject = mJsonObject.getJSONObject("data");
                //Collection User Data
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_ID, mDataObject.getString("user_id"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_NAME, mDataObject.getString("name"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_EMAIL, mDataObject.getString("email"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_ROLE, mDataObject.getString("role"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.USER_PROFILE_PIC, mDataObject.getString("profile_pic"));
                setDataOnWidgets();
                save();
            } else {
                showToast(mActivity, mJsonObject.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    private void getTimezonesFromJson() {
        if (mTimezoneArrayList != null) {
            mTimezoneArrayList.clear();
        }

        try {
            //JSONObject mJsonObject = new JSONObject(loadJSONFromAsset());
            JSONArray mJsonArray = new JSONArray(loadJSONFromAsset());
            Log.e(TAG, "***Array Size***" + mJsonArray.length());

            for (int i = 0; i < mJsonArray.length(); i++) {
                JSONObject mDataObj = mJsonArray.getJSONObject(i);
                TimeZoneModel mModel = new TimeZoneModel();
                if (!mDataObj.isNull("timezone"))
                    mModel.setTimezone(mDataObj.getString("timezone"));
                if (!mDataObj.isNull("timezoneAbr"))
                    mModel.setTimezoneAbr(mDataObj.getString("timezoneAbr"));
                mTimezoneArrayList.add(mModel);
            }

            setTimeZoneAdapter();

            Log.e(TAG, "***Array Size***" + mTimezoneArrayList.size());
        } catch (Exception e) {
            e.toString();
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("timezones.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    /*
     *  time zone adapter
     * */
    private void setTimeZoneAdapter() {
        Typeface fontStyle = Typeface.createFromAsset(mActivity.getAssets(),
                "Purissima Bold W00 Regular.ttf");
        ArrayAdapter<TimeZoneModel> mSizeAd = new ArrayAdapter<TimeZoneModel>(this, android.R.layout.simple_spinner_item, mTimezoneArrayList) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                HideKey();

                TextView itemTextView = v.findViewById(R.id.itemTV);
                String strName = mTimezoneArrayList.get(position).getTimezoneAbr() + " - " + mTimezoneArrayList.get(position).getTimezone();
                itemTextView.setText(strName);
                itemTextView.setTypeface(fontStyle);
                return v;
            }
        };
        timezone_sp.setAdapter(mSizeAd);
        timezone_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ((TextView) view).setTypeface(fontStyle);
                ((TextView) view).setTextSize(23);
                ((TextView) view).setPadding(0, 0, 0, 0);
                ((TextView) view).setTextColor(getResources().getColor(R.color.colorWhite));
                strTimezone = mTimezoneArrayList.get(position).getTimezoneAbr();
//                showToast(mActivity, strTimezone);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Set Selections
        if (strTimezone != null && strTimezone.length() > 0) {
            for (int i = 0; i < mTimezoneArrayList.size(); i++) {
                if (strTimezone.equals(mTimezoneArrayList.get(i).getTimezoneAbr())) {
                    timezone_sp.setSelection(i);
                }
            }
        }
    }
}

