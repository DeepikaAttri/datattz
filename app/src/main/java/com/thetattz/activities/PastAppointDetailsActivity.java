package com.thetattz.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.fonts.TextViewRegular;
import com.thetattz.fonts.TextViewSemiBold;
import com.thetattz.models.AppointmentsModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class PastAppointDetailsActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = PastAppointDetailsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = PastAppointDetailsActivity.this;

    /**
     * Widgets Main Layout
     */
    @BindView(R.id.rlBackRL)
    RelativeLayout rlBackRL;
    @BindView(R.id.imgProfilePicCIV)
    CircleImageView imgProfilePicCIV;
    @BindView(R.id.txtNameTV)
    TextViewSemiBold txtNameTV;
    @BindView(R.id.txtShopNameTV)
    TextViewSemiBold txtShopNameTV;
    @BindView(R.id.txtStatusTV)
    TextViewRegular txtStatusTV;
    @BindView(R.id.txtMonthFormatTV)
    TextViewSemiBold txtMonthFormatTV;
    @BindView(R.id.txtDayFormatTV)
    TextViewSemiBold txtDayFormatTV;
    @BindView(R.id.txtStartEndTimeTV)
    TextViewRegular txtStartEndTimeTV;
    @BindView(R.id.txtTrasactionIDTV)
    TextViewRegular txtTrasactionIDTV;
    @BindView(R.id.txtPriceTV)
    TextViewSemiBold txtPriceTV;

    /*
     * Initialize...Objects
     * */
    AppointmentsModel mAppointmentsModel;
    String PaymentMethod = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_appoint_details);
        setStatusBar();
        //Butter Knife
        ButterKnife.bind(this);
        //Get Intent Data
        getIntentData();
    }

    /*
     * Get Intent Data
     * */
    private void getIntentData() {
        if (getIntent() != null) {
            mAppointmentsModel = (AppointmentsModel) getIntent().getSerializableExtra(Constants.MODEL);
            /*
             * Getting Appointment Details
             * */
            if (ConnectivityReceiver.isConnected())
                executeGetDetails();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    /*
     * Widgets Click Listners
     * */
    @OnClick(R.id.rlBackRL)
    public void onViewClicked() {
        onBackPressed();
    }

    private void executeGetDetails() {
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        String mTimeZone = timeZone.getID();
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.APPOINTMENTS_DETAILS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            mParams.put("appointment_id", mAppointmentsModel.getAppointment_id());
            mParams.put("appointment_status", "5");
            mParams.put("timezone", mTimeZone);

            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };
        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            /*
             * Set Up Data on Widgets
             * */
            if (response.getString("status").equals("1")) {
                if (!response.isNull("data")) {
                    //Set Data on Widgets
                    JSONObject mDataObj = response.getJSONObject("data");

                    //Get Payment Type
                    if (!mDataObj.isNull("payment_method")) {

                        if (mDataObj.getString("payment_method").equals("1")) {
                            PaymentMethod = "Mobile pay";
                        } else if (mDataObj.getString("payment_method").equals("2")) {
                            PaymentMethod = "Cash";
                        }
                    }
                    //Get Artist Details
                    if (!mDataObj.isNull("artist_detail")) {
                        JSONObject mArtistObj = mDataObj.getJSONObject("artist_detail");
                        if (!mArtistObj.isNull("name"))
                            txtNameTV.setText(CapitalizedFullName(mArtistObj.getString("name")));
                        if (!mArtistObj.isNull("profile_pic") && mArtistObj.getString("profile_pic").contains("http"))
                            Picasso.with(mActivity).load(mArtistObj.getString("profile_pic"))
                                    .placeholder(R.drawable.ic_p_pp)
                                    .error(R.drawable.ic_p_pp)
                                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                                    .into(imgProfilePicCIV);
                        if (!mArtistObj.isNull("city"))
                            txtShopNameTV.setText(mArtistObj.getString("city"));
                        if (!mArtistObj.isNull("start_time"))
                            txtStartEndTimeTV.setText(mArtistObj.getString("start_time"));
                        if (!mArtistObj.isNull("date")) {
                            String[] dateArray = mArtistObj.getString("date").split("-");
                            String strDay = dateArray[0];
                            String strMonthName = gettingMonthNameFromNumber(dateArray[1]);
                            txtMonthFormatTV.setText(strMonthName);
                            txtDayFormatTV.setText(strDay);
                        }
                    }

                    // Get Appointment Details
                    if (!mDataObj.isNull("appointment_detail")) {
                        JSONObject mAppointmentObj = mDataObj.getJSONObject("appointment_detail");
                        if (!mAppointmentObj.isNull("transaction_id"))
                            txtTrasactionIDTV.setText(mAppointmentObj.getString("transaction_id"));
                        if (!mAppointmentObj.isNull("price"))
                            txtPriceTV.setText(mAppointmentObj.getString("price") + "  " + PaymentMethod);
                    }
                }
            } else {
                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }
}
