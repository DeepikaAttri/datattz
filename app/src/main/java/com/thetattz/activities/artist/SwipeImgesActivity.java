package com.thetattz.activities.artist;

import android.app.Activity;
import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import com.thetattz.R;
import com.thetattz.activities.BaseActivity;
import com.thetattz.adapters.DetailsSlidingImageAdapter;
import com.thetattz.models.MyTattozModel;
import com.thetattz.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class SwipeImgesActivity extends BaseActivity {
    /*
     * Initlaize Activity
     * */
    Activity mActivity = SwipeImgesActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = SwipeImgesActivity.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.mIndicatorCI)
    CircleIndicator mIndicatorCI;
    @BindView(R.id.mViewPagerVP)
    ViewPager mViewPagerVP;

    /*
    * Initialize Objects
    * */
    DetailsSlidingImageAdapter mDetailsSlidingImageAdapter;
    ArrayList<MyTattozModel> mMyTattozArrayList = new ArrayList();
    int mPosition = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_imges);
        setStatusBar();
        //ButterKnife
        ButterKnife.bind(this);
        //Get Intent Data
        getIntentData();
        //Set Data on Widgets
        setDataOnWidgets();
    }

    /*
     * Get Intent Data
     * */
    private void getIntentData() {
        if (getIntent() != null) {
            mMyTattozArrayList = (ArrayList<MyTattozModel>) getIntent().getSerializableExtra(Constants.LIST);
            mPosition = getIntent().getIntExtra(Constants.POSITION, 0);
        }
    }


    /*
     * Set Data on Widgets
     * */
    private void setDataOnWidgets() {
        //Set Images
        mDetailsSlidingImageAdapter = new DetailsSlidingImageAdapter(mActivity, this, mMyTattozArrayList);
        mViewPagerVP.setAdapter(mDetailsSlidingImageAdapter);
        mViewPagerVP.setCurrentItem(mPosition);
        mIndicatorCI.setViewPager(mViewPagerVP);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }




}
