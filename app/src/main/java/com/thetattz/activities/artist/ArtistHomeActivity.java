package com.thetattz.activities.artist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.thetattz.R;
import com.thetattz.activities.BaseActivity;
import com.thetattz.fragments.SettingFragment;
import com.thetattz.fragments.artist.ArtistAppointmentsFragment;
import com.thetattz.fragments.artist.ArtistClientFragment;
import com.thetattz.fragments.artist.ArtistHomeFragment;
import com.thetattz.fragments.artist.ArtistNotificationFragment;
import com.thetattz.utils.TheTattzPrefrences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ArtistHomeActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ArtistHomeActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = ArtistHomeActivity.this;


    /**
     * Widgets Main Layout
     */
    @BindView(R.id.containerFL)
    FrameLayout containerFL;
    @BindView(R.id.imgTab1IV)
    ImageView imgTab1IV;
    @BindView(R.id.tab1LL)
    LinearLayout tab1LL;
    @BindView(R.id.imgTab2IV)
    ImageView imgTab2IV;
    @BindView(R.id.tab2LL)
    LinearLayout tab2LL;
    @BindView(R.id.imgTab3IV)
    ImageView imgTab3IV;
    @BindView(R.id.tab3LL)
    LinearLayout tab3LL;
    @BindView(R.id.imgTab4IV)
    ImageView imgTab4IV;
    @BindView(R.id.tab4LL)
    LinearLayout tab4LL;
    @BindView(R.id.imgTab5IV)
    ImageView imgTab5IV;
    @BindView(R.id.tab5LL)
    LinearLayout tab5LL;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_home);
        setStatusBar();
        //Butter Knife
        ButterKnife.bind(this);
        //By Default Selec TAB 1
        perfromTab1Click();

    }

    @OnClick({R.id.tab1LL, R.id.tab2LL, R.id.tab3LL, R.id.tab4LL, R.id.tab5LL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tab1LL:
                perfromTab1Click();
                break;
            case R.id.tab2LL:
                perfromTab2Click();
                break;
            case R.id.tab3LL:
                perfromTab3Click();
                break;
            case R.id.tab4LL:
                perfromTab4Click();
                break;
            case R.id.tab5LL:
                perfromTab5Click();
                break;
        }
    }

    private void perfromTab1Click() {
        imgTab1IV.setImageResource(R.drawable.ic_tab1_s);
        imgTab2IV.setImageResource(R.drawable.ic_tab2_us);
        imgTab3IV.setImageResource(R.drawable.ic_tab3_us);
        imgTab4IV.setImageResource(R.drawable.ic_tab4);
        imgTab5IV.setImageResource(R.drawable.ic_tab5_us);
        setTheme(R.style.AppThemeWhite);
        switchFragment(mActivity, new ArtistHomeFragment(), false, null);
    }

    public void perfromTab2Click() {
        imgTab1IV.setImageResource(R.drawable.ic_tab1_us);
        imgTab2IV.setImageResource(R.drawable.ic_tab2_s);
        imgTab3IV.setImageResource(R.drawable.ic_tab3_us);
        imgTab4IV.setImageResource(R.drawable.ic_tab4);
        imgTab5IV.setImageResource(R.drawable.ic_tab5_us);
        setTheme(R.style.AppThemeWhite);
        switchFragment(mActivity, new ArtistAppointmentsFragment(), false, null);
    }

    public void perfromTab3Click() {
        imgTab1IV.setImageResource(R.drawable.ic_tab1_us);
        imgTab2IV.setImageResource(R.drawable.ic_tab2_us);
        imgTab3IV.setImageResource(R.drawable.ic_tab3_s);
        imgTab4IV.setImageResource(R.drawable.ic_tab4);
        imgTab5IV.setImageResource(R.drawable.ic_tab5_us);
        setTheme(R.style.AppThemeWhite);
        switchFragment(mActivity, new ArtistClientFragment(), false, null);
    }

    public void perfromTab4Click() {
        imgTab1IV.setImageResource(R.drawable.ic_tab1_us);
        imgTab2IV.setImageResource(R.drawable.ic_tab2_us);
        imgTab3IV.setImageResource(R.drawable.ic_tab3_us);
        imgTab4IV.setImageResource(R.drawable.ic_tab4);
        imgTab5IV.setImageResource(R.drawable.ic_tab5_us);
        setTheme(R.style.AppThemeWhite);
        switchFragment(mActivity, new ArtistNotificationFragment(), false, null);
    }

    public void perfromTab5Click() {
        imgTab1IV.setImageResource(R.drawable.ic_tab1_us);
        imgTab2IV.setImageResource(R.drawable.ic_tab2_us);
        imgTab3IV.setImageResource(R.drawable.ic_tab3_us);
        imgTab4IV.setImageResource(R.drawable.ic_tab4);
        imgTab5IV.setImageResource(R.drawable.ic_tab5_s);
        setTheme(R.style.AppThemeWhite);
        switchFragment(mActivity, new SettingFragment(), false, null);
    }


    /********
     *Replace Fragment In Activity
     **********/
    public void switchFragment(Activity activity, Fragment fragment, boolean addToStack, Bundle bundle) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.containerFL, fragment);
            if (addToStack)
                ft.addToBackStack(null);
            if (bundle != null)
                fragment.setArguments(bundle);
            ft.commitAllowingStateLoss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TheTattzPrefrences.writeInteger(mActivity, TheTattzPrefrences.ARTIST_HOME_SELECTED_TAB, 0);
    }


    // In your activity
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }


}
