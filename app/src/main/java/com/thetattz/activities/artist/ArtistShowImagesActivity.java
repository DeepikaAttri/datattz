package com.thetattz.activities.artist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.thetattz.R;
import com.thetattz.activities.BaseActivity;
import com.thetattz.adapters.ArtistShowImagesAdapter;
import com.thetattz.interfaces.SeeMoreClickInterface;
import com.thetattz.models.MyTattozModel;
import com.thetattz.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ArtistShowImagesActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ArtistShowImagesActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Intance
     */
    Activity mActivity = ArtistShowImagesActivity.this;

    /**
     * Widgets
     */
    @BindView(R.id.myImagesRecyclerViewRV)
    RecyclerView myImagesRecyclerViewRV;
    @BindView(R.id.rlBackRL)
    RelativeLayout rlBackRL;

    ArtistShowImagesAdapter mArtistShowImagesAdapter;

    ArrayList<MyTattozModel> mMyTattozArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_show_images);
        ButterKnife.bind(this);
        setStatusBar();
        /*calling getArrayList*/
        getArrayList();

        //set Photos Adapter
        if (getDeviceSizeType().equals("mobile_bear")) {
            setPhotosAdapter();
        } else {
            setPhotosAdapter7_10();
        }

    }

    /*getting arraylist from previous activity*/
    private void getArrayList() {
        Intent intent = getIntent();
        if (intent != null) {
            mMyTattozArrayList = (ArrayList<MyTattozModel>) getIntent().getSerializableExtra(Constants.LIST);
        }
    }



    SeeMoreClickInterface mSeeMoreClickInterface = new SeeMoreClickInterface() {
        @Override
        public void mClick(MyTattozModel mMyTattozModel, int position) {
            Intent mIntent =new Intent(mActivity, SwipeImgesActivity.class);
            mIntent.putExtra(Constants.LIST,mMyTattozArrayList);
            mIntent.putExtra(Constants.POSITION,position);
            startActivityForResult(mIntent,Constants.REQUEST_CODE);
        }
    };

    /*set photos adapter*/
    private void setPhotosAdapter() {
        if (mMyTattozArrayList != null) {
            myImagesRecyclerViewRV.setHasFixedSize(false);
            mArtistShowImagesAdapter = new ArtistShowImagesAdapter(mActivity, mMyTattozArrayList,mSeeMoreClickInterface);
            RecyclerView.LayoutManager mLayoutManagerC = new GridLayoutManager(mActivity, 2);
            myImagesRecyclerViewRV.setLayoutManager(mLayoutManagerC);
            myImagesRecyclerViewRV.setAdapter(mArtistShowImagesAdapter);
        }
    }

    /*set photo adapter for 7/10*/
    private void setPhotosAdapter7_10() {
        if (mMyTattozArrayList != null) {
            myImagesRecyclerViewRV.setNestedScrollingEnabled(false);
            myImagesRecyclerViewRV.setHasFixedSize(false);
            mArtistShowImagesAdapter = new ArtistShowImagesAdapter(mActivity, mMyTattozArrayList,mSeeMoreClickInterface);
            RecyclerView.LayoutManager mLayoutManagerC = new GridLayoutManager(mActivity, 3);
            myImagesRecyclerViewRV.setLayoutManager(mLayoutManagerC);
            myImagesRecyclerViewRV.setAdapter(mArtistShowImagesAdapter);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null){
            int position = data.getIntExtra(Constants.POSITION,0);
            mMyTattozArrayList.remove(position);
            mArtistShowImagesAdapter.notifyDataSetChanged();
        }
    }

    /*
     * Getting Screen Type: Like mobile phone, 7'' tablet, 10'' tablet
     * */
    public String getDeviceSizeType() {
        String strType = mActivity.getResources().getString(R.string.screen_type);
        return strType;
    }

    /*
     * Error Toast Message
     * */
    public void showToast(Activity mActivity, String strMessage) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        Intent mIntent = new Intent();
        setResult(Constants.REQUEST_CODE_PICTURE,mIntent);
        finish();
    }
    /*
     * Widgets Click Listner
     * */
    @OnClick({R.id.rlBackRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBackRL:
                onBackPressed();
                break;
        }
    }
}
