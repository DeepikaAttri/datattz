package com.thetattz.activities.artist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.BaseActivity;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubscriptionActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SubscriptionActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = SubscriptionActivity.this;

    /**
     * Widgets Main Layout
     */
    @BindView(R.id.rlBackRL)
    RelativeLayout rlBackRL;
    @BindView(R.id.subscriptionStatusTV)
    TextView subscriptionStatusTV;
    @BindView(R.id.subscribeButton)
    TextView subscribeButton;

    Boolean isSubscriptionActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        setStatusBar();
        //ButterKnife
        ButterKnife.bind(this);
        //Get User Details
        performGetUserDetails();
    }

    /*
     * Widgets click listner
     * */
    @OnClick({R.id.rlBackRL, R.id.subscribeButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBackRL:
                onBackPressed();
                break;
            case R.id.subscribeButton:
                performSubscribeClick();
                break;
        }
    }

    /*
     * Getting User Profile Details
     * */
    private void performGetUserDetails() {
        if (ConnectivityReceiver.isConnected())
            executeGetUserDetailsAPI();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    /*
     * Submit contact Us details
     * */
    private void performSubscribeClick() {
        Intent mIntent = new Intent(mActivity, SubscriptionSubmitActivity.class);
        startActivity(mIntent);
    }

    /*Execute Get User Details API*/
    private void executeGetUserDetailsAPI() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.GET_USER_DETAILS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseDetailsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseDetailsResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                JSONObject mDataObject = response.getJSONObject("data");
                //Set Data On Widgets
                if (mDataObject.has("isSubscriptionActive")) {
                    if (!mDataObject.isNull("isSubscriptionActive")) {
                        isSubscriptionActive = mDataObject.getBoolean("isSubscriptionActive");
                    }

                    if (isSubscriptionActive.equals(false)) {
                        subscribeButton.setVisibility(View.VISIBLE);
                        subscriptionStatusTV.setText(R.string.inactive);
                        subscriptionStatusTV.setTextColor(getResources().getColor(R.color.colorTextDark));
                    } else {
                        subscribeButton.setVisibility(View.GONE);
                        subscriptionStatusTV.setText(R.string.active);
                        subscriptionStatusTV.setTextColor(getResources().getColor(R.color.colorGreen));
                    }
                }

            } else {
                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }
}
