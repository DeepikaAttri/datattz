package com.thetattz.activities.artist;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thetattz.R;
import com.thetattz.activities.BaseActivity;
import com.thetattz.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OpenPaYpalLinkActivity extends BaseActivity /*implements AdvancedWebView.Listener*/ {

    /*
     * Initialize Activity
     * */
    Activity mActivity = OpenPaYpalLinkActivity.this;
    /*
     * Get Activity Name
     * */
    String TAG = OpenPaYpalLinkActivity.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.txtTitleTV)
    TextView txtTitleTV;
    @BindView(R.id.webview)
    WebView mWebView;

    /*
     * Initlize Objects
     * */
    String strLinkUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_pa_ypal_link);
        setStatusBar();
        ButterKnife.bind(this);

        // mWebView.setListener(this, this);
        if (getIntent() != null) {
            strLinkUrl = getIntent().getStringExtra(Constants.PAYPAL_LINK);
        }

        mWebView.loadUrl(strLinkUrl);
    }

    @OnClick({R.id.cancelRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancelRL:
                onBackPressed();
                break;
        }
    }

//    @SuppressLint("NewApi")
//    @Override
//    protected void onResume() {
//        super.onResume();
//        mWebView.onResume();
//        // ...
//    }
//
//    @SuppressLint("NewApi")
//    @Override
//    protected void onPause() {
//        mWebView.onPause();
//        // ...
//        super.onPause();
//    }
//
//    @Override
//    protected void onDestroy() {
//        mWebView.onDestroy();
//        // ...
//        super.onDestroy();
//    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        super.onActivityResult(requestCode, resultCode, intent);
//        mWebView.onActivityResult(requestCode, resultCode, intent);
//        // ...
//    }

    @Override
    public void onBackPressed() {
//        if (!mWebView.onBackPressed()) {
//            return;
//        }
        // ...
        if (mWebView.getUrl().contains("https://www.datattz.com/Webservices/PayPalIndentification.php")
                || mWebView.getUrl().contains("http://161.97.132.85/datattz/Webservices/PayPalIndentification.php")) {
            super.onBackPressed();
        }

    }

//    @Override
//    public void onPageStarted(String url, Bitmap favicon) {
//    }
//
//    @Override
//    public void onPageFinished(String url) {
//        if (url.contains("https://www.datattz.com/Webservices/PayPalIndentification.php")){
//            onBackPressed();
//        }
//    }
//
//    @Override
//    public void onPageError(int errorCode, String description, String failingUrl) {
//    }
//
//    @Override
//    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
//    }
//
//    @Override
//    public void onExternalPageRequest(String url) {
//    }
}
