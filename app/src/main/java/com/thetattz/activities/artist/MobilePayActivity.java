package com.thetattz.activities.artist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.BaseActivity;
import com.thetattz.fonts.EditTextBold;
import com.thetattz.fonts.TextViewBold;
import com.thetattz.fonts.TextViewSemiBold;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MobilePayActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = MobilePayActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = MobilePayActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.txtCancelTV)
    TextViewBold txtCancelTV;

    @BindView(R.id.txtEditTV)
    TextView txtEditTV;
    @BindView(R.id.txtSaveTV)
    TextView txtSaveTV;
    @BindView(R.id.editFirstNameET)
    EditTextBold editFirstNameET;
    @BindView(R.id.editLastNameET)
    EditTextBold editLastNameET;
    @BindView(R.id.txtDateOfBirthTV)
    TextViewBold txtDateOfBirthTV;
    @BindView(R.id.editStreetAddressET)
    EditTextBold editStreetAddressET;
    @BindView(R.id.editCityET)
    EditTextBold editCityET;
    @BindView(R.id.editStateRegionET)
    EditTextBold editStateRegionET;
    @BindView(R.id.txtCountryNameTV)
    TextViewBold txtCountryNameTV;
    @BindView(R.id.editPersonalIdentificationET)
    EditTextBold editPersonalIdentificationET;
    @BindView(R.id.editBanckAccountHolderNameET)
    EditTextBold editBanckAccountHolderNameET;
    @BindView(R.id.editBankRoutingNumberET)
    EditTextBold editBankRoutingNumberET;
    @BindView(R.id.editBankAccountNumberET)
    EditTextBold editBankAccountNumberET;
    @BindView(R.id.paypalEmail)
    EditTextBold paypalEmail;
    @BindView(R.id.verifyTV)
    TextViewSemiBold verifyTV;

    String strPayPalEmail = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_pay);
        setStatusBar();
        //butter knife
        ButterKnife.bind(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        perfromGetDetails();
    }

    /*
     * Widgets Click Listner
     * */
    @OnClick({R.id.txtCancelTV, R.id.txtDateOfBirthTV, R.id.txtEditTV, R.id.txtSaveTV, R.id.verifyTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtCancelTV:
                onBackPressed();
                break;
            case R.id.txtDateOfBirthTV:
                perfromDateOfBirth();
                break;
            case R.id.txtEditTV:
                performEditClick();
                break;
            case R.id.txtSaveTV:
                perfromSaveClick();
                break;
            case R.id.verifyTV:
                performVerifyPaypalClick();
                break;
        }
    }

    private void performVerifyPaypalClick() {
        if (ConnectivityReceiver.isConnected())
            excutePaypalLogInApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    //paypal log in
    private void excutePaypalLogInApi() {

        showProgressDialog(mActivity);

        String strURL = Constants.PAYPAL_LOGIN + "?user_id=" + getUserID();

        Log.e(TAG, "*****Response****" + strURL);

        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dismissProgressDialog();
                Log.e(TAG, "*****Response****" + response);
                try {
                    parseJsonResponse(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "*****Response****" + error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        TheTattzApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void parseJsonResponse(String response) {
        try {

            JSONObject mJsonObjectt = new JSONObject(response);

            if (mJsonObjectt.getString("status").equals("1")) {

                String login_url = mJsonObjectt.getString("login_url");

                Intent mIntent = new Intent(mActivity, OpenPaYpalLinkActivity.class);
                mIntent.putExtra(Constants.PAYPAL_LINK, login_url);
                startActivity(mIntent);

            } else {
                showToast(mActivity, mJsonObjectt.getString("message"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void enable() {
        editFirstNameET.setEnabled(true);
        editFirstNameET.setSelection(editFirstNameET.getText().toString().trim().length());

        editLastNameET.setEnabled(true);
        editLastNameET.setSelection(editLastNameET.getText().toString().trim().length());

        txtDateOfBirthTV.setEnabled(true);

        editStreetAddressET.setEnabled(true);
        editStreetAddressET.setSelection(editStreetAddressET.getText().toString().trim().length());

        editCityET.setEnabled(true);
        editCityET.setSelection(editCityET.getText().toString().trim().length());

        editStateRegionET.setEnabled(true);
        editStateRegionET.setSelection(editStateRegionET.getText().toString().trim().length());

        editPersonalIdentificationET.setEnabled(true);
        editPersonalIdentificationET.setSelection(editPersonalIdentificationET.getText().toString().trim().length());

        editBanckAccountHolderNameET.setEnabled(true);
        editBanckAccountHolderNameET.setSelection(editBanckAccountHolderNameET.getText().toString().trim().length());

        editBankRoutingNumberET.setEnabled(true);
        editBankRoutingNumberET.setSelection(editBankRoutingNumberET.getText().toString().trim().length());

        editBankAccountNumberET.setEnabled(true);
        editBankAccountNumberET.setSelection(editBankAccountNumberET.getText().toString().trim().length());

        txtEditTV.setVisibility(View.GONE);
        txtSaveTV.setVisibility(View.VISIBLE);
    }

    private void disable() {
        editFirstNameET.setEnabled(false);
        editLastNameET.setEnabled(false);
        txtDateOfBirthTV.setEnabled(false);
        editStreetAddressET.setEnabled(false);
        editCityET.setEnabled(false);
        editStateRegionET.setEnabled(false);
        editPersonalIdentificationET.setEnabled(false);
        editBanckAccountHolderNameET.setEnabled(false);
        editBankRoutingNumberET.setEnabled(false);
        editBankAccountNumberET.setEnabled(false);
        txtEditTV.setVisibility(View.VISIBLE);
        txtSaveTV.setVisibility(View.GONE);
    }

    private void performEditClick() {
        enable();
    }

    private void perfromGetDetails() {
        if (ConnectivityReceiver.isConnected()) {
            executeGetDetailsApi(getAccountDetailsID());
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    private void perfromSaveClick() {
        if (isValidate()) {
            if (ConnectivityReceiver.isConnected()) {
                executeUpdateApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }

    private void perfromDateOfBirth() {
        showDatePickerDialog(mActivity, txtDateOfBirthTV);
    }

    /*
     * Validations
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editFirstNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name));
            flag = false;
        } else if (editLastNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name));
            flag = false;
        } else if (txtDateOfBirthTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_dateof_birth));
            flag = false;
        } else if (editStreetAddressET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_street_address));
            flag = false;
        } else if (editCityET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_city));
            flag = false;
        } else if (editStateRegionET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_state_region));
            flag = false;
        } else if (editBanckAccountHolderNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_bank_account_holder_name));
            flag = false;
        } else if (editBankRoutingNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_routing_number));
            flag = false;
        } else if (editBankAccountNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_account_nuber));
            flag = false;
        } else if (editPersonalIdentificationET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_identification_nuber));
            flag = false;
        } else if (paypalEmail.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_paypal_account));
            flag = false;
        }else if (!isValidEmaillId(paypalEmail.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_paypal_account));
            flag = false;
        } else if(strPayPalEmail == null && strPayPalEmail.equals("")){
            showAlertDialog(mActivity, getString(R.string.please_verify_paypal_account));
            flag = false;
        }
        return flag;
    }

    private void executeGetDetailsApi(String strAccountDetailId) {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.ARTIST_GET_ACCOUNT_DETAIL;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("account_detail_id", strAccountDetailId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "**Api**" + mParams);
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseGetAccountDetailsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }
        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseGetAccountDetailsResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                JSONObject mDataObject = response.getJSONObject("data");
                editFirstNameET.setText(mDataObject.getString("firstname"));
                editLastNameET.setText(mDataObject.getString("lastname"));
                txtDateOfBirthTV.setText(mDataObject.getString("dob"));
                editStreetAddressET.setText(mDataObject.getString("street_address"));
                editCityET.setText(mDataObject.getString("city"));
                editStateRegionET.setText(mDataObject.getString("state"));
                txtCountryNameTV.setText(mDataObject.getString("country"));
                editPersonalIdentificationET.setText(mDataObject.getString("personal_identification_number"));
                editBanckAccountHolderNameET.setText(mDataObject.getString("bank_acc_holder_name"));
                editBankAccountNumberET.setText(mDataObject.getString("bank_acc_number"));
                editBankRoutingNumberET.setText(mDataObject.getString("bank_routing_number"));

                if (!mDataObject.isNull("paypal_id") && !mDataObject.getString("paypal_id").equals("")) {
                    strPayPalEmail = mDataObject.getString("paypal_id");
                    paypalEmail.setText(mDataObject.getString("paypal_id"));
                    paypalEmail.setEnabled(false);
                    verifyTV.setText(getString(R.string.verified));
                    verifyTV.setTextColor(getResources().getColor(R.color.colorGreen));
                    verifyTV.setEnabled(false);
                } else {
                    paypalEmail.setText("");
                    paypalEmail.setEnabled(true);
                    verifyTV.setText(getString(R.string.verify));
                    verifyTV.setTextColor(getResources().getColor(R.color.colorBlack));
                    verifyTV.setEnabled(true);
                }

                disable();

            } else {
                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    /*
     * Update Account Details
     * */
    private void executeUpdateApi() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.ARTIST_UPDATE_ACCOUNT_DETAIL;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            mParams.put("account_detail_id", getAccountDetailsID());
            mParams.put("firstname", editFirstNameET.getText().toString().trim());
            mParams.put("lastname", editLastNameET.getText().toString().trim());
            mParams.put("dob", txtDateOfBirthTV.getText().toString().trim());
            mParams.put("street_address", editStreetAddressET.getText().toString().trim());
            mParams.put("city", editCityET.getText().toString().trim());
            mParams.put("state", editStateRegionET.getText().toString().trim());
            mParams.put("country", txtCountryNameTV.getText().toString().trim());
            mParams.put("personal_identification_number", editPersonalIdentificationET.getText().toString().trim());
            mParams.put("bank_acc_holder_name", editBanckAccountHolderNameET.getText().toString().trim());
            mParams.put("bank_routing_number", editBankRoutingNumberET.getText().toString().trim());
            mParams.put("bank_acc_number", editBankAccountNumberET.getText().toString().trim());

            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }


        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseUpdateAccountDetailsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseUpdateAccountDetailsResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                showToast(mActivity, response.getString("message"));
                disable();
            } else {
                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

}
