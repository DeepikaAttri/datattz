package com.thetattz.activities.artist;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.BaseActivity;
import com.thetattz.activities.contacts.MyContactsActivity;
import com.thetattz.adapters.ServicesAdapter;
import com.thetattz.adapters.artist.ArtistTimeSlotsAdapter;
import com.thetattz.fonts.EditTextRegular;
import com.thetattz.fonts.TextViewBold;
import com.thetattz.fonts.TextViewSemiBold;
import com.thetattz.interfaces.ArtistSelectTimeSlotInterface;
import com.thetattz.interfaces.SelectServiceInterface;
import com.thetattz.models.FreeTimeSlotsModel;
import com.thetattz.models.ServicesModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateAppointmentsActivity extends BaseActivity {
    public final int REQUEST_PERMISSION = 384;
    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeContacts = Manifest.permission.WRITE_CONTACTS;
    public String readContacts = Manifest.permission.READ_CONTACTS;
    /**
     * Getting the Current Class Name
     */
    String TAG = CreateAppointmentsActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Intance
     */
    Activity mActivity = CreateAppointmentsActivity.this;
    /*
     * Widgets
     * */
    @BindView(R.id.txtCancelTV)
    TextViewBold txtCancelTV;
    @BindView(R.id.txtSaveTV)
    TextViewBold txtSaveTV;
    @BindView(R.id.rbClientRB)
    RadioButton rbClientRB;
    @BindView(R.id.rbOtherRB)
    RadioButton rbOtherRB;
    @BindView(R.id.mAppointmentTypeRG)
    RadioGroup mAppointmentTypeRG;
    @BindView(R.id.mSelectTimeRG)
    RadioGroup mSelectTimeRG;
    @BindView(R.id.mCalenderCV)
    CalendarView mCalenderCV;
    @BindView(R.id.artistNotFountTV)
    TextViewSemiBold artistNotFountTV;
    @BindView(R.id.mTimeSlotsRecyclerView)
    RecyclerView mTimeSlotsRecyclerView;
    @BindView(R.id.txtAllContactsTV)
    TextViewBold txtAllContactsTV;
    @BindView(R.id.editNameET)
    EditTextRegular editNameET;
    @BindView(R.id.editNumberET)
    EditTextRegular editNumberET;
    @BindView(R.id.servicesRV)
    RecyclerView servicesRV;
    @BindView(R.id.servicesNotFountTV)
    TextViewSemiBold servicesNotFountTV;
    @BindView(R.id.headerLL)
    LinearLayout headerLL;
    @BindView(R.id.txtAvailabilityTV)
    TextView txtAvailabilityTV;
    @BindView(R.id.availableRB)
    RadioButton availableRB;
    @BindView(R.id.customRB)
    RadioButton customRB;
    @BindView(R.id.artistAvailibilityLL)
    LinearLayout artistAvailibilityLL;
    @BindView(R.id.artistCustomLL)
    LinearLayout artistCustomLL;

    @BindView(R.id.showtimeTV)
    TextView showtimeTV;

    @BindView(R.id.layoutCustomTimeRL)
    RelativeLayout layoutCustomTimeRL;


    /*
     * Initialize... Objects
     * */
    ServicesModel mServicesModel = new ServicesModel();
    FreeTimeSlotsModel mFreeTimeSlotsModel = new FreeTimeSlotsModel();
    ArrayList<ServicesModel> mServicesArrayList = new ArrayList<ServicesModel>();

    ServicesAdapter mServicesAdapter;
    ArtistTimeSlotsAdapter mTimeSlotsAdapter;


    String strAppointmentType = "client";
    String strTimeType = "available";
    String strCalendarDate = "";
    String strDayDate = "";
    String artistShopStartTime = "";
    String artistShopEndTime = "";

    SelectServiceInterface mSelectServiceInterface = new SelectServiceInterface() {
        @Override
        public void getSelectedService(ServicesModel mServicesModel11) {
            Log.e(TAG, "**Service Name**" + mServicesModel11.getService_name());
            mServicesModel = mServicesModel11;
        }
    };

    /*
     * Time Slots Click Listner
     * */
    ArtistSelectTimeSlotInterface mSelectTimeSlotInterface = new ArtistSelectTimeSlotInterface() {
        @Override
        public void getTimeSlot(FreeTimeSlotsModel mFreeTimeSlotsModel11) {
            mFreeTimeSlotsModel = mFreeTimeSlotsModel11;
            Log.e(TAG, "**Time Slot**" + mFreeTimeSlotsModel.getStartTime());
        }
    };

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_appointments);
        ButterKnife.bind(this);

        setStatusBar();
        //Set Time type

        selectTimeType();
        //Set Calenderview

        setCalendar();
        //Set Appointment Type

        setAppointmentTypeLayout();
        //Get Services Data

        executeGetServices();
    }

    private void selectTimeType() {
        Typeface font = Typeface.createFromAsset(getAssets(), "ProximaNova-Semibold.ttf");
        availableRB.setTypeface(font);
        customRB.setTypeface(font);

        mSelectTimeRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.availableRB:
                        //Get Appointments time api:
                        txtAvailabilityTV.setText(getResources().getString(R.string.based_on_your_availability_hours_and_booking_preferences));
                        artistAvailibilityLL.setVisibility(View.VISIBLE);
                        artistCustomLL.setVisibility(View.GONE);
                        strTimeType = "available";
                        Log.e(TAG, "**Available**" + strTimeType);
                        break;
                    case R.id.customRB:
                        txtAvailabilityTV.setText(getResources().getString(R.string.override_on_your_availability_hours_and_booking_preferences));
                        artistAvailibilityLL.setVisibility(View.GONE);
                        artistCustomLL.setVisibility(View.VISIBLE);
                        strTimeType = "custom";
                        Log.e(TAG, "**Custom**" + strTimeType);
                        break;
                }
            }
        });
    }

    private void perfromDurationClick() {
        createAppointmentBottomSheetDialog(showtimeTV);
    }


    private void setCalendar() {
        mCalenderCV.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                strCalendarDate = dayOfMonth + "-" + (month + 1) + "-" + year;
                if (IsCalenderAvailable(strCalendarDate)) {
                    Log.e(TAG, "**Calendar Date Listner**" + strCalendarDate);
                    strDayDate = convertTimeToDayName(strCalendarDate);
                    Log.e(TAG, "**Day Date Listner**" + strDayDate);
                    if (strTimeType.equals("available")) {
                        //Get Appointments time api:
                        executeGetTimeSlots();
                    }
                } else {
                    strCalendarDate = "";
                    showAlertDialog(mActivity, getString(R.string.you_can_not));
                    mCalenderCV.setDate(System.currentTimeMillis(), false, true);
                }
            }
        });
    }

    private void setAppointmentTypeLayout() {
        Typeface font = Typeface.createFromAsset(getAssets(), "ProximaNova-Semibold.ttf");
        rbClientRB.setTypeface(font);
        rbOtherRB.setTypeface(font);

        mAppointmentTypeRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.rbClientRB:
                        strAppointmentType = "client";
                        Log.e(TAG, "**Mobile Pay**" + strAppointmentType);
                        break;
                    case R.id.rbOtherRB:
                        strAppointmentType = "other";
                        Log.e(TAG, "**Shop Pay**" + strAppointmentType);
                        break;
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.txtCancelTV, R.id.txtSaveTV, R.id.txtAllContactsTV, R.id.layoutCustomTimeRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtCancelTV:
                onBackPressed();
                break;
            case R.id.txtSaveTV:
                perfromSaveClick();
                break;
            case R.id.txtAllContactsTV:
                perfromAllContactsClick();
                break;
            case R.id.layoutCustomTimeRL:
                perfromDurationClick();
                break;
        }
    }

    private void perfromSaveClick() {
        if (isValidate()) {
            if (ConnectivityReceiver.isConnected())
                executeBookAppointment();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void perfromAllContactsClick() {
        if (checkPermission()) {
            Intent mIntent = new Intent(mActivity, MyContactsActivity.class);
            startActivityForResult(mIntent, 999);
        } else {
            requestPermission();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            editNameET.setText(data.getStringExtra(Constants.CONTACT_NAME));
            editNumberET.setText(data.getStringExtra(Constants.CONTACT_NUMBER));

            editNameET.setSelection(editNameET.getText().toString().trim().length());
            editNumberET.setSelection(editNumberET.getText().toString().trim().length());
        }
    }

    private boolean isValidate() {
        boolean flag = true;
        if (strAppointmentType.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_select_appointment_type));
            flag = false;
        } else if (strTimeType.equals("available") && mFreeTimeSlotsModel.getStartTime() == null) {
            showAlertDialog(mActivity, getString(R.string.please_select_appoint));
            flag = false;
        } else if (strTimeType.equals("custom") && showtimeTV.getText().toString().trim().length() == 0) {
            showAlertDialog(mActivity, getString(R.string.please_enter_custom_time));
            flag = false;
        } else if (strTimeType.equals("custom") && strCalendarDate.length() == 0) {
            showAlertDialog(mActivity, getString(R.string.please_select_approintment_day));
            flag = false;
        } else if (editNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_name));
            flag = false;
        } else if (editNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_number));
            flag = false;
        } else if (editNumberET.getText().toString().trim().length() < 10) {
            showAlertDialog(mActivity, getString(R.string.please_enter_number_valid));
            flag = false;
        } else if (mServicesModel.getService_name().length() == 0) {
            showAlertDialog(mActivity, getString(R.string.please_select_service));
            flag = false;
        } else if (mServicesModel.getService_name().length() == 0) {
            showAlertDialog(mActivity, getString(R.string.please_select_service));
            flag = false;
        }
        return flag;
    }

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) READ CONTACTS PERMISSION
     * 2) WRITE CONTACTS PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeContacts);
        int read = ContextCompat.checkSelfPermission(mActivity, readContacts);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestPermission() {
        requestPermissions(new String[]{writeContacts, readContacts}, REQUEST_PERMISSION);
        ActivityCompat.requestPermissions(mActivity, new String[]{writeContacts, readContacts}, REQUEST_PERMISSION);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //Set Contacts Adapter
                    perfromAllContactsClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
        }
    }

    /*
     * Get Artist Services
     * */
    private void executeGetServices() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.GET_SERVICES_LISTING;
        Log.e(TAG, "**Api**" + mApiUrl);

        JSONObject mParams = new JSONObject();
        try {
            mParams.put("artist_id", getUserID());
            Log.e(TAG, "**Params**" + "Service== " + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }


        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + "Service== " + response.toString());
                        parseServicesResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);

    }

    private void parseServicesResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                if (!response.isNull("data")) {
                    JSONArray mJsonArray = response.getJSONArray("data");
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mDataObj = mJsonArray.getJSONObject(i);
                        ServicesModel mModel = new ServicesModel();
                        if (!mDataObj.isNull("id"))
                            mModel.setId(mDataObj.getString("id"));
                        if (!mDataObj.isNull("artist_id"))
                            mModel.setArtist_id(mDataObj.getString("artist_id"));
                        if (!mDataObj.isNull("service_name"))
                            mModel.setService_name(mDataObj.getString("service_name"));
                        if (!mDataObj.isNull("description"))
                            mModel.setDescription(mDataObj.getString("description"));
                        if (!mDataObj.isNull("duration"))
                            mModel.setDuration(mDataObj.getString("duration"));
                        if (!mDataObj.isNull("price"))
                            mModel.setPrice(mDataObj.getString("price"));
                        if (!mDataObj.isNull("enable"))
                            mModel.setEnable(mDataObj.getString("enable"));

                        mServicesArrayList.add(mModel);
                    }
                }


                if (mServicesArrayList.size() > 0) {
                    servicesNotFountTV.setVisibility(View.GONE);
                    servicesRV.setVisibility(View.VISIBLE);
                    //Set Up Recycler View Data
                    setAdapter();
                } else {
                    servicesNotFountTV.setVisibility(View.VISIBLE);
                    servicesRV.setVisibility(View.GONE);
                }

            } else {
                showToast(mActivity, response.getString("message"));
            }

        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    private void setAdapter() {
        if (mServicesArrayList.size() > 0)
            servicesRV.setBackgroundResource(R.drawable.bg_box_rectangle);
        mServicesAdapter = new ServicesAdapter(mActivity, mServicesArrayList, mSelectServiceInterface);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity);
        servicesRV.setLayoutManager(mLayoutManagerC);
        servicesRV.setAdapter(mServicesAdapter);
    }

    /*
     * Execute Getting Time Slots:
     * */
    private void executeGetTimeSlots() {
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        String mTimeZone = timeZone.getID();

        showProgressDialog(mActivity);
        final String mApiUrl = Constants.ARTIST_BOOKED_TIME_SLOT;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            mParams.put("artist_id", getUserID());
            mParams.put("date", strCalendarDate);
            mParams.put("day", strDayDate);
            mParams.put("timezone", mTimeZone);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseTimeSlotsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                        Log.e(TAG, "**ERROR**" + error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseTimeSlotsResponse(JSONObject response) {
        ArrayList<FreeTimeSlotsModel> mAlreadyBookedArrayList = new ArrayList<FreeTimeSlotsModel>();
        try {
            if (response.getString("status").equals("1")) {
                if (!response.isNull("start_time"))
                    artistShopStartTime = response.getString("start_time");
                if (!response.isNull("end_time"))
                    artistShopEndTime = response.getString("end_time");

                /*Need to Implement New things*/
                if (!response.isNull("free_slots")) {
                    JSONArray mJsonArray = response.getJSONArray("free_slots");
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mDataObj = mJsonArray.getJSONObject(i);
                        FreeTimeSlotsModel mModel = new FreeTimeSlotsModel();
                        if (!mDataObj.isNull("start_time"))
                            mModel.setStartTime(mDataObj.getString("start_time"));
                        if (!mDataObj.isNull("end_time"))
                            mModel.setEndTime("end_time");

                        mAlreadyBookedArrayList.add(mModel);
                    }
                }


                //Working and with Appointments
                artistNotFountTV.setVisibility(View.GONE);
                artistAvailibilityLL.setVisibility(View.VISIBLE);
                mTimeSlotsRecyclerView.setVisibility(View.VISIBLE);
                //Set Up Recycler View Data
                setTimeSlotAdapter(mAlreadyBookedArrayList);
            } else {
                //Working and with Appointments
                artistNotFountTV.setVisibility(View.VISIBLE);
                artistAvailibilityLL.setVisibility(View.VISIBLE);
                mTimeSlotsRecyclerView.setVisibility(View.GONE);
                setTimeSlotAdapter(mAlreadyBookedArrayList);
//                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }


    /*
     * Set Time Slot Adapter
     * */
    private void setTimeSlotAdapter(ArrayList<FreeTimeSlotsModel> mTimeSlotsArrayList) {
        mTimeSlotsAdapter = new ArtistTimeSlotsAdapter(mActivity, mTimeSlotsArrayList, mSelectTimeSlotInterface);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        mTimeSlotsRecyclerView.setLayoutManager(mLayoutManagerC);
        mTimeSlotsRecyclerView.setAdapter(mTimeSlotsAdapter);
    }


    /*
     * Execute Book Appointment Api
     * */
    private void executeBookAppointment() {
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        String mTimeZone = timeZone.getID();
        String strTime = "";
        if (strTimeType.equals("available")) {
            strTime = mFreeTimeSlotsModel.getStartTime();
        } else {
            strTime = showtimeTV.getText().toString();
        }

        long strTimeInMillies = convertTimeStringToLongAppointment(strCalendarDate + " " + strTime);

        Log.e(TAG, "executeBookAppointment: " + strTimeInMillies);


        showProgressDialog(mActivity);
        final String mApiUrl = Constants.BOOK_APPOINTMENTS_BY_ARTIST;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            mParams.put("artist_id", getUserID());
            mParams.put("username", editNameET.getText().toString().trim());
            mParams.put("phone_no", editNumberET.getText().toString().trim());
            mParams.put("service_id", mServicesModel.getId());
            mParams.put("date", strCalendarDate);
            mParams.put("time", "" + strTimeInMillies);

            if (strTimeType.equals("available")) {
                mParams.put("time_type", "true");
            } else {
                mParams.put("time_type", "false");
            }

            mParams.put("appointment_type", strAppointmentType);
            mParams.put("timezone", mTimeZone);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseBookAppointResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseBookAppointResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                showToast(mActivity, response.getString("message"));
                finish();
            } else {
                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }
}
