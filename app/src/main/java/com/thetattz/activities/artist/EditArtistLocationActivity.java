package com.thetattz.activities.artist;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.BaseActivity;
import com.thetattz.fonts.TextViewBold;
import com.thetattz.models.ArtistHoursModel;
import com.thetattz.models.EditArtistModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditArtistLocationActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = EditArtistLocationActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = EditArtistLocationActivity.this;


    /**
     * Widgets
     */
    @BindView(R.id.txtCancelTV)
    TextViewBold txtCancelTV;
    @BindView(R.id.txtSaveTV)
    TextViewBold txtSaveTV;
    @BindView(R.id.editArtistShopNameET)
    EditText editArtistShopNameET;
    @BindView(R.id.editStreetAddressET)
    EditText editStreetAddressET;
    @BindView(R.id.editBuildingFloorEtcET)
    EditText editBuildingFloorEtcET;
    @BindView(R.id.editCityET)
    EditText editCityET;

    @BindView(R.id.txtDay1TV)
    TextView txtDay1TV;
    @BindView(R.id.txtDay1StartEndTimeTV)
    TextView txtDay1StartEndTimeTV;

    @BindView(R.id.txtDay2TV)
    TextView txtDay2TV;
    @BindView(R.id.txtDay2StartEndTimeTV)
    TextView txtDay2StartEndTimeTV;

    @BindView(R.id.txtDay3TV)
    TextView txtDay3TV;
    @BindView(R.id.txtDay3StartEndTimeTV)
    TextView txtDay3StartEndTimeTV;

    @BindView(R.id.txtDay4TV)
    TextView txtDay4TV;
    @BindView(R.id.txtDay4StartEndTimeTV)
    TextView txtDay4StartEndTimeTV;

    @BindView(R.id.txtDay5TV)
    TextView txtDay5TV;
    @BindView(R.id.txtDay5StartEndTimeTV)
    TextView txtDay5StartEndTimeTV;

    @BindView(R.id.txtDay6TV)
    TextView txtDay6TV;
    @BindView(R.id.txtDay6StartEndTimeTV)
    TextView txtDay6StartEndTimeTV;

    @BindView(R.id.txtDay7TV)
    TextView txtDay7TV;
    @BindView(R.id.txtDay7StartEndTimeTV)
    TextView txtDay7StartEndTimeTV;

    /*
     * Break Days Hours
     * */
    @BindView(R.id.txtDay1BreakTV)
    TextView txtDay1BreakTV;
    @BindView(R.id.txtDay1StartEndTimeBreakTV)
    TextView txtDay1StartEndTimeBreakTV;

    @BindView(R.id.txtDay2BreakTV)
    TextView txtDay2BreakTV;
    @BindView(R.id.txtDay2StartEndTimeBreakTV)
    TextView txtDay2StartEndTimeBreakTV;

    @BindView(R.id.txtDay3BreakTV)
    TextView txtDay3BreakTV;
    @BindView(R.id.txtDay3StartEndTimeBreakTV)
    TextView txtDay3StartEndTimeBreakTV;

    @BindView(R.id.txtDay4BreakTV)
    TextView txtDay4BreakTV;
    @BindView(R.id.txtDay4StartEndTimeBreakTV)
    TextView txtDay4StartEndTimeBreakTV;

    @BindView(R.id.txtDay5BreakTV)
    TextView txtDay5BreakTV;
    @BindView(R.id.txtDay5StartEndTimeBreakTV)
    TextView txtDay5StartEndTimeBreakTV;

    @BindView(R.id.txtDay6BreakTV)
    TextView txtDay6BreakTV;
    @BindView(R.id.txtDay6StartEndTimeBreakTV)
    TextView txtDay6StartEndTimeBreakTV;

    @BindView(R.id.txtDay7BreakTV)
    TextView txtDay7BreakTV;
    @BindView(R.id.txtDay7StartEndTimeBreakTV)
    TextView txtDay7StartEndTimeBreakTV;


    /*
     * Initialize Objects...
     * */
    EditArtistModel mModel = new EditArtistModel();


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_artist_location);
        setStatusBar();
        ButterKnife.bind(this);
        //Get Artist Details
        getArtistDetails();
    }


    @OnClick({R.id.txtCancelTV, R.id.txtSaveTV, R.id.txtDay1StartEndTimeTV, R.id.txtDay2StartEndTimeTV,
            R.id.txtDay3StartEndTimeTV, R.id.txtDay4StartEndTimeTV, R.id.txtDay5StartEndTimeTV,
            R.id.txtDay6StartEndTimeTV, R.id.txtDay7StartEndTimeTV, R.id.txtDay1StartEndTimeBreakTV,
            R.id.txtDay2StartEndTimeBreakTV, R.id.txtDay3StartEndTimeBreakTV, R.id.txtDay4StartEndTimeBreakTV, R.id.txtDay5StartEndTimeBreakTV,
            R.id.txtDay6StartEndTimeBreakTV, R.id.txtDay7StartEndTimeBreakTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtCancelTV:
                performCancelClick();
                break;
            case R.id.txtSaveTV:
                performSaveClick();
                break;
            case R.id.txtDay1StartEndTimeTV:
                createBottomSheetDialog(mActivity, txtDay1StartEndTimeTV);
                break;
            case R.id.txtDay2StartEndTimeTV:
                createBottomSheetDialog(mActivity, txtDay2StartEndTimeTV);
                break;
            case R.id.txtDay3StartEndTimeTV:
                createBottomSheetDialog(mActivity, txtDay3StartEndTimeTV);
                break;
            case R.id.txtDay4StartEndTimeTV:
                createBottomSheetDialog(mActivity, txtDay4StartEndTimeTV);
                break;
            case R.id.txtDay5StartEndTimeTV:
                createBottomSheetDialog(mActivity, txtDay5StartEndTimeTV);
                break;
            case R.id.txtDay6StartEndTimeTV:
                createBottomSheetDialog(mActivity, txtDay6StartEndTimeTV);
                break;
            case R.id.txtDay7StartEndTimeTV:
                createBottomSheetDialog(mActivity, txtDay7StartEndTimeTV);
                break;
            /*break hours*/
            case R.id.txtDay1StartEndTimeBreakTV:
                createBottomSheetDialog(mActivity, txtDay1StartEndTimeBreakTV);
                break;
            case R.id.txtDay2StartEndTimeBreakTV:
                createBottomSheetDialog(mActivity, txtDay2StartEndTimeBreakTV);
                break;
            case R.id.txtDay3StartEndTimeBreakTV:
                createBottomSheetDialog(mActivity, txtDay3StartEndTimeBreakTV);
                break;
            case R.id.txtDay4StartEndTimeBreakTV:
                createBottomSheetDialog(mActivity, txtDay4StartEndTimeBreakTV);
                break;
            case R.id.txtDay5StartEndTimeBreakTV:
                createBottomSheetDialog(mActivity, txtDay5StartEndTimeBreakTV);
                break;
            case R.id.txtDay6StartEndTimeBreakTV:
                createBottomSheetDialog(mActivity, txtDay6StartEndTimeBreakTV);
                break;
            case R.id.txtDay7StartEndTimeBreakTV:
                createBottomSheetDialog(mActivity, txtDay7StartEndTimeBreakTV);
                break;

        }
    }

    public void performCancelClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = mActivity.getFragmentManager();
        fm.popBackStack();
        super.onBackPressed();
    }

    /*
     *
     * Execute Update Location :
     * */
    private void performSaveClick() {
        if (isValidate()) {
            if (ConnectivityReceiver.isConnected())
                executeUpdateArtistLocationAPI();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }

    }

    /*
     * Set up validations for Sign Up fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editArtistShopNameET.getText().toString().trim().equals("") &&
                editBuildingFloorEtcET.getText().toString().trim().equals("") &&
                editCityET.getText().toString().trim().equals("") &&
                editStreetAddressET.getText().toString().trim().equals("") &&
                txtDay1StartEndTimeTV.getText().toString().trim().equals("") &&
                txtDay2StartEndTimeTV.getText().toString().trim().equals("") &&
                txtDay3StartEndTimeTV.getText().toString().trim().equals("") &&
                txtDay4StartEndTimeTV.getText().toString().trim().equals("") &&
                txtDay5StartEndTimeTV.getText().toString().trim().equals("") &&
                txtDay6StartEndTimeTV.getText().toString().trim().equals("") &&
                txtDay7StartEndTimeTV.getText().toString().trim().equals("") &&
                txtDay1StartEndTimeBreakTV.getText().toString().trim().equals("") &&
                txtDay2StartEndTimeBreakTV.getText().toString().trim().equals("") &&
                txtDay3StartEndTimeBreakTV.getText().toString().trim().equals("") &&
                txtDay4StartEndTimeBreakTV.getText().toString().trim().equals("") &&
                txtDay5StartEndTimeBreakTV.getText().toString().trim().equals("") &&
                txtDay6StartEndTimeBreakTV.getText().toString().trim().equals("") &&
                txtDay7StartEndTimeBreakTV.getText().toString().trim().equals("")) {
            flag = false;
        }
        return flag;
    }



    /*
     * Get Artist Details
     * */

    private void getArtistDetails() {
        if (ConnectivityReceiver.isConnected())
            executeArtistDetailsAPI();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void executeArtistDetailsAPI() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.GET_LOCATION_AND_HOURS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());

                        parseDetailsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseDetailsResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                JSONObject mDataObject = response.getJSONObject("data");
                if (!mDataObject.isNull("shopname"))
                    mModel.setShopname(mDataObject.getString("shopname"));
                if (!mDataObject.isNull("building_name"))
                    mModel.setBuilding_name(mDataObject.getString("building_name"));
                if (!mDataObject.isNull("street_address"))
                    mModel.setStreet_address(mDataObject.getString("street_address"));
                if (!mDataObject.isNull("city"))
                    mModel.setCity(mDataObject.getString("city"));


                if (!mDataObject.isNull("hours")) {

                    ArrayList<ArtistHoursModel> mArrayList = new ArrayList<>();

                    JSONArray mJsonArray = mDataObject.getJSONArray("hours");
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mObj = mJsonArray.getJSONObject(i);
                        ArtistHoursModel mHoursModel = new ArtistHoursModel();
                        if (!mObj.isNull("id"))
                            mHoursModel.setId(mObj.getString("id"));
                        if (!mObj.isNull("artist_id"))
                            mHoursModel.setArtist_id(mObj.getString("artist_id"));
                        if (!mObj.isNull("day"))
                            mHoursModel.setDay(mObj.getString("day"));
                        if (!mObj.isNull("start_time"))
                            mHoursModel.setStart_time(mObj.getString("start_time"));
                        if (!mObj.isNull("end_time"))
                            mHoursModel.setEnd_time(mObj.getString("end_time"));
                        if (!mObj.isNull("status"))
                            mHoursModel.setStatus(mObj.getString("status"));
                        if (!mObj.isNull("break_end_time"))
                            mHoursModel.setBreak_end_time(mObj.getString("break_end_time"));
                        if (!mObj.isNull("break_start_time"))
                            mHoursModel.setBreak_start_time(mObj.getString("break_start_time"));

                        mArrayList.add(mHoursModel);
                    }

                    mModel.setHoursArrayList(mArrayList);
                }

                /*
                 * Set Data On Widgets
                 * */
                setDataOnWidgets(mModel);

            } else {
                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }

    private void setDataOnWidgets(EditArtistModel mModel) {
        editArtistShopNameET.setText(mModel.getShopname());
        editArtistShopNameET.setSelection(editArtistShopNameET.getText().toString().trim().length());

        editStreetAddressET.setText(mModel.getStreet_address());
        editStreetAddressET.setSelection(editStreetAddressET.getText().toString().trim().length());

        editBuildingFloorEtcET.setText(mModel.getBuilding_name());
        editBuildingFloorEtcET.setSelection(editBuildingFloorEtcET.getText().toString().trim().length());

        editCityET.setText(mModel.getCity());
        editCityET.setSelection(editCityET.getText().toString().trim().length());

        /*
         * Set Hours
         * */
        for (int i = 0; i < mModel.getHoursArrayList().size(); i++) {
            ArtistHoursModel mArtistHoursModel = mModel.getHoursArrayList().get(i);

            if (mArtistHoursModel.getDay().equals(getString(R.string.sunday))) {
                txtDay1TV.setText(mArtistHoursModel.getDay());
                if (mArtistHoursModel.getStart_time().length() > 0)
                    txtDay1StartEndTimeTV.setText(mArtistHoursModel.getStart_time() + "-" + mArtistHoursModel.getEnd_time());
                if (mArtistHoursModel.getBreak_start_time().length() > 0)
                    txtDay1StartEndTimeBreakTV.setText(mArtistHoursModel.getBreak_start_time() + "-" + mArtistHoursModel.getBreak_end_time());
            }

            if (mArtistHoursModel.getDay().equals(getString(R.string.monday))) {
                txtDay2TV.setText(mArtistHoursModel.getDay());
                if (mArtistHoursModel.getStart_time().length() > 0)
                    txtDay2StartEndTimeTV.setText(mArtistHoursModel.getStart_time() + "-" + mArtistHoursModel.getEnd_time());
                if (mArtistHoursModel.getBreak_start_time().length() > 0)
                    txtDay2StartEndTimeBreakTV.setText(mArtistHoursModel.getBreak_start_time() + "-" + mArtistHoursModel.getBreak_end_time());
            }
            if (mArtistHoursModel.getDay().equals(getString(R.string.tuesday))) {
                txtDay3TV.setText(mArtistHoursModel.getDay());
                if (mArtistHoursModel.getStart_time().length() > 0)
                    txtDay3StartEndTimeTV.setText(mArtistHoursModel.getStart_time() + "-" + mArtistHoursModel.getEnd_time());
                if (mArtistHoursModel.getBreak_start_time().length() > 0)
                    txtDay3StartEndTimeBreakTV.setText(mArtistHoursModel.getBreak_start_time() + "-" + mArtistHoursModel.getBreak_end_time());
            }
            if (mArtistHoursModel.getDay().equals(getString(R.string.wednesday))) {
                txtDay4TV.setText(mArtistHoursModel.getDay());
                if (mArtistHoursModel.getStart_time().length() > 0)
                    txtDay4StartEndTimeTV.setText(mArtistHoursModel.getStart_time() + "-" + mArtistHoursModel.getEnd_time());
                if (mArtistHoursModel.getBreak_start_time().length() > 0)
                    txtDay4StartEndTimeBreakTV.setText(mArtistHoursModel.getBreak_start_time() + "-" + mArtistHoursModel.getBreak_end_time());
            }
            if (mArtistHoursModel.getDay().equals(getString(R.string.thursday))) {
                txtDay5TV.setText(mArtistHoursModel.getDay());
                if (mArtistHoursModel.getStart_time().length() > 0)
                    txtDay5StartEndTimeTV.setText(mArtistHoursModel.getStart_time() + "-" + mArtistHoursModel.getEnd_time());
                if (mArtistHoursModel.getBreak_start_time().length() > 0)
                    txtDay5StartEndTimeBreakTV.setText(mArtistHoursModel.getBreak_start_time() + "-" + mArtistHoursModel.getBreak_end_time());
            }
            if (mArtistHoursModel.getDay().equals(getString(R.string.friday))) {
                txtDay6TV.setText(mArtistHoursModel.getDay());
                if (mArtistHoursModel.getStart_time().length() > 0)
                    txtDay6StartEndTimeTV.setText(mArtistHoursModel.getStart_time() + "-" + mArtistHoursModel.getEnd_time());
                if (mArtistHoursModel.getBreak_start_time().length() > 0)
                    txtDay6StartEndTimeBreakTV.setText(mArtistHoursModel.getBreak_start_time() + "-" + mArtistHoursModel.getBreak_end_time());
            }
            if (mArtistHoursModel.getDay().equals(getString(R.string.saturday))) {
                txtDay7TV.setText(mArtistHoursModel.getDay());
                if (mArtistHoursModel.getStart_time().length() > 0)
                    txtDay7StartEndTimeTV.setText(mArtistHoursModel.getStart_time() + "-" + mArtistHoursModel.getEnd_time());
                if (mArtistHoursModel.getBreak_start_time().length() > 0)
                    txtDay7StartEndTimeBreakTV.setText(mArtistHoursModel.getBreak_start_time() + "-" + mArtistHoursModel.getBreak_end_time());
            }

        }

    }


    private void executeUpdateArtistLocationAPI() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.EDIT_LOCATION_AND_HOURS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            mParams.put("shopname", editArtistShopNameET.getText().toString().trim());
            mParams.put("street_address", editStreetAddressET.getText().toString().trim());
            mParams.put("building_name", editBuildingFloorEtcET.getText().toString().trim());
            mParams.put("city", editCityET.getText().toString().trim());

            JSONArray mHoursArray = new JSONArray();

            if (txtDay1TV.getText().toString().trim().length() > 0) {
                JSONObject mJSJsonObject = new JSONObject();
                mJSJsonObject.put("day", txtDay1TV.getText().toString().trim());
                mJSJsonObject.put("start_time", getStartTime(txtDay1StartEndTimeTV));
                mJSJsonObject.put("end_time", getEndTime(txtDay1StartEndTimeTV));
                mJSJsonObject.put("break_start_time", getStartTime(txtDay1StartEndTimeBreakTV));
                mJSJsonObject.put("break_end_time", getEndTime(txtDay1StartEndTimeBreakTV));
                mJSJsonObject.put("id", getDayID(getString(R.string.sunday)));
                mHoursArray.put(mJSJsonObject);
            }
            if (txtDay2TV.getText().toString().trim().length() > 0) {
                JSONObject mJSJsonObject = new JSONObject();
                mJSJsonObject.put("day", txtDay2TV.getText().toString().trim());
                mJSJsonObject.put("start_time", getStartTime(txtDay2StartEndTimeTV));
                mJSJsonObject.put("end_time", getEndTime(txtDay2StartEndTimeTV));
                mJSJsonObject.put("break_start_time", getStartTime(txtDay2StartEndTimeBreakTV));
                mJSJsonObject.put("break_end_time", getEndTime(txtDay2StartEndTimeBreakTV));
                mJSJsonObject.put("id", getDayID(getString(R.string.monday)));
                mHoursArray.put(mJSJsonObject);
            }
            if (txtDay3TV.getText().toString().trim().length() > 0) {
                JSONObject mJSJsonObject = new JSONObject();
                mJSJsonObject.put("day", txtDay3TV.getText().toString().trim());
                mJSJsonObject.put("start_time", getStartTime(txtDay3StartEndTimeTV));
                mJSJsonObject.put("end_time", getEndTime(txtDay3StartEndTimeTV));
                mJSJsonObject.put("break_start_time", getStartTime(txtDay3StartEndTimeBreakTV));
                mJSJsonObject.put("break_end_time", getEndTime(txtDay3StartEndTimeBreakTV));
                mJSJsonObject.put("id", getDayID(getString(R.string.tuesday)));
                mHoursArray.put(mJSJsonObject);
            }
            if (txtDay4TV.getText().toString().trim().length() > 0) {
                JSONObject mJSJsonObject = new JSONObject();
                mJSJsonObject.put("day", txtDay4TV.getText().toString().trim());
                mJSJsonObject.put("start_time", getStartTime(txtDay4StartEndTimeTV));
                mJSJsonObject.put("end_time", getEndTime(txtDay4StartEndTimeTV));
                mJSJsonObject.put("break_start_time", getStartTime(txtDay4StartEndTimeBreakTV));
                mJSJsonObject.put("break_end_time", getEndTime(txtDay4StartEndTimeBreakTV));
                mJSJsonObject.put("id", getDayID(getString(R.string.wednesday)));
                mHoursArray.put(mJSJsonObject);
            }
            if (txtDay5TV.getText().toString().trim().length() > 0) {
                JSONObject mJSJsonObject = new JSONObject();
                mJSJsonObject.put("day", txtDay5TV.getText().toString().trim());
                mJSJsonObject.put("start_time", getStartTime(txtDay5StartEndTimeTV));
                mJSJsonObject.put("end_time", getEndTime(txtDay5StartEndTimeTV));
                mJSJsonObject.put("break_start_time", getStartTime(txtDay5StartEndTimeBreakTV));
                mJSJsonObject.put("break_end_time", getEndTime(txtDay5StartEndTimeBreakTV));
                mJSJsonObject.put("id", getDayID(getString(R.string.thursday)));
                mHoursArray.put(mJSJsonObject);
            }
            if (txtDay6TV.getText().toString().trim().length() > 0) {
                JSONObject mJSJsonObject = new JSONObject();
                mJSJsonObject.put("day", txtDay6TV.getText().toString().trim());
                mJSJsonObject.put("start_time", getStartTime(txtDay6StartEndTimeTV));
                mJSJsonObject.put("end_time", getEndTime(txtDay6StartEndTimeTV));
                mJSJsonObject.put("break_start_time", getStartTime(txtDay6StartEndTimeBreakTV));
                mJSJsonObject.put("break_end_time", getEndTime(txtDay6StartEndTimeBreakTV));
                mJSJsonObject.put("id", getDayID(getString(R.string.friday)));
                mHoursArray.put(mJSJsonObject);
            }
            if (txtDay7TV.getText().toString().trim().length() > 0) {
                JSONObject mJSJsonObject = new JSONObject();
                mJSJsonObject.put("day", txtDay7TV.getText().toString().trim());
                mJSJsonObject.put("start_time", getStartTime(txtDay7StartEndTimeTV));
                mJSJsonObject.put("end_time", getEndTime(txtDay7StartEndTimeTV));
                mJSJsonObject.put("break_start_time", getStartTime(txtDay7StartEndTimeBreakTV));
                mJSJsonObject.put("break_end_time", getEndTime(txtDay7StartEndTimeBreakTV));
                mJSJsonObject.put("id", getDayID(getString(R.string.saturday)));
                mHoursArray.put(mJSJsonObject);
            }


            mParams.put("hours", mHoursArray);

            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());

                        parseUpdateResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseUpdateResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                showToast(mActivity, response.getString("message"));
                Intent mIntent = new Intent();
                setResult(656, mIntent);
                onBackPressed();
            } else {
                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }


    private String getStartTime(TextView mTextView) {
        String[] mArray = {};
        if (mTextView.getText().toString().trim().length() > 2) {
            mArray = mTextView.getText().toString().trim().split("-");
            return mArray[0];
        }

        return "";
    }

    private String getEndTime(TextView mTextView) {
        String[] mArray = {};
        if (mTextView.getText().toString().trim().length() > 2) {
            mArray = mTextView.getText().toString().trim().split("-");
            return mArray[1];
        }

        return "";
    }

    private String getDayID(String mDay) {
        String strID = "";
        for (int i = 0; i < mModel.getHoursArrayList().size(); i++) {
            if (mModel.getHoursArrayList().get(i).getDay().equals(mDay)) {
                strID = mModel.getHoursArrayList().get(i).getId();
            }
        }

        return strID;
    }

}
