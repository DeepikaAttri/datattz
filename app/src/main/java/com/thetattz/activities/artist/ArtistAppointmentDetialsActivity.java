package com.thetattz.activities.artist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.BaseActivity;
import com.thetattz.chat.ChatActivity;
import com.thetattz.models.AppointmentsModel;
import com.thetattz.models.ArtistAppointmentDetailsModel;
import com.thetattz.models.ArtistAppointmentDoneModel;
import com.thetattz.models.ArtistConfirmCancelModel;
import com.thetattz.models.RoomIdModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;
import com.thetattz.views.StuButton;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ArtistAppointmentDetialsActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ArtistAppointmentDetialsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = ArtistAppointmentDetialsActivity.this;


    /**
     * Widgets Main Layout
     */
    @BindView(R.id.rlBackRL)
    RelativeLayout rlBackRL;
    @BindView(R.id.imgProfilePicCIV)
    CircleImageView imgProfilePicCIV;
    @BindView(R.id.txtNameTV)
    TextView txtNameTV;
    @BindView(R.id.txtStatusTV)
    TextView txtStatusTV;
    @BindView(R.id.txtDayFormatTV)
    TextView txtDayFormatTV;
    @BindView(R.id.txtMonthFormatTV)
    TextView txtMonthFormatTV;
    @BindView(R.id.txtStartEndTimeTV)
    TextView txtStartEndTimeTV;
    @BindView(R.id.layoutConfirmLL)
    LinearLayout layoutConfirmLL;
    @BindView(R.id.layoutCancelLL)
    LinearLayout layoutCancelLL;
    @BindView(R.id.txtFullDateFormatTV)
    TextView txtFullDateFormatTV;
    @BindView(R.id.txtTimeSlotFormatTV)
    TextView txtTimeSlotFormatTV;
    @BindView(R.id.txtAddressTV)
    TextView txtAddressTV;
    @BindView(R.id.txtPriceTV)
    TextView txtPriceTV;
    @BindView(R.id.btnSendMessageB)
    Button btnSendMessageB;
    @BindView(R.id.btnSwipeCompleteBTN)
    StuButton btnSwipeCompleteBTN;
    @BindView(R.id.mViewV)
    View mViewV;
    @BindView(R.id.txtUserNameFirstLetterTV)
    TextView txtUserNameFirstLetterTV;
    @BindView(R.id.addressLl)
    LinearLayout addressLl;

    /*
     * Initialize...Objects
     * */
    ArtistAppointmentDetailsModel mModel;
    AppointmentsModel mAppointmentsModel;
    String strAppointmentType = "";
    String strPaymentMethod = "";
    String PaymentMethod = "";

    //For Pending Appointment Status is : 1
    //For confirmed Appointment Status is : 2
    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_appointment_detials);
        setStatusBar();
        ButterKnife.bind(this);
        getIntentData();
        setCompleteAppointment();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            mAppointmentsModel = (AppointmentsModel) getIntent().getSerializableExtra(Constants.MODEL);
            strAppointmentType = getIntent().getStringExtra(Constants.APPOINTMENT_TYPE);
            getDetialsData();
        }
    }


    /*
     * Set Complete Functionalitly
     * */
    private void setCompleteAppointment() {
        btnSwipeCompleteBTN.setOnUnlockListener(new StuButton.OnUnlockListener() {
            @Override
            public void onUnlock() {
                getCompleteAppointment();
            }
        });
    }

    private void getCompleteAppointment() {
        if (isValidate()) {
            if (ConnectivityReceiver.isConnected())
                executeCompleteAppointmentApi();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    private boolean isValidate() {
        boolean flag = true;
        if (!IsAppointmentComplete(mAppointmentsModel.getDate())) {
            showAlertDialog(mActivity, getString(R.string.you_cant_complete));
            btnSwipeCompleteBTN.reset();
            flag = false;
        }
        return flag;
    }

    /*
     * Widget Click Listener
     * */
    @OnClick({R.id.rlBackRL, R.id.layoutConfirmLL, R.id.layoutCancelLL, R.id.btnSendMessageB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBackRL:
                onBackPressed();
                break;
            case R.id.layoutConfirmLL:
                performConfirmClick();
                break;
            case R.id.layoutCancelLL:
                performCancelClick();
                break;
            case R.id.btnSendMessageB:
                performSendMessageClick();
                break;
        }
    }

    private void performSendMessageClick() {
        if (ConnectivityReceiver.isConnected()) {
            executeChatRoomID();
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }

    }

    private void performCancelClick() {
        if (ConnectivityReceiver.isConnected())
            executeStatusConfirmCancelApi("3", "artist");
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void performConfirmClick() {
        if (ConnectivityReceiver.isConnected())
            executeStatusConfirmCancelApi("2", "artist");
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }


    private void getDetialsData() {
        if (ConnectivityReceiver.isConnected())
            executeApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));

    }

    private void executeApi() {
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        String mTimeZone = timeZone.getID();
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.APPOINTMENT_DETATILS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("timezone", mTimeZone);
            mParams.put("appointment_id", mAppointmentsModel.getAppointment_id());
            if (strAppointmentType.equals(getString(R.string.type_pending)))
                mParams.put("appointment_status", "1");
            else if (strAppointmentType.equals(getString(R.string.type_confirm)))
                mParams.put("appointment_status", "2");

            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        try {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.serializeNulls();
                            Gson gson = gsonBuilder.create();
                            mModel = gson.fromJson(response.toString(), ArtistAppointmentDetailsModel.class);
                            strPaymentMethod = mModel.getData().getPayment_method();
                            if (mModel.getStatus() == 1) {
                                setDataOnWidgets(mModel);
                            } else {
                                showToast(mActivity, mModel.getMessage());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "**Error**" + e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }
        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void setDataOnWidgets(ArtistAppointmentDetailsModel mModel) {
        if (mAppointmentsModel.getCreated_by().equals("client")) {
            btnSendMessageB.setVisibility(View.VISIBLE);
            layoutConfirmLL.setVisibility(View.VISIBLE);
            mViewV.setVisibility(View.VISIBLE);
            imgProfilePicCIV.setVisibility(View.VISIBLE);
            txtUserNameFirstLetterTV.setVisibility(View.GONE);
            addressLl.setVisibility(View.VISIBLE);
        } else {
            addressLl.setVisibility(View.GONE);
            layoutConfirmLL.setVisibility(View.GONE);
            btnSendMessageB.setVisibility(View.GONE);
            mViewV.setVisibility(View.GONE);
            imgProfilePicCIV.setVisibility(View.GONE);
            txtUserNameFirstLetterTV.setVisibility(View.VISIBLE);
        }

        //Profile Pic
        if (mModel.getData().getUser_detail().getProfile_pic() != null && mModel.getData().getUser_detail().getProfile_pic().contains("http"))
            Picasso.with(mActivity).load(mModel.getData().getUser_detail().getProfile_pic())
                    .placeholder(R.drawable.ic_p_pp)
                    .error(R.drawable.ic_p_pp)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgProfilePicCIV);
        else
            imgProfilePicCIV.setImageResource(R.drawable.ic_p_pp);

        txtNameTV.setText(CapitalizedFullName(mAppointmentsModel.getName()));
        txtUserNameFirstLetterTV.setText(mAppointmentsModel.getName());

        if (strAppointmentType.equals(getString(R.string.type_pending))) {
            txtStatusTV.setText(getString(R.string.type_pending));
            layoutConfirmLL.setVisibility(View.VISIBLE);
            addressLl.setVisibility(View.GONE);
            mViewV.setVisibility(View.VISIBLE);
        } else if (strAppointmentType.equals(getString(R.string.type_confirm))) {
            txtStatusTV.setText(getString(R.string.type_confirm));
            layoutConfirmLL.setVisibility(View.GONE);
            addressLl.setVisibility(View.VISIBLE);
            mViewV.setVisibility(View.GONE);
        }
        txtDayFormatTV.setText(convertTimeToDayNumber(mAppointmentsModel.getDate()));
        txtMonthFormatTV.setText(convertTimeToMonthName3Digit(mAppointmentsModel.getDate()));
        txtStartEndTimeTV.setText(mModel.getData().getAppointment_detail().getStart_time());
        txtFullDateFormatTV.setText(mModel.getData().getAppointment_detail().getDate());
        txtTimeSlotFormatTV.setText(mModel.getData().getAppointment_detail().getStart_time() + " - " + mModel.getData().getAppointment_detail().getEnd_time()
                + "(" + mModel.getData().getAppointment_detail().getTimezoneAbr() + ")");
        //Set Address
        StringBuilder mBuilder = new StringBuilder();
        if (mModel.getData().getAppointment_detail().getShopname() != null && mModel.getData().getAppointment_detail().getShopname().length() > 0) {
            mBuilder.append(mModel.getData().getAppointment_detail().getShopname());
        }
        if (mModel.getData().getAppointment_detail().getBuilding_name() != null && mModel.getData().getAppointment_detail().getBuilding_name().length() > 0) {
            mBuilder.append(", " + mModel.getData().getAppointment_detail().getBuilding_name());
        }
        if (mModel.getData().getAppointment_detail().getStreet_address() != null && mModel.getData().getAppointment_detail().getStreet_address().length() > 0) {
            mBuilder.append(", " + mModel.getData().getAppointment_detail().getStreet_address());
        }
        if (mModel.getData().getAppointment_detail().getCity() != null && mModel.getData().getAppointment_detail().getCity().length() > 0) {
            mBuilder.append(", " + mModel.getData().getAppointment_detail().getCity());
        }
        String strAddress = mBuilder.toString();
        if (strAddress != null && strAddress.length() > 0) {
            txtAddressTV.setText(strAddress);
        } else {
            txtAddressTV.setText(getString(R.string.not_specified));
        }

        if (mModel.getData().getPayment_method().equals("1")) {
            PaymentMethod = "Mobile pay";
        } else if (mModel.getData().getPayment_method().equals("2")) {
            PaymentMethod = "Cash";
        }

        txtPriceTV.setText(mModel.getData().getAppointment_detail().getPrice() + "  " + PaymentMethod);
    }

    /*
     * Call Confirm Status Api
     * */
    private void executeStatusConfirmCancelApi(final String status, String role) {
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        String mTimeZone = timeZone.getID();

        showProgressDialog(mActivity);
        final String mApiUrl = Constants.UPDATE_APPOINTMENT_STATUS_BY_ARTIST;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("appointment_id", mAppointmentsModel.getAppointment_id());
            mParams.put("appointment_status", status);
            mParams.put("role", role);
            mParams.put("timezone", mTimeZone);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        try {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.serializeNulls();
                            Gson gson = gsonBuilder.create();
                            ArtistConfirmCancelModel mModel = gson.fromJson(response.toString(), ArtistConfirmCancelModel.class);
                            if (mModel.getStatus() == 1) {
                                //Set Data:
//                                showSuccessAlertDialog(mActivity, mModel.getMessage());

                                if (status.equals("2")) {
                                    showSuccessAlertDialog(mActivity, getString(R.string.appointment_confirmed));
                                } else if (status.equals("3")) {
                                    showSuccessAlertDialog(mActivity, getString(R.string.appointment_cancelled));
                                }

                            } else {
                                showToast(mActivity, mModel.getMessage());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "**Error**" + e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };
        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }


    /*
     *
     * Execute Complete Appointment
     * */
    private void executeCompleteAppointmentApi() {
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        String mTimeZone = timeZone.getID();

        showProgressDialog(mActivity);
        final String mApiUrl = Constants.APPOINTMENT_DONE_BY_ARTIST;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("appointment_id", mAppointmentsModel.getAppointment_id());
            mParams.put("payment_method", strPaymentMethod);
            mParams.put("timezone", mTimeZone);

            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        try {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.serializeNulls();
                            Gson gson = gsonBuilder.create();
                            ArtistAppointmentDoneModel mModel = gson.fromJson(response.toString(), ArtistAppointmentDoneModel.class);
                            if (mModel.getStatus() == 1) {
                                //Set Data:
                                showSuccessAlertDialog(mActivity, mModel.getMessage());
                                finish();
                            } else {
                                showToast(mActivity, mModel.getMessage());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "**Error**" + e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }


    /*
     * Private Room ID:
     *
     * */
    private void executeChatRoomID() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.GET_MESSAGE_ROOM_ID;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("request_id", getUserID());
            mParams.put("user_id", mModel.getData().getUser_detail().getUser_id());

            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        try {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.serializeNulls();
                            Gson gson = gsonBuilder.create();
                            RoomIdModel mRoomIdModel = gson.fromJson(response.toString(), RoomIdModel.class);
                            if (mRoomIdModel.getStatus() == 1) {
                                mModel.setmAppointmentID(mAppointmentsModel.getAppointment_id());
                                mModel.setmRoomId(mRoomIdModel.getRoomId());

                                Intent mIntent = new Intent(mActivity, ChatActivity.class);
                                mIntent.putExtra(Constants.MODEL, mModel);
                                mIntent.putExtra(Constants.TYPES, getString(R.string.type_artist));
                                startActivity(mIntent);
                            } else {
                                showToast(mActivity, mModel.getMessage());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "**Error**" + e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }


}
