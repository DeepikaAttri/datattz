package com.thetattz.activities.artist;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.BaseActivity;
import com.thetattz.fonts.EditTextBold;
import com.thetattz.fonts.TextViewBold;
import com.thetattz.fonts.TextViewSemiBold;
import com.thetattz.models.StateSpinnerModel;
import com.thetattz.models.UserDetailsModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;
import com.thetattz.volley.AppHelper;
import com.thetattz.volley.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddArtistPaymentActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = AddArtistPaymentActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = AddArtistPaymentActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.txtCancelTV)
    TextViewBold txtCancelTV;
    @BindView(R.id.txtSaveTV)
    TextViewBold txtSaveTV;
    @BindView(R.id.editFirstNameET)
    EditTextBold editFirstNameET;
    @BindView(R.id.editLastNameET)
    EditTextBold editLastNameET;
    @BindView(R.id.txtDateOfBirthTV)
    TextViewBold txtDateOfBirthTV;
    @BindView(R.id.editStreetAddressET)
    EditTextBold editStreetAddressET;
    @BindView(R.id.editCityET)
    EditTextBold editCityET;
    @BindView(R.id.editStateRegionET)
    EditTextBold editStateRegionET;
    @BindView(R.id.txtCountryNameTV)
    TextViewBold txtCountryNameTV;
    @BindView(R.id.editPersonalIdentificationET)
    EditTextBold editPersonalIdentificationET;
    @BindView(R.id.editBanckAccountHolderNameET)
    EditTextBold editBanckAccountHolderNameET;
    @BindView(R.id.editBankRoutingNumberET)
    EditTextBold editBankRoutingNumberET;
    @BindView(R.id.editBankAccountNumberET)
    EditTextBold editBankAccountNumberET;
    @BindView(R.id.paypalEmail)
    EditTextBold paypalEmail;
    @BindView(R.id.verifyTV)
    TextViewSemiBold verifyTV;

    @BindView(R.id.txtPhoneNumberET)
    EditTextBold txtPhoneNumberET;
    @BindView(R.id.editLine1ET)
    EditTextBold editLine1ET;
    @BindView(R.id.editLine2ET)
    EditTextBold editLine2ET;
    @BindView(R.id.editPostalCodeET)
    EditTextBold editPostalCodeET;

    @BindView(R.id.uploadFrontImageTV)
    TextViewBold uploadFrontImageTV;
    @BindView(R.id.fileFrontImageTV)
    TextViewBold fileFrontImageTV;

    @BindView(R.id.uploadBackImageTV)
    TextViewBold uploadBackImageTV;
    @BindView(R.id.fileBackImageTV)
    TextViewBold fileBackImageTV;

    @BindView(R.id.StateRegionSpinner)
    Spinner StateRegionSpinner;

    Typeface font;
    String selectedState = "", ssn_last4 = "";
    private String strPayPalEmail = "";

    String StrImageType = "", StrFrontFileId = "", StrBackFileId = "";

    public final int REQUEST_CAMERA = 384;
    public final int REQUEST_GALLERY = 348;

    Bitmap mBitmapFrontImage, mBitmapLastImage;

    private ArrayList<StateSpinnerModel> StateModelArrayList = new ArrayList<>();

    int front_status = 0, back_status = 0;
    Handler handler = new Handler();

    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_payment);
        setStatusBar();
        //Butter Knife
        ButterKnife.bind(this);

        executeStatesApi();

        txtPhoneNumberET.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(txtPhoneNumberET);
        txtPhoneNumberET.addTextChangedListener(addLineNumberFormatter);

        txtCancelTV.setEnabled(true);
        txtSaveTV.setEnabled(true);
        uploadFrontImageTV.setEnabled(true);
        uploadBackImageTV.setEnabled(true);
    }


    public class UsPhoneNumberFormatter implements TextWatcher {
        private EditText etMobile;

        public UsPhoneNumberFormatter(EditText edt) {
            etMobile = edt;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            String text = etMobile.getText().toString();
            int textlength = etMobile.getText().length();

            if (text.endsWith(" "))
                return;

            if (textlength == 1) {
                if (!text.contains("(")) {
                    etMobile.setText(new StringBuilder(text).insert(text.length() - 1, "(").toString());
                    etMobile.setSelection(etMobile.getText().length());
                }
            } else if (textlength == 5) {
                if (!text.contains(")")) {
                    etMobile.setText(new StringBuilder(text).insert(text.length() - 1, ")").toString());
                    etMobile.setSelection(etMobile.getText().length());
                }
            } else if (textlength == 6) {
                if (!text.contains(" ")) {
                    etMobile.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                    etMobile.setSelection(etMobile.getText().length());
                }
            } else if (textlength == 10) {
                if (!text.contains("-")) {
                    etMobile.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                    etMobile.setSelection(etMobile.getText().length());
                }
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

//        getUserDetailsApi();
    }

    /*
     * Execute Get User Details Api
     * */
    private void getUserDetailsApi() {
        if (ConnectivityReceiver.isConnected())
            executeProfileApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void executeProfileApi() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.GET_USER_DETAILS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        try {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.serializeNulls();
                            Gson gson = gsonBuilder.create();
                            UserDetailsModel mModel = gson.fromJson(response.toString(), UserDetailsModel.class);
                            if (mModel.getStatus() == 1) {
                                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.ACCOUNT_DETAIL_ID, mModel.getData().getAccount_detail_id());
                                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.PAYPAL_VERIFICATION, mModel.getData().getPaypal_verification());
                                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.PAYPAL_EMAIL_ID, mModel.getData().getPaypal_id());

                                checkPayPalVerification();

                            } else {
                                showToast(mActivity, mModel.getMessage());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "**Error**" + e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void checkPayPalVerification() {
        if (!getPaypalEmail().equals("")) {
            paypalEmail.setText(getPaypalEmail());
            paypalEmail.setEnabled(false);
            verifyTV.setText(getString(R.string.verified));
            verifyTV.setTextColor(getResources().getColor(R.color.colorGreen));
            verifyTV.setEnabled(false);
        } else {
            paypalEmail.setText("");
            paypalEmail.setEnabled(true);
            verifyTV.setText(getString(R.string.verify));
            verifyTV.setTextColor(getResources().getColor(R.color.colorBlack));
            verifyTV.setEnabled(true);
        }
    }

    /*
     * Widgets Click Listner
     * */
    @OnClick({R.id.txtCancelTV, R.id.txtDateOfBirthTV, R.id.txtSaveTV, R.id.verifyTV, R.id.uploadFrontImageTV,
            R.id.uploadBackImageTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtCancelTV:
                onBackPressed();
                break;
            case R.id.txtDateOfBirthTV:
                showDatePickerDialog(mActivity, txtDateOfBirthTV);
                break;
            case R.id.txtSaveTV:
                perfromSaveClick();
                break;
            case R.id.verifyTV:
                performVerifyPaypalClick();
                break;
            case R.id.uploadFrontImageTV:
                performFrontUploadImageClick();
                break;

            case R.id.uploadBackImageTV:
                performBackUploadImageClick();
                break;
        }
    }

    private void performBackUploadImageClick() {
        StrImageType = "back";
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private void performFrontUploadImageClick() {
        StrImageType = "front";
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
        }
    }


    /**
     * Start pick image activity with chooser.
     */
    public void onSelectImageClick() {
        CropImage.startPickImageActivity(this);
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
//                .setAspectRatio(200, 100)
                .setMultiTouchEnabled(false)
                .start(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                try {

                    //disable all buttons
                    txtCancelTV.setEnabled(false);
                    txtSaveTV.setEnabled(false);
                    uploadFrontImageTV.setEnabled(false);
                    uploadBackImageTV.setEnabled(false);

                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());

                    if (StrImageType.equals("front")) {
                        mBitmapFrontImage = BitmapFactory.decodeStream(imageStream);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                while (front_status < 100) {

                                    front_status += 5;

                                    try {
                                        Thread.sleep(300);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {

                                            fileFrontImageTV.setText("Uploading " + String.valueOf(front_status) + ".00%");

                                            if (front_status == 100) {
                                                executeUploadDocumentApi("front", mBitmapFrontImage);
                                            }
                                        }
                                    });
                                }
                            }
                        }).start();

                    } else if (StrImageType.equals("back")) {
                        mBitmapLastImage = BitmapFactory.decodeStream(imageStream);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                while (back_status < 100) {

                                    back_status += 5;

                                    try {
                                        Thread.sleep(300);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {

                                            fileBackImageTV.setText("Uploading " + String.valueOf(back_status) + ".00%");

                                            if (back_status == 100) {
                                                executeUploadDocumentApi("back", mBitmapLastImage);
                                            }
                                        }
                                    });
                                }
                            }
                        }).start();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(mActivity, "Cropping failed: " + result.getError());
            }
        }


    }

    private void performVerifyPaypalClick() {
//        if (isValidate()) {
        if (ConnectivityReceiver.isConnected()) {
            excutePaypalLogInApi();
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }
//        }
    }

    //paypal log in
    private void excutePaypalLogInApi() {

        showProgressDialog(mActivity);

        String strURL = Constants.PAYPAL_LOGIN + "?user_id=" + getUserID();

        Log.e(TAG, "*****Response****" + strURL);

        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dismissProgressDialog();
                Log.e(TAG, "*****Response****" + response);
                try {
                    parseJsonResponse(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "*****Response****" + error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        TheTattzApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void parseJsonResponse(String response) {
        try {

            JSONObject mJsonObjectt = new JSONObject(response);

            if (mJsonObjectt.getString("status").equals("1")) {

                String login_url = mJsonObjectt.getString("login_url");

                Intent mIntent = new Intent(mActivity, OpenPaYpalLinkActivity.class);
                mIntent.putExtra(Constants.PAYPAL_LINK, login_url);
                startActivity(mIntent);

            } else {
                showToast(mActivity, mJsonObjectt.getString("message"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void perfromSaveClick() {
        if (isValidate()) {
            if (ConnectivityReceiver.isConnected()) {
                executeCreateArtistStripeAccountApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }

    /*
     * Validations txtPhoneNumberET
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editFirstNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name));
            flag = false;
        } else if (editLastNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name));
            flag = false;
        } else if (txtDateOfBirthTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_dateof_birth));
            flag = false;
        } else if (txtPhoneNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_phone_number));
            flag = false;
        } else if (editLine1ET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_line_1));
            flag = false;
        } else if (editLine2ET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_line_2));
            flag = false;
        }
//        else if (editStreetAddressET.getText().toString().trim().equals("")) {
//            showAlertDialog(mActivity, getString(R.string.please_enter_street_address));
//            flag = false;
//        }
        else if (editCityET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_city));
            flag = false;
        } else if (selectedState.equals("State/Region")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_state_region));
            flag = false;
        } else if (editPostalCodeET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_postal_code));
            flag = false;
        } else if (editBanckAccountHolderNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_bank_account_holder_name));
            flag = false;
        } else if (editBankRoutingNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_routing_number));
            flag = false;
        } else if (editBankAccountNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_account_nuber));
            flag = false;
        } else if (editPersonalIdentificationET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_identification_nuber));
            flag = false;
        } else if (editPersonalIdentificationET.getText().toString().trim().length() < 4) {
            showAlertDialog(mActivity, getString(R.string.invalid_identification_nuber));
            flag = false;
        }
//        else if (paypalEmail.getText().toString().trim().equals("")) {
//            showAlertDialog(mActivity, getString(R.string.please_enter_paypal_account));
//            flag = false;
//        } else if (!isValidEmaillId(paypalEmail.getText().toString().trim())) {
//            showAlertDialog(mActivity, getString(R.string.please_enter_valid_paypal_account));
//            flag = false;
//        } else if (getPaypalEmail().equals("")) {
//            showAlertDialog(mActivity, getString(R.string.please_verify_paypal_account));
//            flag = false;
//        }
        else if (StrFrontFileId.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_add_front_image));
            flag = false;
        } else if (StrBackFileId.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_add_back_image));
            flag = false;
        }
        return flag;
    }

    private void executeSubmitApi() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.ARTIST_ADD_ACCOUNT_DETAIL;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            mParams.put("artist_id", getUserID());
            mParams.put("firstname", editFirstNameET.getText().toString().trim());
            mParams.put("lastname", editLastNameET.getText().toString().trim());
            mParams.put("dob", txtDateOfBirthTV.getText().toString().trim());
            mParams.put("street_address", editStreetAddressET.getText().toString().trim());
            mParams.put("city", editCityET.getText().toString().trim());
            mParams.put("state", editStateRegionET.getText().toString().trim());
            mParams.put("country", txtCountryNameTV.getText().toString().trim());
            mParams.put("personal_identification_number", editPersonalIdentificationET.getText().toString().trim());
            mParams.put("bank_acc_holder_name", editBanckAccountHolderNameET.getText().toString().trim());
            mParams.put("bank_routing_number", editBankRoutingNumberET.getText().toString().trim());
            mParams.put("bank_acc_number", editBankAccountNumberET.getText().toString().trim());
            mParams.put("paypal_id", getPaypalEmail());

            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseAddAccountDetailsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseAddAccountDetailsResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                showToast(mActivity, response.getString("message"));
                TheTattzPrefrences.writeString(mActivity, TheTattzPrefrences.ACCOUNT_DETAIL_ID, response.getString("account_detail_id"));
                finish();
            } else {
                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    private void executeStatesApi() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.GET_STATE_NAME;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());

                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {

        try {
            if (response.getString("status").equals("1")) {
                if (!response.isNull("data")) {
                    JSONArray mJsonArray = response.getJSONArray("data");

                    StateSpinnerModel StateModel0 = new StateSpinnerModel();

                    StateModel0.setName("State/Region");
                    StateModel0.setAbbr("State/Region");
                    StateModel0.setId("0");
                    StateModelArrayList.add(StateModel0);

                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mDataObj = mJsonArray.getJSONObject(i);

                        StateSpinnerModel StateModel = new StateSpinnerModel();

                        if (!mDataObj.isNull("id"))
                            StateModel.setId(mDataObj.getString("id"));
                        if (!mDataObj.isNull("abbr"))
                            StateModel.setAbbr(mDataObj.getString("abbr"));
                        if (!mDataObj.isNull("name"))
                            StateModel.setName(mDataObj.getString("name"));

                        StateModelArrayList.add(StateModel);
                    }

                    setStateAdapter();
                }

            } else {

                Toast.makeText(mActivity, response.getString("message"), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    /*
     *  state adapter
     * */
    private void setStateAdapter() {
        font = Typeface.createFromAsset(mActivity.getAssets(),
                "ProximaNova-Semibold.ttf");
        ArrayAdapter<StateSpinnerModel> mBrandAd = new ArrayAdapter<StateSpinnerModel>(this, android.R.layout.simple_spinner_item, StateModelArrayList) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                HideKey();

                TextView itemTextView = v.findViewById(R.id.itemTV);
                String strName = StateModelArrayList.get(position).getName();
                String strAbbr = StateModelArrayList.get(position).getAbbr();
                if (StateModelArrayList.get(position).getId().equals("0")) {
                    itemTextView.setText("State/Region");
                    itemTextView.setTextColor(getResources().getColor(R.color.colorTextLight));
                } else {
                    itemTextView.setText(strName + " - " + strAbbr);
                    itemTextView.setTextColor(getResources().getColor(R.color.colorBlack));
                }
                itemTextView.setTypeface(font);
                return v;
            }

        };
        StateRegionSpinner.setAdapter(mBrandAd);
        StateRegionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ((TextView) view).setTypeface(font);
                selectedState = StateRegionSpinner.getSelectedItem().toString();

                if (StateModelArrayList.get(position).getId().equals("0")) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorTextLight));
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorBlack));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Set Selections
//        if (mItemProductModel.getBrand_name() != null && mItemProductModel.getBrand_name().length() > 0) {
//            for (int i = 0; i < brandModelArrayList.size(); i++) {
//                if (mItemProductModel.getBrand_name().equals(brandModelArrayList.get(i).getName())) {
//                    brandSp.setSelection(i);
//                }
//            }
//        }
    }

    private void executeUploadDocumentApi(final String ImageType, final Bitmap mBitmap) {
        String ApiUrl = Constants.UPLOAD_DOCUMENT;
        Log.e(TAG, "Api Url::::" + ApiUrl);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                ApiUrl, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.e(TAG, "onResponse: " + resultResponse);

                try {
                    JSONObject mJsonObject = new JSONObject(resultResponse);

                    if (StrImageType.equals("front")) {
                        StrFrontFileId = mJsonObject.getString("file_id");
                        fileFrontImageTV.setText(StrFrontFileId);
                    } else if (StrImageType.equals("back")) {
                        StrBackFileId = mJsonObject.getString("file_id");
                        fileBackImageTV.setText(StrBackFileId);
                    }

                    txtCancelTV.setEnabled(true);
                    txtSaveTV.setEnabled(true);
                    uploadFrontImageTV.setEnabled(true);
                    uploadBackImageTV.setEnabled(true);

                    back_status = 0;
                    front_status = 0;

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String strBody = "";
                if (error.networkResponse != null) {
                    try {
                        strBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                        } else if (error instanceof AuthFailureError) {
                            showAlertDialog(mActivity, getString(R.string.auth_failed));
                        } else if (error instanceof ServerError) {
                            showAlertDialog(mActivity, getString(R.string.server_error));
                        } else if (error instanceof NetworkError) {
                            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                        } else if (error instanceof ParseError) {
                            showAlertDialog(mActivity, getString(R.string.parse_error));
                        } else {
                            showAlertDialog(mActivity, strBody);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                }
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("artist_id", getUserID());
                params.put("image_type", ImageType);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "multipart/form-data");
                return headers;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                if (mBitmap != null)
                    params.put("image", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, mBitmap), "image/jpeg"));
                return params;
            }
        };
        TheTattzApplication.getInstance().addToRequestQueue(multipartRequest);
    }

    private void executeCreateArtistStripeAccountApi() {
        String number = "+1" + txtPhoneNumberET.getText().toString().trim()
                .replace("(", "")
                .replace(")", "")
                .replace("-", "")
                .replace(" ", "");

        if (editPersonalIdentificationET.getText().toString().trim().length() == 4) {
            ssn_last4 = editPersonalIdentificationET.getText().toString().trim();
        } else if (editPersonalIdentificationET.getText().toString().trim().length() > 4) {
            ssn_last4 = editPersonalIdentificationET.getText().toString().trim()
                    .substring(editPersonalIdentificationET.getText().toString().trim().length() - 4);
        } else {
            // whatever is appropriate in this case
            throw new IllegalArgumentException("word has less than 4 characters!");
        }
        Log.e(TAG, "**Api**" + ssn_last4);

        showProgressDialog(mActivity);
        final String mApiUrl = Constants.CREATE_ARTIST_STRIPE_ACCOUNT;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            mParams.put("artist_id", getUserID());
            mParams.put("first_name", editFirstNameET.getText().toString().trim());
            mParams.put("last_name", editLastNameET.getText().toString().trim());
            mParams.put("city", editCityET.getText().toString().trim());
            mParams.put("country", "US");
            mParams.put("line1", editLine1ET.getText().toString().trim());
            mParams.put("line2", editLine2ET.getText().toString().trim());
            mParams.put("postal_code", editPostalCodeET.getText().toString().trim());
            mParams.put("state", selectedState);
            mParams.put("dob", txtDateOfBirthTV.getText().toString().trim());
            mParams.put("id_number", editPersonalIdentificationET.getText().toString().trim());
            mParams.put("phone", number);
            mParams.put("ssn_last_4", ssn_last4);
            mParams.put("back_document", StrBackFileId);
            mParams.put("front_document", StrFrontFileId);
            mParams.put("account_holder_name", editBanckAccountHolderNameET.getText().toString().trim());
            mParams.put("routing_number", editBankRoutingNumberET.getText().toString().trim());
            mParams.put("account_number", editBankAccountNumberET.getText().toString().trim());

            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();

                        try {
                            if (response.getString("status").equals("1")) {

                                Log.e(TAG, "**RESPONSE**" + response.toString());
                                TheTattzPrefrences.writeString(mActivity, Constants.PAYMENT, Constants.SUBMITTED);
                                onBackPressed();

                            } else {
                                showAlertDialog(mActivity, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }
}
