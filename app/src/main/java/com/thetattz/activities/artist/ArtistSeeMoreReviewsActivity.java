package com.thetattz.activities.artist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.thetattz.R;
import com.thetattz.activities.BaseActivity;
import com.thetattz.adapters.ArtistAllRatingAdapter;
import com.thetattz.models.ArtistRatingModel;
import com.thetattz.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ArtistSeeMoreReviewsActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = ArtistSeeMoreReviewsActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = ArtistSeeMoreReviewsActivity.this;


    /**
     * Widgets Main Layout
     */

    @BindView(R.id.rlBackRL)
    RelativeLayout rlBackRL;
    @BindView(R.id.artistReviewsRV)
    RecyclerView artistReviewsRV;
    @BindView(R.id.llNoDataFountTV)
    LinearLayout llNoDataFountTV;
    ArtistAllRatingAdapter mArtistRatingAdapter;
    ArrayList<ArtistRatingModel> mArtistRatingArrayList = new ArrayList<ArtistRatingModel>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_see_more_reviews);
        ButterKnife.bind(this);
        setStatusBar();
        /*calling getArrayList*/
        getArrayList();

        /*set reviews adapter*/
        setReviewsAdapter();

    }

    /*getting arraylist from previous activity*/
    private void getArrayList() {
        Intent intent = getIntent();
        if (intent != null) {
            mArtistRatingArrayList = (ArrayList<ArtistRatingModel>) getIntent().getSerializableExtra(Constants.LIST);
            if (mArtistRatingArrayList.size() > 0){
                llNoDataFountTV.setVisibility(View.GONE);
                artistReviewsRV.setVisibility(View.VISIBLE);
            }else{
                llNoDataFountTV.setVisibility(View.VISIBLE);
                artistReviewsRV.setVisibility(View.GONE);
            }
        }
    }



    /*
     * Set Rating Reviews Adapter
     * */
    private void setReviewsAdapter() {
        mArtistRatingAdapter = new ArtistAllRatingAdapter(mActivity, mArtistRatingArrayList);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity);
        artistReviewsRV.setLayoutManager(mLayoutManagerC);
        artistReviewsRV.setAdapter(mArtistRatingAdapter);
    }


    /*
     * Widgets Click Listner
     * */
    @OnClick({R.id.rlBackRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBackRL:
                onBackPressed();
                break;
        }}

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
