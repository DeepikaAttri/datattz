package com.thetattz.activities.artist;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.BaseActivity;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ArtistRatingsActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ArtistRatingsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = ArtistRatingsActivity.this;

    /**
     * Widgets
     */
    @BindView(R.id.txtCancelTV)
    TextView txtCancelTV;

    @BindView(R.id.mRatingBarRB)
    RatingBar mRatingBarRB;

    @BindView(R.id.reviewsET)
    EditText reviewsET;

    String strArtistID = "", strrating = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_ratings);
        setStatusBar();
        //Butter Knife
        ButterKnife.bind(this);

        getIntentData();

        mRatingBarRB.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                strrating = String.valueOf(rating);
            }
        });
    }

    private void getIntentData() {
        if (getIntent() != null) {

            strArtistID = getIntent().getStringExtra(Constants.ARTIST_ID);
        }
    }

    /*
     * Set up validations ratings
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (strrating.equals("")) {
            showAlertDialog(mActivity, "Please give ratings");
            flag = false;
        } else if (reviewsET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "please write reviews");
            flag = false;
        }
        return flag;
    }

    @OnClick({R.id.txtCancelTV, R.id.txtSaveTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtCancelTV:
                onBackPressed();
                break;

            case R.id.txtSaveTV:
                performSaveReviewClick();
                break;
        }
    }

    private void performSaveReviewClick() {
        if (isValidate()) {
            if (ConnectivityReceiver.isConnected())
                executeRatingAPI();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    /*Execute Get User Details API*/
    private void executeRatingAPI() {

        Calendar calander = Calendar.getInstance();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEE MMMM dd HH:mm:ss z yyyy");
        String date = simpledateformat.format(calander.getTime());

        long timeInMilliseconds = 0;
        try {
            Date mDate = simpledateformat.parse(date);
            timeInMilliseconds = mDate.getTime()/1000;
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        showProgressDialog(mActivity);
        final String mApiUrl = Constants.ADD_RATING_TO_ARTIST;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            mParams.put("artist_id", strArtistID);
            mParams.put("rating", strrating);
            mParams.put("creation_time", "" + timeInMilliseconds);
            mParams.put("review", reviewsET.getText().toString());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());

                        try {
                            if (response.getString("status").equals("1")) {

                                showSuccessAlertDialog(mActivity, response.getString("message"));

                            } else {
                                showToast(mActivity, response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }
}