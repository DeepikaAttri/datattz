package com.thetattz.activities.artist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.thetattz.R;
import com.thetattz.activities.BaseActivity;
import com.thetattz.activities.HomeActivity;
import com.thetattz.fragments.artist.ArtistConfirmedFragment;
import com.thetattz.fragments.artist.ArtistPendingFragment;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ArtistAppointActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ArtistAppointActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ArtistAppointActivity.this;

    /**
     * Widgets
     */
    @BindView(R.id.txtConfirmedTabTV)
    TextView txtConfirmedTabTV;
    @BindView(R.id.viewConfirmedTabV)
    View viewConfirmedTabV;
    @BindView(R.id.layoutIConfirmedLL)
    LinearLayout layoutIConfirmedLL;
    @BindView(R.id.txtPendingTV)
    TextView txtPendingTV;
    @BindView(R.id.viewPendingTabV)
    View viewPendingTabV;
    @BindView(R.id.layoutPendingLL)
    LinearLayout layoutPendingLL;
    @BindView(R.id.containerFL)
    FrameLayout containerFL;
    @BindView(R.id.rlBackRL)
    RelativeLayout rlBackRL;

    String strPushType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_appoint);

        setStatusBar();

        ButterKnife.bind(this);

        perfromConfirmedClick();

        if (getIntent().getStringExtra(Constants.NOTIFICATION_TYPE) != null) {
            strPushType = getIntent().getStringExtra(Constants.NOTIFICATION_TYPE);
        }

        if (TheTattzPrefrences.readString(mActivity, Constants.APPOINTMENT_TYPE, "").equals(mActivity.getResources().getString(R.string.type_pending)) && TheTattzPrefrences.readString(mActivity, Constants.TYPES, "").equals(mActivity.getResources().getString(R.string.type_artist))) {
            perfromPendingClick();
        } else if (TheTattzPrefrences.readString(mActivity, Constants.APPOINTMENT_TYPE, "").equals(mActivity.getResources().getString(R.string.type_confirm)) && TheTattzPrefrences.readString(mActivity, Constants.TYPES, "").equals(mActivity.getResources().getString(R.string.type_artist))) {
            perfromConfirmedClick();
        }
    }

    @OnClick({R.id.layoutIConfirmedLL, R.id.layoutPendingLL, R.id.rlBackRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutIConfirmedLL:
                perfromConfirmedClick();
                break;
            case R.id.layoutPendingLL:
                perfromPendingClick();
                break;
            case R.id.rlBackRL:
                perfromBackClick();
                break;
        }
    }

    private void perfromConfirmedClick() {
        txtConfirmedTabTV.setTextColor(getResources().getColor(R.color.colorTextLight));
        viewConfirmedTabV.setBackgroundColor(getResources().getColor(R.color.colorTextLight));
        txtPendingTV.setTextColor(getResources().getColor(R.color.colorTextLight));
        viewPendingTabV.setBackgroundColor(getResources().getColor(R.color.colorTrasperant));

        ArtistConfirmedFragment fragment = new ArtistConfirmedFragment();
        switchFragment(mActivity, fragment, false, null);
    }

    private void perfromPendingClick() {
        txtConfirmedTabTV.setTextColor(getResources().getColor(R.color.colorTextLight));
        viewConfirmedTabV.setBackgroundColor(getResources().getColor(R.color.colorTrasperant));
        txtPendingTV.setTextColor(getResources().getColor(R.color.colorTextLight));
        viewPendingTabV.setBackgroundColor(getResources().getColor(R.color.colorTextLight));

        ArtistPendingFragment fragment = new ArtistPendingFragment();
        switchFragment(mActivity, fragment, false, null);


    }

    private void perfromBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {

        /* to manage onBackPressed for push and normal intent */
        if (!strPushType.equals("") && strPushType.equals(Constants.PUSH)) {
            if (getUserRole().equals(Constants.ROLE_ARTIST)) {
                Intent mIntent = new Intent(mActivity, ArtistHomeActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mIntent.putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH);
                startActivity(mIntent);
                finish();
            } else if (getUserRole().equals(Constants.ROLE_CLIENT)) {
                Intent mIntent = new Intent(mActivity, HomeActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mIntent.putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH);
                startActivity(mIntent);
                finish();
            }
        } else {
            finish();
        }
    }

    /********
     *Replace Fragment In Activity
     **********/
    public void switchFragment(Activity activity, Fragment fragment, boolean addToStack, Bundle bundle) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.containerFL, fragment);
            if (addToStack)
                ft.addToBackStack(null);
            if (bundle != null)
                fragment.setArguments(bundle);
            ft.commitAllowingStateLoss();
        }
    }

}
