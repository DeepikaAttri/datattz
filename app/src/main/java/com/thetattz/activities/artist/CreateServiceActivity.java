package com.thetattz.activities.artist;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.aigestudio.wheelpicker.WheelPicker;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.BaseActivity;
import com.thetattz.fonts.EditTextRegular;
import com.thetattz.fonts.TextViewBold;
import com.thetattz.fonts.TextViewRegular;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateServiceActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = CreateServiceActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = CreateServiceActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.txtCancelTV)
    TextViewBold txtCancelTV;
    @BindView(R.id.txtSaveTV)
    TextViewBold txtSaveTV;
    @BindView(R.id.editNameET)
    EditTextRegular editNameET;
    @BindView(R.id.editPriceET)
    EditTextRegular editPriceET;
    @BindView(R.id.txtDurationET)
    TextViewRegular txtDurationET;
    @BindView(R.id.editDescriptionET)
    EditTextRegular editDescriptionET;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_service);
        setStatusBar();
        //Butter Knife
        ButterKnife.bind(this);
    }


    @OnClick({R.id.txtCancelTV, R.id.txtSaveTV, R.id.txtDurationET})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtCancelTV:
                performCancelClick();
                break;
            case R.id.txtSaveTV:
                performSaveClick();
                break;
            case R.id.txtDurationET:
                perfromDurationClick();
                break;
        }
    }

    private void perfromDurationClick() {
        createBottomSheetDialog(txtDurationET);
    }


    private void performCancelClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = mActivity.getFragmentManager();
        fm.popBackStack();
        super.onBackPressed();
    }

    private void performSaveClick() {
        if (isValidate()) {
            if (ConnectivityReceiver.isConnected()) {
                executeCreateServiceApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }


    /*
     * Set up validations
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_service_name));
            flag = false;
        } else if (editPriceET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_service_price));
            flag = false;
        } else if (txtDurationET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_service_duration));
            flag = false;
        } else if (editDescriptionET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_service_descriptions));
            flag = false;
        }
        return flag;
    }


    public void createBottomSheetDialog(TextView mTextView) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        @SuppressLint("InflateParams") View sheetView = mActivity.getLayoutInflater()
                .inflate(R.layout.dialog_service_duration_timer, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        Typeface mTypeface = Typeface.createFromAsset(getAssets(), "ProximaNova-Regular.ttf");

        WheelPicker hoursWP = sheetView.findViewById(R.id.hoursWP);
        hoursWP.setTypeface(mTypeface);

        WheelPicker hourTextWP = sheetView.findViewById(R.id.hourTextWP);
        hourTextWP.setTypeface(mTypeface);

        WheelPicker minuteWP = sheetView.findViewById(R.id.minuteWP);
        minuteWP.setTypeface(mTypeface);

        WheelPicker minuteTextWP = sheetView.findViewById(R.id.minuteTextWP);
        minuteTextWP.setTypeface(mTypeface);


        TextView txtCancelTV = sheetView.findViewById(R.id.txtCancelTV);
        TextView txtSaveTV = sheetView.findViewById(R.id.txtSaveTV);


        hoursWP.setAtmospheric(true);
        hoursWP.setCyclic(false);
        hoursWP.setCurved(true);
        hoursWP.setData(getHours());

        hourTextWP.setAtmospheric(true);
        hourTextWP.setCyclic(false);
        hourTextWP.setCurved(true);
        hourTextWP.setData(getHoursText());


        minuteWP.setAtmospheric(true);
        minuteWP.setCyclic(false);
        minuteWP.setCurved(true);
        minuteWP.setData(getMinutesInLimit());

        minuteTextWP.setAtmospheric(true);
        minuteTextWP.setCyclic(false);
        minuteTextWP.setCurved(true);
        minuteTextWP.setData(getMinutesText());


        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        //11:00 PM - 04:02 PM
        txtSaveTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strHour = getHours().get(hoursWP.getCurrentItemPosition());
                String strMinutes = getMinutesInLimit().get(minuteWP.getCurrentItemPosition());

                if (strHour.equals("0") && strMinutes.equals("0")) {
                    mTextView.setHint(getString(R.string.duration));
                } else if (strHour.equals("0") && strMinutes.equals("30")) {
                    mTextView.setText(strMinutes + " minutes ");
                } else if (!strHour.equals("0") && strMinutes.equals("0")) {
                    mTextView.setText(strHour + " hours ");
                } else {
                    mTextView.setText(strHour + " hours " + strMinutes + " minutes ");
                }

                mBottomSheetDialog.dismiss();
            }
        });
    }


    /*
     *
     * Execute  Logout Api
     * */
    private void executeCreateServiceApi() {
        showProgressDialog(mActivity);
        final String mApiUrl =
                Constants.ADD_SERVICE;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("artist_id", getUserID());
            mParams.put("service_name", editNameET.getText().toString().trim());
            mParams.put("price", editPriceET.getText().toString().trim());
            mParams.put("duration", txtDurationET.getText().toString().trim());
            mParams.put("description", editDescriptionET.getText().toString().trim());

            Log.e(TAG, "**Params**" + mParams.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                showToast(mActivity, getString(R.string.service_created_successfully));
                Intent mIntent = new Intent();
                setResult(363,mIntent);
                onBackPressed();
            } else {
                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }




}
