package com.thetattz.activities.artist;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.thetattz.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubscriptionSubmitActivity extends AppCompatActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SubscriptionSubmitActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = SubscriptionSubmitActivity.this;

    /**
     * Widgets Main Layout
     */
    @BindView(R.id.backLL)
    LinearLayout backLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_submit);

        //ButterKnife
        ButterKnife.bind(this);
    }

    /*
     * Widgets click listner
     * */
    @OnClick({R.id.backLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backLL:
                onBackPressed();
                break;
        }
    }
}
