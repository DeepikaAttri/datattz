package com.thetattz.activities.artist;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.thetattz.R;

public class ArtistAllAppointmentsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_all_appointments);
    }
}
