package com.thetattz.activities.artist;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.BaseActivity;
import com.thetattz.models.ArtistStripeDetailsModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentMethodActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = PaymentMethodActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = PaymentMethodActivity.this;

    @BindView(R.id.enableMobilePayTv)
    TextView enableMobilePayTv;

    @BindView(R.id.AddAccountTV)
    TextView AddAccountTV;

    @BindView(R.id.ProcessingGif)
    ImageView ProcessingGif;

    @BindView(R.id.SubmittedAccountLL)
    LinearLayout SubmittedAccountLL;

    @BindView(R.id.txtAccountTV)
    TextView txtAccountTV;

    @BindView(R.id.detailsLL)
    LinearLayout detailsLL;

    @BindView(R.id.txtNameTV)
    TextView txtNameTV;

    @BindView(R.id.txtAccountNumberTV)
    TextView txtAccountNumberTV;

    @BindView(R.id.txtSS4TV)
    TextView txtSS4TV;

    @BindView(R.id.txtAddressTV)
    TextView txtAddressTV;

    @BindView(R.id.txtPhoneTV)
    TextView txtPhoneTV;

    @BindView(R.id.txtPayoutStatusTV)
    TextView txtPayoutStatusTV;

    @BindView(R.id.EditAccountTV)
    TextView EditAccountTV;

    String FirstName = "", LastName = "", FullName = "", Line1 = "", Line2 = "", City = "", State = "",
            Country = "", PostalCode = "", FullAddress = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);

        //butter knife
        ButterKnife.bind(this);

        Glide.with(this)
                .load(R.raw.progress)
                .into(ProcessingGif);

        TheTattzPrefrences.writeString(mActivity, Constants.PAYMENT, "");
    }

    @OnClick({R.id.enableMobilePayTv, R.id.rlBackRL, R.id.AddAccountTV, R.id.EditAccountTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.enableMobilePayTv:
                performEnableMobilePayClick();
                break;

            case R.id.rlBackRL:
                onBackPressed();
                break;

            case R.id.AddAccountTV:
                performAddAccountClick();
                break;

            case R.id.EditAccountTV:
                performEditAccountClick();
                break;
        }
    }

    private void performEditAccountClick() {
        showEditAlertDialog(mActivity);
    }

    private void performAddAccountClick() {
        startActivity(new Intent(mActivity, AddArtistPaymentActivity.class));
    }

    private void performEnableMobilePayClick() {
        startActivity(new Intent(mActivity, AddArtistPaymentActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ConnectivityReceiver.isConnected()) {
            executeGetArtistAccountDetailApi();
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    private void executeGetArtistAccountDetailApi() {
        showProgressDialog(mActivity);
        final String mApiUrl = Constants.GET_ARTIST_ACCOUNT_DETAIL;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            mParams.put("artist_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseGetArtistAccountDetailsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseGetArtistAccountDetailsResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                SubmittedAccountLL.setVisibility(View.GONE);
                detailsLL.setVisibility(View.VISIBLE);

                if (!response.isNull("data")) {
                    JSONObject mDataObject = response.getJSONObject("data");

                    ArtistStripeDetailsModel mArtistStripeDetailsModel = new ArtistStripeDetailsModel();

                    if (!mDataObject.isNull("account_detail_id"))
                        mArtistStripeDetailsModel.setAccount_detail_id(mDataObject.getString("account_detail_id"));

                    if (!mDataObject.isNull("firstname")) {
                        mArtistStripeDetailsModel.setFirstname(mDataObject.getString("firstname"));
                        FirstName = mDataObject.getString("firstname");
                        txtNameTV.setText(FirstName);
                    }

                    if (!mDataObject.isNull("lastname")) {
                        mArtistStripeDetailsModel.setLastname(mDataObject.getString("lastname"));
                        LastName = mDataObject.getString("lastname");
                        FullName = FirstName + " " + LastName;
                        txtNameTV.setText(FullName);
                    }

                    if (!mDataObject.isNull("dob"))
                        mArtistStripeDetailsModel.setDob(mDataObject.getString("dob"));

                    if (!mDataObject.isNull("line1")) {
                        mArtistStripeDetailsModel.setLine1(mDataObject.getString("line1"));

                        Line1 = mDataObject.getString("line1");
                    }

                    if (!mDataObject.isNull("line2")) {
                        mArtistStripeDetailsModel.setLine2(mDataObject.getString("line2"));

                        Line2 = mDataObject.getString("line2");
                    }

                    if (!mDataObject.isNull("city")) {
                        mArtistStripeDetailsModel.setCity(mDataObject.getString("city"));

                        City = mDataObject.getString("city");
                    }

                    if (!mDataObject.isNull("state")) {
                        mArtistStripeDetailsModel.setState(mDataObject.getString("state"));

                        State = mDataObject.getString("state");
                    }

                    if (!mDataObject.isNull("country")) {
                        mArtistStripeDetailsModel.setCountry(mDataObject.getString("country"));

                        Country = mDataObject.getString("country");
                    }

                    if (!mDataObject.isNull("postal_code")) {
                        mArtistStripeDetailsModel.setPostal_code(mDataObject.getString("postal_code"));

                        PostalCode = mDataObject.getString("postal_code");
                    }

                    FullAddress = Line1 + ", " + Line2 + ", " + City + ", " + State + ", " + PostalCode
                            + ", " + Country;
                    txtAddressTV.setText(FullAddress);

                    if (!mDataObject.isNull("id_number"))
                        mArtistStripeDetailsModel.setId_number(mDataObject.getString("id_number"));

                    if (!mDataObject.isNull("phone")) {
                        mArtistStripeDetailsModel.setPhone(mDataObject.getString("phone"));
                        txtPhoneTV.setText(mDataObject.getString("phone"));
                    }

                    if (!mDataObject.isNull("ssn_last_4")) {
                        mArtistStripeDetailsModel.setSsn_last_4(mDataObject.getString("ssn_last_4"));
                        String mask = "XXXXX" + mDataObject.getString("ssn_last_4");
                        txtSS4TV.setText(mask);
                    }

                    if (!mDataObject.isNull("front_document"))
                        mArtistStripeDetailsModel.setFront_document(mDataObject.getString("front_document"));

                    if (!mDataObject.isNull("back_document"))
                        mArtistStripeDetailsModel.setBack_document(mDataObject.getString("back_document"));

                    if (!mDataObject.isNull("bank_acc_holder_name"))
                        mArtistStripeDetailsModel.setBank_acc_holder_name(mDataObject.getString("bank_acc_holder_name"));

                    if (!mDataObject.isNull("bank_acc_number")) {
                        mArtistStripeDetailsModel.setBank_acc_number(mDataObject.getString("bank_acc_number"));
                        String mask = mDataObject.getString("bank_acc_number").replaceAll("\\w(?=\\w{4})", "X");
                        txtAccountNumberTV.setText(mask);
                    }

                    if (!mDataObject.isNull("bank_routing_number"))
                        mArtistStripeDetailsModel.setBank_routing_number(mDataObject.getString("bank_routing_number"));

                    if (!mDataObject.isNull("verified"))
                        mArtistStripeDetailsModel.setVerified(mDataObject.getString("verified"));

                    if (!mDataObject.isNull("enabled"))
                        mArtistStripeDetailsModel.setEnabled(mDataObject.getString("enabled"));

                    if (!mDataObject.isNull("artist_id"))
                        mArtistStripeDetailsModel.setArtist_id(mDataObject.getString("artist_id"));

                    if (!mDataObject.isNull("stripe_account_id"))
                        mArtistStripeDetailsModel.setStripe_account_id(mDataObject.getString("stripe_account_id"));

                    if (!mDataObject.isNull("stripe_bank_id"))
                        mArtistStripeDetailsModel.setStripe_bank_id(mDataObject.getString("stripe_bank_id"));

                    if (!mDataObject.isNull("creation_time"))
                        mArtistStripeDetailsModel.setCreation_time(mDataObject.getString("creation_time"));

                }

            } else if (response.getString("status").equals("0")) {
                txtAccountTV.setText(R.string.no_account_added);
                AddAccountTV.setVisibility(View.VISIBLE);
                ProcessingGif.setVisibility(View.GONE);
                SubmittedAccountLL.setVisibility(View.VISIBLE);
                detailsLL.setVisibility(View.GONE);
            } else if (response.getString("status").equals("2")) {
                txtAccountTV.setText(R.string.account_submitted);
                AddAccountTV.setVisibility(View.GONE);
                ProcessingGif.setVisibility(View.VISIBLE);
                SubmittedAccountLL.setVisibility(View.VISIBLE);
                detailsLL.setVisibility(View.GONE);
            } else if (response.getString("status").equals("3")) {
                txtAccountTV.setText(R.string.not_verified);
                AddAccountTV.setVisibility(View.VISIBLE);
                ProcessingGif.setVisibility(View.GONE);
                SubmittedAccountLL.setVisibility(View.VISIBLE);
                detailsLL.setVisibility(View.GONE);
            } else if (response.getString("status").equals("4")) {
                txtAccountTV.setText(R.string.stripe_error);
                showToast(mActivity, response.getString("message"));
                AddAccountTV.setVisibility(View.VISIBLE);
                ProcessingGif.setVisibility(View.GONE);
                SubmittedAccountLL.setVisibility(View.VISIBLE);
                detailsLL.setVisibility(View.GONE);
            } else {
                showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showEditAlertDialog(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_edit_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView btnCancel = alertDialog.findViewById(R.id.btnCancel);
        TextView btnContinue = alertDialog.findViewById(R.id.btnContinue);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                startActivity(new Intent(mActivity, AddArtistPaymentActivity.class));
            }
        });

        alertDialog.show();
    }
}
