package com.thetattz.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.adapters.ArtistServicesAdapter;
import com.thetattz.models.ArtistServiceModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzSingleton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistServicesFragment extends BaseFragment {


    /**
     * Getting the Current Class Name
     */
    String TAG = ArtistServicesFragment.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    @BindView(R.id.mServicesRecyclerViewRV)
    RecyclerView mServicesRecyclerViewRV;

    /*
     * Initialize  Unbinder for butterknife;
     * */
    private Unbinder mUnbinder;

    /*
     *
     * Initialize Objects...
     * */
    String  strArtistID = "";
    ArtistServicesAdapter mArtistServicesAdapter;
    ArrayList<ArtistServiceModel> mServicesArrayList = TheTattzSingleton.getInstance().getServicesArrayList();

    /*
     * Default Constructor
     * */

    public ArtistServicesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_artist_services, container, false);
        setStatusBar();
        mUnbinder = ButterKnife.bind(this, mView);
        //Get Intent Data
        getArgumentsData();
        return mView;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    private void getArgumentsData() {
        if (getArguments() != null) {
            strArtistID = getArguments().getString(Constants.ARTIST_ID);
            //Set Data onWidgets
            if (mServicesArrayList.size() > 0)
                setDataOnWidgets();
            else
                //Get User Details
                getArtistDetails();
        }

    }

    private void getArtistDetails() {
        if (ConnectivityReceiver.isConnected())
            executeArtistDetailsAPI();
        else
            showToast(getActivity(), getString(R.string.internet_connection_error));
    }

    /*Execute Get User Details API*/
    private void executeArtistDetailsAPI() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.ARTIST_SERVICES_LIST;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("artist_id", strArtistID);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseDetailsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseDetailsResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                if (!response.isNull("data")){
                    JSONArray mServicesArray = response.getJSONArray("data");
                    for (int i = 0; i < mServicesArray.length(); i++){
                        JSONObject mDataObj = mServicesArray.getJSONObject(i);
                        ArtistServiceModel mModel = new ArtistServiceModel();
                        if (!mDataObj.isNull("id"))
                            mModel.setId(mDataObj.getString("id"));
                        if (!mDataObj.isNull("artist_id"))
                            mModel.setArtist_id(mDataObj.getString("artist_id"));
                        if (!mDataObj.isNull("service_name"))
                            mModel.setService_name(mDataObj.getString("service_name"));
                        if (!mDataObj.isNull("description"))
                            mModel.setDescription(mDataObj.getString("description"));
                        if (!mDataObj.isNull("duration"))
                            mModel.setDuration(mDataObj.getString("duration"));
                        if (!mDataObj.isNull("price"))
                            mModel.setPrice(mDataObj.getString("price"));
                        if (!mDataObj.isNull("enable"))
                            mModel.setEnable(mDataObj.getString("enable"));

                        mServicesArrayList.add(mModel);

                    }
                }

                //Set Data on Widgets
                setDataOnWidgets();

            } else {
                showToast(getActivity(), response.getString("message"));
            }

        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    private void setDataOnWidgets() {
        setWorkInfoAdapter();
    }

    private void setWorkInfoAdapter() {
        mServicesRecyclerViewRV.setBackgroundResource(R.drawable.bg_box_rectangle);
        mArtistServicesAdapter = new ArtistServicesAdapter(getActivity(), mServicesArrayList);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(getActivity());
        mServicesRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        mServicesRecyclerViewRV.setAdapter(mArtistServicesAdapter);
    }
}
