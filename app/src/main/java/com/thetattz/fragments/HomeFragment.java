package com.thetattz.fragments;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.ArtistDetailsActivity;
import com.thetattz.activities.BookApointmentActivity;
import com.thetattz.activities.ClientGalleryImagesActivity;
import com.thetattz.activities.HomeActivity;
import com.thetattz.adapters.MyArtistAdapter;
import com.thetattz.adapters.MyTattozAdapter;
import com.thetattz.interfaces.ClientGalleryClickInterface;
import com.thetattz.interfaces.MyArtistCallBack;
import com.thetattz.models.MyArtistModel;
import com.thetattz.models.MyTattozModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;
import com.thetattz.volley.AppHelper;
import com.thetattz.volley.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {

    /**
     * Getting the Current Class Name
     */
    String TAG = HomeFragment.this.getClass().getSimpleName();

    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;


    /**
     * Widgets
     */
    @BindView(R.id.txtMyArtistTV)
    TextView txtMyArtistTV;
    @BindView(R.id.txtFindArtistTV)
    TextView txtFindArtistTV;
    @BindView(R.id.myArtistsRecyclerViewRV)
    RecyclerView myArtistsRecyclerViewRV;
    @BindView(R.id.txtMyTattosTV)
    TextView txtMyTattosTV;
    @BindView(R.id.txtAddTattosTV)
    TextView txtAddTattosTV;
    @BindView(R.id.myTattozRecyclerViewRV)
    RecyclerView myTattozRecyclerViewRV;
    @BindView(R.id.txtUserNameTV)
    TextView txtUserNameTV;
    @BindView(R.id.imgCoverPicIV)
    ImageView imgCoverPicIV;

    /*
     * Initialize  Unbinder for butterknife;
     * */
    private Unbinder mUnbinder;

    /*
     *
     * Initialize Objects...
     * */
    MyArtistAdapter mMyArtistAdapter;
    ArrayList<MyArtistModel> mMyArtistArrayList = new ArrayList<>();

    MyTattozAdapter mMyTattozAdapter;
    ArrayList<MyTattozModel> mMyTattozArrayList = new ArrayList<>();

    /*
     * Default Constructor
     * */
    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_home, container, false);
        setStatusBar();
        mUnbinder = ButterKnife.bind(this, mView);
        //SetUp User Details
        setUserDetails();

        //Execute Home API:
        getHomeScreenData();

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setUserDetails() {
        txtUserNameTV.setText(CapitalizedFullName(getUserName()));

        if (getUserProfilePicture() != null && getUserProfilePicture().contains("http")) {
            Picasso.with(getActivity()).load(getUserProfilePicture())
                    .placeholder(R.drawable.bg_card_t_white)
                    .error(R.drawable.bg_card_t_white)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgCoverPicIV);

        } else {
            imgCoverPicIV.setImageResource(R.drawable.bg_card_t_white);
        }
    }

    /*
     * Widgets click Listner
     * */
    @OnClick({R.id.txtFindArtistTV, R.id.txtAddTattosTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtFindArtistTV:
                perfromFindArtistClick();
                break;
            case R.id.txtAddTattosTV:
                performAddTattoosClick();
                break;
        }
    }

    private void perfromFindArtistClick() {
        ((HomeActivity) getActivity()).perfromTab3Click();
    }

    private void getHomeScreenData() {
        if (ConnectivityReceiver.isConnected()) {
            executeApi();
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }
    }

    /*
     *
     * Execute  SignUp Api
     * */
    private void executeApi() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.CLIENT_HOME_API;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {
        mMyArtistArrayList.clear();
        mMyTattozArrayList.clear();
        try {
            if (response.getString("status").equals("1")) {
                if (!response.isNull("my_artists")) {
                    JSONArray mMyArtistArray = response.getJSONArray("my_artists");

                    for (int i = 0; i < mMyArtistArray.length(); i++) {
                        JSONObject mDataObj = mMyArtistArray.getJSONObject(i);
                        MyArtistModel mMyArtistModel = new MyArtistModel();
                        mMyArtistModel.setUser_id(mDataObj.getString("user_id"));
                        mMyArtistModel.setName(mDataObj.getString("name"));
                        if (!mDataObj.isNull("profile_pic"))
                            mMyArtistModel.setProfile_pic(mDataObj.getString("profile_pic"));

                        if (!mDataObj.isNull("timezoneAbr"))
                            mMyArtistModel.setTimezoneAbr(mDataObj.getString("timezoneAbr"));

                        mMyArtistArrayList.add(mMyArtistModel);
                    }
                }

                if (!response.isNull("my_tattoos")) {
                    JSONArray mMyTattosArray = response.getJSONArray("my_tattoos");
                    for (int i = 0; i < mMyTattosArray.length(); i++) {
                        JSONObject mDataObj = mMyTattosArray.getJSONObject(i);

                        MyTattozModel mMyTattozModel = new MyTattozModel();

                        mMyTattozModel.setId(mDataObj.getString("id"));
                        mMyTattozModel.setUser_id(mDataObj.getString("user_id"));
                        if (!mDataObj.isNull("image"))
                            mMyTattozModel.setImage(mDataObj.getString("image"));

                        mMyTattozArrayList.add(mMyTattozModel);
                    }
                }


                //Set Up Recycler View Data
                if (getDeviceSizeType().equals("mobile_bear")) {
                    setMyArtistAdapterM();
                    setMyTattosAdapterM();
                } else {
                    setMyArtistAdapter7_10();
                    setMyTattosAdapter7_10();
                }

            } else {
                if (getDeviceSizeType().equals("mobile_bear")) {
                    setMyArtistAdapterM();
                    setMyTattosAdapterM();
                } else {
                    setMyArtistAdapter7_10();
                    setMyTattosAdapter7_10();
                }
                showToast(getActivity(), response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }


    MyArtistCallBack mMyArtistCallBack = new MyArtistCallBack() {
        @Override
        public void clickToArtistDetials(MyArtistModel mModel) {
            Intent mIntent = new Intent(getActivity(), ArtistDetailsActivity.class);
            mIntent.putExtra(Constants.MODEL, mModel);
            startActivityForResult(mIntent,555);
        }

        @Override
        public void clickToBookAppointment(MyArtistModel mModel) {

            Intent mIntent = new Intent(getActivity(), BookApointmentActivity.class);
            mIntent.putExtra(Constants.MODEL, mModel);
            startActivity(mIntent);
        }
    };


    private void setMyArtistAdapterM() {
        myArtistsRecyclerViewRV.setNestedScrollingEnabled(false);
        myArtistsRecyclerViewRV.setHasFixedSize(false);
        mMyArtistAdapter = new MyArtistAdapter(getActivity(), mMyArtistArrayList,mMyArtistCallBack);
        RecyclerView.LayoutManager mLayoutManagerC = new GridLayoutManager(getActivity(), 2);
        myArtistsRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        myArtistsRecyclerViewRV.setAdapter(mMyArtistAdapter);
    }

    private void setMyTattosAdapterM() {
        myTattozRecyclerViewRV.setNestedScrollingEnabled(false);
        myTattozRecyclerViewRV.setHasFixedSize(false);
        mMyTattozAdapter = new MyTattozAdapter(getActivity(), mMyTattozArrayList,mClientGalleryClickInterface);
        RecyclerView.LayoutManager mLayoutManagerC = new GridLayoutManager(getActivity(), 2);
        myTattozRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        myTattozRecyclerViewRV.setAdapter(mMyTattozAdapter);
    }

    /*
    *
    * */
    private void setMyArtistAdapter7_10() {
        myArtistsRecyclerViewRV.setNestedScrollingEnabled(false);
        myArtistsRecyclerViewRV.setHasFixedSize(false);
        mMyArtistAdapter = new MyArtistAdapter(getActivity(), mMyArtistArrayList,mMyArtistCallBack);
        RecyclerView.LayoutManager mLayoutManagerC = new GridLayoutManager(getActivity(), 3);
        myArtistsRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        myArtistsRecyclerViewRV.setAdapter(mMyArtistAdapter);
    }

    private void setMyTattosAdapter7_10() {
        myTattozRecyclerViewRV.setNestedScrollingEnabled(false);
        myTattozRecyclerViewRV.setHasFixedSize(false);
        mMyTattozAdapter = new MyTattozAdapter(getActivity(), mMyTattozArrayList,mClientGalleryClickInterface);
        RecyclerView.LayoutManager mLayoutManagerC = new GridLayoutManager(getActivity(), 3);
        myTattozRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        myTattozRecyclerViewRV.setAdapter(mMyTattozAdapter);
    }


    ClientGalleryClickInterface mClientGalleryClickInterface = new ClientGalleryClickInterface() {
        @Override
        public void mClick(MyTattozModel mMyTattozModel, int position) {
            Intent mIntent =new Intent(getActivity(), ClientGalleryImagesActivity.class);
            mIntent.putExtra(Constants.LIST,mMyTattozArrayList);
            mIntent.putExtra(Constants.POSITION,position);
            startActivityForResult(mIntent, Constants.REQUEST_CODE);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }


    /*
     * Add User Tattos with
     * Camera Gallery functionality
     * */
    public void performAddTattoosClick() {
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }


    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(getActivity(), writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(getActivity(), writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(getActivity(), writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }


    private void requestPermission() {
        requestPermissions(new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    public void onSelectImageClick() {
        CropImage.startPickImageActivity(getActivity());
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(100, 100)
                .setMultiTouchEnabled(false)
                .start(getActivity());
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                try {

                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(result.getUri());

                    Bitmap bitmap = BitmapFactory.decodeStream(imageStream);

                    executeUploadTattoImage(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(getActivity(), "Cropping failed: " + result.getError());
            }
        }


        if (data != null && requestCode == Constants.REQUEST_CODE){
            int position = data.getIntExtra(Constants.POSITION,0);
            mMyTattozArrayList.remove(position);
            mMyTattozAdapter.notifyDataSetChanged();
        }

        if (data != null && requestCode == 555){
           getHomeScreenData();
        }

    }

    private void executeUploadTattoImage(Bitmap mBitmap) {
        showProgressDialog(getActivity());

        String ApiUrl = Constants.ADD_TATTOOS;
        Log.e(TAG, "Api Url::::" + ApiUrl);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                ApiUrl, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                dismissProgressDialog();
                String resultResponse = new String(response.data);
                Log.e(TAG, "onResponse: " + resultResponse);
                parseImageUploadResponse(resultResponse);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                String strBody = "";
                if (error.networkResponse != null) {
                    try {
                        strBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            showAlertDialog(getActivity(), getString(R.string.internet_connection_error));
                        } else if (error instanceof AuthFailureError) {
                            showAlertDialog(getActivity(), getString(R.string.auth_failed));
                        } else if (error instanceof ServerError) {
                            showAlertDialog(getActivity(), getString(R.string.server_error));
                        } else if (error instanceof NetworkError) {
                            showAlertDialog(getActivity(), getString(R.string.internet_connection_error));
                        } else if (error instanceof ParseError) {
                            showAlertDialog(getActivity(), getString(R.string.parse_error));
                        } else {
                            showAlertDialog(getActivity(), strBody);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    showAlertDialog(getActivity(), getString(R.string.internet_connection_error));
                }
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", getUserID());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "multipart/form-data");
                return headers;


            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                if (mBitmap != null)
                    params.put("image", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(getActivity(), mBitmap), "image/jpeg"));
                return params;
            }
        };

        TheTattzApplication.getInstance().addToRequestQueue(multipartRequest);
    }

    private void parseImageUploadResponse(String resultResponse) {
        try {
            JSONObject mJsonObject = new JSONObject(resultResponse);
            if (mJsonObject.getString("status").equals("1")) {
                mMyTattozArrayList.clear();
                JSONObject mDataObject = mJsonObject.getJSONObject("data");
                JSONArray mMyTattosArray = mDataObject.getJSONArray("images");
                for (int i = 0; i < mMyTattosArray.length(); i++) {
                    JSONObject mDataObj = mMyTattosArray.getJSONObject(i);

                    MyTattozModel mMyTattozModel = new MyTattozModel();

                    mMyTattozModel.setId(mDataObj.getString("id"));
                    mMyTattozModel.setUser_id(mDataObj.getString("user_id"));
                    if (!mDataObj.isNull("image"))
                        mMyTattozModel.setImage(mDataObj.getString("image"));

                    mMyTattozArrayList.add(mMyTattozModel);
                }

                //mMyTattozAdapter.notifyDataSetChanged();
                //Set Up Recycler View Data
                if (getDeviceSizeType().equals("mobile_bear")) {
                    setMyTattosAdapterM();
                } else {
                    setMyTattosAdapter7_10();
                }

            } else {
                showToast(getActivity(), mJsonObject.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }



}
