package com.thetattz.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.thetattz.R;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class AppointmentFragment extends BaseFragment {

    /**
     * Getting the Current Class Name
     */
    String TAG = AppointmentFragment.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    @BindView(R.id.txtUpcomingTabTV)
    TextView txtUpcomingTabTV;
    @BindView(R.id.viewUpcomingTabV)
    View viewUpcomingTabV;
    @BindView(R.id.layoutUpcomingLL)
    LinearLayout layoutUpcomingLL;
    @BindView(R.id.txtPastTV)
    TextView txtPastTV;
    @BindView(R.id.viewPastTabV)
    View viewPastTabV;
    @BindView(R.id.layoutPastLL)
    LinearLayout layoutPastLL;
    @BindView(R.id.containerAppointMentFL)
    FrameLayout containerAppointMentFL;

    /*
     * Initialize  Unbinder for butterknife;
     * */
    private Unbinder mUnbinder;

    /*
     *
     * Initialize Objects...
     * */

    /*
     * Default ConstructorArtistAppointmentsFragment
     * */
    public AppointmentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_appointment, container, false);
        setStatusBar();
        mUnbinder = ButterKnife.bind(this, mView);

        performUpcomingClick();

        if (!TheTattzPrefrences.readString(getActivity(), Constants.APPOINTMENT_TYPE, "").equals("") && TheTattzPrefrences.readString(getActivity(), Constants.APPOINTMENT_TYPE, "").equals(getActivity().getResources().getString(R.string.type_confirm)) && TheTattzPrefrences.readString(getActivity(), Constants.TYPES, "").equals(getActivity().getResources().getString(R.string.type_client))) {
            performUpcomingClick();
        } else if (!TheTattzPrefrences.readString(getActivity(), Constants.APPOINTMENT_TYPE, "").equals("")) {
            performPastClick();
        }
        return mView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    /*
     * Widgets click listner
     * */
    @OnClick({R.id.layoutUpcomingLL, R.id.layoutPastLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutUpcomingLL:
                performUpcomingClick();
                break;
            case R.id.layoutPastLL:
                performPastClick();
                break;
        }
    }

    private void performUpcomingClick() {
        txtUpcomingTabTV.setTextColor(getResources().getColor(R.color.colorWhite));
        viewUpcomingTabV.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        txtPastTV.setTextColor(getResources().getColor(R.color.colorWhite));
        viewPastTabV.setBackgroundColor(getResources().getColor(R.color.colorTrasperant));

        switchFragment(getActivity(), new AppointmentUpcomingFragment(), false, null);
    }

    private void performPastClick() {
        txtUpcomingTabTV.setTextColor(getResources().getColor(R.color.colorWhite));
        viewUpcomingTabV.setBackgroundColor(getResources().getColor(R.color.colorTrasperant));
        txtPastTV.setTextColor(getResources().getColor(R.color.colorWhite));
        viewPastTabV.setBackgroundColor(getResources().getColor(R.color.colorWhite));

        switchFragment(getActivity(), new AppointmentPastFragment(), false, null);
    }

    /********
     *Replace Fragment In Activity
     **********/
    private void switchFragment(Activity activity, Fragment fragment, boolean addToStack, Bundle bundle) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.containerAppointMentFL, fragment);
            if (addToStack)
                ft.addToBackStack(null);
            if (bundle != null)
                fragment.setArguments(bundle);
            ft.commitAllowingStateLoss();
        }
    }
}
