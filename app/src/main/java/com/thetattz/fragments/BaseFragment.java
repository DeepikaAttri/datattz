package com.thetattz.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.aigestudio.wheelpicker.BuildConfig;
import com.thetattz.R;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class BaseFragment extends Fragment {


    /*
     * Get Is user login or not
     * */
    public boolean isLogin() {
        return TheTattzPrefrences.readBoolean(getActivity(), TheTattzPrefrences.ISLOGIN, false);
    }

    /*
     * Get Is user id
     * */
    public String getUserID() {
        return TheTattzPrefrences.readString(getActivity(), TheTattzPrefrences.USER_ID, "");
    }

    /*
     * Get Is user Email
     * */
    public String getUserEmail() {
        return TheTattzPrefrences.readString(getActivity(), TheTattzPrefrences.USER_EMAIL, "");
    }

    /*
     * Get Is user Email
     * */
    public String getUserRole() {
        return TheTattzPrefrences.readString(getActivity(), TheTattzPrefrences.USER_ROLE, "");
    }

    /*
     * Get Is user Name
     * */
    public String getUserName() {
        return TheTattzPrefrences.readString(getActivity(), TheTattzPrefrences.USER_NAME, "");
    }

    /*
     * Get Is user Profile Pic
     * */
    public String getUserProfilePicture() {
        return TheTattzPrefrences.readString(getActivity(), TheTattzPrefrences.USER_PROFILE_PIC, "");
    }

    /*
     * Get Current Latitude
     * */
    public String getCurrentLatitude() {
        return TheTattzPrefrences.readString(getActivity(), TheTattzPrefrences.CURRENT_LOCATION_LATITUDE, "");
    }

    /*
     * Get Current Longitude
     * */
    public String getCurrentLongitude() {
        return TheTattzPrefrences.readString(getActivity(), TheTattzPrefrences.CURRENT_LOCATION_LONGITUDE, "");
    }


    /*
     * Artist Home Selected TAB
     * */
    public int getArtistHomeSelectedTab() {
        return TheTattzPrefrences.readInteger(getActivity(), TheTattzPrefrences.ARTIST_HOME_SELECTED_TAB, 0);
    }


    /*
     * Get getAccountDetailsID
     * */
    public String getAccountDetailsID() {
        return TheTattzPrefrences.readString(getActivity(), TheTattzPrefrences.ACCOUNT_DETAIL_ID, "");
    }

    /*
     * Get PayPal Verification
     * */
    public String getPaypalVerification() {
        return TheTattzPrefrences.readString(getActivity(), TheTattzPrefrences.PAYPAL_VERIFICATION, "");
    }

    /*
     * Get PayPal Email
     * */
    public String getPaypalEmail() {
        return TheTattzPrefrences.readString(getActivity(), TheTattzPrefrences.PAYPAL_EMAIL_ID, "");
    }

    /*
     * Get Is subscription active or not
     * */
    public boolean isSubscriptionActive() {
        return TheTattzPrefrences.readBoolean(getActivity(), TheTattzPrefrences.isSubscriptionActive, false);
    }

    private Dialog progressDialog;


    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);

        progressDialog.show();


    }

    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /*
     * Hide Keyboard
     * */
    public boolean hideKeyBoad(Context context, View view) {
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        return true;
    }

    /*
     * Validate Email Address
     * */
    public boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    /*
     * Share Gmail Intent
     * */
    public void shareIntentGmail(Activity mActivity) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "");
        mActivity.startActivity(Intent.createChooser(sharingIntent, mActivity.getString(R.string.app_name)));
    }


    /*
     * Error Toast Message
     * */
    public void showToast(Activity mActivity, String strMessage) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
    }


    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    /*
     *
     * Convert Simple Date to Formated Date
     * */
    public String getConvertedDate(String mString) {
        String[] mArray = mString.split("T");
        String formattedDate = "";
        try {
            DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd");

            // parse the date string into Date object
            Date date = srcDf.parse(mArray[0]);

            DateFormat destDf = new SimpleDateFormat("dd MMMM, yyyy");

            // format the date into another format
            formattedDate = destDf.format(date);

            System.out.println("Converted date is : " + formattedDate);
        } catch (Exception e) {
            e.toString();
        }

        return formattedDate;
    }


    //used to display only time
    public static long timeConverterLong(String strDate) {
        long millisecondsSinceEpoch = 0;
        SimpleDateFormat f = new SimpleDateFormat("dd MMMM, yyyy");
        Date parseDate = null;
        try {
            parseDate = f.parse(strDate);
            millisecondsSinceEpoch = parseDate.getTime();
            return millisecondsSinceEpoch;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return millisecondsSinceEpoch;
    }

    //Remove E from Double Value
    public static String convertEvalueToNormal(double value) //Got here 6.743240136E7 or something..
    {
        DecimalFormat formatter;

        if (value - (int) value > 0.0)
            formatter = new DecimalFormat("0.0"); // Here you can also deal with rounding if you wish..
        else
            formatter = new DecimalFormat("0.0");


        String money = formatter.format(value).replace(',', '.');

        return money;
    }


    /*
     * Convert Time String to Milliseconds
     * */
    public long convertTimeStringToLong(String strDate) {
        long millisecondsSinceEpoch = 0;
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
        Date parseDate = null;
        try {
            parseDate = f.parse(strDate);
            millisecondsSinceEpoch = parseDate.getTime();
            return millisecondsSinceEpoch;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return millisecondsSinceEpoch;
    }


    /*
     * Convert Time Milliseconds to String
     * @Return date in specified format.
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format
     * @return String representing date in specified format
     * */
    public String convertTimeLongToString(Long mMilliseconds) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mMilliseconds);
        return formatter.format(calendar.getTime());
    }

    /*
     * Convert Time Milliseconds to String
     * @Return date in specified format.
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format
     * @return String representing date in specified format
     * */
    public String convertTimeLongToStringMonthYear(Long mMilliseconds) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mMilliseconds);
        return formatter.format(calendar.getTime());
    }


    /*
     *
     * function to convert imageView to Bitmap
     * */
    public Bitmap convertImageViewToBitmap(ImageView v) {

        Bitmap bm = ((BitmapDrawable) v.getDrawable()).getBitmap();

        return bm;
    }


    /*
     * Generate String Dynamically
     * */
    public String getAlphaNumericString() {
        int n = 20;

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }


    /*
     * Getting Screen Type: Like mobile phone, 7'' tablet, 10'' tablet
     * */
    public String getDeviceSizeType() {
        String strType = getActivity().getResources().getString(R.string.screen_type);
        return strType;
    }


    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    @SuppressLint("NewApi")
    public String getRealPathFromURI_API19(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }


    /*
     * Share app links
     * */
    public void shareAppLink() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            String shareMessage = "\nLet me recommend this application to you.\n\n";
//            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=com.thetattz";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "Choose One"));
        } catch (Exception e) {
        }
    }

    /*
     * Share artist details
     * */
    public void shareArtistDetails(String shareMessage) {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "Choose One"));
        } catch (Exception e) {
        }
    }

    /*
     * Get Hours List
     * */
    public List<String> getHours() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.HOURS.length; i++) {
            mArrayList.add(Constants.HOURS[i]);
        }
        return mArrayList;
    }

    /*
     * Get Minutes List
     * */
    public List<String> getMinutes() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.MINUTESS.length; i++) {
            mArrayList.add(Constants.MINUTESS[i]);
        }
        return mArrayList;
    }

    /*
     * Get AM_PM List
     * */
    public List<String> getAmPm() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.AMPM.length; i++) {
            mArrayList.add(Constants.AMPM[i]);
        }
        return mArrayList;
    }

    /*
     * Get Months List
     * */
    public List<String> getMonths() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.MONTHS_OF_YEAR.length; i++) {
            mArrayList.add(Constants.MONTHS_OF_YEAR[i]);
        }
        return mArrayList;
    }

    /*
     * Get Years List
     * */
    public List<String> getYears() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.YEARS.length; i++) {
            mArrayList.add(Constants.YEARS[i]);
        }
        return mArrayList;
    }


    /*
     *
     * Convert Month Name to Month Number
     * */
    public String getMonthNumberFromName(String strMonth) {
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[0])) {
            return "01";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[1])) {
            return "02";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[2])) {
            return "03";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[3])) {
            return "04";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[4])) {
            return "05";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[5])) {
            return "06";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[6])) {
            return "07";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[7])) {
            return "08";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[8])) {
            return "09";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[9])) {
            return "10";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[10])) {
            return "11";
        }
        if (strMonth.equals(Constants.MONTHS_OF_YEAR[11])) {
            return "12";
        } else {
            return null;
        }

    }


    /*
     * Date Picker Dialog
     * */
    public void showDatePickerDialog(Activity mActivity, final TextView mTextView) {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int intMonth = monthOfYear + 1;
                        mTextView.setText(getFormatedString("" + dayOfMonth) + "/" + getFormatedString("" + intMonth) + "/" + year);
                    }
                }, mYear, mMonth, mDay);

        //Set Minimum Date
        //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        //Set Maximum Date
        //datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 568025136000L);

        datePickerDialog.show();
    }

    public String getFormatedString(String strTemp) {
        String strActual = "";

        if (strTemp.length() == 1) {
            strActual = "0" + strTemp;
        } else if (strTemp.length() == 2) {
            strActual = strTemp;
        }

        return strActual;
    }


    /*
     * Get Day Number //21-04-2020//dd-MM-yyyy
     * */
    public String convertTimeToDayNumber(String input) {
        String mArray[] = input.split("-");

        return mArray[0];
    }

    /*
     * Get Month Name //21-04-2020
     * */
    public String convertTimeToMonthName3Digit(String input) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat inFormat = new SimpleDateFormat("dd-MM-yyyy");

        Date date = null;
        try {
            date = inFormat.parse(input);
        } catch (ParseException e) {
        }

        SimpleDateFormat outFormat = new SimpleDateFormat("MMM");
        String goal = outFormat.format(date);

        return goal;
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFullName(String fullname) {
        String[] firstLastName = {};
        String FirstName = "", LastName = "";
        if (fullname != null && fullname.contains(" ")) {
            firstLastName = fullname.split(" ");
            FirstName = fullname.substring(0, fullname.lastIndexOf(' '));
            LastName = fullname.substring(fullname.lastIndexOf(" ")+1);
        } else {
            FirstName = fullname;
        }
        String fname = "";
        String lname = "";
        if (FirstName != null && !FirstName.equals("")) {
            fname = FirstName.substring(0, 1).toUpperCase() + FirstName.substring(1).toLowerCase();
        }
        if (LastName != null && !LastName.equals("")) {
            lname = LastName.substring(0, 1).toUpperCase() + LastName.substring(1).toLowerCase();
        }
        String name = fname + " " + lname;

        return name;
    }

    //Capitalized first letter of String
    public String CapitalizedString(String myString) {
        String upperString = myString.substring(0, 1).toUpperCase() + myString.substring(1).toLowerCase();

        return upperString;
    }

    /*
     * Set Up Status Bar
     * */
    public void setStatusBar() {
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark
        getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));// set status background white
    }
}
