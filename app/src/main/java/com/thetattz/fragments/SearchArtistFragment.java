package com.thetattz.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.adapters.SearchArtistAdapter;
import com.thetattz.fonts.EditTextRegular;
import com.thetattz.models.SearchArtistModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class SearchArtistFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = SearchArtistFragment.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    @BindView(R.id.editSearchArtistET)
    EditTextRegular editSearchArtistET;
    @BindView(R.id.editSearchLocationET)
    EditTextRegular editSearchLocationET;
    @BindView(R.id.searchRecyclerViewRV)
    RecyclerView searchRecyclerViewRV;

    /*
     * Initialize  Unbinder for butterknife;
     * */
    private Unbinder mUnbinder;

    /*
     *
     * Initialize Objects...
     * */
    SearchArtistAdapter mSearchAdapter;
    ArrayList<SearchArtistModel> mSearchModelArrayList = new ArrayList<>();

    /*
     * Default Constructor
     * */
    public SearchArtistFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_search, container, false);
        setStatusBar();
        mUnbinder = ButterKnife.bind(this, mView);
        performSearchAPI();
        setUpSearchonBoth();
        return mView;
    }

    private void setUpSearchonBoth() {
        editSearchLocationET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Your piece of code on keyboard search click
                    getSearchApi();
                    return true;
                }
                return false;
            }
        });
    }


    private void getSearchApi() {
        if (ConnectivityReceiver.isConnected())
            executeArtistDataApi();
        else
            showToast(getActivity(), getString(R.string.internet_connection_error));
    }


    private void performSearchAPI() {
        if (ConnectivityReceiver.isConnected())
            executeApi();
        else
            showAlertDialog(getActivity(), getString(R.string.internet_connection_error));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }


    /*
     *
     * Execute Search Api
     * */
    private void executeApi() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.SEARCH_ARTIST;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            mParams.put("user_latitude", getCurrentLatitude());
            mParams.put("user_longitude", getCurrentLongitude());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }


        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {
        mSearchModelArrayList.clear();
        try {
            if (response.getString("status").equals("1")) {
                if (!response.isNull("data")) {
                    JSONArray mJsonArray = response.getJSONArray("data");
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mDataObj = mJsonArray.getJSONObject(i);
                        SearchArtistModel mSearchArtistModel = new SearchArtistModel();
                        if (!mDataObj.isNull("user_id"))
                            mSearchArtistModel.setUser_id(mDataObj.getString("user_id"));
                        if (!mDataObj.isNull("name"))
                            mSearchArtistModel.setName(mDataObj.getString("name"));
                        if (!mDataObj.isNull("email"))
                            mSearchArtistModel.setEmail(mDataObj.getString("email"));
                        if (!mDataObj.isNull("phone"))
                            mSearchArtistModel.setPhone(mDataObj.getString("phone"));
                        if (!mDataObj.isNull("password"))
                            mSearchArtistModel.setPassword(mDataObj.getString("password"));
                        if (!mDataObj.isNull("profile_pic"))
                            mSearchArtistModel.setProfile_pic(mDataObj.getString("profile_pic"));
                        if (!mDataObj.isNull("shopname"))
                            mSearchArtistModel.setShopname(mDataObj.getString("shopname"));
                        if (!mDataObj.isNull("street_address"))
                            mSearchArtistModel.setStreet_address(mDataObj.getString("street_address"));
                        if (!mDataObj.isNull("building_name"))
                            mSearchArtistModel.setBuilding_name(mDataObj.getString("building_name"));
                        if (!mDataObj.isNull("city"))
                            mSearchArtistModel.setCity(mDataObj.getString("city"));
                        if (!mDataObj.isNull("role"))
                            mSearchArtistModel.setRole(mDataObj.getString("role"));
                        if (!mDataObj.isNull("enable"))
                            mSearchArtistModel.setEnable(mDataObj.getString("enable"));
                        if (!mDataObj.isNull("creation_time"))
                            mSearchArtistModel.setCreation_time(mDataObj.getString("creation_time"));
                        if (!mDataObj.isNull("rating"))
                            mSearchArtistModel.setRating(mDataObj.getString("rating"));


                        mSearchModelArrayList.add(mSearchArtistModel);
                    }
                }
                //Set Up Recycler View Data
                setAdapter();

            } else {
                showToast(getActivity(), response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    private void setAdapter() {
        if (mSearchModelArrayList.size() > 0)
            searchRecyclerViewRV.setBackgroundColor(getResources().getColor(R.color.colorBlueBGBox));

        mSearchAdapter = new SearchArtistAdapter(getActivity(), mSearchModelArrayList);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(getActivity());
        searchRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        searchRecyclerViewRV.setAdapter(mSearchAdapter);
    }


    private void executeArtistDataApi() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.SEARCH_ARTIST_LOCATION;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            mParams.put("name", editSearchArtistET.getText().toString().trim());
            mParams.put("location", editSearchLocationET.getText().toString().trim());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }


        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }
}
