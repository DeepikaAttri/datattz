package com.thetattz.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.BookApointmentActivity;
import com.thetattz.activities.artist.SwipeImgesActivity;
import com.thetattz.adapters.ArtistDetailsPhotosAdapter;
import com.thetattz.adapters.ArtistDetailsPhotosAdapter_7_10;
import com.thetattz.adapters.WorkInfoAdapter;
import com.thetattz.chat.ChatActivity;
import com.thetattz.fonts.TextViewBold;
import com.thetattz.fonts.TextViewRegular;
import com.thetattz.interfaces.SeeMoreClickInterface;
import com.thetattz.models.ArtistAppointmentDetailsModel;
import com.thetattz.models.MyArtistModel;
import com.thetattz.models.MyTattozModel;
import com.thetattz.models.RoomIdModel;
import com.thetattz.models.SearchArtistModel;
import com.thetattz.models.WorkInfo;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzSingleton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistInfoFragment extends BaseFragment {

    /**
     * Getting the Current Class Name
     */
    String TAG = ArtistInfoFragment.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    @BindView(R.id.txtNameTV)
    TextViewBold txtNameTV;
    @BindView(R.id.mRatingBarRB)
    MaterialRatingBar mRatingBarRB;
    @BindView(R.id.favoriteLL)
    LinearLayout favoriteLL;
    @BindView(R.id.bookLL)
    LinearLayout bookLL;
    @BindView(R.id.shareLL)
    LinearLayout shareLL;
    @BindView(R.id.txtAddressTV)
    TextViewRegular txtAddressTV;
    @BindView(R.id.txtAddress2TV)
    TextViewRegular txtAddress2TV;
    @BindView(R.id.dayTimeRecyclerViewRV)
    RecyclerView dayTimeRecyclerViewRV;
    @BindView(R.id.myPhotosRecyclerViewRV)
    RecyclerView myPhotosRecyclerViewRV;
    @BindView(R.id.imgBackGroundIV)
    ImageView imgBackGroundIV;
    @BindView(R.id.imgFavUnFavIV)
    ImageView imgFavUnFavIV;
    @BindView(R.id.timezoneTV)
    TextView timezoneTV;
    /*
     *
     * Initialize Objects...
     * */
    String strArtistID = "";
    String strFavOrNotStatus = "";
    SearchArtistModel mSearchModel = new SearchArtistModel();
    MyArtistModel mMyArtistModel;
    ArrayList<WorkInfo> mWorkInfoArrayList = TheTattzSingleton.getInstance().getWorkInfoArrayList();
    WorkInfoAdapter mWorkInfoAdapter;
    ArrayList<MyTattozModel> mPhotosArrayList = TheTattzSingleton.getInstance().getPhotosArrayList();
    ArtistDetailsPhotosAdapter mArtistPhotosAdapter;
    ArtistDetailsPhotosAdapter_7_10 mArtistPhotosAdapter_7_10;
    ArtistAppointmentDetailsModel mModel = new ArtistAppointmentDetailsModel();
    /*
     * Initialize  Unbinder for butterknife;
     * */
    private Unbinder mUnbinder;

    /*
     * Default Constructor
     * */

    public ArtistInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_artist_info, container, false);
        setStatusBar();
        mUnbinder = ButterKnife.bind(this, mView);
        //Get Intent Data
        getArgumentsData();
        //Apply Rating to Artist
        setRatingBarLisnter();
        return mView;
    }


    private void setRatingBarLisnter() {
        mRatingBarRB.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if (b)
                    setApplyRatingToAritst();
            }
        });


    }

    private void setApplyRatingToAritst() {
        if (ConnectivityReceiver.isConnected())
            executeRatingAPI();
        else
            showToast(getActivity(), getString(R.string.internet_connection_error));
    }


    private void getArgumentsData() {
        if (getArguments() != null && getArguments().getSerializable(Constants.MODEL) != null) {
            mMyArtistModel = (MyArtistModel) getArguments().getSerializable(Constants.MODEL);
            strArtistID = mMyArtistModel.getUser_id();
            //Get User Details
            getArtistDetails();
        } else if (getArguments() != null && getArguments().getSerializable(Constants.MODEL_NEW) != null) {
            mSearchModel = (SearchArtistModel) getArguments().getSerializable(Constants.MODEL_NEW);
            strArtistID = mSearchModel.getUser_id();
            getArtistDetails();
        }
    }

    private void getArtistDetails() {
        if (ConnectivityReceiver.isConnected())
            executeArtistDetailsAPI();
        else
            showToast(getActivity(), getString(R.string.internet_connection_error));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }


    /*Execute Get User Details API*/
    private void executeArtistDetailsAPI() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.GET_USER_DETAILS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", strArtistID);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseDetailsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }
        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseDetailsResponse(JSONObject response) {
        mWorkInfoArrayList.clear();
        mPhotosArrayList.clear();
        try {
            if (response.getString("status").equals("1")) {
                JSONObject mDataObject = response.getJSONObject("data");

                mSearchModel.setUser_id(mDataObject.getString("user_id"));
                if (!mDataObject.isNull("name"))
                    mSearchModel.setName(mDataObject.getString("name"));
                if (!mDataObject.isNull("email"))
                    mSearchModel.setEmail(mDataObject.getString("email"));
                if (!mDataObject.isNull("profile_pic"))
                    mSearchModel.setProfile_pic(mDataObject.getString("profile_pic"));
                if (!mDataObject.isNull("shopname"))
                    mSearchModel.setShopname(mDataObject.getString("shopname"));
                if (!mDataObject.isNull("building_name"))
                    mSearchModel.setBuilding_name(mDataObject.getString("building_name"));
                if (!mDataObject.isNull("street_address"))
                    mSearchModel.setStreet_address(mDataObject.getString("street_address"));
                if (!mDataObject.isNull("city"))
                    mSearchModel.setCity(mDataObject.getString("city"));
                if (!mDataObject.isNull("timezone"))
                    mSearchModel.setTimezone(mDataObject.getString("timezone"));
                if (!mDataObject.isNull("timezoneAbr"))
                    mSearchModel.setTimezoneAbr(mDataObject.getString("timezoneAbr"));
                if (!mDataObject.isNull("rating"))
                    mSearchModel.setRating(mDataObject.getString("rating"));

                //Work Info
                if (!response.isNull("work_info")) {
                    JSONArray mWorkInfoArray = response.getJSONArray("work_info");
                    for (int i = 0; i < mWorkInfoArray.length(); i++) {
                        JSONObject mWorkInfoObj = mWorkInfoArray.getJSONObject(i);
                        WorkInfo mWorkInfo = new WorkInfo();
                        mWorkInfo.setDay(mWorkInfoObj.getString("day"));
                        mWorkInfo.setStart_time(mWorkInfoObj.getString("start_time"));
                        mWorkInfo.setEnd_time(mWorkInfoObj.getString("end_time"));
                        if (!mWorkInfoObj.isNull("start_time") && mWorkInfoObj.getString("start_time").length() > 0) {
                            mWorkInfoArrayList.add(mWorkInfo);
                        }

                    }
                }

                //Photos
                if (!response.isNull("my_tattoos")) {
                    JSONArray mTattosArray = response.getJSONArray("my_tattoos");
                    for (int i = 0; i < mTattosArray.length(); i++) {
                        JSONObject mWorkInfoObj = mTattosArray.getJSONObject(i);
                        MyTattozModel mMyTattozModel = new MyTattozModel();
                        mMyTattozModel.setId(mWorkInfoObj.getString("id"));
                        mMyTattozModel.setUser_id(mWorkInfoObj.getString("user_id"));
                        mMyTattozModel.setImage(mWorkInfoObj.getString("image"));

                        mPhotosArrayList.add(mMyTattozModel);
                    }
                }

                setDataOnWidgets();
            } else {
                showToast(getActivity(), response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }

    private void setDataOnWidgets() {
        //Collection User Data
        if (mSearchModel.getProfile_pic() != null && mSearchModel.getProfile_pic().contains("http"))
            Picasso.with(getActivity()).load(mSearchModel.getProfile_pic())
                    .placeholder(R.drawable.bg_card_t_white)
                    .error(R.drawable.bg_card_t_white)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgBackGroundIV);
        else
            imgBackGroundIV.setImageResource(R.drawable.bg_card_t_white);


        txtNameTV.setText(CapitalizedFullName(mSearchModel.getName()));

        //Set Address Validations
        if (mSearchModel.getShopname().length() == 0 && mSearchModel.getBuilding_name().length() == 0) {
            txtAddressTV.setText(getString(R.string.not_specified));
        } else {
            if (mSearchModel.getShopname() != null && mSearchModel.getBuilding_name() != null && mSearchModel.getShopname().length() > 0 && mSearchModel.getBuilding_name().length() > 0) {
                txtAddressTV.setText(mSearchModel.getShopname() + ", " + mSearchModel.getBuilding_name());
            } else if (mSearchModel.getShopname() != null && mSearchModel.getBuilding_name() != null && mSearchModel.getShopname().length() > 0 && mSearchModel.getBuilding_name().length() == 0) {
                txtAddressTV.setText(mSearchModel.getShopname());
            } else if (mSearchModel.getShopname() != null && mSearchModel.getBuilding_name() != null && mSearchModel.getShopname().length() == 0 && mSearchModel.getBuilding_name().length() > 0) {
                txtAddressTV.setText(mSearchModel.getBuilding_name());
            }
        }

        if (mSearchModel.getStreet_address().length() == 0 && mSearchModel.getCity().length() == 0) {
            txtAddress2TV.setText(getString(R.string.not_specified));
        } else {
            if (mSearchModel.getStreet_address() != null && mSearchModel.getCity() != null && mSearchModel.getStreet_address().length() > 0 && mSearchModel.getCity().length() > 0) {
                txtAddress2TV.setText(mSearchModel.getStreet_address() + ", " + mSearchModel.getCity());
            } else if (mSearchModel.getStreet_address() != null && mSearchModel.getCity() != null && mSearchModel.getStreet_address().length() > 0 && mSearchModel.getCity().length() == 0) {
                txtAddress2TV.setText(mSearchModel.getStreet_address());
            } else if (mSearchModel.getStreet_address() != null && mSearchModel.getCity() != null && mSearchModel.getStreet_address().length() == 0 && mSearchModel.getCity().length() > 0) {
                txtAddress2TV.setText(mSearchModel.getCity());
            }
        }

//        txtAddressTV.setText(mSearchModel.getShopname() + "," + mSearchModel.getBuilding_name() + "," + mSearchModel.getStreet_address() + "," + mSearchModel.getCity());

        if (mSearchModel.getRating() != null && mSearchModel.getRating().length() > 0)
            mRatingBarRB.setRating(Float.parseFloat(mSearchModel.getRating()));

        //Set Work Info Adapter
        setWorkInfoAdapter();

        //set Photos Adapter
        if (getDeviceSizeType().equals("mobile_bear")) {
            setPhotosAdapter();
        } else {
            setPhotosAdapter7_10();
        }

        if (mSearchModel.getTimezoneAbr() != null)
            timezoneTV.setText("Note: Times are displayed in " + mSearchModel.getTimezoneAbr() + ".");

        //Check Is User Favorite or Not
//        executeCheckUserIsFavOrNot();
    }


    private void setWorkInfoAdapter() {
        dayTimeRecyclerViewRV.setNestedScrollingEnabled(false);
        dayTimeRecyclerViewRV.setHasFixedSize(false);
        mWorkInfoAdapter = new WorkInfoAdapter(getActivity(), mWorkInfoArrayList);
        RecyclerView.LayoutManager mLayoutManagerC = new GridLayoutManager(getActivity(), 2);
        dayTimeRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        dayTimeRecyclerViewRV.setAdapter(mWorkInfoAdapter);
    }

    private void setPhotosAdapter() {
        myPhotosRecyclerViewRV.setNestedScrollingEnabled(false);
        myPhotosRecyclerViewRV.setHasFixedSize(false);
        mArtistPhotosAdapter = new ArtistDetailsPhotosAdapter(getActivity(), mPhotosArrayList, new SeeMoreClickInterface() {
            @Override
            public void mClick(MyTattozModel mMyTattozModel, int position) {
                Intent mIntent = new Intent(getActivity(), SwipeImgesActivity.class);
                mIntent.putExtra(Constants.LIST, mPhotosArrayList);
                mIntent.putExtra(Constants.POSITION, position);
                startActivityForResult(mIntent, Constants.REQUEST_CODE);
            }
        });
        RecyclerView.LayoutManager mLayoutManagerC = new GridLayoutManager(getActivity(), 2);
        myPhotosRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        myPhotosRecyclerViewRV.setAdapter(mArtistPhotosAdapter);
    }

    private void setPhotosAdapter7_10() {
        myPhotosRecyclerViewRV.setNestedScrollingEnabled(false);
        myPhotosRecyclerViewRV.setHasFixedSize(false);
        mArtistPhotosAdapter_7_10 = new ArtistDetailsPhotosAdapter_7_10(getActivity(), mPhotosArrayList, new SeeMoreClickInterface() {
            @Override
            public void mClick(MyTattozModel mMyTattozModel, int position) {
                Intent mIntent = new Intent(getActivity(), SwipeImgesActivity.class);
                mIntent.putExtra(Constants.LIST, mPhotosArrayList);
                mIntent.putExtra(Constants.POSITION, position);
                startActivityForResult(mIntent, Constants.REQUEST_CODE);
            }
        });
        RecyclerView.LayoutManager mLayoutManagerC = new GridLayoutManager(getActivity(), 3);
        myPhotosRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        myPhotosRecyclerViewRV.setAdapter(mArtistPhotosAdapter_7_10);
    }

    @OnClick({R.id.favoriteLL, R.id.bookLL, R.id.shareLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.favoriteLL:
//                performFavoriteClick();
                executeChatRoomID();
                break;
            case R.id.bookLL:
                performBookClick();
                break;
            case R.id.shareLL:
                String ArtistDetails = "Artist Name: " + mSearchModel.getName() + ", " + "Email: " + mSearchModel.getEmail() + ", " + mSearchModel.getShopname() + ", " + "Address: " + mSearchModel.getBuilding_name() + ", " + mSearchModel.getStreet_address() + ", " + mSearchModel.getCity();
                shareArtistDetails(ArtistDetails);
                break;
        }
    }

    private void performBookClick() {
        Intent mIntent = new Intent(getActivity(), BookApointmentActivity.class);
        if (mMyArtistModel != null && mMyArtistModel.getUser_id().length() > 0)
            mIntent.putExtra(Constants.MODEL, mMyArtistModel);

        if (mSearchModel != null && mSearchModel.getUser_id().length() > 0)
            mIntent.putExtra(Constants.MODEL_NEW, mSearchModel);

        getActivity().startActivity(mIntent);
    }

    /*Execute Get User Details API*/
    private void executeRatingAPI() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.ADD_RATING_TO_ARTIST;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            mParams.put("artist_id", strArtistID);
            mParams.put("rating", "" + mRatingBarRB.getRating());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseRatingResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseRatingResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                showToast(getActivity(), response.getString("message"));
                mRatingBarRB.setRating(Float.parseFloat(response.getString("rating")));
            } else {
                showToast(getActivity(), response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }


    /*
     * Execute Check Is Artist is Favorite or Not
     * */
    private void executeCheckUserIsFavOrNot() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.CHECK_USER_FAV_OR_NOT;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            mParams.put("artist_id", strArtistID);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseFavOrNotResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseFavOrNotResponse(JSONObject response) {
        //{"status":1,"message":"Artist exists in favourite list"}
        try {
            if (response.getString("status").equals("1")) {
                strFavOrNotStatus = "1";
//                imgFavUnFavIV.setImageResource(R.drawable.ic_unfav);
            } else {
                strFavOrNotStatus = "0";
//                imgFavUnFavIV.setImageResource(R.drawable.ic_fav);
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }

    private void performFavoriteClick() {
        if (strFavOrNotStatus.equals("1")) {
            executeRemoveFavoriteApi();
        } else if (strFavOrNotStatus.equals("0")) {
            executeAddFavoriteApi();
        }
    }

    /*
     * Execute Artist Favorite Api
     * */
    private void executeAddFavoriteApi() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.ADD_FAVORITE;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            mParams.put("artist_id", strArtistID);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseFavoriteResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseFavoriteResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                strFavOrNotStatus = "1";
//                imgFavUnFavIV.setImageResource(R.drawable.ic_unfav);
                showToast(getActivity(), getString(R.string.add_to_fav));
            } else {
                showToast(getActivity(), response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }

    /*
     * Execute Artist Remove Favorite Api
     * */
    private void executeRemoveFavoriteApi() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.REMOVE_FAVORITE;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            mParams.put("artist_id", strArtistID);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseRemoveFavoriteResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseRemoveFavoriteResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                strFavOrNotStatus = "0";
//                imgFavUnFavIV.setImageResource(R.drawable.ic_fav);
                showToast(getActivity(), getString(R.string.remove_to_fav));
            } else {
                showToast(getActivity(), response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }

    /*
     * Private Room ID:
     *
     * */
    private void executeChatRoomID() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.GET_MESSAGE_ROOM_ID;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("request_id", getUserID());
            mParams.put("user_id", mSearchModel.getUser_id());

            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        try {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.serializeNulls();
                            Gson gson = gsonBuilder.create();
                            RoomIdModel mRoomIdModel = gson.fromJson(response.toString(), RoomIdModel.class);
                            if (mRoomIdModel.getStatus() == 1) {
                                mModel.setmAppointmentID("");
                                mModel.setmRoomId(mRoomIdModel.getRoomId());

                                Intent mIntent = new Intent(getActivity(), ChatActivity.class);
                                mIntent.putExtra(Constants.MODEL, mModel);
                                mIntent.putExtra(Constants.TYPES, getString(R.string.type_client));
                                mIntent.putExtra(Constants.ROOM_ID, mRoomIdModel.getRoomId());
                                mIntent.putExtra(Constants.RECEIVER_ID, mSearchModel.getUser_id());
                                mIntent.putExtra(Constants.SENDER_PROFILE, mSearchModel.getProfile_pic());
                                mIntent.putExtra(Constants.SENDER_NAME, mSearchModel.getName());
                                mIntent.putExtra(Constants.TYPES, getString(R.string.type_details));
                                startActivity(mIntent);
                            } else {
                                showToast(getActivity(), mModel.getMessage());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "**Error**" + e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }
}
