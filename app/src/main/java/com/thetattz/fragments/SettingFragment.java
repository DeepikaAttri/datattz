package com.thetattz.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.ContactUsActivity;
import com.thetattz.activities.EditAccountActivity;
import com.thetattz.activities.InfomationActivity;
import com.thetattz.activities.SignInSignUpActivity;
import com.thetattz.activities.artist.PaymentMethodActivity;
import com.thetattz.activities.artist.SubscriptionActivity;
import com.thetattz.activities.clientpayment.ClientCardsListActivity;
import com.thetattz.models.UserDetailsModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends BaseFragment {

    /**
     * Getting the Current Class Name
     */
    String TAG = SettingFragment.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.editAccountLL)
    LinearLayout editAccountLL;
    @BindView(R.id.paymentMethodLL)
    LinearLayout paymentMethodLL;
    @BindView(R.id.inviteFriendsLL)
    LinearLayout inviteFriendsLL;
    @BindView(R.id.contactUsLL)
    LinearLayout contactUsLL;
    @BindView(R.id.termsAndServicesLL)
    LinearLayout termsAndServicesLL;
    @BindView(R.id.logoutLL)
    LinearLayout logoutLL;

    @BindView(R.id.facebookLL)
    LinearLayout facebookLL;
    @BindView(R.id.instagramLL)
    LinearLayout instagramLL;
    @BindView(R.id.twitterLL)
    LinearLayout twitterLL;
    @BindView(R.id.websiteLL)
    LinearLayout websiteLL;
    @BindView(R.id.subscriptionLL)
    LinearLayout subscriptionLL;

    /*
     * Initialize Objects...
     * */
    Unbinder mUnbinder;

    /*
     * Initialize Objects...
     * */
    String strPushToken = "";

    /*
     * Default Fragment constructor
     * */

    public SettingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_setting, container, false);
        setStatusBar();
        mUnbinder = ButterKnife.bind(this, mView);
        //Get Push Token
        getPushToken();
        getUserDetailsApi();
        return mView;
    }

    private void getPushToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {

                        // Get new Instance ID token
                        strPushToken = task.getResult().getToken();
                        Log.e(TAG, "**Push Token**" + strPushToken);
                    }
                });
    }

    @OnClick({R.id.editAccountLL, R.id.paymentMethodLL, R.id.inviteFriendsLL, R.id.contactUsLL,
            R.id.termsAndServicesLL, R.id.logoutLL, R.id.facebookLL, R.id.instagramLL, R.id.twitterLL,
            R.id.websiteLL, R.id.subscriptionLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.editAccountLL:
                performEditAccountClick();
                break;
            case R.id.paymentMethodLL:
                performPaymentClick();
                break;
            case R.id.inviteFriendsLL:
                performInviteFriendClick();
                break;
            case R.id.contactUsLL:
                performContactUs();
                break;
            case R.id.termsAndServicesLL:
                performTermServicesClick();
                break;
            case R.id.logoutLL:
                performLogoutClick();
                break;
            case R.id.facebookLL:
                performFacebookClick();
                break;
            case R.id.instagramLL:
                performInstagramClick();
                break;
            case R.id.twitterLL:
                performTwitterClick();
                break;
            case R.id.websiteLL:
                performWebSiteClick();
                break;
            case R.id.subscriptionLL:
                performSubscriptionClick();
                break;
        }
    }

    private void performSubscriptionClick() {
        Intent mIntent = new Intent(getActivity(), SubscriptionActivity.class);
        startActivity(mIntent);
    }

    private void performFacebookClick() {
        Intent mIntent = new Intent(getActivity(), InfomationActivity.class);
        mIntent.putExtra(Constants.TYPES, getString(R.string.facebook));
        startActivity(mIntent);
    }

    private void performInstagramClick() {
        Intent mIntent = new Intent(getActivity(), InfomationActivity.class);
        mIntent.putExtra(Constants.TYPES, getString(R.string.instagram));
        startActivity(mIntent);
    }

    private void performTwitterClick() {
        Intent mIntent = new Intent(getActivity(), InfomationActivity.class);
        mIntent.putExtra(Constants.TYPES, getString(R.string.twitter));
        startActivity(mIntent);
    }

    private void performWebSiteClick() {
        Intent mIntent = new Intent(getActivity(), InfomationActivity.class);
        mIntent.putExtra(Constants.TYPES, getString(R.string.website));
        startActivity(mIntent);
    }

    private void performEditAccountClick() {
        //startActivity(new Intent(getActivity(), EditAccountActivity.class));
        Intent mIntent = new Intent(getActivity(), EditAccountActivity.class);
        mIntent.putExtra("DETAILS", "DETAILS");
        startActivity(mIntent);
    }

    private void performPaymentClick() {
        if (getUserRole().equals(Constants.ROLE_ARTIST)) {
//            if (getAccountDetailsID().length() == 0) {
            startActivity(new Intent(getActivity(), PaymentMethodActivity.class));
//            } else {
//                startActivity(new Intent(getActivity(), MobilePayActivity.class));
//            }
        } else if (getUserRole().equals(Constants.ROLE_CLIENT)) {
            startActivity(new Intent(getActivity(), ClientCardsListActivity.class));
        }
    }

    private void performInviteFriendClick() {
        shareAppLink();
    }

    private void performContactUs() {
        startActivity(new Intent(getActivity(), ContactUsActivity.class));
    }

    private void performTermServicesClick() {
        Intent mIntent = new Intent(getActivity(), InfomationActivity.class);
        mIntent.putExtra(Constants.TYPES, getString(R.string.terms_of_services));
        startActivity(mIntent);
    }

    private void performLogoutClick() {
        logoutAlertDialog(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }


    /*
     *
     * Logout Dialog
     * */
    public void logoutAlertDialog(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_logout);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtCancelTV = alertDialog.findViewById(R.id.txtCancelTV);
        TextView txtLogoutTV = alertDialog.findViewById(R.id.txtLogoutTV);

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        txtLogoutTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (ConnectivityReceiver.isConnected()) {
                    executeLogoutApi();
                } else {
                    showToast(mActivity, getString(R.string.internet_connection_error));
                }
            }
        });
        alertDialog.show();
    }


    /*
     *
     * Execute  Logout Api
     * */
    private void executeLogoutApi() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.LOGOUT;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            mParams.put("device_token", strPushToken);
            mParams.put("device_type", "android");
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseLogoutResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseLogoutResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                showToast(getActivity(), response.getString("message"));

                SharedPreferences preferences = TheTattzPrefrences.getPreferences(getActivity());
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();
                editor.apply();

                Intent mIntent = new Intent(getActivity(), SignInSignUpActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mIntent);
                getActivity().finish();

            } else {
                showToast(getActivity(), response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }


    /*
     * Execute Get User Details Api
     * */
    private void getUserDetailsApi() {
        if (ConnectivityReceiver.isConnected())
            executeProfileApi();
        else
            showToast(getActivity(), getString(R.string.internet_connection_error));
    }

    private void executeProfileApi() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.GET_USER_DETAILS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        try {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.serializeNulls();
                            Gson gson = gsonBuilder.create();
                            UserDetailsModel mModel = gson.fromJson(response.toString(), UserDetailsModel.class);
                            if (mModel.getStatus() == 1) {
                                TheTattzPrefrences.writeString(getActivity(), TheTattzPrefrences.ACCOUNT_DETAIL_ID, mModel.getData().getAccount_detail_id());
                                TheTattzPrefrences.writeString(getActivity(), TheTattzPrefrences.PAYPAL_VERIFICATION, mModel.getData().getPaypal_verification());
                                TheTattzPrefrences.writeString(getActivity(), TheTattzPrefrences.PAYPAL_EMAIL_ID, mModel.getData().getPaypal_id());
                            } else {
                                showToast(getActivity(), mModel.getMessage());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "**Error**" + e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }
}
