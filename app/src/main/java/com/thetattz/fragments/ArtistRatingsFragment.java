package com.thetattz.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.artist.ArtistRatingsActivity;
import com.thetattz.adapters.RatingsAdapter;
import com.thetattz.models.ArtistRatingModel;
import com.thetattz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistRatingsFragment extends BaseFragment {

    /*
     * Get class Name
     * */
    String TAG = ArtistRatingsFragment.this.getClass().getSimpleName();

    Unbinder unbinder;

    /*
     * Widgets
     * */
    @BindView(R.id.txtReviewsTV)
    TextView txtReviewsTV;

    @BindView(R.id.ratingsRv)
    RecyclerView ratingsRv;

    @BindView(R.id.llNoDataFountTV)
    LinearLayout llNoDataFountTV;

    RatingsAdapter ratingsAdapter;
    ArrayList<ArtistRatingModel> mArtistRatingArrayList = new ArrayList<ArtistRatingModel>();

    String strArtistID;

    public ArtistRatingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_artist_ratings, container, false);
        setStatusBar();
        unbinder = ButterKnife.bind(this, mView);

        getArgumentsData();

        return mView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    private void getArgumentsData() {
        if (getArguments() != null) {
            strArtistID = getArguments().getString(Constants.ARTIST_ID);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mArtistRatingArrayList != null){
            mArtistRatingArrayList.clear();
            executeArtistRatingReviewsApi();
        }
    }

    @OnClick({R.id.txtReviewsTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtReviewsTV:
                performReviewsClick();
                break;
        }
    }

    private void performReviewsClick() {
        startActivity(new Intent(getActivity(), ArtistRatingsActivity.class).putExtra(Constants.ARTIST_ID, strArtistID));
    }

    /*
     *
     * Execute  Rating Api
     * */
    private void executeArtistRatingReviewsApi() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.ARTIST_RATING_LIST;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("artist_id", strArtistID);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                        Log.e(TAG, "ERROR" + error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }
        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                //Set Data
                if (!response.isNull("data")) {
                    JSONArray mJsonArray = response.getJSONArray("data");
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mDataObj = mJsonArray.getJSONObject(i);
                        ArtistRatingModel mModel = new ArtistRatingModel();
                        if (!mDataObj.isNull("id"))
                            mModel.setId(mDataObj.getString("id"));
                        if (!mDataObj.isNull("user_id"))
                            mModel.setUser_id(mDataObj.getString("user_id"));
                        if (!mDataObj.isNull("rating"))
                            mModel.setRating(mDataObj.getString("rating"));
                        if (!mDataObj.isNull("review"))
                            mModel.setReview(mDataObj.getString("review"));
                        if (!mDataObj.isNull("creation_time"))
                            mModel.setCreation_time(mDataObj.getString("creation_time"));
                        if (!mDataObj.isNull("name"))
                            mModel.setName(mDataObj.getString("name"));
                        if (!mDataObj.isNull("profile_pic"))
                            mModel.setProfile_pic(mDataObj.getString("profile_pic"));

                        mArtistRatingArrayList.add(mModel);
                    }

                    if (mArtistRatingArrayList.size() > 0) {
                        llNoDataFountTV.setVisibility(View.GONE);
                        setReviewsAdapter();
                    } else {
                        llNoDataFountTV.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                llNoDataFountTV.setVisibility(View.VISIBLE);
                setReviewsAdapter();
            }

        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    /*
     * Set Rating Reviews Adapter
     * */
    private void setReviewsAdapter() {
        ratingsAdapter = new RatingsAdapter(getActivity(), mArtistRatingArrayList);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(getActivity());
        ratingsRv.setLayoutManager(mLayoutManagerC);
        ratingsRv.setAdapter(ratingsAdapter);
    }
}
