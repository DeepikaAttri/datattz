package com.thetattz.fragments.artist;


import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.artist.ArtistSeeMoreReviewsActivity;
import com.thetattz.activities.artist.ArtistShowImagesActivity;
import com.thetattz.activities.artist.CreateServiceActivity;
import com.thetattz.activities.artist.EditArtistLocationActivity;
import com.thetattz.activities.artist.SwipeImgesActivity;
import com.thetattz.adapters.ArtistHomeServicesAdapter;
import com.thetattz.adapters.ArtistPhotosAdapter;
import com.thetattz.adapters.ArtistPhotosAdapter_7_10;
import com.thetattz.adapters.ArtistRatingAdapter;
import com.thetattz.adapters.ArtistWorkInfoAdapter;
import com.thetattz.fonts.TextViewRegular;
import com.thetattz.fragments.BaseFragment;
import com.thetattz.interfaces.DeleteServiceInterface;
import com.thetattz.interfaces.SeeMoreClickInterface;
import com.thetattz.models.ArtistHomeServiceModel;
import com.thetattz.models.ArtistModel;
import com.thetattz.models.ArtistRatingModel;
import com.thetattz.models.MyTattozModel;
import com.thetattz.models.WorkInfo;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;
import com.thetattz.volley.AppHelper;
import com.thetattz.volley.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistHomeFragment extends BaseFragment {

    /**
     * Getting the Current Class Name
     */
    String TAG = ArtistHomeFragment.this.getClass().getSimpleName();
    /*
     * Widgets
     * */
    @BindView(R.id.imgBackGroundIV)
    ImageView imgBackGroundIV;
    @BindView(R.id.txtNameTV)
    TextView txtNameTV;
    @BindView(R.id.txtInfoTabTV)
    TextView txtInfoTabTV;
    @BindView(R.id.viewInfoTabV)
    View viewInfoTabV;
    @BindView(R.id.layoutInfoLL)
    LinearLayout layoutInfoLL;
    @BindView(R.id.txtServicesTV)
    TextView txtServicesTV;
    @BindView(R.id.viewServicesTabV)
    View viewServicesTabV;
    @BindView(R.id.layoutServicesLL)
    LinearLayout layoutServicesLL;
    @BindView(R.id.containerArtistHomeRL)
    RelativeLayout containerArtistHomeRL;
    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    String writeCamera = Manifest.permission.CAMERA;

    @BindView(R.id.mArtistHomeInfoFragmentLL)
    LinearLayout mArtistHomeInfoFragmentLL;
    @BindView(R.id.txtAddress1TV)
    TextViewRegular txtAddress1TV;
    @BindView(R.id.txtAddress2TV)
    TextViewRegular txtAddress2TV;
    @BindView(R.id.mMyGallerySeeMoreTV)
    TextView mMyGallerySeeMoreTV;
    @BindView(R.id.dayTimeRecyclerViewRV)
    RecyclerView dayTimeRecyclerViewRV;
    @BindView(R.id.myPhotosRecyclerViewRV)
    RecyclerView myPhotosRecyclerViewRV;
    @BindView(R.id.txtAddTattosTV)
    TextView txtAddTattosTV;
    @BindView(R.id.locationLL)
    LinearLayout locationLL;
    @BindView(R.id.mViewRL)
    RelativeLayout mViewRL;
    @BindView(R.id.mReviewsSeeMoreTV)
    TextView mReviewsSeeMoreTV;
    @BindView(R.id.artistInfoReviewsRV)
    RecyclerView artistInfoReviewsRV;
    @BindView(R.id.llNoDataFountTV)
    LinearLayout llNoDataFountTV;
    @BindView(R.id.timezoneTV)
    TextView timezoneTV;
    /*
     * Initialize Objects...
     * */
    ArrayList<WorkInfo> mWorkInfoArrayList = new ArrayList<>();
    ArtistWorkInfoAdapter mArtistWorkInfoAdapter;

    ArtistModel mArtistModel = new ArtistModel();

    ArrayList<MyTattozModel> mMyTattozArrayList = new ArrayList<>();
    ArtistPhotosAdapter mArtistPhotosAdapter;
    ArtistPhotosAdapter_7_10 mArtistPhotosAdapter_7_10;

    ArtistRatingAdapter mArtistRatingAdapter;
    ArrayList<ArtistRatingModel> mArtistRatingArrayList = new ArrayList<ArtistRatingModel>();
    String currentPhotoPath = "";

    /*
     * mArtistHomeServicesFragmentLL
     * Artist Home Servicesa Fragment Layout
     * Widgets
     *
     * */
    @BindView(R.id.mArtistHomeServicesFragmentLL)
    LinearLayout mArtistHomeServicesFragmentLL;
    @BindView(R.id.txtAddServiceTV)
    TextView txtAddServiceTV;
    @BindView(R.id.servicesRecyclerViewRV)
    RecyclerView servicesRecyclerViewRV;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;
    /*
     * Initialize Objects...
     * */
    ArrayList<ArtistHomeServiceModel> mServicesArrayList = new ArrayList<ArtistHomeServiceModel>();
    ArtistHomeServicesAdapter mArtistHomeServicesAdapter;
    DeleteServiceInterface mDeleteServiceInterface = new DeleteServiceInterface() {
        @Override
        public void mDeleteService(ArtistHomeServiceModel mArtistHomeServiceModel) {
            showDeleteDialog(mArtistHomeServiceModel);
        }
    };
    SeeMoreClickInterface mSeeMoreClickInterface = new SeeMoreClickInterface() {
        @Override
        public void mClick(MyTattozModel mMyTattozModel, int position) {
            Intent mIntent = new Intent(getActivity(), SwipeImgesActivity.class);
            mIntent.putExtra(Constants.LIST, mMyTattozArrayList);
            mIntent.putExtra(Constants.POSITION, position);
            startActivityForResult(mIntent, Constants.REQUEST_CODE);
        }
    };
    /*
     * Initialize  Unbinder for butterknife;
     * */
    private Unbinder mUnbinder;

    /*
     * Default Constructor
     * */
    public ArtistHomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_artist_home, container, false);
        setStatusBar();
        //Butter Knife Binder
        mUnbinder = ButterKnife.bind(this, mView);
        //Set Default Fragment
        setDefaultFragment();
        if (getArtistHomeSelectedTab() == Constants.ARTIST_HOME_TAB_1) {
            performTab1Click();
        } else if (getArtistHomeSelectedTab() == Constants.ARTIST_HOME_TAB_3) {
            perfromTab3Click();
        }

        return mView;
    }

    private void setDefaultFragment() {
        if (getArtistHomeSelectedTab() == 0)
            TheTattzPrefrences.writeInteger(getActivity(), TheTattzPrefrences.ARTIST_HOME_SELECTED_TAB, Constants.ARTIST_HOME_TAB_1);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateTopHeader();
    }

    private void updateTopHeader() {
        if (getUserProfilePicture() != null && getUserProfilePicture().contains("http")) {
            //Collection User Data
            Picasso.with(getActivity()).load(getUserProfilePicture())
                    .placeholder(R.drawable.bg_card_t_white)
                    .error(R.drawable.bg_card_t_white)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgBackGroundIV);
        }

        if (getUserName() != null) {
            txtNameTV.setText(CapitalizedFullName(getUserName()));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    @OnClick({R.id.layoutInfoLL, R.id.layoutServicesLL, R.id.mMyGallerySeeMoreTV, R.id.mReviewsSeeMoreTV, R.id.txtAddTattosTV,
            R.id.locationLL, R.id.mViewRL, R.id.txtAddress1TV, R.id.txtAddress2TV, R.id.dayTimeRecyclerViewRV, R.id.txtAddServiceTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutInfoLL:
                performTab1Click();
                break;
            case R.id.layoutServicesLL:
                perfromTab3Click();
                break;
            case R.id.mMyGallerySeeMoreTV:
                perfromMyGallerySeeMoreClick();
                break;
            case R.id.mReviewsSeeMoreTV:
                performSeeMoreReviewsClick();
                break;
            case R.id.txtAddTattosTV:
                performAddTattoosClick();
                break;
            case R.id.locationLL:
                performLocationClick();
                break;
            case R.id.mViewRL:
                performLocationClick();
                break;
            case R.id.txtAddress1TV:
                performLocationClick();
                break;
            case R.id.txtAddress2TV:
                performLocationClick();
                break;
            case R.id.dayTimeRecyclerViewRV:
                performLocationClick();
                break;
            case R.id.txtAddServiceTV:
                performAddServicesClick();
                break;
        }
    }

    private void performTab1Click() {
        txtNameTV.setText(CapitalizedFullName(getUserName()));
        txtInfoTabTV.setTextColor(getResources().getColor(R.color.colorBlue));
        viewInfoTabV.setBackgroundColor(getResources().getColor(R.color.colorBlue));
        txtServicesTV.setTextColor(getResources().getColor(R.color.colorWhite));
        viewServicesTabV.setBackgroundColor(getResources().getColor(R.color.colorTrasperant));

        mArtistHomeInfoFragmentLL.setVisibility(View.VISIBLE);
        mArtistHomeServicesFragmentLL.setVisibility(View.GONE);
        TheTattzPrefrences.writeInteger(getActivity(), TheTattzPrefrences.ARTIST_HOME_SELECTED_TAB, Constants.ARTIST_HOME_TAB_1);
        getArtistDetails();
    }

    private void perfromTab3Click() {
        txtInfoTabTV.setTextColor(getResources().getColor(R.color.colorWhite));
        viewInfoTabV.setBackgroundColor(getResources().getColor(R.color.colorTrasperant));

        txtServicesTV.setTextColor(getResources().getColor(R.color.colorBlue));
        viewServicesTabV.setBackgroundColor(getResources().getColor(R.color.colorBlue));

        mArtistHomeInfoFragmentLL.setVisibility(View.GONE);
        mArtistHomeServicesFragmentLL.setVisibility(View.VISIBLE);
        TheTattzPrefrences.writeInteger(getActivity(), TheTattzPrefrences.ARTIST_HOME_SELECTED_TAB, Constants.ARTIST_HOME_TAB_3);
        mServicesArrayList.clear();
        getServicesData();
    }

    /***********************************************************************************************
     * Artist Home Info Fragment
     * Functionality:::::::::
     *
     * */
    private void performLocationClick() {
        Intent mIntent = new Intent(getActivity(), EditArtistLocationActivity.class);
        getActivity().startActivityForResult(mIntent, 656);
    }

    /*
     * Add User Tattos with
     * Camera Gallery functionality
     * */
    public void performAddTattoosClick() {
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(getActivity(), writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(getActivity(), writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(getActivity(), writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        requestPermissions(new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    public void onSelectImageClick() {
        CropImage.startPickImageActivity(getActivity());
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(100, 100)
                .setMultiTouchEnabled(false)
                .start(getActivity());
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                try {

                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(result.getUri());

                    Bitmap bitmap = BitmapFactory.decodeStream(imageStream);

                    executeUploadTattoImage(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(getActivity(), "Cropping failed: " + result.getError());
            }
        }
        if (requestCode == 656) {
            performTab1Click();
            updateTopHeader();
        }
        if (requestCode == 363) {
            perfromTab3Click();
        }

        if (data != null && requestCode == Constants.REQUEST_CODE) {
            getArtistDetails();
        }

        if (data != null && requestCode == Constants.REQUEST_CODE_PICTURE) {
            getArtistDetails();
        }
    }

    private void executeUploadTattoImage(Bitmap mBitmap) {
        showProgressDialog(getActivity());
        String ApiUrl = Constants.ADD_TATTOOS;
        Log.e(TAG, "Api Url::::" + ApiUrl);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                ApiUrl, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                dismissProgressDialog();
                String resultResponse = new String(response.data);
                Log.e(TAG, "onResponse: " + resultResponse);
                parseImageUploadResponse(resultResponse);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                String strBody = "";
                if (error.networkResponse != null) {
                    try {
                        strBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            showAlertDialog(getActivity(), getString(R.string.internet_connection_error));
                        } else if (error instanceof AuthFailureError) {
                            showAlertDialog(getActivity(), getString(R.string.auth_failed));
                        } else if (error instanceof ServerError) {
                            showAlertDialog(getActivity(), getString(R.string.server_error));
                        } else if (error instanceof NetworkError) {
                            showAlertDialog(getActivity(), getString(R.string.internet_connection_error));
                        } else if (error instanceof ParseError) {
                            showAlertDialog(getActivity(), getString(R.string.parse_error));
                        } else {
                            showAlertDialog(getActivity(), strBody);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    showAlertDialog(getActivity(), getString(R.string.internet_connection_error));
                }
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", getUserID());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "multipart/form-data");
                return headers;


            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                if (mBitmap != null)
                    params.put("image", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(getActivity(), mBitmap), "image/jpeg"));
                return params;
            }
        };

        TheTattzApplication.getInstance().addToRequestQueue(multipartRequest);
    }

    private void parseImageUploadResponse(String resultResponse) {
        try {
            JSONObject mJsonObject = new JSONObject(resultResponse);

            Log.e("qq", mJsonObject.toString());
            if (mJsonObject.getString("status").equals("1")) {
                mMyTattozArrayList.clear();
                JSONObject mDataObject = mJsonObject.getJSONObject("data");
                JSONArray mMyTattosArray = mDataObject.getJSONArray("images");
                for (int i = 0; i < mMyTattosArray.length(); i++) {
                    JSONObject mDataObj = mMyTattosArray.getJSONObject(i);

                    MyTattozModel mMyTattozModel = new MyTattozModel();

                    mMyTattozModel.setId(mDataObj.getString("id"));
                    mMyTattozModel.setUser_id(mDataObj.getString("user_id"));
                    if (!mDataObj.isNull("image"))
                        mMyTattozModel.setImage(mDataObj.getString("image"));

                    mMyTattozArrayList.add(mMyTattozModel);
                }

                if (getDeviceSizeType().equals("mobile_bear")) {
                    setPhotosAdapter();
                } else {
                    setPhotosAdapter7_10();
                }

                if (mMyTattozArrayList.size() > 2) {
                    mMyGallerySeeMoreTV.setVisibility(View.VISIBLE);
                } else {
                    mMyGallerySeeMoreTV.setVisibility(View.GONE);
                }

            } else {
                showToast(getActivity(), mJsonObject.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    private void perfromMyGallerySeeMoreClick() {
        if (mMyTattozArrayList != null) {
            Intent i = new Intent(getActivity(), ArtistShowImagesActivity.class);
            i.putExtra(Constants.LIST, mMyTattozArrayList);
            startActivityForResult(i, Constants.REQUEST_CODE_PICTURE);
        }
    }

    private void performSeeMoreReviewsClick() {
        if (mArtistRatingArrayList != null) {
            Intent i = new Intent(getActivity(), ArtistSeeMoreReviewsActivity.class);
            i.putExtra(Constants.LIST, mArtistRatingArrayList);
            startActivity(i);
        }
    }

    private void getArtistDetails() {
        if (ConnectivityReceiver.isConnected())
            executeArtistDetailsAPI();
        else
            showToast(getActivity(), getString(R.string.internet_connection_error));
    }

    /*Execute Get User Details API*/
    private void executeArtistDetailsAPI() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.ARTIST_GET_USER_DETAILS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());

                        parseDetailsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseDetailsResponse(JSONObject response) {
        mWorkInfoArrayList.clear();
        mMyTattozArrayList.clear();
        try {
            if (response.getString("status").equals("1")) {


                JSONObject mDataObject = response.getJSONObject("data");

                mArtistModel.setUser_id(mDataObject.getString("user_id"));

                if (!mDataObject.isNull("name")) {
                    mArtistModel.setName(mDataObject.getString("name"));
                    TheTattzPrefrences.writeString(getActivity(), TheTattzPrefrences.USER_NAME, mDataObject.getString("name"));
                }

                if (!mDataObject.isNull("email"))
                    mArtistModel.setEmail(mDataObject.getString("email"));
                if (!mDataObject.isNull("profile_pic")) {
                    mArtistModel.setProfile_pic(mDataObject.getString("profile_pic"));
                    TheTattzPrefrences.writeString(getActivity(), TheTattzPrefrences.USER_PROFILE_PIC, mDataObject.getString("profile_pic"));
                }
                if (!mDataObject.isNull("shopname"))
                    mArtistModel.setShopname(mDataObject.getString("shopname"));
                if (!mDataObject.isNull("building_name"))
                    mArtistModel.setBuilding_name(mDataObject.getString("building_name"));
                if (!mDataObject.isNull("street_address"))
                    mArtistModel.setStreet_address(mDataObject.getString("street_address"));
                if (!mDataObject.isNull("city"))
                    mArtistModel.setCity(mDataObject.getString("city"));
                if (!mDataObject.isNull("rating"))
                    mArtistModel.setRating(mDataObject.getString("rating"));
                if (!mDataObject.isNull("account_detail_id")) {
                    mArtistModel.setAccount_detail_id("account_detail_id");
                    TheTattzPrefrences.writeString(getActivity(), TheTattzPrefrences.ACCOUNT_DETAIL_ID, mDataObject.getString("account_detail_id"));
                }
                if (!mDataObject.isNull("timezone"))
                    mArtistModel.setTimezone(mDataObject.getString("timezone"));
                if (!mDataObject.isNull("timezoneAbr"))
                    mArtistModel.setTimezoneAbr(mDataObject.getString("timezoneAbr"));
                if (!mDataObject.isNull("rating"))

                    //Work Info
                    if (!response.isNull("work_info")) {
                        JSONArray mWorkInfoArray = response.getJSONArray("work_info");
                        if (mWorkInfoArray.length() > 0) {
                            for (int i = 0; i < mWorkInfoArray.length(); i++) {
                                JSONObject mWorkInfoObj = mWorkInfoArray.getJSONObject(i);
                                WorkInfo mWorkInfo = new WorkInfo();
                                mWorkInfo.setDay(mWorkInfoObj.getString("day"));
                                mWorkInfo.setStart_time(mWorkInfoObj.getString("start_time"));
                                mWorkInfo.setEnd_time(mWorkInfoObj.getString("end_time"));

                                mWorkInfoArrayList.add(mWorkInfo);
                            }
                        }
                    }

                //Photos
                if (!response.isNull("my_tattoos")) {
                    JSONArray mTattosArray = response.getJSONArray("my_tattoos");
                    for (int i = 0; i < mTattosArray.length(); i++) {
                        JSONObject mWorkInfoObj = mTattosArray.getJSONObject(i);
                        MyTattozModel mMyTattozModel = new MyTattozModel();
                        mMyTattozModel.setId(mWorkInfoObj.getString("id"));
                        mMyTattozModel.setUser_id(mWorkInfoObj.getString("user_id"));
                        mMyTattozModel.setImage(mWorkInfoObj.getString("image"));

                        mMyTattozArrayList.add(mMyTattozModel);
                    }
                }

                setDataOnWidgets();
            } else {
                showAlertDialog(getActivity(), response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Exception**" + e.toString());
        }
    }

    private void setDataOnWidgets() {
        if (mArtistModel.getProfile_pic() != null && mArtistModel.getProfile_pic().contains("http")) {
            //Collection User Data
            Picasso.with(getActivity()).load(mArtistModel.getProfile_pic())
                    .placeholder(R.drawable.bg_card_t_white)
                    .error(R.drawable.bg_card_t_white)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgBackGroundIV);
        }

        if (mArtistModel.getName() != null) {
            txtNameTV.setText(CapitalizedFullName(mArtistModel.getName()));
        }

        if (mArtistModel.getTimezoneAbr() != null) {
            timezoneTV.setText("Note: Times are displayed in " + mArtistModel.getTimezoneAbr() + ".");
        }

        //Set Address Validations
        if (mArtistModel.getShopname().length() == 0 && mArtistModel.getBuilding_name().length() == 0) {
            txtAddress1TV.setText(getString(R.string.not_specified));
        } else {
            if (mArtistModel.getShopname() != null && mArtistModel.getBuilding_name() != null && mArtistModel.getShopname().length() > 0 && mArtistModel.getBuilding_name().length() > 0) {
                txtAddress1TV.setText(mArtistModel.getShopname() + ", " + mArtistModel.getBuilding_name());
            } else if (mArtistModel.getShopname() != null && mArtistModel.getBuilding_name() != null && mArtistModel.getShopname().length() > 0 && mArtistModel.getBuilding_name().length() == 0) {
                txtAddress1TV.setText(mArtistModel.getShopname());
            } else if (mArtistModel.getShopname() != null && mArtistModel.getBuilding_name() != null && mArtistModel.getShopname().length() == 0 && mArtistModel.getBuilding_name().length() > 0) {
                txtAddress1TV.setText(mArtistModel.getBuilding_name());
            }
        }
        if (mArtistModel.getStreet_address().length() == 0 && mArtistModel.getCity().length() == 0) {
            txtAddress2TV.setText(getString(R.string.not_specified));
        } else {
            if (mArtistModel.getStreet_address() != null && mArtistModel.getCity() != null && mArtistModel.getStreet_address().length() > 0 && mArtistModel.getCity().length() > 0) {
                txtAddress2TV.setText(mArtistModel.getStreet_address() + ", " + mArtistModel.getCity());
            } else if (mArtistModel.getStreet_address() != null && mArtistModel.getCity() != null && mArtistModel.getStreet_address().length() > 0 && mArtistModel.getCity().length() == 0) {
                txtAddress2TV.setText(mArtistModel.getStreet_address());
            } else if (mArtistModel.getStreet_address() != null && mArtistModel.getCity() != null && mArtistModel.getStreet_address().length() == 0 && mArtistModel.getCity().length() > 0) {
                txtAddress2TV.setText(mArtistModel.getCity());
            }
        }


        setWorkInfoAdapter();

        //set Photos Adapter
        //Set Validations
        if (mMyTattozArrayList.size() > 2) {
            mMyGallerySeeMoreTV.setVisibility(View.VISIBLE);
        } else {
            mMyGallerySeeMoreTV.setVisibility(View.GONE);
        }
        if (getDeviceSizeType().equals("mobile_bear")) {
            setPhotosAdapter();
        } else {
            setPhotosAdapter7_10();
        }


        //Get Rating Reviews Data
        executeArtistRatingReviewsApi();
    }

    private void setWorkInfoAdapter() {
        dayTimeRecyclerViewRV.setNestedScrollingEnabled(false);
        dayTimeRecyclerViewRV.setHasFixedSize(false);
        mArtistWorkInfoAdapter = new ArtistWorkInfoAdapter(getActivity(), mWorkInfoArrayList);
        RecyclerView.LayoutManager mLayoutManagerC = new GridLayoutManager(getActivity(), 2);
        dayTimeRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        dayTimeRecyclerViewRV.setAdapter(mArtistWorkInfoAdapter);
    }

    private void setPhotosAdapter() {
        myPhotosRecyclerViewRV.setNestedScrollingEnabled(false);
        myPhotosRecyclerViewRV.setHasFixedSize(false);
        mArtistPhotosAdapter = new ArtistPhotosAdapter(getActivity(), mMyTattozArrayList, mSeeMoreClickInterface);
        RecyclerView.LayoutManager mLayoutManagerC = new GridLayoutManager(getActivity(), 2);
        myPhotosRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        myPhotosRecyclerViewRV.setAdapter(mArtistPhotosAdapter);
    }

    private void setPhotosAdapter7_10() {
        myPhotosRecyclerViewRV.setNestedScrollingEnabled(false);
        myPhotosRecyclerViewRV.setHasFixedSize(false);
        mArtistPhotosAdapter_7_10 = new ArtistPhotosAdapter_7_10(getActivity(), mMyTattozArrayList, mSeeMoreClickInterface);
        RecyclerView.LayoutManager mLayoutManagerC = new GridLayoutManager(getActivity(), 3);
        myPhotosRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        myPhotosRecyclerViewRV.setAdapter(mArtistPhotosAdapter_7_10);
    }

    /*
     *
     * Execute  Rating Api
     * */
    private void executeArtistRatingReviewsApi() {
        final String mApiUrl = Constants.ARTIST_RATING_LIST;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("artist_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "ERROR" + error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                //Set Data
                if (!response.isNull("data")) {
                    JSONArray mJsonArray = response.getJSONArray("data");
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mDataObj = mJsonArray.getJSONObject(i);
                        ArtistRatingModel mModel = new ArtistRatingModel();
                        if (!mDataObj.isNull("id"))
                            mModel.setId(mDataObj.getString("id"));
                        if (!mDataObj.isNull("user_id"))
                            mModel.setUser_id(mDataObj.getString("user_id"));
                        if (!mDataObj.isNull("rating"))
                            mModel.setRating(mDataObj.getString("rating"));
                        if (!mDataObj.isNull("review"))
                            mModel.setReview(mDataObj.getString("review"));
                        if (!mDataObj.isNull("creation_time"))
                            mModel.setCreation_time(mDataObj.getString("creation_time"));
                        if (!mDataObj.isNull("name"))
                            mModel.setName(mDataObj.getString("name"));
                        if (!mDataObj.isNull("profile_pic"))
                            mModel.setProfile_pic(mDataObj.getString("profile_pic"));

                        mArtistRatingArrayList.add(mModel);
                    }

                    if (mArtistRatingArrayList.size() > 0) {
                        llNoDataFountTV.setVisibility(View.GONE);
                        setReviewsAdapter();
                    } else {
                        llNoDataFountTV.setVisibility(View.VISIBLE);
                        mReviewsSeeMoreTV.setVisibility(View.GONE);
                    }
                }
            } else {
                llNoDataFountTV.setVisibility(View.VISIBLE);
                setReviewsAdapter();
            }


            if (mArtistRatingArrayList.size() > 2) {
                mReviewsSeeMoreTV.setVisibility(View.VISIBLE);
            } else {
                mReviewsSeeMoreTV.setVisibility(View.GONE);
            }


        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    /*
     * Set Rating Reviews Adapter
     * */
    private void setReviewsAdapter() {
        mArtistRatingAdapter = new ArtistRatingAdapter(getActivity(), mArtistRatingArrayList);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(getActivity());
        artistInfoReviewsRV.setLayoutManager(mLayoutManagerC);
        artistInfoReviewsRV.setAdapter(mArtistRatingAdapter);
    }

    /*
     * ***************************************************************************************** */


    /***************************************************************************************
     * ARTIST HOME TAB 3 SELECTED SERVICES FRAGMENT START
     * **************************************************************************************/


    private void performAddServicesClick() {
        Intent mIntent = new Intent(getActivity(), CreateServiceActivity.class);
        startActivityForResult(mIntent, 363);

    }


    private void getServicesData() {
        if (ConnectivityReceiver.isConnected()) {
            executeApi();
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }
    }

    /*
     *
     * Execute  Rating Api
     * */
    private void executeApi() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.ARTIST_HOME_SERVICES_LISTING;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("artist_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseServicesResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseServicesResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                //Set Data
                if (!response.isNull("data")) {
                    JSONArray mJsonArray = response.getJSONArray("data");
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mDataObj = mJsonArray.getJSONObject(i);
                        ArtistHomeServiceModel mModel = new ArtistHomeServiceModel();

                        if (!mDataObj.isNull("id"))
                            mModel.setId(mDataObj.getString("id"));
                        if (!mDataObj.isNull("artist_id"))
                            mModel.setArtist_id(mDataObj.getString("artist_id"));
                        if (!mDataObj.isNull("service_name"))
                            mModel.setService_name(mDataObj.getString("service_name"));
                        if (!mDataObj.isNull("description"))
                            mModel.setDescription(mDataObj.getString("description"));
                        if (!mDataObj.isNull("duration"))
                            mModel.setDuration(mDataObj.getString("duration"));
                        if (!mDataObj.isNull("price"))
                            mModel.setPrice(mDataObj.getString("price"));
                        if (!mDataObj.isNull("enable"))
                            mModel.setEnable(mDataObj.getString("enable"));

                        mServicesArrayList.add(mModel);
                    }

                    if (mServicesArrayList.size() > 0) {
                        txtNoDataFountTV.setVisibility(View.GONE);
                        setAdapter();
                    } else {
                        txtNoDataFountTV.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                txtNoDataFountTV.setVisibility(View.VISIBLE);
                showToast(getActivity(), response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    /*
     * Set Rating Reviews Adapter
     * */
    private void setAdapter() {
        mArtistHomeServicesAdapter = new ArtistHomeServicesAdapter(getActivity(), mServicesArrayList, mDeleteServiceInterface);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(getActivity());
        servicesRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        servicesRecyclerViewRV.setAdapter(mArtistHomeServicesAdapter);
    }

    /*
     *
     * Delete Alert Dialog
     * */
    public void showDeleteDialog(ArtistHomeServiceModel mArtistHomeServiceModel) {
        final Dialog alertDialog = new Dialog(getActivity());
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_delete_item);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView txtCancelTV = alertDialog.findViewById(R.id.txtCancelTV);
        TextView txtDeleteTV = alertDialog.findViewById(R.id.txtDeleteTV);
        txtMessageTV.setText(getString(R.string.are_your_sure_you_want_to_delete));
        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        txtDeleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mDeleteApi(mArtistHomeServiceModel);

            }
        });
        alertDialog.show();
    }

    private void mDeleteApi(ArtistHomeServiceModel mArtistHomeServiceModel) {
        if (ConnectivityReceiver.isConnected()) {
            executeDeleteApi(mArtistHomeServiceModel);
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }
    }

    private void executeDeleteApi(ArtistHomeServiceModel mArtistHomeServiceModel) {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.DELETE_SERVICE;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("id", mArtistHomeServiceModel.getId());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseDeleteResponse(response, mArtistHomeServiceModel);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseDeleteResponse(JSONObject response, ArtistHomeServiceModel mArtistHomeServiceModel) {
        try {
            if (response.getString("status").equals("1")) {
                showToast(getActivity(), response.getString("message"));
                mServicesArrayList.remove(mArtistHomeServiceModel);
                if (mServicesArrayList.size() > 0) {
                    txtNoDataFountTV.setVisibility(View.GONE);
                    mArtistHomeServicesAdapter.notifyDataSetChanged();
                } else {
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    mArtistHomeServicesAdapter.notifyDataSetChanged();
                }
                setAdapter();
            }
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }


    /***************************************************************************************
     * ARTIST HOME TAB 3 SELECTED SERVICES FRAGMENT END FUNCTIONALITY
     * **************************************************************************************/


}