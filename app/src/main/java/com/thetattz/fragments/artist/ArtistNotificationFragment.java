package com.thetattz.fragments.artist;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.adapters.ArtistNotificationAdapter;
import com.thetattz.fragments.BaseFragment;
import com.thetattz.models.ArtistNotificationModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistNotificationFragment extends BaseFragment {

    /*
     * Get class Name
     * */
    String TAG = ArtistNotificationFragment.this.getClass().getSimpleName();

    Unbinder unbinder;

    /*
     * Widgets
     * */
    @BindView(R.id.ArtistNotificationRV)
    RecyclerView ArtistNotificationRV;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;

    /*
     * Initialize Objects
     * */
    ArtistNotificationAdapter mArtistNotificationAdapter;
    ArrayList<ArtistNotificationModel> mArrayList = new ArrayList<>();

    public ArtistNotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_aritst_notification, container, false);
        setStatusBar();
        unbinder = ButterKnife.bind(this, mView);

        getNotificationData();

        return mView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


    private void getNotificationData() {
        if (ConnectivityReceiver.isConnected())
            executeNotificationApi();
        else
            showToast(getActivity(), getString(R.string.internet_connection_error));
    }


    /*
     *
     * Execute  SignUp Api
     * */
    private void executeNotificationApi() {
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        String mTimeZone = timeZone.getID();

        showProgressDialog(getActivity());
        final String mApiUrl = Constants.NOTIFICATON_ACTIVITY;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("user_id", getUserID());
            mParams.put("timezone", mTimeZone);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                if (!response.isNull("data")) {
                    JSONArray mArray = response.getJSONArray("data");
                    for (int i = 0; i < mArray.length(); i++) {
                        JSONObject mData = mArray.getJSONObject(i);
                        ArtistNotificationModel mModel = new ArtistNotificationModel();

                        if (!mData.isNull("notification_id")) {
                            mModel.setNotificationId(mData.getString("notification_id"));
                        }
                        if (!mData.isNull("message")) {
                            mModel.setMessage(mData.getString("message"));
                        }
                        if (!mData.isNull("notification_type")) {
                            mModel.setNotificationType(mData.getString("notification_type"));
                        }
                        if (!mData.isNull("user_id")) {
                            mModel.setUserId(mData.getString("user_id"));
                        }
                        if (!mData.isNull("sender_id")) {
                            mModel.setSenderId(mData.getString("sender_id"));
                        }
                        if (!mData.isNull("appointment_id")) {
                            mModel.setAppointmentId(mData.getString("appointment_id"));
                        }
                        if (!mData.isNull("appointment_status")) {
                            mModel.setAppointmentStatus(mData.getString("appointment_status"));
                        }
                        if (!mData.isNull("creation_date")) {
                            mModel.setCreationDate(mData.getString("creation_date"));
                        }
                        if (!mData.isNull("viewed_status")) {
                            mModel.setViewedStatus(mData.getString("viewed_status"));
                        }
                        if (!mData.isNull("tab_seen")) {
                            mModel.setTabSeen(mData.getString("tab_seen"));
                        }
                        if (!mData.isNull("sender_name")) {
                            mModel.setSenderName(mData.getString("sender_name"));
                        }
                        if (!mData.isNull("profile_pic")) {
                            mModel.setProfilePic(mData.getString("profile_pic"));
                        }
                        if (!mData.isNull("room_id")) {
                            mModel.setRoomId(mData.getString("room_id"));
                        }
                        if (!mData.isNull("detail_time")) {
                            mModel.setDetail_time(mData.getString("detail_time"));
                        }

                        mArrayList.add(mModel);
                    }
                }

                if (mArrayList.size() > 0) {
                    ArtistNotificationRV.setVisibility(View.VISIBLE);
                    txtNoDataFountTV.setVisibility(View.GONE);
                    setAdapter();
                } else {
                    ArtistNotificationRV.setVisibility(View.GONE);
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    setAdapter();
                }

            } else {
                ArtistNotificationRV.setVisibility(View.GONE);
                txtNoDataFountTV.setVisibility(View.VISIBLE);
                setAdapter();
                showAlertDialog(getActivity(), response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    private void setAdapter() {
        ArtistNotificationRV.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mArtistNotificationAdapter = new ArtistNotificationAdapter(getActivity(), mArrayList);
        ArtistNotificationRV.setAdapter(mArtistNotificationAdapter);
        mArtistNotificationAdapter.notifyDataSetChanged();
    }
}