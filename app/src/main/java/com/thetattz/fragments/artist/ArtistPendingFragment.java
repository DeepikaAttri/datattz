package com.thetattz.fragments.artist;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.adapters.PendingAppointmentAdapter;
import com.thetattz.fragments.BaseFragment;
import com.thetattz.models.AppointmentsModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistPendingFragment extends BaseFragment {

    String TAG = ArtistPendingFragment.this.getClass().getSimpleName();


    Unbinder unbinder;

    /*
     * Widgets
     * */
    @BindView(R.id.pendingAppointRV)
    RecyclerView pendingAppointRV;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;

    /*
     * Initialize Objects
     * */
    PendingAppointmentAdapter mPendingAdapter;
    ArrayList<AppointmentsModel> mAppointmentArrayList = new ArrayList<>();


    public ArtistPendingFragment() {
        // Required empty public constructor
    }


    //For Pending Appointment Status is : 1
    //For confirmed Appointment Status is : 2
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_artist_pending, container, false);
        setStatusBar();
        unbinder = ButterKnife.bind(this, mView);

        return mView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        getAppointments();
    }

    /*
     *
     * Execute Search Api
     * */
    private void getAppointments() {

        if (ConnectivityReceiver.isConnected())
            executeApi();
        else
            showToast(getActivity(), getString(R.string.internet_connection_error));

    }

    private void executeApi() {
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        String mTimeZone = timeZone.getID();
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.ARTIST_APPOINTMENTS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            mParams.put("artist_id", getUserID());
            mParams.put("appointment_status", "1");
            mParams.put("timezone", mTimeZone);

            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }
        };
        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {
        mAppointmentArrayList.clear();
        try {
            if (response.getString("status").equals("1")) {
                if (!response.isNull("data")) {
                    JSONArray mJsonArray = response.getJSONArray("data");
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mDataObj = mJsonArray.getJSONObject(i);
                        AppointmentsModel mUpcomingAppointModel = new AppointmentsModel();
                        if (!mDataObj.isNull("appointment_id"))
                            mUpcomingAppointModel.setAppointment_id(mDataObj.getString("appointment_id"));
                        if (!mDataObj.isNull("start_time"))
                            mUpcomingAppointModel.setStart_time(mDataObj.getString("start_time"));
                        if (!mDataObj.isNull("end_time"))
                            mUpcomingAppointModel.setEnd_time(mDataObj.getString("end_time"));
                        if (!mDataObj.isNull("name"))
                            mUpcomingAppointModel.setName(mDataObj.getString("name"));
                        if (!mDataObj.isNull("profile_pic"))
                            mUpcomingAppointModel.setProfile_pic(mDataObj.getString("profile_pic"));
                        if (!mDataObj.isNull("city"))
                            mUpcomingAppointModel.setCity(mDataObj.getString("city"));
                        if (!mDataObj.isNull("date"))
                            mUpcomingAppointModel.setDate(mDataObj.getString("date"));
                        if (!mDataObj.isNull("duration"))
                            mUpcomingAppointModel.setDuration(mDataObj.getString("duration"));
                        if (!mDataObj.isNull("created_by"))
                            mUpcomingAppointModel.setCreated_by(mDataObj.getString("created_by"));
                        if (!mDataObj.isNull("timezone"))
                            mUpcomingAppointModel.setTimezone(mDataObj.getString("timezone"));
                        if (!mDataObj.isNull("timezoneAbr"))
                            mUpcomingAppointModel.setTimezoneAbr(mDataObj.getString("timezoneAbr"));

                        mAppointmentArrayList.add(mUpcomingAppointModel);
                    }
                }

                if (mAppointmentArrayList.size() > 0) {
                    txtNoDataFountTV.setVisibility(View.GONE);
                    pendingAppointRV.setVisibility(View.VISIBLE);
                    setAdapter();
                } else {
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    pendingAppointRV.setVisibility(View.GONE);
                    setAdapter();
                }
            } else {
                showToast(getActivity(), response.getString("message"));
                txtNoDataFountTV.setVisibility(View.VISIBLE);
                pendingAppointRV.setVisibility(View.GONE);
                setAdapter();
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    private void setAdapter() {
        pendingAppointRV.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mPendingAdapter = new PendingAppointmentAdapter(getActivity(), mAppointmentArrayList);
        pendingAppointRV.setAdapter(mPendingAdapter);
    }
}