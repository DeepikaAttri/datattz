package com.thetattz.fragments.artist;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.adapters.ArtistClientsAdapter;
import com.thetattz.fonts.TextViewSemiBold;
import com.thetattz.fragments.BaseFragment;
import com.thetattz.models.ArtistClientModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistClientFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = ArtistClientFragment.this.getClass().getSimpleName();


    /**
     * Widgets
     */
    @BindView(R.id.mClientRecylerViewRV)
    StickyListHeadersListView mClientRecylerViewRV;
    @BindView(R.id.txtInviteFriendsTV)
    TextViewSemiBold txtInviteFriendsTV;
    @BindView(R.id.layoutNoDataFoundLL)
    LinearLayout layoutNoDataFoundLL;
    @BindView(R.id.rlBackRL)
    RelativeLayout rlBackRL;

    /*
     * Initialize  Unbinder for butterknife;
     * */
    private Unbinder mUnbinder;

    /*
     *
     * Initialize Objects...
     * */
    ArtistClientsAdapter mArtistClientsAdapter;
    ArrayList<ArtistClientModel> mClientArrayList = new ArrayList<>();

    /*
     * Default Constructor
     * */

    public ArtistClientFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_artist_client, container, false);
        setStatusBar();
        mUnbinder = ButterKnife.bind(this, mView);
        //Perform Client Api
        performClientAPI();

        return mView;
    }

    private void performClientAPI() {
        if (ConnectivityReceiver.isConnected())
            executeApi();
        else
            showAlertDialog(getActivity(), getString(R.string.internet_connection_error));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    /*
     *
     * Execute Search Api
     * */
    private void executeApi() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.ARTIST_CLIENT_LISTING;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            mParams.put("artist_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                if (!response.isNull("data")) {
                    JSONArray mJsonArray = response.getJSONArray("data");
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mDataObj = mJsonArray.getJSONObject(i);
                        ArtistClientModel mArtistClientModel = new ArtistClientModel();
                        if (!mDataObj.isNull("user_id"))
                            mArtistClientModel.setUser_id(mDataObj.getString("user_id"));

                        if (!mDataObj.isNull("name"))
                            mArtistClientModel.setName(mDataObj.getString("name"));

                        if (!mDataObj.isNull("phone"))
                            mArtistClientModel.setPhone(mDataObj.getString("phone"));

                        if (!mDataObj.isNull("profile_pic"))
                            mArtistClientModel.setProfile_pic(mDataObj.getString("profile_pic"));

                        mClientArrayList.add(mArtistClientModel);
                    }
                }

                //Set Up Recycler View Data
                setAdapter();

            } else {
                showToast(getActivity(), response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    private void setAdapter() {
        if (mClientArrayList.size() > 0) {
            mClientRecylerViewRV.setVisibility(View.VISIBLE);
            layoutNoDataFoundLL.setVisibility(View.GONE);
        } else {
            mClientRecylerViewRV.setVisibility(View.GONE);
            layoutNoDataFoundLL.setVisibility(View.VISIBLE);
        }
        int size = mClientArrayList.size();

        Collections.sort(mClientArrayList, new Comparator<ArtistClientModel>() {
            @Override
            public int compare(ArtistClientModel s1, ArtistClientModel s2) {
                return s1.getName().compareToIgnoreCase(s2.getName());
            }
        });
        mArtistClientsAdapter = new ArtistClientsAdapter(getActivity(), mClientArrayList);
        mClientRecylerViewRV.setFastScrollEnabled(true);
        mClientRecylerViewRV.setAreHeadersSticky(false);
        mClientRecylerViewRV.setAdapter(mArtistClientsAdapter);
        mArtistClientsAdapter.notifyDataSetChanged();
    }


    @OnClick(R.id.txtInviteFriendsTV)
    public void onViewClicked() {
        shareAppLink();
    }

    /*
     * Widgets click listner
     * */
    @OnClick({R.id.rlBackRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBackRL:

                break;

        }

    }
}
