package com.thetattz.fragments.artist;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.savvi.rangedatepicker.CalendarPickerView;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.artist.ArtistAppointActivity;
import com.thetattz.activities.artist.CreateAppointmentsActivity;
import com.thetattz.activities.artist.SubscriptionSubmitActivity;
import com.thetattz.fonts.TextViewSemiBold;
import com.thetattz.fragments.BaseFragment;
import com.thetattz.models.ArtistAppointmentModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistAppointmentsFragment extends BaseFragment {

    /*
     * Widgets
     * */
    @BindView(R.id.rlAddRL)
    RelativeLayout rlAddRL;
    @BindView(R.id.rlAppointmentsRL)
    RelativeLayout rlAppointmentsRL;
    @BindView(R.id.mCalenderCV)
    CalendarPickerView mCalenderCV;
    @BindView(R.id.txtNoDataFountTV)
    TextViewSemiBold txtNoDataFountTV;
    @BindView(R.id.imgLeftIV)
    LinearLayout imgLeftIV;
    @BindView(R.id.imgRightIV)
    LinearLayout imgRightIV;
    /*
     * Initalize objects...
     * */
    String strCurrentMonthYear = "";
    Boolean isSubscriptionActive = false;
    /*
     * Calender
     * */
    SimpleDateFormat mSimpleDateFormat;
    Calendar mCalendar;
    Date mTommaroDate;
    Calendar mCalenderNextYear = Calendar.getInstance();
    /*
     * TAG Getting Class Name
     * */
    private String TAG = ArtistAppointmentsFragment.this.getClass().getSimpleName();
    /*
     * Initialize  Unbinder for butterknife;
     * */
    private Unbinder mUnbinder;

    /*
     * Default Constructor
     * */
    public ArtistAppointmentsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_artist_appointments, container, false);
        setStatusBar();
        mUnbinder = ButterKnife.bind(this, mView);

//        isSubscriptionActive = isSubscriptionActive();

        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        setDatesOnCalendar();
    }

    /*
     * Widgets click listner
     * */
    @OnClick({R.id.rlAddRL, R.id.rlAppointmentsRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlAddRL:
                performAddAppointmentClick();
                break;
            case R.id.rlAppointmentsRL:
                performAppointmentsImgClick();
                break;
        }
    }

    private void performAppointmentsImgClick() {
//        if (isSubscriptionActive.equals(false)) {
//            showSubscriptionAlertDialog(getActivity());
//        } else {
            TheTattzPrefrences.writeString(getActivity(), Constants.APPOINTMENT_TYPE, "");
            startActivity(new Intent(getActivity(), ArtistAppointActivity.class));
//        }
    }

    private void performAddAppointmentClick() {
//        if (isSubscriptionActive.equals(false)) {
//            showSubscriptionAlertDialog(getActivity());
//        } else {
            startActivity(new Intent(getActivity(), CreateAppointmentsActivity.class));
//        }
    }

    private void setDatesOnCalendar() {
        try {
            mCalendar = Calendar.getInstance();
            mSimpleDateFormat = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());

            mCalenderNextYear.add(Calendar.YEAR, 50);

//            SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
//            mTommaroDate = dateformat.parse("1-" + getCurrentDate());

            Calendar lastYear = Calendar.getInstance();
            lastYear.add(Calendar.YEAR, 0);
            mTommaroDate = lastYear.getTime();

            long mDateInMillies = mCalendar.getTimeInMillis();
            strCurrentMonthYear = convertTimeLongToStringMonthYear(mDateInMillies);
            Log.e(TAG, "**Calendar Date**" + strCurrentMonthYear);

            mCalenderCV.scrollToDate(mTommaroDate);
            imgLeftIV.setVisibility(View.GONE);

            mCalenderCV.init(mTommaroDate, mCalenderNextYear.getTime(), mSimpleDateFormat)
                    .inMode(CalendarPickerView.SelectionMode.MULTIPLE);

            mCalenderCV.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true;
                }
            });

            imgRightIV.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    imgLeftIV.setVisibility(View.VISIBLE);

                    mCalendar.add(Calendar.MONTH, 1);

                    Date resultdate = new Date(mCalendar.getTimeInMillis());
                    mCalenderCV.scrollToDate(resultdate);

                    long mDateInMillies = mCalendar.getTimeInMillis();
                    strCurrentMonthYear = convertTimeLongToStringMonthYear(mDateInMillies);
                    Log.e(TAG, "**Calendar Date**" + strCurrentMonthYear);

                    getAppointmentDetails();
                }
            });

            imgLeftIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("TAG", "monMYJJ::" + Calendar.MONTH);
                    mCalendar.add(Calendar.MONTH, -1);

                    Date resultdate = new Date(mCalendar.getTimeInMillis());

                    //calender current date
                    String dayOfTheWeek = (String) DateFormat.format("EEEE", resultdate); // Week day
                    String day = (String) DateFormat.format("dd", resultdate); // day
                    String monthString = (String) DateFormat.format("MMM", resultdate); // Month
                    String monthNumber = (String) DateFormat.format("MM", resultdate); // Month in number
                    String year = (String) DateFormat.format("yyyy", resultdate); // Year

                    //calender current month
                    int CalenderMonth = Integer.parseInt(monthNumber);

                    mCalenderCV.scrollToDate(resultdate);
                    Log.e("TAG", "mon::" + resultdate);

                    long mDateInMillies = mCalendar.getTimeInMillis();
                    strCurrentMonthYear = convertTimeLongToStringMonthYear(mDateInMillies);
                    Log.e(TAG, "**Calendar Date**" + strCurrentMonthYear);

                    //current month
                    int month = Calendar.DAY_OF_MONTH;

                    if (CalenderMonth == month) {
                        imgLeftIV.setVisibility(View.GONE);
//                        showToast(getActivity(), getString(R.string.previous___));
                        getAppointmentDetails();
                    } else {
                        imgLeftIV.setVisibility(View.VISIBLE);
                        getAppointmentDetails();
                    }
                }
            });

            /*
             * Get Appointments
             * */
            getAppointmentDetails();

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    private String getCurrentDate() {
        String date = new SimpleDateFormat("MM-yyyy", Locale.getDefault()).format(new Date());
        return date;
    }

    private void getAppointmentDetails() {
        if (ConnectivityReceiver.isConnected()) {
            executeAppointmentsApi();
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }
    }

    private void executeAppointmentsApi() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.GET_MONTHLY_APPOINTMENTS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();

        try {
            mParams.put("artist_id", getUserID());
            mParams.put("calendar", strCurrentMonthYear);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }
        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {
        try {

            /*
             * Set Up Data on Widgets
             * */
            if (response.getString("status").equals("1")) {
                txtNoDataFountTV.setVisibility(View.GONE);
                if (!response.isNull("data")) {
                    ArrayList<ArtistAppointmentModel> mArrayList = new ArrayList<>();
                    JSONArray mJsonArray = response.getJSONArray("data");

                    for (int i = 0; i < mJsonArray.length(); i++) {
                        ArtistAppointmentModel mModel = new ArtistAppointmentModel();
                        JSONObject mDataObj = mJsonArray.getJSONObject(i);
                        if (!mDataObj.isNull("id")) {
                            mModel.setId(mDataObj.getString("id"));
                        }
                        if (!mDataObj.isNull("date")) {
                            mModel.setDate(mDataObj.getString("date"));
                        }

                        mArrayList.add(mModel);
                    }

                    //Set Dates On Calender View
                    setDatesOnCalendarUpdated(mArrayList);

                }
            } else {
                txtNoDataFountTV.setVisibility(View.VISIBLE);
                showToast(getActivity(), response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    private void setDatesOnCalendarUpdated(ArrayList<ArtistAppointmentModel> mArrayList) {
        /*
         * ARRAYLIST TO SHOW SPECIFIC DATES
         * */
        Calendar c = Calendar.getInstance();

        ArrayList<Date> mAppointmentList = new ArrayList<>();
        try {

            for (int i = 0; i < mArrayList.size(); i++) {

                SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
                String strdate = mArrayList.get(i).getDate();
                Date newdate = dateformat.parse(strdate);
                Date mDate = null;
//                mAppointmentList.add(newdate);

                try {
                    mDate = new SimpleDateFormat("dd-MM-yyyy").parse(strdate);

                    mAppointmentList.add(mDate);

                    if (c.getTime().after(mDate)) {

                        String today = c.getTime().toString().substring(0, 10);
                        String datee = mDate.toString().substring(0, 10);

                        if (!today.equals(datee)) {
                            mAppointmentList.remove(mDate);
                        }
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
//            if (mAppointmentList.size() > 0)
//                mTommaroDate = mAppointmentList.get(0);

            mCalenderCV.init(mTommaroDate, mCalenderNextYear.getTime(), mSimpleDateFormat)
                    .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                    .withHighlightedDates(mAppointmentList);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    //subscription alert dialog
//    public void showSubscriptionAlertDialog(Activity mActivity) {
//        final Dialog alertDialog = new Dialog(mActivity);
//        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        alertDialog.setContentView(R.layout.dialog_subscription_alert);
//        alertDialog.setCanceledOnTouchOutside(false);
//        alertDialog.setCancelable(false);
//        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//        // set the custom dialog components - text, image and button
//        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
//        TextView btnCancel = alertDialog.findViewById(R.id.btnCancel);
//        TextView btnSubscribe = alertDialog.findViewById(R.id.btnSubscribe);
//
//        btnCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialog.dismiss();
//            }
//        });
//        btnSubscribe.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialog.dismiss();
//                Intent mIntent = new Intent(mActivity, SubscriptionSubmitActivity.class);
//                startActivity(mIntent);
//            }
//        });
//        alertDialog.show();
//    }
}
