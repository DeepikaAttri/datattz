package com.thetattz.fragments.artist;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.adapters.ArtistRatingAdapter;
import com.thetattz.fragments.BaseFragment;
import com.thetattz.models.ArtistRatingModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistHomeRatingFragment extends BaseFragment {


    /**
     * Getting the Current Class Name
     */
    String TAG = ArtistHomeRatingFragment.this.getClass().getSimpleName();

    public static String ARTISTINFO;

    /**
     * Widgets
     */
    @BindView(R.id.ratingRecyclerviewRV)
    RecyclerView ratingRecyclerviewRV;
    @BindView(R.id.llNoDataFountTV)
    LinearLayout llNoDataFountTV;
    @BindView(R.id.tvReviews)
    TextView tvReviews;

    /*
     * Initialize  Unbinder for butterknife;
     * */
    private Unbinder mUnbinder;

    /*
     *
     * Initialize Objects...
     * */
    ArrayList<ArtistRatingModel> mArtistRatingArrayList = new ArrayList<ArtistRatingModel>();
    ArtistRatingAdapter mArtistRatingAdapter;

    /*
     * Default Constructor
     * */
    public ArtistHomeRatingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_artist_home_rating, container, false);
        setStatusBar();
        //Butter Knife
        mUnbinder = ButterKnife.bind(this, mView);
        //Get Artist Data
        getRatingData();
        return mView;
    }

    private void getRatingData() {
        if (ConnectivityReceiver.isConnected()) {
            executeApi();
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }
    }

    /*
     *
     * Execute  Rating Api
     * */
    private void executeApi() {
        showProgressDialog(getActivity());
        final String mApiUrl = Constants.ARTIST_RATING_LIST;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("artist_id", getUserID());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                //Set Data
                if (!response.isNull("data")) {
                    JSONArray mJsonArray = response.getJSONArray("data");
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mDataObj = mJsonArray.getJSONObject(i);
                        ArtistRatingModel mModel = new ArtistRatingModel();
                        if (!mDataObj.isNull("id"))
                            mModel.setId(mDataObj.getString("id"));
                        if (!mDataObj.isNull("user_id"))
                            mModel.setUser_id(mDataObj.getString("user_id"));
                        if (!mDataObj.isNull("rating"))
                            mModel.setRating(mDataObj.getString("rating"));
                        if (!mDataObj.isNull("review"))
                            mModel.setReview(mDataObj.getString("review"));
                        if (!mDataObj.isNull("creation_time"))
                            mModel.setCreation_time(mDataObj.getString("creation_time"));
                        if (!mDataObj.isNull("name"))
                            mModel.setName(mDataObj.getString("name"));
                        if (!mDataObj.isNull("profile_pic"))
                            mModel.setProfile_pic(mDataObj.getString("profile_pic"));

                        mArtistRatingArrayList.add(mModel);
                    }

                    if (mArtistRatingArrayList.size() > 0) {
                        llNoDataFountTV.setVisibility(View.GONE);
                        setAdapter();
                    } else {
                        llNoDataFountTV.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                llNoDataFountTV.setVisibility(View.VISIBLE);
                setAdapter();
                //  showToast(getActivity(), response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }

    /*
     * Set Rating Reviews Adapter
     * */
    private void setAdapter() {
        mArtistRatingAdapter = new ArtistRatingAdapter(getActivity(), mArtistRatingArrayList);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(getActivity());
        ratingRecyclerviewRV.setLayoutManager(mLayoutManagerC);
        ratingRecyclerviewRV.setAdapter(mArtistRatingAdapter);
    }
}
