package com.thetattz.models;

public class SendMessageModel{
	private SendMessageData data;
	private String message;
	private int status;

	public void setData(SendMessageData data){
		this.data = data;
	}

	public SendMessageData getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"SendMessageModel{" +
			"data = '" + data + '\'' +
			",message = '" + message + '\'' +
			",status = '" + status + '\'' +
			"}";
		}
}
