package com.thetattz.models;

public class ArtistStripeDetailsModel {

    String account_detail_id;
    String firstname;
    String lastname;
    String dob;
    String line1;
    String line2;
    String city;
    String state;
    String country;
    String postal_code;
    String id_number;
    String phone;
    String ssn_last_4;
    String front_document;
    String back_document;
    String bank_acc_holder_name;
    String bank_acc_number;
    String bank_routing_number;
    String verified;
    String enabled;
    String artist_id;
    String stripe_account_id;
    String stripe_bank_id;
    String creation_time;

    public String getAccount_detail_id() {
        return account_detail_id;
    }

    public void setAccount_detail_id(String account_detail_id) {
        this.account_detail_id = account_detail_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSsn_last_4() {
        return ssn_last_4;
    }

    public void setSsn_last_4(String ssn_last_4) {
        this.ssn_last_4 = ssn_last_4;
    }

    public String getFront_document() {
        return front_document;
    }

    public void setFront_document(String front_document) {
        this.front_document = front_document;
    }

    public String getBack_document() {
        return back_document;
    }

    public void setBack_document(String back_document) {
        this.back_document = back_document;
    }

    public String getBank_acc_holder_name() {
        return bank_acc_holder_name;
    }

    public void setBank_acc_holder_name(String bank_acc_holder_name) {
        this.bank_acc_holder_name = bank_acc_holder_name;
    }

    public String getBank_acc_number() {
        return bank_acc_number;
    }

    public void setBank_acc_number(String bank_acc_number) {
        this.bank_acc_number = bank_acc_number;
    }

    public String getBank_routing_number() {
        return bank_routing_number;
    }

    public void setBank_routing_number(String bank_routing_number) {
        this.bank_routing_number = bank_routing_number;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getArtist_id() {
        return artist_id;
    }

    public void setArtist_id(String artist_id) {
        this.artist_id = artist_id;
    }

    public String getStripe_account_id() {
        return stripe_account_id;
    }

    public void setStripe_account_id(String stripe_account_id) {
        this.stripe_account_id = stripe_account_id;
    }

    public String getStripe_bank_id() {
        return stripe_bank_id;
    }

    public void setStripe_bank_id(String stripe_bank_id) {
        this.stripe_bank_id = stripe_bank_id;
    }

    public String getCreation_time() {
        return creation_time;
    }

    public void setCreation_time(String creation_time) {
        this.creation_time = creation_time;
    }
}
