package com.thetattz.models;

import java.io.Serializable;

public class MyTattozModel implements Serializable {
 String id = "";
 String user_id = "";
 String image = "";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
