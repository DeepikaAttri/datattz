package com.thetattz.models;

public class ArtistNotificationModel{
	private String notificationType;
	private String appointmentStatus;
	private String userId;
	private String tabSeen;
	private String profilePic;
	private String appointmentId;
	private String senderName;
	private String notificationId;
	private String creationDate;
	private String message;
	private String senderId;
	private String viewedStatus;
	private String roomId;
	private String detail_time;

	public void setNotificationType(String notificationType){
		this.notificationType = notificationType;
	}

	public String getNotificationType(){
		return notificationType;
	}

	public void setAppointmentStatus(String appointmentStatus){
		this.appointmentStatus = appointmentStatus;
	}

	public String getAppointmentStatus(){
		return appointmentStatus;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setTabSeen(String tabSeen){
		this.tabSeen = tabSeen;
	}

	public String getTabSeen(){
		return tabSeen;
	}

	public void setProfilePic(String profilePic){
		this.profilePic = profilePic;
	}

	public String getProfilePic(){
		return profilePic;
	}

	public void setAppointmentId(String appointmentId){
		this.appointmentId = appointmentId;
	}

	public String getAppointmentId(){
		return appointmentId;
	}

	public void setSenderName(String senderName){
		this.senderName = senderName;
	}

	public String getSenderName(){
		return senderName;
	}

	public void setNotificationId(String notificationId){
		this.notificationId = notificationId;
	}

	public String getNotificationId(){
		return notificationId;
	}

	public void setCreationDate(String creationDate){
		this.creationDate = creationDate;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setSenderId(String senderId){
		this.senderId = senderId;
	}

	public String getSenderId(){
		return senderId;
	}

	public void setViewedStatus(String viewedStatus){
		this.viewedStatus = viewedStatus;
	}

	public String getViewedStatus(){
		return viewedStatus;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getDetail_time() {
		return detail_time;
	}

	public void setDetail_time(String detail_time) {
		this.detail_time = detail_time;
	}

	@Override
 	public String toString(){
		return 
			"ArtistNotificationModel{" + 
			"notification_type = '" + notificationType + '\'' + 
			",appointment_status = '" + appointmentStatus + '\'' + 
			",user_id = '" + userId + '\'' + 
			",tab_seen = '" + tabSeen + '\'' + 
			",profile_pic = '" + profilePic + '\'' + 
			",appointment_id = '" + appointmentId + '\'' + 
			",sender_name = '" + senderName + '\'' + 
			",notification_id = '" + notificationId + '\'' + 
			",creation_date = '" + creationDate + '\'' + 
			",message = '" + message + '\'' + 
			",sender_id = '" + senderId + '\'' + 
			",viewed_status = '" + viewedStatus + '\'' + 
			"}";
		}
}
