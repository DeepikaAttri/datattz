package com.thetattz.models;

import java.io.Serializable;

public class FreeTimeSlotsModel implements Serializable {
	private String startTime;
	private String endTime;
	private String bothTime;

	public String getBothTime() {
		return bothTime;
	}

	public void setBothTime(String bothTime) {
		this.bothTime = bothTime;
	}

	public void setStartTime(String startTime){
		this.startTime = startTime;
	}

	public String getStartTime(){
		return startTime;
	}

	public void setEndTime(String endTime){
		this.endTime = endTime;
	}

	public String getEndTime(){
		return endTime;
	}

	@Override
 	public String toString(){
		return
			"FreeTimeSlotsModel{" +
			"start_time = '" + startTime + '\'' +
			",end_time = '" + endTime + '\'' +
			",end_time = '" + bothTime + '\'' +
			"}";
		}
}
