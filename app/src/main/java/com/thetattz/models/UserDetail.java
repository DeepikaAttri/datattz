package com.thetattz.models;

import java.io.Serializable;

public class UserDetail implements Serializable {
	private String user_id;
	private String name;
	private String profile_pic;
	private String city;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfile_pic() {
		return profile_pic;
	}

	public void setProfile_pic(String profile_pic) {
		this.profile_pic = profile_pic;
	}

	@Override
 	public String toString(){
		return
			"UserDetail{" +
			"user_id = '" + user_id + '\'' +
			",city = '" + city + '\'' +
			",name = '" + name + '\'' +
			",profile_pic = '" + profile_pic + '\'' +
			"}";
		}
}
