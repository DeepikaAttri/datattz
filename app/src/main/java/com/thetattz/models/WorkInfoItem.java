package com.thetattz.models;

public class WorkInfoItem{
	private String startTime;
	private String breakStartTime;
	private String endTime;
	private String id;
	private String artistId;
	private String day;
	private String breakEndTime;
	private String status;

	public void setStartTime(String startTime){
		this.startTime = startTime;
	}

	public String getStartTime(){
		return startTime;
	}

	public void setBreakStartTime(String breakStartTime){
		this.breakStartTime = breakStartTime;
	}

	public String getBreakStartTime(){
		return breakStartTime;
	}

	public void setEndTime(String endTime){
		this.endTime = endTime;
	}

	public String getEndTime(){
		return endTime;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setArtistId(String artistId){
		this.artistId = artistId;
	}

	public String getArtistId(){
		return artistId;
	}

	public void setDay(String day){
		this.day = day;
	}

	public String getDay(){
		return day;
	}

	public void setBreakEndTime(String breakEndTime){
		this.breakEndTime = breakEndTime;
	}

	public String getBreakEndTime(){
		return breakEndTime;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"WorkInfoItem{" + 
			"start_time = '" + startTime + '\'' + 
			",break_start_time = '" + breakStartTime + '\'' + 
			",end_time = '" + endTime + '\'' + 
			",id = '" + id + '\'' + 
			",artist_id = '" + artistId + '\'' + 
			",day = '" + day + '\'' + 
			",break_end_time = '" + breakEndTime + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
