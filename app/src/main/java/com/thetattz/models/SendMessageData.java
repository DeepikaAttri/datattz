package com.thetattz.models;

public class SendMessageData {
	private String id;
	private String sender_id;
	private String receiver_id;
	private String room_id;
	private String appointment_id;
	private String message;
	private String message_time;
	private String message_seen;
	private String message_current_time;
	private String sender_profile_pic;
	private String message_time_val;

	public String getMessage_time_val() {
		return message_time_val;
	}

	public void setMessage_time_val(String message_time_val) {
		this.message_time_val = message_time_val;
	}

	public String getSender_profile_pic() {
		return sender_profile_pic;
	}

	public void setSender_profile_pic(String sender_profile_pic) {
		this.sender_profile_pic = sender_profile_pic;
	}

	public String getMessage_current_time() {
		return message_current_time;
	}

	public void setMessage_current_time(String message_current_time) {
		this.message_current_time = message_current_time;
	}

	public String getAppointment_id() {
		return appointment_id;
	}

	public void setAppointment_id(String appointment_id) {
		this.appointment_id = appointment_id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSender_id() {
		return sender_id;
	}

	public void setSender_id(String sender_id) {
		this.sender_id = sender_id;
	}

	public String getReceiver_id() {
		return receiver_id;
	}

	public void setReceiver_id(String receiver_id) {
		this.receiver_id = receiver_id;
	}

	public String getRoom_id() {
		return room_id;
	}

	public void setRoom_id(String room_id) {
		this.room_id = room_id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage_seen() {
		return message_seen;
	}

	public void setMessage_seen(String message_seen) {
		this.message_seen = message_seen;
	}

	public String getMessage_time() {
		return message_time;
	}

	public void setMessage_time(String message_time) {
		this.message_time = message_time;
	}

	@Override
 	public String toString(){
		return
			"Data{" +
			"room_id = '" + room_id + '\'' +
			",message_time = '" + message_time + '\'' +
			",message_seen = '" + message_seen + '\'' +
			",receiver_id = '" + receiver_id + '\'' +
			",appointment_id = '" +appointment_id  + '\'' +
			",id = '" + id + '\'' +
			",message = '" + message + '\'' +
			",sender_id = '" + sender_id + '\'' +
			"}";
		}
}
