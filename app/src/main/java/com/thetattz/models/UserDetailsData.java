package com.thetattz.models;

public class UserDetailsData {
	private String user_id;
	private String customer_id;
	private String paypal_id;
	private String paypal_payer_id;
	private String paypal_token;
	private String paypal_verification;
	private String name;
	private String email;
	private String phone;
	private String password;
	private String profile_pic;
	private String shopname;
	private String street_address;
	private String building_name;
	private String city;
	private String role;
	private String enable;
	private String latitude;
	private String longitude;
	private String creation_time;
	private String rating;
	private String account_detail_id;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getPaypal_id() {
		return paypal_id;
	}

	public void setPaypal_id(String paypal_id) {
		this.paypal_id = paypal_id;
	}

	public String getPaypal_payer_id() {
		return paypal_payer_id;
	}

	public void setPaypal_payer_id(String paypal_payer_id) {
		this.paypal_payer_id = paypal_payer_id;
	}

	public String getPaypal_token() {
		return paypal_token;
	}

	public void setPaypal_token(String paypal_token) {
		this.paypal_token = paypal_token;
	}

	public String getPaypal_verification() {
		return paypal_verification;
	}

	public void setPaypal_verification(String paypal_verification) {
		this.paypal_verification = paypal_verification;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfile_pic() {
		return profile_pic;
	}

	public void setProfile_pic(String profile_pic) {
		this.profile_pic = profile_pic;
	}

	public String getShopname() {
		return shopname;
	}

	public void setShopname(String shopname) {
		this.shopname = shopname;
	}

	public String getStreet_address() {
		return street_address;
	}

	public void setStreet_address(String street_address) {
		this.street_address = street_address;
	}

	public String getBuilding_name() {
		return building_name;
	}

	public void setBuilding_name(String building_name) {
		this.building_name = building_name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getCreation_time() {
		return creation_time;
	}

	public void setCreation_time(String creation_time) {
		this.creation_time = creation_time;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getAccount_detail_id() {
		return account_detail_id;
	}

	public void setAccount_detail_id(String account_detail_id) {
		this.account_detail_id = account_detail_id;
	}

	@Override
 	public String toString(){
		return
			"Data{" +
			"creation_time = '" + creation_time + '\'' +
			",street_address = '" + street_address + '\'' +
			",building_name = '" + building_name + '\'' +
			",role = '" + role + '\'' +
			",city = '" + city + '\'' +
			",latitude = '" + latitude + '\'' +
			",profile_pic = '" + profile_pic + '\'' +
			",rating = '" + rating + '\'' +
			",password = '" + password + '\'' +
			",user_id = '" + user_id + '\'' +
			",phone = '" + phone + '\'' +
			",enable = '" + enable + '\'' +
			",shopname = '" + shopname + '\'' +
			",name = '" + name + '\'' +
			",paypal_id = '" + paypal_id + '\'' +
			",customer_id = '" + customer_id + '\'' +
			",email = '" + email + '\'' +
			",longitude = '" + longitude + '\'' +
			",account_detail_id = '" + account_detail_id + '\'' +
			"}";
		}
}
