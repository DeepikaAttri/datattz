package com.thetattz.models;

public class RoomIdModel{
	private String room_id;
	private String room_user_id;
	private String message;
	private int status;

	public void setRoomId(String roomId){
		this.room_id = roomId;
	}

	public String getRoomId(){
		return room_id;
	}

	public void setRoomUserId(String roomUserId){
		this.room_user_id = roomUserId;
	}

	public String getRoomUserId(){
		return room_user_id;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"RoomIdModel{" +
			"room_id = '" + room_id + '\'' +
			",room_user_id = '" + room_user_id + '\'' +
			",message = '" + message + '\'' +
			",status = '" + status + '\'' +
			"}";
		}
}
