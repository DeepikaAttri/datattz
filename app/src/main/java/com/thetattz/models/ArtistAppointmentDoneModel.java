package com.thetattz.models;

public class ArtistAppointmentDoneModel {
	private String message;
	private int status;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"ArtistConfirmCancelModel{" +
			"message = '" + message + '\'' +
			",status = '" + status + '\'' +
			"}";
		}
}
