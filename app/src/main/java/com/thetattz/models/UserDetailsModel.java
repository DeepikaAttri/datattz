package com.thetattz.models;

import java.util.List;

public class UserDetailsModel{
	private UserDetailsData data;
	private List<WorkInfoItem> workInfo;
	private String message;
	private List<Object> myTattoos;
	private int status;

	public void setData(UserDetailsData data){
		this.data = data;
	}

	public UserDetailsData getData(){
		return data;
	}

	public void setWorkInfo(List<WorkInfoItem> workInfo){
		this.workInfo = workInfo;
	}

	public List<WorkInfoItem> getWorkInfo(){
		return workInfo;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setMyTattoos(List<Object> myTattoos){
		this.myTattoos = myTattoos;
	}

	public List<Object> getMyTattoos(){
		return myTattoos;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"UserDetailsModel{" +
			"data = '" + data + '\'' +
			",work_info = '" + workInfo + '\'' +
			",message = '" + message + '\'' +
			",my_tattoos = '" + myTattoos + '\'' +
			",status = '" + status + '\'' +
			"}";
		}
}