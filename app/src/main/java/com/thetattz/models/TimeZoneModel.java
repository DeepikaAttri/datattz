package com.thetattz.models;

import java.io.Serializable;

public class TimeZoneModel implements Serializable {
    String timezoneAbr = "";
    String timezone = "";

    public String getTimezoneAbr() {
        return timezoneAbr;
    }

    public void setTimezoneAbr(String timezoneAbr) {
        this.timezoneAbr = timezoneAbr;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    @Override
    public String toString() {
        return timezone;
    }
}
