package com.thetattz.models;

import java.io.Serializable;

public class ArtistHoursModel implements Serializable {
 String id = "";
 String artist_id = "";
 String day = "";
 String start_time = "";
 String end_time = "";
 String break_start_time = "";
 String break_end_time = "";
 String status = "";

    public String getBreak_start_time() {
        return break_start_time;
    }

    public void setBreak_start_time(String break_start_time) {
        this.break_start_time = break_start_time;
    }

    public String getBreak_end_time() {
        return break_end_time;
    }

    public void setBreak_end_time(String break_end_time) {
        this.break_end_time = break_end_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getArtist_id() {
        return artist_id;
    }

    public void setArtist_id(String artist_id) {
        this.artist_id = artist_id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
