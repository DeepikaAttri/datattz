package com.thetattz.models;

import java.io.Serializable;
import java.util.ArrayList;

public class EditArtistModel implements Serializable {


    String shopname = "";
    String street_address = "";
    String building_name = "";
    String city = "";

    ArrayList<ArtistHoursModel> hoursArrayList = new ArrayList<>();


    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public String getStreet_address() {
        return street_address;
    }

    public void setStreet_address(String street_address) {
        this.street_address = street_address;
    }

    public String getBuilding_name() {
        return building_name;
    }

    public void setBuilding_name(String building_name) {
        this.building_name = building_name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public ArrayList<ArtistHoursModel> getHoursArrayList() {
        return hoursArrayList;
    }

    public void setHoursArrayList(ArrayList<ArtistHoursModel> hoursArrayList) {
        this.hoursArrayList = hoursArrayList;
    }
}
