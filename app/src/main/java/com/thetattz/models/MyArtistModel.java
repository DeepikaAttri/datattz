package com.thetattz.models;

import java.io.Serializable;

public class MyArtistModel implements Serializable {

    String user_id = "";
    String name = "";
    String profile_pic = "";
    String timezone = "";
    String timezoneAbr = "";

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getTimezoneAbr() {
        return timezoneAbr;
    }

    public void setTimezoneAbr(String timezoneAbr) {
        this.timezoneAbr = timezoneAbr;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }
}
