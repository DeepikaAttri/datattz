package com.thetattz.models;

import java.io.Serializable;

public class ArtistAppointmentModel implements Serializable {

    String id = "";
    String date = "";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
