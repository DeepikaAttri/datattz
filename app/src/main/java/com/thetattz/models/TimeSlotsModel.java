package com.thetattz.models;

import java.io.Serializable;

public class TimeSlotsModel implements Serializable {
    String time = "";
    String service_id = "";
    String duration = "";


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
