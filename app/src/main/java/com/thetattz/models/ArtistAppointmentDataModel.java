package com.thetattz.models;

import com.thetattz.chat.ChatItem;

import java.io.Serializable;
import java.util.ArrayList;

public class ArtistAppointmentDataModel implements Serializable {
	private AppointmentDetail appointment_detail;
	private UserDetail user_detail;
	private ArrayList<ChatItem> chat;
	private ArtistDetail artist_detail;
	private String payment_method;

	public AppointmentDetail getAppointment_detail() {
		return appointment_detail;
	}

	public void setAppointment_detail(AppointmentDetail appointment_detail) {
		this.appointment_detail = appointment_detail;
	}

	public UserDetail getUser_detail() {
		return user_detail;
	}

	public void setUser_detail(UserDetail user_detail) {
		this.user_detail = user_detail;
	}

	public ArrayList<ChatItem> getChat() {
		return chat;
	}

	public void setChat(ArrayList<ChatItem> chat) {
		this.chat = chat;
	}

	public ArtistDetail getArtist_detail() {
		return artist_detail;
	}

	public void setArtist_detail(ArtistDetail artist_detail) {
		this.artist_detail = artist_detail;
	}

	public String getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}
}