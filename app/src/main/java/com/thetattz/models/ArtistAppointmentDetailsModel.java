package com.thetattz.models;

import java.io.Serializable;

public class ArtistAppointmentDetailsModel implements Serializable {
	private ArtistAppointmentDataModel data;
	private String message;
	private int status;
	private String mAppointmentID;
	private String mRoomId;

	public String getmAppointmentID() {
		return mAppointmentID;
	}

	public void setmAppointmentID(String mAppointmentID) {
		this.mAppointmentID = mAppointmentID;
	}

	public String getmRoomId() {
		return mRoomId;
	}

	public void setmRoomId(String mRoomId) {
		this.mRoomId = mRoomId;
	}

	public void setData(ArtistAppointmentDataModel data){
		this.data = data;
	}

	public ArtistAppointmentDataModel getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"ArtistAppointmentDetailsModel{" +
			"data = '" + data + '\'' +
			",message = '" + message + '\'' +
			",status = '" + status + '\'' +
			"}";
		}
}
