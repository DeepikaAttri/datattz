package com.thetattz.models;

import java.io.Serializable;

public class ArtistDetail implements Serializable {
	private String artist_id;
	private String name;
	private String profile_pic;
	private String city;
	private String start_time;
	private String date;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setstart_time(String start_time){
		this.start_time = start_time;
	}

	public String getstart_time(){
		return start_time;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setprofile_pic(String profile_pic){
		this.profile_pic = profile_pic;
	}

	public String getprofile_pic(){
		return profile_pic;
	}

	public void setArtistId(String artistId){
		this.artist_id = artistId;
	}

	public String getArtistId(){
		return artist_id;
	}

	@Override
 	public String toString(){
		return
			"ArtistDetail{" +
			"date = '" + date + '\'' +
			",start_time = '" + start_time + '\'' +
			",city = '" + city + '\'' +
			",name = '" + name + '\'' +
			",profile_pic = '" + profile_pic + '\'' +
			",artist_id = '" + artist_id + '\'' +
			"}";
		}
}
