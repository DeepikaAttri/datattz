package com.thetattz;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.github.tamir7.contacts.Contacts;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;
import com.thetattz.volley.LruBitmapCache;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;

public class TheTattzApplication extends Application {
    /**
     * Getting the Current Class Name
     */
    public static final String TAG = TheTattzApplication.class.getSimpleName();
    /**
     * Volley Request Time Stamp
     */
    public static final int CONNECTION_TIMEOUT = 120 * 1000;//120 Seconds
    /**
     * Initialize the Applications Instance
     */
    private static TheTattzApplication mInstance;
    /**
     * Initialize the Volley RequestQueue
     */
    private RequestQueue mRequestQueue;

    /*
     * Image Loader
     * */
    private ImageLoader mImageLoader;

    /**
     * Synchronized the Application Class Instance
     */
    public static synchronized TheTattzApplication getInstance() {
            return mInstance;
    }

    /*
     * Socket
     * */
    private Socket mSocket;

    {
        try {
            mSocket = IO.socket(Constants.SOCKET_URL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Required initialization logic here!
        mInstance = this;

        //Initialize Contacts:
        Contacts.initialize(this);
    }

    public Socket getSocket() {
        return mSocket;
    }

    /**
     * Return the RequestQueue
     */
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // Don't forget the volley request queue
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    /**
     * Add Volley Request In queue with TAG
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setRetryPolicy(new DefaultRetryPolicy(CONNECTION_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }


    /**
     * Add Volley Request In queue without TAG
     */
    public <T> void addToRequestQueue(Request<T> req) {
        req.setRetryPolicy(new DefaultRetryPolicy(
                CONNECTION_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    /*
     * Check Out Internet connection
     * */
    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    /*
     *
     * Return the Image Loader
     * */
    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }
}
