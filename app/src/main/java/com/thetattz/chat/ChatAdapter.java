package com.thetattz.chat;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.utils.Constants;
import com.thetattz.views.RelativeTimeTextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {
    String TAG = ChatAdapter.this.getClass().getSimpleName();
    Activity mActivity;
    ArrayList<AllMessagesItem> mArrayList = new ArrayList<>();

    public ChatAdapter(Activity mActivity, ArrayList<AllMessagesItem> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_message_comment, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AllMessagesItem mModel = mArrayList.get(position);

        if (mModel.getProfile_pic() != null && mModel.getProfile_pic().contains("http"))
            Picasso.with(mActivity).load(mModel.getProfile_pic().trim().replaceAll(" ", "%20"))
                    .placeholder(R.drawable.ic_p_pp)
                    .error(R.drawable.ic_p_pp)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.imgProfilePicCIV);
        else
            holder.imgProfilePicCIV.setImageResource(R.drawable.ic_p_pp);

        holder.txtMessageTV.setText(mModel.getMessage());

        if (mModel.getMessage_time_val() != null && !mModel.getMessage_time_val().equals("")) {
            holder.txtDateTimeTV.setText(convertTimeStampToDate(Long.parseLong(mModel.getMessage_time_val()))
                    .replace("am", "AM").replace("pm", "PM").replace("-", "/"));
        }
    }

    @Override
    public int getItemCount() {
        if (mArrayList.size() > 0)
            return mArrayList.size();
        else
            return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imgProfilePicCIV;
        public TextView txtMessageTV;
        public RelativeTimeTextView txtDateTimeTV;
        public View mViewV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgProfilePicCIV = itemView.findViewById(R.id.imgProfilePicCIV);
            txtMessageTV = itemView.findViewById(R.id.txtMessageTV);
            txtDateTimeTV = itemView.findViewById(R.id.txtDateTimeTV);
            mViewV = itemView.findViewById(R.id.mViewV);
        }
    }

    /*
     * Convert Time String to Milliseconds //08-04-2020 12:45 PM
     * */
    public long convertTimeStringToLongNotifications(String strDate) {
        long millisecondsSinceEpoch = 0;
        SimpleDateFormat f = new SimpleDateFormat("MM/dd/yyyy");
        Date parseDate = null;
        try {
            parseDate = f.parse(strDate);
            millisecondsSinceEpoch = parseDate.getTime();
            return millisecondsSinceEpoch;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return millisecondsSinceEpoch;
    }

    public String convertTimeStampToDate(long unixSeconds) {
        /* get timezone */
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        List<String> mList = Arrays.asList(TimeZone.getAvailableIDs());
        Log.e(TAG, "***TimeZoneList***" + mList);
        String timezone = timeZone.getID();
        Log.e(TAG, "***TimeZone***" + timezone);

        /* get time acc. to timezone */
        Calendar c = Calendar.getInstance();
        Date date = c.getTime(); //current date and time in UTC
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        df.setTimeZone(TimeZone.getTimeZone(timezone)); //format in given timezone
        String strDateCurrent = df.format(date);

        String currentDate = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault()).format(new Date());

        Date d = new Date(unixSeconds * 1000);
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
//        SimpleDateFormat formatterToday = new SimpleDateFormat("HH:mm");
        DateFormat dateFormat = android.text.format.DateFormat.getTimeFormat(mActivity);

        if (formatter.format(d).equals(currentDate) || formatter.format(d).equals(strDateCurrent)) {
            return dateFormat.format(d);
        } else {
            return formatter.format(d);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}
