package com.thetattz.chat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.BaseActivity;
import com.thetattz.activities.HomeActivity;
import com.thetattz.activities.artist.ArtistHomeActivity;
import com.thetattz.models.ArtistAppointmentDetailsModel;
import com.thetattz.models.SendMessageModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class ChatActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ChatActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = ChatActivity.this;

    /**
     * Widgets Main Layout
     */
    @BindView(R.id.rlBackRL)
    RelativeLayout rlBackRL;
    @BindView(R.id.imgProfilePicCIV)
    CircleImageView imgProfilePicCIV;
    @BindView(R.id.txtUserNameTV)
    TextView txtUserNameTV;
    @BindView(R.id.messageRecyclerViewRV)
    RecyclerView messageRecyclerViewRV;
    @BindView(R.id.editMessageET)
    EditText editMessageET;
    @BindView(R.id.imgSendMsgIV)
    ImageView imgSendMsgIV;
    @BindView(R.id.mViewProgressIV)
    com.wang.avi.AVLoadingIndicatorView mViewProgressIV;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    /*
     * Initialize Objects...
     * */
    ArtistAppointmentDetailsModel mModel;
    String strType = "", Room_id = "", Sender_Profile = "", Appointment_Id = "", Artist_Id = "",
            Sender_Name = "", receiver_id = "", strPushType = "";
    ArrayList<AllMessagesItem> mArrayList = new ArrayList<>();
    ChatAdapter mChatAdapter;
    int PAGE_NO = 1;
    boolean isPagination = false;
    private Socket mSocket;

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };
    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Error connecting");
                }
            });
        }
    };
    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject mDataObject = new JSONObject(args[1].toString());
                        Log.e(TAG, "**Message Data**" + mDataObject.toString());
                        AllMessagesItem mAllMessagesItem = new AllMessagesItem();

                        mAllMessagesItem.setId(mDataObject.getString("id"));
                        mAllMessagesItem.setSender_id(mDataObject.getString("sender_id"));
                        mAllMessagesItem.setReceiver_id(mDataObject.getString("receiver_id"));
                        mAllMessagesItem.setRoom_id(mDataObject.getString("room_id"));
                        mAllMessagesItem.setAppointment_id(mDataObject.getString("appointment_id"));
                        mAllMessagesItem.setMessage(mDataObject.getString("message"));
                        mAllMessagesItem.setMessage_time(mDataObject.getString("message_time"));
                        mAllMessagesItem.setMessage_time_val(mDataObject.getString("message_time_val"));
                        mAllMessagesItem.setMessage_seen(mDataObject.getString("message_seen"));
                        mAllMessagesItem.setMessage_current_time(mDataObject.getString("message_current_time"));
                        mAllMessagesItem.setProfile_pic(mDataObject.getString("sender_profile_pic"));

                        mArrayList.add(mAllMessagesItem);
                        mChatAdapter.notifyDataSetChanged();
                        editMessageET.setText("");
                        scrollToBottom();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject mJsonData = null;
                    try {
                        mJsonData = new JSONObject(String.valueOf(args[1]));

                        scrollToBottom();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };
    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };
    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    boolean isTyping = (boolean) args[1];

                }
            });
        }
    };
    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };
    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {

        }
    };

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        setStatusBar();

        ButterKnife.bind(this);

        getIntentData();

//        setEditFocus();

        //Set Pagination
        setPagination();

        //Set Adapter
        setAdapter();
    }

    private void setPagination() {
        mSwipeRefreshLayout.setColorSchemeColors(Color.BLUE, Color.RED, Color.GREEN);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                PAGE_NO++;
                isPagination = true;
                executeGetAllMessages();
            }
        });
    }

    /*
     * Set Edit Message Focus
     * */
    private void setEditFocus() {
        editMessageET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                messageRecyclerViewRV.scrollToPosition(mArrayList.size() - 1);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    /*
     * Socket Chat
     * */
    private void getIntentData() {
        if (getIntent() != null) {

            if ((ArtistAppointmentDetailsModel) getIntent().getSerializableExtra(Constants.MODEL) != null) {
                if ((ArtistAppointmentDetailsModel) getIntent().getSerializableExtra(Constants.MODEL) != null) {
                    mModel = (ArtistAppointmentDetailsModel) getIntent().getSerializableExtra(Constants.MODEL);
                    strType = getIntent().getStringExtra(Constants.TYPES);
                    Room_id = mModel.getmRoomId();
                    Appointment_Id = mModel.getmAppointmentID();

                    if (strType.equals(getString(R.string.type_artist))) {
                        receiver_id = mModel.getData().getUser_detail().getUser_id();
                        Sender_Profile = mModel.getData().getUser_detail().getProfile_pic();
                        Sender_Name = mModel.getData().getUser_detail().getName();
                    } else if (strType.equals(getString(R.string.type_details))) {
                        strType = getIntent().getStringExtra(Constants.TYPES);
                        Room_id = getIntent().getStringExtra(Constants.ROOM_ID);
                        receiver_id = getIntent().getStringExtra(Constants.RECEIVER_ID);
                        Sender_Profile = getIntent().getStringExtra(Constants.SENDER_PROFILE);
                        Sender_Name = getIntent().getStringExtra(Constants.SENDER_NAME);
                    } else {
                        receiver_id = mModel.getData().getArtist_detail().getArtistId();
                        Sender_Profile = mModel.getData().getArtist_detail().getprofile_pic();
                        Sender_Name = mModel.getData().getArtist_detail().getName();
                    }
                }
            } else {
                if (getIntent().getStringExtra(Constants.TYPES) != null)
                    strType = getIntent().getStringExtra(Constants.TYPES);

                if (getIntent().getStringExtra(Constants.ROOM_ID) != null)
                    Room_id = getIntent().getStringExtra(Constants.ROOM_ID);

                if (getIntent().getStringExtra(Constants.SENDER_ID) != null)
                    receiver_id = getIntent().getStringExtra(Constants.SENDER_ID);

                if (getIntent().getStringExtra(Constants.APPOINTMENT_ID) != null)
                    Appointment_Id = getIntent().getStringExtra(Constants.APPOINTMENT_ID);

                if (getIntent().getStringExtra(Constants.SENDER_PROFILE) != null)
                    Sender_Profile = getIntent().getStringExtra(Constants.SENDER_PROFILE);

                if (getIntent().getStringExtra(Constants.SENDER_NAME) != null)
                    Sender_Name = getIntent().getStringExtra(Constants.SENDER_NAME);
            }

            if (getIntent().getStringExtra(Constants.NOTIFICATION_TYPE) != null) {
                strPushType = getIntent().getStringExtra(Constants.NOTIFICATION_TYPE);
            }

            setDataOnWidgets();
            setUpSocketData();
        }
    }

    private void setUpSocketData() {
        TheTattzApplication app = (TheTattzApplication) getApplication();

        mSocket = app.getSocket();
        mSocket.emit("ConncetedChat", Room_id);
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("newMessage", onNewMessage);
        mSocket.on("leaveChat", onUserLeft);
        mSocket.connect();

        getMessagesData();
    }

    private void getMessagesData() {
        if (ConnectivityReceiver.isConnected())
            executeGetAllMessages();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void setDataOnWidgets() {
//        if (strType.equals(getString(R.string.type_artist))) {
//
//            if (mModel != null) {
//
//                if (mModel.getData().getUser_detail().getProfile_pic() != null && mModel.getData().getUser_detail().getProfile_pic().contains("http"))
//                    Picasso.with(mActivity).load(mModel.getData().getUser_detail().getProfile_pic())
//                            .placeholder(R.drawable.ic_p_pp)
//                            .error(R.drawable.ic_p_pp)
//                            .memoryPolicy(MemoryPolicy.NO_CACHE)
//                            .into(imgProfilePicCIV);
//                else
//                    imgProfilePicCIV.setImageResource(R.drawable.ic_p_pp);
//
//                txtUserNameTV.setText(mModel.getData().getUser_detail().getName());
//            }
//        } else {
//
//            if (mModel != null) {
//
//                if (mModel.getData().getArtist_detail().getprofile_pic() != null && mModel.getData().getArtist_detail().getprofile_pic().contains("http"))
//                    Picasso.with(mActivity).load(mModel.getData().getArtist_detail().getprofile_pic())
//                            .placeholder(R.drawable.ic_p_pp)
//                            .error(R.drawable.ic_p_pp)
//                            .memoryPolicy(MemoryPolicy.NO_CACHE)
//                            .into(imgProfilePicCIV);
//                else
//                    imgProfilePicCIV.setImageResource(R.drawable.ic_p_pp);
//
//                txtUserNameTV.setText(mModel.getData().getArtist_detail().getName());
//            }
//        }
        if (Sender_Profile != null && Sender_Profile.contains("http"))
            Picasso.with(mActivity).load(Sender_Profile)
                    .placeholder(R.drawable.ic_p_pp)
                    .error(R.drawable.ic_p_pp)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgProfilePicCIV);
        else
            imgProfilePicCIV.setImageResource(R.drawable.ic_p_pp);

        txtUserNameTV.setText(CapitalizedFullName(Sender_Name));
    }

    /*
     * Widgets click listeners
     * */
    @OnClick({R.id.rlBackRL, R.id.imgSendMsgIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBackRL:
                onBackPressed();
                break;
            case R.id.imgSendMsgIV:
                performSendMsgClick();
                break;
        }
    }

    @Override
    public void onBackPressed() {

        /* to manage onBackPressed for push and normal intent */
        if (!strPushType.equals("") && strPushType.equals(Constants.PUSH)) {

            if (getUserRole().equals(Constants.ROLE_ARTIST)) {
                Intent mIntent = new Intent(mActivity, ArtistHomeActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mIntent.putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH);
                startActivity(mIntent);
                finish();
            } else if (getUserRole().equals(Constants.ROLE_CLIENT)) {
                Intent mIntent = new Intent(mActivity, HomeActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mIntent.putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH);
                startActivity(mIntent);
                finish();
            }

        } else {
            finish();
        }

    }

    private void performSendMsgClick() {
        if (editMessageET.getText().toString().length() > 0 && !editMessageET.getText().toString().trim().equals("")) {
            if (ConnectivityReceiver.isConnected())
                executeSendMessage();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            showToast(mActivity, getString(R.string.write_something));
        }
    }

    private void executeSendMessage() {
        long unixTime = System.currentTimeMillis() / 1000L;
        Log.e(TAG, "**unixTime**" + String.valueOf(unixTime));

        mViewProgressIV.setVisibility(View.VISIBLE);
        imgSendMsgIV.setVisibility(View.GONE);
        final String mApiUrl = Constants.CHAT_PUT_MESSAGE;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("message", editMessageET.getText().toString().trim());
//            if (strType.equals(getString(R.string.type_artist))) {
            mParams.put("sender_id", getUserID());
            mParams.put("receiver_id", receiver_id);
//            } else {
//                mParams.put("sender_id", getUserID());
//                mParams.put("receiver_id", mModel.getData().getArtist_detail().getArtistId());
//            }
            mParams.put("room_id", Room_id);
            mParams.put("appointment_id", Appointment_Id);
            mParams.put("message_time_val", unixTime);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mViewProgressIV.setVisibility(View.GONE);
                        imgSendMsgIV.setVisibility(View.VISIBLE);
                        Log.e(TAG, "**RESPONSE**" + response.toString());

                        try {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.serializeNulls();
                            Gson gson = gsonBuilder.create();

                            SendMessageModel mRoomIdModel = gson.fromJson(response.toString(), SendMessageModel.class);

                            if (mRoomIdModel.getStatus() == 1) {
                                JSONObject mMessageData = response.getJSONObject("data");
                                AllMessagesItem mItemModel = new AllMessagesItem();
                                if (mRoomIdModel.getData().getId() != null) {
                                    mItemModel.setId(mRoomIdModel.getData().getId());
                                }
                                if (mRoomIdModel.getData().getSender_id() != null) {
                                    mItemModel.setSender_id(mRoomIdModel.getData().getSender_id());
                                }
                                if (mRoomIdModel.getData().getReceiver_id() != null) {
                                    mItemModel.setReceiver_id(mRoomIdModel.getData().getReceiver_id());
                                }
                                if (mRoomIdModel.getData().getRoom_id() != null) {
                                    mItemModel.setRoom_id(mRoomIdModel.getData().getRoom_id());
                                }
                                if (mRoomIdModel.getData().getAppointment_id() != null) {
                                    mItemModel.setAppointment_id(mRoomIdModel.getData().getAppointment_id());
                                }
                                if (mRoomIdModel.getData().getMessage() != null) {
                                    mItemModel.setMessage(mRoomIdModel.getData().getMessage());
                                }
                                if (mRoomIdModel.getData().getMessage_time() != null) {
                                    mItemModel.setMessage_time(mRoomIdModel.getData().getMessage_time());
                                }
                                if (mRoomIdModel.getData().getMessage_time_val() != null) {
                                    mItemModel.setMessage_time_val(mRoomIdModel.getData().getMessage_time_val());
                                }
                                if (mRoomIdModel.getData().getMessage_seen() != null) {
                                    mItemModel.setMessage_seen(mRoomIdModel.getData().getMessage_seen());
                                }
                                if (mRoomIdModel.getData().getMessage_current_time() != null) {
                                    mItemModel.setMessage_current_time(mRoomIdModel.getData().getMessage_current_time());
                                }
                                if (mRoomIdModel.getData().getSender_profile_pic() != null) {
                                    mItemModel.setProfile_pic(mRoomIdModel.getData().getSender_profile_pic());
                                }

                                mArrayList.add(mItemModel);

                                mChatAdapter.notifyDataSetChanged();
                                editMessageET.setText("");
                                scrollToBottom();

                                // perform the sending message attempt.
                                Gson gson1 = new Gson();
                                String mOjectString = gson1.toJson(mRoomIdModel.getData());
                                JSONObject obj = null;
                                try {
                                    obj = new JSONObject(mOjectString);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Log.e("TAG", "*****Msg****" + mOjectString);

                                // perform the sending message attempt.
                                mSocket.emit("newMessage", Room_id, obj);

                            } else {
                                showToast(mActivity, mRoomIdModel.getMessage());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "**Error**" + e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mViewProgressIV.setVisibility(View.GONE);
                        imgSendMsgIV.setVisibility(View.VISIBLE);
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }
        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void scrollToBottom() {
        messageRecyclerViewRV.scrollToPosition(mChatAdapter.getItemCount() - 1);
    }

    private void executeGetAllMessages() {
        if (isPagination == false)
            showProgressDialog(mActivity);

        final String mApiUrl = Constants.GET_MESSAGE_ALL_MESSAGES;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("page_no", PAGE_NO);
            mParams.put("room_id", Room_id);
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dismissProgressDialog();
                        mSwipeRefreshLayout.setRefreshing(false);
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        try {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.serializeNulls();
                            Gson gson = gsonBuilder.create();
                            GetAllMessagesModel mMessagesModel = gson.fromJson(response.toString(), GetAllMessagesModel.class);
                            if (mMessagesModel.getStatus() == 1) {
                                mArrayList.addAll(0, mMessagesModel.getAll_messages());
                                editMessageET.setText("");

                                Collections.sort(mArrayList, new Comparator<AllMessagesItem>() {
                                    @Override
                                    public int compare(AllMessagesItem lhs, AllMessagesItem rhs) {
                                        return lhs.getMessage_time_val().compareTo(rhs.getMessage_time_val());
                                    }
                                });

                                if (isPagination == false) {
                                    setAdapter();
                                    scrollToBottom();
                                } else {
                                    isPagination = false;
                                    mChatAdapter.notifyDataSetChanged();
                                    mSwipeRefreshLayout.setRefreshing(false);
                                }

                                Log.e(TAG, "**Message List Size**" + mArrayList.size());
                            } else {
//                                showToast(mActivity, mMessagesModel.getMessage());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "**Error**" + e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void setAdapter() {
        messageRecyclerViewRV.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
        mChatAdapter = new ChatAdapter(mActivity, mArrayList);
        messageRecyclerViewRV.setAdapter(mChatAdapter);

        if (PAGE_NO == 1) {
            if (editMessageET.hasFocus()) {
                messageRecyclerViewRV.scrollToPosition(mArrayList.size() - 1);
            }
            messageRecyclerViewRV.scrollToPosition(mArrayList.size() - 1);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mChatAdapter != null) {
            mChatAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if (mChatAdapter != null) {
            mChatAdapter.notifyDataSetChanged();
        }
    }
}
