package com.thetattz.chat;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AllMessagesItem  implements Serializable, Comparable<AllMessagesItem> {
	private String id;
	private String sender_id;
	private String receiver_id;
	private String room_id;
	private String appointment_id;
	private String message;
	private String message_time;
	private String message_seen;
	private String profile_pic;
	private String message_current_time;
	private String message_time_val;

	public String getMessage_time_val() {
		return message_time_val;
	}

	public void setMessage_time_val(String message_time_val) {
		this.message_time_val = message_time_val;
	}

	public String getMessage_current_time() {
		return message_current_time;
	}

	public void setMessage_current_time(String message_current_time) {
		this.message_current_time = message_current_time;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSender_id() {
		return sender_id;
	}

	public void setSender_id(String sender_id) {
		this.sender_id = sender_id;
	}

	public String getReceiver_id() {
		return receiver_id;
	}

	public void setReceiver_id(String receiver_id) {
		this.receiver_id = receiver_id;
	}

	public String getRoom_id() {
		return room_id;
	}

	public void setRoom_id(String room_id) {
		this.room_id = room_id;
	}

	public String getAppointment_id() {
		return appointment_id;
	}

	public void setAppointment_id(String appointment_id) {
		this.appointment_id = appointment_id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage_time() {
		return message_time;
	}

	public void setMessage_time(String message_time) {
		this.message_time = message_time;
	}

	public String getMessage_seen() {
		return message_seen;
	}

	public void setMessage_seen(String message_seen) {
		this.message_seen = message_seen;
	}

	public String getProfile_pic() {
		return profile_pic;
	}

	public void setProfile_pic(String profile_pic) {
		this.profile_pic = profile_pic;
	}

	@Override
 	public String toString(){
		return
			"AllMessagesItem{" +
			"room_id = '" + room_id + '\'' +
			",message_time = '" + message_time + '\'' +
			",message_seen = '" + message_seen + '\'' +
			",receiver_id = '" + receiver_id + '\'' +
			",profile_pic = '" + profile_pic + '\'' +
			",appointment_id = '" + appointment_id + '\'' +
			",id = '" + id + '\'' +
			",message = '" + message + '\'' +
			",sender_id = '" + sender_id + '\'' +
			"}";
		}


	@Override
	public int compareTo(AllMessagesItem f) {
		if (convertTimeStringToLongChating(message_current_time)  > convertTimeStringToLongChating(f.getMessage_current_time())) {
			return 0;
		} else if (convertTimeStringToLongChating(message_current_time) < convertTimeStringToLongChating(f.getMessage_current_time())) {
			return -1;
		} else {
			return 1;
		}
	}


	/*
	 * Convert Time String to Milliseconds //20\/04\/2020 07:04 PM
	 * */
	public long convertTimeStringToLongChating(String strDate) {
		long millisecondsSinceEpoch = 0;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");
		Date parseDate = null;
		try {
			parseDate = f.parse(strDate);
			millisecondsSinceEpoch = parseDate.getTime();
			return millisecondsSinceEpoch;
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return millisecondsSinceEpoch;
	}


}
