package com.thetattz.chat;

import java.util.ArrayList;

public class GetAllMessagesModel{
	private String room_id;
	private ArrayList<AllMessagesItem> all_messages;
	private String message;
	private int status;

	public String getRoom_id() {
		return room_id;
	}

	public void setRoom_id(String room_id) {
		this.room_id = room_id;
	}

	public ArrayList<AllMessagesItem> getAll_messages() {
		return all_messages;
	}

	public void setAll_messages(ArrayList<AllMessagesItem> all_messages) {
		this.all_messages = all_messages;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"GetAllMessagesModel{" +
			"room_id = '" + room_id + '\'' +
			",all_messages = '" + all_messages + '\'' +
			",message = '" + message + '\'' +
			",status = '" + status + '\'' +
			"}";
		}
}