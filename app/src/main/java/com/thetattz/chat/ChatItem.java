package com.thetattz.chat;

import java.io.Serializable;

public class ChatItem implements Serializable {
	private String profile_pic;
	private String message_id;
	private String message;
	private String message_date;


	public String getMessage_date() {
		return message_date;
	}

	public void setMessage_date(String message_date) {
		this.message_date = message_date;
	}

	public String getProfile_pic() {
		return profile_pic;
	}

	public void setProfile_pic(String profile_pic) {
		this.profile_pic = profile_pic;
	}

	public String getMessage_id() {
		return message_id;
	}

	public void setMessage_id(String message_id) {
		this.message_id = message_id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
 	public String toString(){
		return
			"ChatItem{" +
			"message_date = '" + message_date + '\'' +
			",profile_pic = '" + profile_pic + '\'' +
			",message_id = '" + message_id + '\'' +
			",message = '" + message + '\'' +
			"}";
		}
}
