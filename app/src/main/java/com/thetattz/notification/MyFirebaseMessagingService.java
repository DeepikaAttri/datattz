package com.thetattz.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.thetattz.R;
import com.thetattz.activities.HomeActivity;
import com.thetattz.activities.artist.ArtistAppointActivity;
import com.thetattz.chat.ChatActivity;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    PendingIntent contentIntent;
    String title = "", message = "", forumId = "", roomID;
    Intent intent;
    private static int NOTIFICATION_ID = 6578;
    Bitmap bitmap;
    int badgeCount;
    String type = "", appointment_status = "", user_id = "", notification_id = "", artist_id = "",
            receiver_id = "", sender_id = "", room_id = "", appointment_id = "", sender_name = "", profile_pic = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if ySou intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        Log.e(TAG, "From: " + remoteMessage.getFrom());

        sendNotificationNew(remoteMessage);
    }

    private void sendNotificationNew(RemoteMessage remoteMessage) {
        if (remoteMessage.getNotification() != null) {
            title = remoteMessage.getNotification().getTitle();
            message = remoteMessage.getNotification().getBody();
        }

        if (remoteMessage.getData().get("type") != null)
            type = String.valueOf(remoteMessage.getData().get("type"));

        String resultResponse = String.valueOf(remoteMessage.getData().get("detail"));

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(String.valueOf(resultResponse));

            if (jsonObject.has("appointment_status")) {
                appointment_status = jsonObject.getString("appointment_status");
            }
            if (jsonObject.has("user_id")) {
                user_id = jsonObject.getString("user_id");
            }
            if (jsonObject.has("notification_id")) {
                notification_id = jsonObject.getString("notification_id");
            }
            if (jsonObject.has("artist_id")) {
                artist_id = jsonObject.getString("artist_id");
            }
            if (jsonObject.has("receiver_id")) {
                receiver_id = jsonObject.getString("receiver_id");
            }
            if (jsonObject.has("sender_id")) {
                sender_id = jsonObject.getString("sender_id");
            }
            if (jsonObject.has("room_id")) {
                room_id = jsonObject.getString("room_id");
            }
            if (jsonObject.has("appointment_id")) {
                appointment_id = jsonObject.getString("appointment_id");
            }
            if (jsonObject.has("sender_name")) {
                sender_name = jsonObject.getString("sender_name");
            }
            if (jsonObject.has("profile_pic")) {
                profile_pic = jsonObject.getString("profile_pic");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (type != null && !type.equals("")) {
            if (type.equalsIgnoreCase("appointment")) {

                if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_ARTIST)) {

                    TheTattzPrefrences.writeString(getApplicationContext(), Constants.TYPES, getApplicationContext().getResources().getString(R.string.type_artist));

                    if (appointment_status.equals("1")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_pending));
                        intent = new Intent(getApplicationContext(), ArtistAppointActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    } else if (appointment_status.equals("2")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_confirm));
                        intent = new Intent(getApplicationContext(), ArtistAppointActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    } else if (appointment_status.equals("3")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_cancelled));
                        intent = new Intent(getApplicationContext(), ArtistAppointActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    } else if (appointment_status.equals("4")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_complete));
                        intent = new Intent(getApplicationContext(), ArtistAppointActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    } else if (appointment_status.equals("5")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_past));
                        intent = new Intent(getApplicationContext(), ArtistAppointActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    }

                } else if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_CLIENT)) {

                    TheTattzPrefrences.writeString(getApplicationContext(), Constants.TYPES, getApplicationContext().getResources().getString(R.string.type_client));

                    if (appointment_status.equals("1")) {
                        intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_pending));
                    } else if (appointment_status.equals("2")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_confirm));
                        intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    } else if (appointment_status.equals("3")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_cancelled));
                        intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    } else if (appointment_status.equals("4")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_complete));
                        intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    } else if (appointment_status.equals("5")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_past));
                        intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    }
                }
            } else if (type.equalsIgnoreCase("chat")) {
                intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.putExtra(Constants.SENDER_ID, sender_id);
                intent.putExtra(Constants.ROOM_ID, room_id);
                intent.putExtra(Constants.SENDER_NAME, sender_name);
                intent.putExtra(Constants.SENDER_PROFILE, profile_pic);
                intent.putExtra(Constants.APPOINTMENT_ID, appointment_id);
                if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_ARTIST)) {
                    intent.putExtra(Constants.TYPES, getString(R.string.type_artist));
                } else if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_CLIENT)) {
                    intent.putExtra(Constants.TYPES, getString(R.string.type_client));
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                contentIntent = PendingIntent.getActivity(this, 1, intent, 0);
            } else if (type.equalsIgnoreCase("general_chat")) {
                intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.putExtra(Constants.SENDER_ID, sender_id);
                intent.putExtra(Constants.ROOM_ID, room_id);
                intent.putExtra(Constants.SENDER_NAME, sender_name);
                intent.putExtra(Constants.SENDER_PROFILE, profile_pic);
                intent.putExtra(Constants.APPOINTMENT_ID, appointment_id);
                if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_ARTIST)) {
                    intent.putExtra(Constants.TYPES, getString(R.string.type_artist));
                } else if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_CLIENT)) {
                    intent.putExtra(Constants.TYPES, getString(R.string.type_client));
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                contentIntent = PendingIntent.getActivity(this, 1, intent, 0);
            }
        } else {
            intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            contentIntent = PendingIntent.getActivity(this, 1, intent, 0);
        }

        intent.putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */,
                intent,
                PendingIntent.FLAG_ONE_SHOT);
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setNumber(badgeCount)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setDefaults(NotificationCompat.DEFAULT_ALL);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {

                NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.default_notification_channel_name),
                        NotificationManager.IMPORTANCE_DEFAULT);
                channel.enableVibration(true);
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
            } else {
                notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
            }
        }
    }

    private void sendNotification(String title, String message, RemoteMessage remoteMessage) {
        type = String.valueOf(remoteMessage.getData().get("type"));

        String resultResponse = String.valueOf(remoteMessage.getData().get("detail"));

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(String.valueOf(resultResponse));

            if (jsonObject.has("appointment_status")) {
                appointment_status = jsonObject.getString("appointment_status");
            }
            if (jsonObject.has("user_id")) {
                user_id = jsonObject.getString("user_id");
            }
            if (jsonObject.has("notification_id")) {
                notification_id = jsonObject.getString("notification_id");
            }
            if (jsonObject.has("artist_id")) {
                artist_id = jsonObject.getString("artist_id");
            }
            if (jsonObject.has("receiver_id")) {
                receiver_id = jsonObject.getString("receiver_id");
            }
            if (jsonObject.has("sender_id")) {
                sender_id = jsonObject.getString("sender_id");
            }
            if (jsonObject.has("room_id")) {
                room_id = jsonObject.getString("room_id");
            }
            if (jsonObject.has("appointment_id")) {
                appointment_id = jsonObject.getString("appointment_id");
            }
            if (jsonObject.has("sender_name")) {
                sender_name = jsonObject.getString("sender_name");
            }
            if (jsonObject.has("profile_pic")) {
                profile_pic = jsonObject.getString("profile_pic");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (type != null) {
            if (type.equalsIgnoreCase("appointment")) {

                if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_ARTIST)) {

                    TheTattzPrefrences.writeString(getApplicationContext(), Constants.TYPES, getApplicationContext().getResources().getString(R.string.type_artist));

                    if (appointment_status.equals("1")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_pending));
                        intent = new Intent(getApplicationContext(), ArtistAppointActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    } else if (appointment_status.equals("2")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_confirm));
                        intent = new Intent(getApplicationContext(), ArtistAppointActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    } else if (appointment_status.equals("3")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_cancelled));
                        intent = new Intent(getApplicationContext(), ArtistAppointActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    } else if (appointment_status.equals("4")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_complete));
                        intent = new Intent(getApplicationContext(), ArtistAppointActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    } else if (appointment_status.equals("5")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_past));
                        intent = new Intent(getApplicationContext(), ArtistAppointActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    }

                } else if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_CLIENT)) {

                    TheTattzPrefrences.writeString(getApplicationContext(), Constants.TYPES, getApplicationContext().getResources().getString(R.string.type_client));

                    if (appointment_status.equals("1")) {
                        intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_pending));

                    } else if (appointment_status.equals("2")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_confirm));
                        intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    } else if (appointment_status.equals("3")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_cancelled));
                        intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    } else if (appointment_status.equals("4")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_complete));
                        intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    } else if (appointment_status.equals("5")) {
                        TheTattzPrefrences.writeString(getApplicationContext(), Constants.APPOINTMENT_TYPE, getApplicationContext().getResources().getString(R.string.type_past));
                        intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contentIntent = PendingIntent.getActivity(this, 1, intent, 0);

                    }
                }
            } else if (type.equalsIgnoreCase("chat")) {
                intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.putExtra(Constants.SENDER_ID, sender_id);
                intent.putExtra(Constants.ROOM_ID, room_id);
                intent.putExtra(Constants.SENDER_NAME, sender_name);
                intent.putExtra(Constants.SENDER_PROFILE, profile_pic);
                intent.putExtra(Constants.APPOINTMENT_ID, appointment_id);
                if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_ARTIST)) {
                    intent.putExtra(Constants.TYPES, getString(R.string.type_artist));
                } else if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_CLIENT)) {
                    intent.putExtra(Constants.TYPES, getString(R.string.type_client));
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                contentIntent = PendingIntent.getActivity(this, 1, intent, 0);
            } else if (type.equalsIgnoreCase("general_chat")) {
                intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.putExtra(Constants.SENDER_ID, sender_id);
                intent.putExtra(Constants.ROOM_ID, room_id);
                intent.putExtra(Constants.SENDER_NAME, sender_name);
                intent.putExtra(Constants.SENDER_PROFILE, profile_pic);
                intent.putExtra(Constants.APPOINTMENT_ID, appointment_id);
                if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_ARTIST)) {
                    intent.putExtra(Constants.TYPES, getString(R.string.type_artist));
                } else if (TheTattzPrefrences.readString(getApplicationContext(), TheTattzPrefrences.USER_ROLE, "").equals(Constants.ROLE_CLIENT)) {
                    intent.putExtra(Constants.TYPES, getString(R.string.type_client));
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                contentIntent = PendingIntent.getActivity(this, 1, intent, 0);
            }
        }

        intent.putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */,
                intent,
                PendingIntent.FLAG_ONE_SHOT);
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setNumber(badgeCount)
                        .setPriority(NotificationCompat.PRIORITY_HIGH).setDefaults(NotificationCompat.DEFAULT_ALL);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {

                NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.default_notification_channel_name),
                        NotificationManager.IMPORTANCE_DEFAULT);
                channel.enableVibration(true);
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
            } else {
                notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
            }
        }
    }

    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            return;
        }
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    public static String getLauncherClassName(Context context) {

        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                String className = resolveInfo.activityInfo.name;
                return className;
            }
        }
        return null;
    }
}
