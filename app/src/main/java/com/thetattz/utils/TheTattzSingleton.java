package com.thetattz.utils;


import com.thetattz.models.AppointmentsModel;
import com.thetattz.models.ArtistServiceModel;
import com.thetattz.models.MyTattozModel;
import com.thetattz.models.SearchArtistModel;
import com.thetattz.models.WorkInfo;

import java.util.ArrayList;

/**
 * Created by android-da on 5/19/18.
 */

public class TheTattzSingleton {


    private static TheTattzSingleton ourInstance;

    private SearchArtistModel mSearchModel = new SearchArtistModel();
    private  ArrayList<WorkInfo> mWorkInfoArrayList = new ArrayList<>();
    private ArrayList<MyTattozModel> mPhotosArrayList = new ArrayList<>();
    private ArrayList<ArtistServiceModel> mServicesArrayList = new ArrayList<>();
    private ArrayList<AppointmentsModel> mPastAppointmentArrayList = new ArrayList<>();
    private ArrayList<AppointmentsModel> mUpcomingAppointmentArrayList = new ArrayList<>();
    private ArrayList<AppointmentsModel> mArtistConfirmedAppointmentsAL = new ArrayList<>();
    private ArrayList<AppointmentsModel> mArtistPendingAppointmentsAL = new ArrayList<>();

    private TheTattzSingleton() { }



    public static TheTattzSingleton getInstance() {
        if (ourInstance == null) {
            ourInstance = new TheTattzSingleton();
        }
        return ourInstance;
    }

    public SearchArtistModel getSearchModel() {
        return mSearchModel;
    }

    public ArrayList<WorkInfo> getWorkInfoArrayList() {
        return mWorkInfoArrayList;
    }

    public ArrayList<MyTattozModel> getPhotosArrayList() {
        return mPhotosArrayList;
    }

    public ArrayList<ArtistServiceModel> getServicesArrayList() {
        return mServicesArrayList;
    }


    public ArrayList<AppointmentsModel> getPastAppointmentArrayList() {
        return mPastAppointmentArrayList;
    }

    public ArrayList<AppointmentsModel> getUpcomingAppointmentArrayList() {
        return mUpcomingAppointmentArrayList;
    }


    public ArrayList<AppointmentsModel> getArtistConfirmedAppointmentsAL() {
        return mArtistConfirmedAppointmentsAL;
    }

    public ArrayList<AppointmentsModel> getArtistPendingAppointmentsAL() {
        return mArtistPendingAppointmentsAL;
    }
}


