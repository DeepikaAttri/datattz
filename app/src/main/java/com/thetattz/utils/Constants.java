package com.thetattz.utils;

public class Constants {

    /*
     * Stripe Payment Keys:
     * */
    //TODO: Change with live stripe key
//    public static final String STRIPE_API_KEY = "pk_test_yS6r7IhwqQl07oog71cXzXGn00TkaKtXhH";
    public static final String STRIPE_SECRET_KEY = "sk_test_gRS8cobVjlfv7CPjAOzLT4tQ00KTb6qxgH";

    //live stripe key
    public static final String STRIPE_API_KEY = "pk_live_51HEWRCFrzSTM0P1IEaECltCHGgDKr38L2Q1Z41tqEgXv5zPnZR5RQ34zakfjRHLBlBS6jFbSbhjIKx8P8mglTddJ005vNhfQC2";

    //test stripe keys
//    public static final String STRIPE_API_KEY = "pk_test_51HEWRCFrzSTM0P1IrIvwuGm3M9ruoAg2ixabdSKCuikalxd6SnpH8P87HywLNRmaR0JKoEDDQIwci2NjRJcL6UCI006Y65gxnk";
//    public static final String STRIPE_SECRET_KEY = "sk_test_51HEWRCFrzSTM0P1Ig2haCvdPUrik2N1wc2RWzLldIWGyePohiGe39476yWaq7zuQSDIMhc44DJz9NfFj3r4pyF0I00AiQO2Zc9";

    //live stripe keys
//    public static final String STRIPE_API_KEY = "pk_live_51HEWRCFrzSTM0P1IEaECltCHGgDKr38L2Q1Z41tqEgXv5zPnZR5RQ34zakfjRHLBlBS6jFbSbhjIKx8P8mglTddJ005vNhfQC2";
//    public static final String STRIPE_SECRET_KEY = "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";

    /*
     * Stating Server Url
     * */
//    public static final String BASE_URL = "https://www.dharmani.com/TheTattz/";
//    public static final String BASE_URL = "https://www.dharmani.com/DatattzWebservices/";

    /*
     * Live Server Url
     * */
    public static final String BASE_URL = "https://www.datattz.com/Webservices/";
    /* New Base Url */
//    public static final String BASE_URL = "http://161.97.132.85/datattz/Webservices/";

    public static final String SOCKET_URL = "https://jaohar-uk.herokuapp.com";

    public static final String STRIPE = "Stripe/";
    public static final String CHAT = "Chat/";

    public static final String SIGN_IN = BASE_URL + "Login.php";
    public static final String SIGN_UP = BASE_URL + "SignUp.php";
    public static final String GET_TIME_BY_TIMEZONE = BASE_URL + "getTimeByTimezone.php";
    public static final String CLIENT_HOME_API = BASE_URL + "ClientHomescreen.php";
    public static final String ADD_TATTOOS = BASE_URL + "AddTattoo_Android.php";
    public static final String UPDATE_PASSWORD = BASE_URL + "UpdatePassword.php";
    public static final String EDIT_USER_PROFILE = BASE_URL + "EditUserProfile_Android.php";
    public static final String GET_USER_DETAILS = BASE_URL + "GetUserDetail.php";
    public static final String SEARCH_ARTIST = BASE_URL + "SearchArtistByLatLong.php";
    public static final String SEARCH_ARTIST_LOCATION = BASE_URL + "SearchArtist.php";
    public static final String ARTIST_SERVICES_LIST = BASE_URL + "GetServiceListing.php";
    public static final String ADD_RATING_TO_ARTIST = BASE_URL + "AddRatingToArtist.php";
    public static final String APPOINTMENTS_OF_CLIENT = BASE_URL + "AppointmentsOfClient.php";
    public static final String APPOINTMENTS_DETAILS = BASE_URL + "AppointmentDetail.php";
    public static final String FORGOT_PASSWORD = BASE_URL + "ForgetPassword.php";
    public static final String LOGOUT = BASE_URL + "Logout.php";
    public static final String BOOK_APPOINTMENT = BASE_URL + "BookAppointmentByClient.php";
    public static final String GET_SERVICES_LISTING = BASE_URL + "GetServiceListing.php";
    public static final String ARTIST_BOOKED_TIME_SLOT = BASE_URL + "ArtistBookedTimeSlotByDate.php";
    public static final String CHECK_USER_FAV_OR_NOT = BASE_URL + "CheckUserFavOrNot.php";
    public static final String ADD_FAVORITE = BASE_URL + "AddFavourite.php";
    public static final String REMOVE_FAVORITE = BASE_URL + "RemoveFavourite.php";
    public static final String NOTIFICATON_ACTIVITY = BASE_URL + "NotificationActivity.php";
    public static final String APPOINTMENT_DETATILS = BASE_URL + "AppointmentDetail.php";
    public static final String PAYPAL_LOGIN = BASE_URL + "PayPalLogin.php";

    //Stripe Payments
    public static final String RETRIVE_ALL_CARDS = BASE_URL + STRIPE + "ReterieveTheCards.php";
    public static final String ADD_CARDS_FOR_PAYMENT = BASE_URL + STRIPE + "AddCardForPayment.php";
    public static final String CLIENT_PAY_CHANGES = BASE_URL + STRIPE + "charge.php";

    //Artist Apis
    public static final String ARTIST_GET_USER_DETAILS = BASE_URL + "GetUserDetail.php";
    public static final String ARTIST_RATING_LIST = BASE_URL + "ArtistRatingList.php";
    public static final String ARTIST_HOME_SERVICES_LISTING = BASE_URL + "GetServiceListing.php";
    public static final String ARTIST_CLIENT_LISTING = BASE_URL + "GetTheClientsList.php";
    public static final String GET_LOCATION_AND_HOURS = BASE_URL + "GetLocationAndHours.php";
    public static final String EDIT_LOCATION_AND_HOURS = BASE_URL + "EditLocationAndHours.php";
    public static final String GET_MONTHLY_APPOINTMENTS = BASE_URL + "GetMonthlyAppointmentsDatesOfArtist.php";
    public static final String BOOK_APPOINTMENTS_BY_ARTIST = BASE_URL + "BookAppointmentByArtist.php";
    public static final String ADD_SERVICE = BASE_URL + "AddServices.php";
    public static final String ARTIST_ADD_ACCOUNT_DETAIL = BASE_URL + "ArtistAccountDetails/AddAccountDetail.php";
    public static final String ARTIST_GET_ACCOUNT_DETAIL = BASE_URL + "ArtistAccountDetails/RetrieveAccountDetail.php";
    public static final String ARTIST_UPDATE_ACCOUNT_DETAIL = BASE_URL + "ArtistAccountDetails/UpdateAccountDetail.php";
    public static final String ARTIST_APPOINTMENTS = BASE_URL + "AppointmentsOfArtist.php";
    public static final String UPDATE_APPOINTMENT_STATUS_BY_ARTIST = BASE_URL + "UpdateAppointmentStatusByArtist.php";
    public static final String APPOINTMENT_DONE_BY_ARTIST = BASE_URL + "AppointmentDoneByArtist.php";

    public static final String CHAT_PUT_MESSAGE = BASE_URL + CHAT + "AddMessage.php";
    public static final String GET_MESSAGE_ROOM_ID = BASE_URL + CHAT + "RequestChat.php";
    public static final String GET_MESSAGE_ALL_MESSAGES = BASE_URL + CHAT + "GetChatListing.php";

    public static final String CONTACT_US = BASE_URL + "ContactUs.php";
    public static final String DELETE_TATTOOS = BASE_URL + "DeleteTattoos.php";
    public static final String DELETE_SERVICE = BASE_URL + "DeleteArtistService.php";
    public static final String CREATE_ARTIST_STRIPE_ACCOUNT = BASE_URL + "Stripe/CreateArtistStripeAccount.php";
    public static final String UPLOAD_DOCUMENT = BASE_URL + "Stripe/UploadDocument.php";
    public static final String GET_ARTIST_ACCOUNT_DETAIL = BASE_URL + "Stripe/GetArtistAccountDetail.php";
    public static final String GET_STATE_NAME = BASE_URL + "Getstatename.php";

    /* implement update badge count API
     * API NAME: updateBadgeCount.php
     * Method : POST (RAW)
     * Param: user_id */
    public static final String UPDATE_BADGE_COUNT = BASE_URL + "updateBadgeCount.php";

    public static final String TERMS_AND_SERVICES_URL = "http://dharmani.com/TheTattz/Services.html";
    public static final String PRIVACY_POLICY_URL = "http://dharmani.com/TheTattz/PrivacyPolicy.html";
    public static final String FACEBOOK = "https://www.facebook.com/DaTattz";
    public static final String INSTAGRAM = "https://www.instagram.com/datattz";
    public static final String TWITTER = "https://twitter.com/jayclipz2";
    //    public static final String WEBSITE = "https://www.thetattz.com";
    public static final String WEBSITE = "https://datattz.com/";

    /*
     * Params
     * */
    public static String MODEL = "model";
    public static String MODEL_NEW = "_new_model";
    public static String ARTIST_ID = "artist_id";
    public static String LIST = "list";
    public static String POSITION = "position";
    public static String WORKINFO_LIST = "workinfo_list";
    public static String JSON_DATA = "json_data";
    public static String TYPES = "types";
    public static int REQUEST_CODE = 545;
    public static int REQUEST_CODE_PICTURE = 963;
    public static int ARTIST_HOME_TAB_1 = 11;
    public static int ARTIST_HOME_TAB_3 = 33;
    public static final String ID = "id";
    public static final String PRICE = "price";
    public static final String APPOINTMENT_TYPE = "appointment_type";
    public static final String APPOINTMENT_ID = "appointment_id";
    public static final String APPOINTMENT_STATUS = "appointment_status";

    //Role
    public static String ROLE_ARTIST = "artist";
    public static String ROLE_CLIENT = "client";

    //Contact Details
    public static String CONTACT_NAME = "contact_name";
    public static String CONTACT_NUMBER = "contact_number";

    //Chat
    public static String ROOM_ID = "room_id";
    public static String RECEIVER_ID = "receiver_id";
    public static String SENDER_ID = "sender_id";
    public static String SENDER_NAME = "sender_name";
    public static String SENDER_PROFILE = "sender_sender";

    //paypal
    public static String PAYPAL_LINK = "paypal_link";

    //payment method
    public static String PAYMENT = "payment";
    public static String SUBMITTED = "submitted";

    /*
     * Static Array:
     *
     * */
    public static String HOURS[] = {"0", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
    public static String HOURSTIME[] = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
    public static String HOURS_TEXT[] = {"Hours"};
    public static String MINUTESS[] = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60"};
    public static String MINUTESS_IN_LIMIT[] = {"0", "30"};
    public static String MINUTESS_LIMIT[] = {"00", "30"};
    public static String MINUTES_TEXT[] = {"Minutes"};
    public static String AMPM[] = {"AM", "PM"};
    public static String MONTHS_OF_YEAR[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    public static String YEARS[] = {"2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033"};

    /* Notification */
    public static String NOTIFICATION_TYPE = "notification_type";
    public static String PUSH = "push";
}
