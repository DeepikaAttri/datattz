package com.thetattz.utils.badgecount;

import android.content.ComponentName;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import java.util.Arrays;
import java.util.List;

import me.leolin.shortcutbadger.Badger;
import me.leolin.shortcutbadger.ShortcutBadgeException;

public class HuaweiHomeBadger implements Badger {

    @Override
    public void executeBadge(Context context, ComponentName componentName, int badgeCount) throws ShortcutBadgeException {
        final Bundle localBundle = new Bundle();
//        localBundle.putString("package", ApplicationLoader.applicationContext.getPackageName());
//        localBundle.putString("class", componentName.getClassName());
//        localBundle.putInt("badgenumber", badgeCount);
//        AndroidUtilities.runOnUIThread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    ApplicationLoader.applicationContext.getContentResolver().call(Uri.parse("content://com.huawei.android.launcher.settings/badge/"), "change_badge", null, localBundle);
//                } catch (Exception e) {
//                    FileLog.e(e);
//                }
//            }
//        });
    }

    @Override
    public List<String> getSupportLaunchers() {
        return Arrays.asList(
                "com.huawei.android.launcher"
        );
    }
}
