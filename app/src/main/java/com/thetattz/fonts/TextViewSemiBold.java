package com.thetattz.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by Dharmani Apps on 1/18/2018.
 */


/*
 * TextView with Custom font family
 * */
public class TextViewSemiBold extends TextView {
    /*
     * Getting Current Class Name
     * */
    private String mTag = TextViewSemiBold.this.getClass().getSimpleName();
    /*
     * Constructor with
     * #Context
     * */
    public TextViewSemiBold(Context context) {
        super(context);
        applyCustomFont(context);
    }

    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * */
    public TextViewSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * */
    public TextViewSemiBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * #int defStyleAttr
     * */
    public TextViewSemiBold(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }


    /*
     * Apply font.
     * */
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new PurssimaSemiBold(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}
