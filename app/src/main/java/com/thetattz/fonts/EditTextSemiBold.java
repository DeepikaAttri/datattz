package com.thetattz.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

/**
 * Created by Dharmani Apps on 1/18/2018.
 */

public class EditTextSemiBold extends EditText {

    /*
     * Getting Current Class Name
     * */
    private String mTag = EditTextSemiBold.this.getClass().getSimpleName();


    /*
     * Constructor with
     * #Context
     * */
    public EditTextSemiBold(Context context) {
        super(context);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * */
    public EditTextSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * */
    public EditTextSemiBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }



    /*
     * Apply font.
     * */

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new PurssimaSemiBold(context).getFont());
        } catch (Exception e) {
            Log.e(mTag, e.toString());
        }
    }
}
