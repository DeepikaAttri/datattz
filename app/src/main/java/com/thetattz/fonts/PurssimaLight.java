package com.thetattz.fonts;

import android.content.Context;
import android.graphics.Typeface;

public class PurssimaLight {
    /*
     * Initialize Context
     * */
    Context mContext;
    /*
     * Initialize the Typeface
     * */
    private Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    private String path = "ProximaNova-Light.ttf";

    /*
     * Default Constructor
     * */
    public PurssimaLight() {
    }

    /*
     * Constructor with Context
     * */
    public PurssimaLight(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null) {
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        }
        return fontTypeface;
    }

}
