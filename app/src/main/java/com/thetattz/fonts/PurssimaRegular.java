package com.thetattz.fonts;

import android.content.Context;
import android.graphics.Typeface;

public class PurssimaRegular {
    /*
     * Initialize Context
     * */
    Context mContext;
    /*
     * Initialize the Typeface
     * */
    private Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    private String path = "ProximaNova-Regular.ttf";

    /*
     * Default Constructor
     * */
    public PurssimaRegular() {
    }

    /*
     * Constructor with Context
     * */
    public PurssimaRegular(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null) {
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        }
        return fontTypeface;
    }

}
