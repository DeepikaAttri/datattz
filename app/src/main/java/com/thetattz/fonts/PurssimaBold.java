package com.thetattz.fonts;

import android.content.Context;
import android.graphics.Typeface;

public class PurssimaBold {
    /*
     * Initialize Context
     * */
    Context mContext;
    /*
     * Initialize the Typeface
     * */
    private Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    private String path = "Purissima Bold W00 Regular.ttf";

    /*
     * Default Constructor
     * */
    public PurssimaBold() {
    }

    /*
     * Constructor with Context
     * */
    public PurssimaBold(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null) {
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        }
        return fontTypeface;
    }

}
