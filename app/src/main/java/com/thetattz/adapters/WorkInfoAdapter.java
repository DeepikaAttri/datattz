package com.thetattz.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.thetattz.R;
import com.thetattz.models.WorkInfo;

import java.util.ArrayList;

/**
 * Created by android-da on 6/7/18.
 */

public class WorkInfoAdapter extends RecyclerView.Adapter<WorkInfoAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<WorkInfo> mArrayList;

    public WorkInfoAdapter(Activity mActivity, ArrayList<WorkInfo> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_work_info, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        WorkInfo mModel = mArrayList.get(position);
        holder.txtDayNameTV.setText(mModel.getDay());
        holder.txtTimeTV.setText(mModel.getStart_time() + " - " + mModel.getEnd_time());
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtDayNameTV, txtTimeTV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTimeTV = itemView.findViewById(R.id.txtTimeTV);
            txtDayNameTV = itemView.findViewById(R.id.txtDayNameTV);
        }
    }
}

