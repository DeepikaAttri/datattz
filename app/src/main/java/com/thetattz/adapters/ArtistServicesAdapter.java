package com.thetattz.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.thetattz.R;
import com.thetattz.models.ArtistServiceModel;

import java.util.ArrayList;

/**
 * Created by android-da on 6/7/18.
 */

public class ArtistServicesAdapter extends RecyclerView.Adapter<ArtistServicesAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<ArtistServiceModel> mArrayList;

    public ArtistServicesAdapter(Activity mActivity, ArrayList<ArtistServiceModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_artist_service, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ArtistServiceModel mModel = mArrayList.get(position);
        holder.txtServiceTV.setText(mModel.getService_name());
        holder.txtTimeTV.setText(mModel.getDuration());
        holder.txtDesTV.setText(mModel.getDescription());
        holder.txtPriceTV.setText("$"+mModel.getPrice());

        if (position == mArrayList.size() - 1){
            holder.mViewV.setVisibility(View.GONE);
        }else{
            holder.mViewV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtServiceTV, txtTimeTV, txtPriceTV, txtDesTV;
        public View mViewV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtServiceTV = itemView.findViewById(R.id.txtServiceTV);
            txtTimeTV = itemView.findViewById(R.id.txtTimeTV);
            txtPriceTV = itemView.findViewById(R.id.txtPriceTV);
            mViewV = itemView.findViewById(R.id.mViewV);
            txtDesTV = itemView.findViewById(R.id.txtDesTV);
        }
    }
}

