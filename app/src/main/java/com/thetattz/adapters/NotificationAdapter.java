package com.thetattz.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.activities.HomeActivity;
import com.thetattz.chat.ChatActivity;
import com.thetattz.models.ArtistNotificationModel;
import com.thetattz.utils.Constants;
import com.thetattz.utils.TheTattzPrefrences;
import com.thetattz.views.RelativeTimeTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<ArtistNotificationModel> mArrayList;

    public NotificationAdapter(Activity mActivity, ArrayList<ArtistNotificationModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ArtistNotificationModel mModel = mArrayList.get(position);

        if (mModel.getNotificationType().equals("appointment")) {
            holder.txtNotificationTypeTV.setText("New Appointment");
        } else {
            holder.txtNotificationTypeTV.setText("New Appointment Comment");
        }

        if (mModel.getProfilePic() != null && mModel.getProfilePic().contains("http")) {
            Picasso.with(mActivity).load(mModel.getProfilePic().trim().replaceAll(" ", "%20"))
                    .placeholder(R.drawable.ic_p_pp)
                    .error(R.drawable.ic_p_pp)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.profileImg);
        } else {
            holder.profileImg.setImageResource(R.drawable.ic_p_pp);
        }

        holder.txtMessageTV.setText(mModel.getMessage());

        holder.txtDateTimeTV.setReferenceTime(convertTimeStringToLongNotifications(convertTimeStampToDate(Long.parseLong(mModel.getDetail_time()))));

//        holder.txtDateTimeTV.setReferenceTime(((BaseActivity) mActivity).convertTimeStringToLongNotifications(mModel.getCreationDate()));

        holder.mainLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TheTattzPrefrences.writeString(mActivity, Constants.TYPES, mActivity.getResources().getString(R.string.type_client));

                if (mModel.getNotificationType().equalsIgnoreCase("appointment")) {

                    if (mModel.getAppointmentStatus().equals("1")) {
                        TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_pending));
                        mActivity.startActivity(new Intent(mActivity, HomeActivity.class));
                        mActivity.finish();
//                        Toast.makeText(mActivity, "Pending", Toast.LENGTH_SHORT).show();
                    } else if (mModel.getAppointmentStatus().equals("2")) {
                        TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_confirm));
                        mActivity.startActivity(new Intent(mActivity, HomeActivity.class));
                        mActivity.finish();
//                        Toast.makeText(mActivity, "Confirmed", Toast.LENGTH_SHORT).show();
                    } else if (mModel.getAppointmentStatus().equals("3")) {
                        TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_cancelled));
                        mActivity.startActivity(new Intent(mActivity, HomeActivity.class));
                        mActivity.finish();
//                        Toast.makeText(mActivity, "Cancelled", Toast.LENGTH_SHORT).show();
                    } else if (mModel.getAppointmentStatus().equals("4")) {
                        TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_complete));
                        mActivity.startActivity(new Intent(mActivity, HomeActivity.class));
                        mActivity.finish();
//                        Toast.makeText(mActivity, "Complete", Toast.LENGTH_SHORT).show();
                    } else if (mModel.getAppointmentStatus().equals("5")) {
                        TheTattzPrefrences.writeString(mActivity, Constants.APPOINTMENT_TYPE, mActivity.getResources().getString(R.string.type_past));
                        mActivity.startActivity(new Intent(mActivity, HomeActivity.class));
                        mActivity.finish();
//                        Toast.makeText(mActivity, "Past", Toast.LENGTH_SHORT).show();
                    }

                } else if (mModel.getNotificationType().equalsIgnoreCase("chat")) {
                    mActivity.startActivity(new Intent(mActivity, ChatActivity.class).
                            putExtra(Constants.ROOM_ID, mModel.getRoomId()).
                            putExtra(Constants.SENDER_ID, mModel.getSenderId()).
                            putExtra(Constants.TYPES, mActivity.getString(R.string.type_artist)).
                            putExtra(Constants.APPOINTMENT_ID, mModel.getAppointmentId()).
                            putExtra(Constants.SENDER_PROFILE, mModel.getProfilePic()).
                            putExtra(Constants.SENDER_NAME, mModel.getSenderName()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView profileImg;
        public TextView txtNotificationTypeTV, txtMessageTV;
        public RelativeTimeTextView txtDateTimeTV;
        public View mViewV;
        LinearLayout mainLL;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            profileImg = itemView.findViewById(R.id.profileImg);
            txtNotificationTypeTV = itemView.findViewById(R.id.txtNotificationTypeTV);
            txtMessageTV = itemView.findViewById(R.id.txtMessageTV);
            txtDateTimeTV = itemView.findViewById(R.id.txtDateTimeTV);
            mViewV = itemView.findViewById(R.id.mViewV);
            mainLL = itemView.findViewById(R.id.mainLL);
        }
    }

    /*
     * Convert Time String to Milliseconds //08-04-2020 12:45 PM
     * */
    public long convertTimeStringToLongNotifications(String strDate) {
        long millisecondsSinceEpoch = 0;
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
        Date parseDate = null;
        try {
            parseDate = f.parse(strDate);
            millisecondsSinceEpoch = parseDate.getTime();
            return millisecondsSinceEpoch;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return millisecondsSinceEpoch;
    }

    public String convertTimeStampToDate(long unixSeconds){
//        // convert seconds to milliseconds
//        Date date = new java.util.Date(unixSeconds*1000L);
//        // the format of your date
//        SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy hh:mm aa");
//        // give a timezone reference for formatting (see comment at the bottom)
//        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
//        String formattedDate = sdf.format(date);
//        System.out.println(formattedDate);
//        return  formattedDate;

        Date d = new Date(unixSeconds * 1000);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
        return formatter.format(d);
    }
}
