package com.thetattz.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.interfaces.MyArtistCallBack;
import com.thetattz.models.MyArtistModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by android-da on 6/7/18.
 */

public class MyArtistAdapter extends RecyclerView.Adapter<MyArtistAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<MyArtistModel> mArrayList;
    MyArtistCallBack mMyArtistCallBack;

    public MyArtistAdapter(Activity mActivity, ArrayList<MyArtistModel> mArrayList, MyArtistCallBack mMyArtistCallBack) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mMyArtistCallBack = mMyArtistCallBack;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_artist, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MyArtistModel mModel = mArrayList.get(position);
        holder.txtNameTV.setText(CapitalizedFullName(mModel.getName()));

        if (mModel.getProfile_pic() != null && mModel.getProfile_pic().contains("http"))
            Picasso.with(mActivity).load(mModel.getProfile_pic())
                    .placeholder(R.drawable.ic_p_pp)
                    .error(R.drawable.ic_p_pp)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.imgBackGroundIV);
        else
            holder.imgBackGroundIV.setImageResource(R.drawable.ic_p_pp);


        if (mModel.getProfile_pic() != null && mModel.getProfile_pic().contains("http"))
            Picasso.with(mActivity).load(mModel.getProfile_pic())
                    .placeholder(R.drawable.ic_placeholder)
                    .error(R.drawable.ic_placeholder)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.imgProfileIV);
        else
            holder.imgBackGroundIV.setImageResource(R.drawable.ic_placeholder);


        holder.btnBookAppointmentB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMyArtistCallBack.clickToBookAppointment(mModel);

            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMyArtistCallBack.clickToArtistDetials(mModel);

            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imgProfileIV;
        public ImageView imgBackGroundIV;
        public TextView txtNameTV;
        public Button btnBookAppointmentB;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgProfileIV = itemView.findViewById(R.id.imgProfileIV);
            imgBackGroundIV = itemView.findViewById(R.id.imgBackGroundIV);
            txtNameTV = itemView.findViewById(R.id.txtNameTV);
            btnBookAppointmentB = itemView.findViewById(R.id.btnBookAppointmentB);
        }
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFullName(String fullname) {
        String[] firstLastName = {};
        String FirstName = "", LastName = "";
        if (fullname != null && fullname.contains(" ")) {
            firstLastName = fullname.split(" ");
            FirstName = fullname.substring(0, fullname.lastIndexOf(' '));
            LastName = fullname.substring(fullname.lastIndexOf(" ")+1);
        } else {
            FirstName = fullname;
        }
        String fname = "";
        String lname = "";
        if (FirstName != null && !FirstName.equals("")) {
            fname = FirstName.substring(0, 1).toUpperCase() + FirstName.substring(1).toLowerCase();
        }
        if (LastName != null && !LastName.equals("")) {
            lname = LastName.substring(0, 1).toUpperCase() + LastName.substring(1).toLowerCase();
        }
        String name = fname + " " + lname;

        return name;
    }
}

