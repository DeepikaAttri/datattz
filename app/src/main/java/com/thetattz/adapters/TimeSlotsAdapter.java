package com.thetattz.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.thetattz.R;
import com.thetattz.interfaces.SelectTimeSlotInterface;
import com.thetattz.models.FreeTimeSlotsModel;

import java.util.ArrayList;

/**
 * Created by android-da on 6/7/18.
 */

public class TimeSlotsAdapter extends RecyclerView.Adapter<TimeSlotsAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<FreeTimeSlotsModel> mArrayList;
    SelectTimeSlotInterface mSelectTimeSlotInterface;
    private int selectedPosition = -1;// no selection by default

    public TimeSlotsAdapter(Activity mActivity, ArrayList<FreeTimeSlotsModel> mArrayList, SelectTimeSlotInterface mSelectTimeSlotInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mSelectTimeSlotInterface = mSelectTimeSlotInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_time_slot, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final FreeTimeSlotsModel mModel = mArrayList.get(position);
        holder.txtTimeTV.setText(mModel.getStartTime());

        if (selectedPosition == position){
            holder.txtTimeTV.setBackgroundResource(R.drawable.bg_timeslot_select);
            holder.txtTimeTV.setTextColor(mActivity.getResources().getColor(R.color.colorBlue));
        }else{
            holder.txtTimeTV.setBackgroundResource(R.drawable.bg_timeslot_unselect);
            holder.txtTimeTV.setTextColor(mActivity.getResources().getColor(R.color.colorWhite));
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectTimeSlotInterface.getTimeSlot(mModel);
                selectedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTimeTV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTimeTV = itemView.findViewById(R.id.txtTimeTV);
        }
    }


}

