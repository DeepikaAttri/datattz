package com.thetattz.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.thetattz.R;
import com.thetattz.interfaces.ClientGalleryClickInterface;
import com.thetattz.models.MyTattozModel;

import java.util.ArrayList;

/**
 * Created by android-da on 6/7/18.
 */

public class MyTattozAdapter extends RecyclerView.Adapter<MyTattozAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<MyTattozModel> mArrayList;
    ClientGalleryClickInterface mClientGalleryClickInterface;

    public MyTattozAdapter(Activity mActivity, ArrayList<MyTattozModel> mArrayList,ClientGalleryClickInterface mClientGalleryClickInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mClientGalleryClickInterface = mClientGalleryClickInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_tattoz, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MyTattozModel mModel = mArrayList.get(position);

        Glide.with(mActivity).load(mModel.getImage())
//                .placeholder(R.drawable.ic_gallery_pp)
//                .error(R.drawable.ic_gallery_pp)
//                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(holder.imgTattozIV);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClientGalleryClickInterface.mClick(mModel,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgTattozIV;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgTattozIV = itemView.findViewById(R.id.imgTattozIV);
        }
    }



}

