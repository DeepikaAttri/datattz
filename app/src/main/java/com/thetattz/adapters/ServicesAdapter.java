package com.thetattz.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.thetattz.R;
import com.thetattz.interfaces.SelectServiceInterface;
import com.thetattz.models.ServicesModel;

import java.util.ArrayList;

/**
 * Created by android-da on 6/7/18.
 */

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<ServicesModel> mArrayList;
    SelectServiceInterface mSelectServiceInterface;
    private int selectedPosition = -1;// no selection by default

    public ServicesAdapter(Activity mActivity, ArrayList<ServicesModel> mArrayList, SelectServiceInterface mSelectServiceInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mSelectServiceInterface = mSelectServiceInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_services_, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        ServicesModel mModel = mArrayList.get(position);
        holder.txtServiceTV.setText(mModel.getService_name());
        holder.txtServiceTimeTV.setText(mModel.getDuration());
        holder.txtServicePriceTV.setText("$" + mModel.getPrice());

        if (mArrayList.size() - 1 == position)
            holder.mViewV.setVisibility(View.GONE);
        else
            holder.mViewV.setVisibility(View.VISIBLE);


        holder.checkBookCB.setChecked(selectedPosition == position);

        holder.checkBookCB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = holder.getAdapterPosition();
                mSelectServiceInterface.getSelectedService(mArrayList.get(selectedPosition));
                try {
                    notifyDataSetChanged();
                } catch (Exception e) {
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RadioButton checkBookCB;
        public TextView txtServiceTV, txtServiceTimeTV, txtServicePriceTV;
        public View mViewV;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            checkBookCB = itemView.findViewById(R.id.checkBookCB);
            txtServiceTV = itemView.findViewById(R.id.txtServiceTV);
            txtServiceTimeTV = itemView.findViewById(R.id.txtServiceTimeTV);
            txtServicePriceTV = itemView.findViewById(R.id.txtServicePriceTV);
            mViewV = itemView.findViewById(R.id.mViewV);
        }
    }


}

