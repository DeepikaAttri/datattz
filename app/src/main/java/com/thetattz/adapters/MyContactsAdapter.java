package com.thetattz.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.tamir7.contacts.Contact;
import com.thetattz.R;
import com.thetattz.interfaces.ContactClickInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by android-da on 6/7/18.
 */

public class MyContactsAdapter extends RecyclerView.Adapter<MyContactsAdapter.MyViewHolder> {
    Activity mActivity;
    List<Contact> mArrayList;
    ContactClickInterface mContactClickInterface;

    public MyContactsAdapter(Activity mActivity, List<Contact> mArrayList,ContactClickInterface mContactClickInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mContactClickInterface = mContactClickInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contacts, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Contact mModel = mArrayList.get(position);

        holder.txtContactNameTV.setText(mModel.getDisplayName());

        holder.txtContactNameTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContactClickInterface.getContactDetails(mModel);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtContactNameTV;
        public View mViewV;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtContactNameTV = itemView.findViewById(R.id.txtContactNameTV);
            mViewV = itemView.findViewById(R.id.mViewV);
        }
    }



    //This method will filter the list
    //here we are passing the filtered data
    //and assigning it to the list with notifydatasetchanged method
    public void filterList(ArrayList<Contact> filterdNames) {
        this.mArrayList = filterdNames;
        notifyDataSetChanged();
    }

}

