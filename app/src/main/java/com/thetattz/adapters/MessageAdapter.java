package com.thetattz.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.models.MessageModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by android-da on 6/7/18.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<MessageModel> mArrayList;

    public MessageAdapter(Activity mActivity, ArrayList<MessageModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_message_comment, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MessageModel mModel = mArrayList.get(position);

        holder.txtMessageTV.setText(mModel.getMessage());

        holder.txtDateTimeTV.setText(mModel.getMessage_date());

        if (mModel.getProfile_pic() != null && mModel.getProfile_pic().contains("http"))
            Picasso.with(mActivity).load(mModel.getProfile_pic())
                    .placeholder(R.drawable.ic_p_pp)
                    .error(R.drawable.ic_p_pp)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.imgProfilePicCIV);
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imgProfilePicCIV;
        public TextView txtMessageTV, txtDateTimeTV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgProfilePicCIV = itemView.findViewById(R.id.imgProfilePicCIV);
            txtMessageTV = itemView.findViewById(R.id.txtMessageTV);
            txtDateTimeTV = itemView.findViewById(R.id.txtDateTimeTV);
        }
    }
}

