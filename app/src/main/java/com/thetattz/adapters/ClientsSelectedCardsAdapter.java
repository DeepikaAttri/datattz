package com.thetattz.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.thetattz.R;
import com.thetattz.interfaces.SelectPaymentInterface;
import com.thetattz.models.CardDetailsModel;

import java.util.ArrayList;

/**
 * Created by android-da on 6/7/18.
 */

public class ClientsSelectedCardsAdapter extends RecyclerView.Adapter<ClientsSelectedCardsAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<CardDetailsModel> mArrayList;

    SelectPaymentInterface mInterfaceSelected;
    private int lastSelectedPosition = -1;


    public ClientsSelectedCardsAdapter(Activity mActivity, ArrayList<CardDetailsModel> mArrayList,SelectPaymentInterface mInterfaceSelected) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mInterfaceSelected = mInterfaceSelected;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_selected_card_detail, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final CardDetailsModel mModel = mArrayList.get(position);

        holder.txtCardNameTV.setText(mModel.getBrand());

        if (mModel.getLast4().equals("0000")) {
            holder.txtCardNameTV.setVisibility(View.GONE);
            holder.imgCardImageIV.setVisibility(View.GONE);
            holder.txtCardNumberTV.setText(mActivity.getResources().getString(R.string.pay_with_other_card));
        }else{
            holder.imgCardImageIV.setVisibility(View.VISIBLE);
            holder.txtCardNameTV.setVisibility(View.VISIBLE);
            holder.txtCardNumberTV.setText(mActivity.getResources().getString(R.string.ending_with) + " " + mModel.getLast4());
        }
        if (mModel.getBrand().toLowerCase().equals("visa")) {
            holder.imgCardImageIV.setImageResource(R.drawable.ic_visa);
        } else if (mModel.getBrand().toLowerCase().equals("discover")) {
            holder.imgCardImageIV.setImageResource(R.drawable.ic_discover);
        } else if (mModel.getBrand().toLowerCase().equals("american express")) {
            holder.imgCardImageIV.setImageResource(R.drawable.ic_ae);
        } else if (mModel.getBrand().toLowerCase().equals("mastercard")) {
            holder.imgCardImageIV.setImageResource(R.drawable.ic_mc);
        } else {
            holder.imgCardImageIV.setImageResource(R.drawable.ic_other);
        }




        //since only one radio button is allowed to be selected,
        // this condition un-checks previous selections
        holder.rbMobilePayRB.setChecked(lastSelectedPosition == position);

        holder.rbMobilePayRB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastSelectedPosition = position;
                notifyDataSetChanged();
                mInterfaceSelected.getSelectedPayment(position,mModel);
            }

        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgCardImageIV;
        public TextView txtCardNameTV, txtCardNumberTV;
        public RadioButton rbMobilePayRB;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgCardImageIV = itemView.findViewById(R.id.imgCardImageIV);
            txtCardNameTV = itemView.findViewById(R.id.txtCardNameTV);
            txtCardNumberTV = itemView.findViewById(R.id.txtCardNumberTV);
            rbMobilePayRB = itemView.findViewById(R.id.rbMobilePayRB);
        }
    }


}

