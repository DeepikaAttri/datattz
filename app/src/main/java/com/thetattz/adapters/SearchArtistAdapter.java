package com.thetattz.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.activities.ArtistDetailsActivity;
import com.thetattz.models.SearchArtistModel;
import com.thetattz.utils.Constants;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by android-da on 6/7/18.
 */

public class SearchArtistAdapter extends RecyclerView.Adapter<SearchArtistAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<SearchArtistModel> mArrayList;

    public SearchArtistAdapter(Activity mActivity, ArrayList<SearchArtistModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SearchArtistModel mModel = mArrayList.get(position);
        holder.txtNameTV.setText(CapitalizedFullName(mModel.getName()));
        holder.txtUserNameFirstLetterTV.setText(mModel.getName());

        holder.txtShopNameTV.setText(mModel.getShopname());
        holder.txtAddressTV.setText(mModel.getStreet_address() + ", " + mModel.getCity());
        if (mModel.getRating() != null && mModel.getRating().length() > 0) {
            holder.txtRatingValueTV.setText(mModel.getRating());
            holder.mRatingBarRB.setRating(Float.parseFloat(mModel.getRating()));
            holder.mRatingBarRB.setEnabled(false);
        }

        if (mModel.getProfile_pic() != null && mModel.getProfile_pic().contains("http")) {
            holder.txtUserNameFirstLetterTV.setVisibility(View.GONE);
            holder.imgProfilePicCIV.setVisibility(View.VISIBLE);
            Picasso.with(mActivity).load(mModel.getProfile_pic())
                    .placeholder(R.drawable.ic_p_pp)
                    .error(R.drawable.ic_p_pp)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.imgProfilePicCIV);

        } else {
            holder.txtUserNameFirstLetterTV.setVisibility(View.VISIBLE);
            holder.imgProfilePicCIV.setVisibility(View.GONE);
        }

        if (mArrayList.size() - 1 == position)
            holder.mViewV.setVisibility(View.GONE);
        else
            holder.mViewV.setVisibility(View.VISIBLE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, ArtistDetailsActivity.class);
                mIntent.putExtra(Constants.MODEL_NEW, mModel);
                mActivity.startActivity(mIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    //This method will filter the list
    //here we are passing the filtered data
    //and assigning it to the list with notifydatasetchanged method
    public void filterList(ArrayList<SearchArtistModel> mArrayList) {
        this.mArrayList = mArrayList;
        if (mArrayList.size() > 0)
            notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imgProfilePicCIV;
        public TextView txtNameTV, txtShopNameTV, txtAddressTV, txtRatingValueTV, txtUserNameFirstLetterTV;
        public MaterialRatingBar mRatingBarRB;
        public View mViewV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgProfilePicCIV = itemView.findViewById(R.id.imgProfilePicCIV);
            txtNameTV = itemView.findViewById(R.id.txtNameTV);
            txtShopNameTV = itemView.findViewById(R.id.txtShopNameTV);
            txtAddressTV = itemView.findViewById(R.id.txtAddressTV);
            txtRatingValueTV = itemView.findViewById(R.id.txtRatingValueTV);
            mRatingBarRB = itemView.findViewById(R.id.mRatingBarRB);
            txtUserNameFirstLetterTV = itemView.findViewById(R.id.txtUserNameFirstLetterTV);

            mViewV = itemView.findViewById(R.id.mViewV);
        }
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFullName(String fullname) {
        String[] firstLastName = {};
        String FirstName = "", LastName = "";
        if (fullname != null && fullname.contains(" ")) {
            firstLastName = fullname.split(" ");
            FirstName = fullname.substring(0, fullname.lastIndexOf(' '));
            LastName = fullname.substring(fullname.lastIndexOf(" ")+1);
        } else {
            FirstName = fullname;
        }
        String fname = "";
        String lname = "";
        if (FirstName != null && !FirstName.equals("")) {
            fname = FirstName.substring(0, 1).toUpperCase() + FirstName.substring(1).toLowerCase();
        }
        if (LastName != null && !LastName.equals("")) {
            lname = LastName.substring(0, 1).toUpperCase() + LastName.substring(1).toLowerCase();
        }
        String name = fname + " " + lname;

        return name;
    }
}

