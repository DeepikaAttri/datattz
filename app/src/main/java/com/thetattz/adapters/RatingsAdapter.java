package com.thetattz.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.models.ArtistRatingModel;
import com.thetattz.views.RelativeTimeTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class RatingsAdapter extends RecyclerView.Adapter<RatingsAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<ArtistRatingModel> mArrayList;

    public RatingsAdapter(Activity mActivity, ArrayList<ArtistRatingModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ratings_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ArtistRatingModel mModel = mArrayList.get(position);

        if (mModel.getProfile_pic() != null && mModel.getProfile_pic().contains("http")) {
            Picasso.with(mActivity).load(mModel.getProfile_pic())
                    .placeholder(R.drawable.ic_p_pp)
                    .error(R.drawable.ic_p_pp)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.imgProfilePicCIV);
        }

        holder.txtNameTV.setText(CapitalizedFullName(mModel.getName()));
        holder.reviewTv.setText(mModel.getReview());
        holder.timeTv.setReferenceTime(convertTimeStringToLongTime(mModel.getCreation_time()));
        holder.mRatingBar.setRating(Float.parseFloat(mModel.getRating()));
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProfilePicCIV;
        TextView txtNameTV, reviewTv;
        RelativeTimeTextView timeTv;
        RatingBar mRatingBar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imgProfilePicCIV = itemView.findViewById(R.id.imgProfilePicCIV);
            txtNameTV = itemView.findViewById(R.id.txtNameTV);
            reviewTv = itemView.findViewById(R.id.reviewTv);
            timeTv = itemView.findViewById(R.id.timeTv);
            mRatingBar = itemView.findViewById(R.id.mRatingBar);
        }
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFullName(String fullname) {
        String[] firstLastName = {};
        String FirstName = "", LastName = "";
        if (fullname != null && fullname.contains(" ")) {
            firstLastName = fullname.split(" ");
            FirstName = fullname.substring(0, fullname.lastIndexOf(' '));
            LastName = fullname.substring(fullname.lastIndexOf(" ")+1);
        } else {
            FirstName = fullname;
        }
        String fname = "";
        String lname = "";
        if (FirstName != null && !FirstName.equals("")) {
            fname = FirstName.substring(0, 1).toUpperCase() + FirstName.substring(1).toLowerCase();
        }
        if (LastName != null && !LastName.equals("")) {
            lname = LastName.substring(0, 1).toUpperCase() + LastName.substring(1).toLowerCase();
        }
        String name = fname + " " + lname;

        return name;
    }

    /*
     * Convert Time String to Milliseconds //08-04-2020 12:45 PM
     * */
    public long convertTimeStringToLongTime(String strDate) {
        long millisecondsSinceEpoch = 0;
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
        Date parseDate = null;
        try {
            parseDate = f.parse(strDate);
            millisecondsSinceEpoch = parseDate.getTime();
            return millisecondsSinceEpoch;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return millisecondsSinceEpoch;
    }
}
