package com.thetattz.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.models.ArtistRatingModel;
import com.thetattz.views.RelativeTimeTextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by android-da on 6/7/18.
 */

public class ArtistRatingAdapter extends RecyclerView.Adapter<ArtistRatingAdapter.MyViewHolder> {
    private final int LIMIT = 2;
    Activity mActivity;
    ArrayList<ArtistRatingModel> mArrayList;

    public ArtistRatingAdapter(Activity mActivity, ArrayList<ArtistRatingModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_artist_rating_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ArtistRatingModel mModel = mArrayList.get(position);

        holder.txtFirstCapitalTV.setText(mModel.getName());
        holder.txtUserNameTV.setText(mModel.getName());
        holder.txtReviewTV.setText(mModel.getReview());
        holder.mRatingBarRB.setRating(Float.parseFloat(mModel.getRating()));

        if (mModel.getProfile_pic() != null && mModel.getProfile_pic().contains("http")) {
            Picasso.with(mActivity).load(mModel.getProfile_pic())
                    .placeholder(R.drawable.ic_p_pp)
                    .error(R.drawable.ic_p_pp)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.imgProfilePicCIV);

            holder.imgProfilePicCIV.setVisibility(View.VISIBLE);
            holder.txtFirstCapitalTV.setVisibility(View.GONE);
        } else {
            holder.imgProfilePicCIV.setVisibility(View.GONE);
            holder.txtFirstCapitalTV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        if (mArrayList.size() > LIMIT) {
            return LIMIT;
        } else {
            return mArrayList.size();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtFirstCapitalTV;
        public CircleImageView imgProfilePicCIV;
        public TextView txtUserNameTV;
        public MaterialRatingBar mRatingBarRB;
        public TextView txtReviewTV;
        public RelativeTimeTextView txtDateTimeTV;
        public View mViewV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtFirstCapitalTV = itemView.findViewById(R.id.txtFirstCapitalTV);
            imgProfilePicCIV = itemView.findViewById(R.id.imgProfilePicCIV);
            txtUserNameTV = itemView.findViewById(R.id.txtUserNameTV);
            mRatingBarRB = itemView.findViewById(R.id.mRatingBarRB);
            txtReviewTV = itemView.findViewById(R.id.txtReviewTV);
            txtDateTimeTV = itemView.findViewById(R.id.txtDateTimeTV);
            mViewV = itemView.findViewById(R.id.mViewV);
        }
    }
}

