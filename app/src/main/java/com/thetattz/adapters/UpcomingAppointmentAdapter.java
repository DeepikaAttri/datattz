package com.thetattz.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.activities.UpcomingAppointDetailsActivity;
import com.thetattz.models.AppointmentsModel;
import com.thetattz.utils.Constants;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by android-da on 6/7/18.
 */

public class UpcomingAppointmentAdapter extends RecyclerView.Adapter<UpcomingAppointmentAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<AppointmentsModel> mArrayList;

    public UpcomingAppointmentAdapter(Activity mActivity, ArrayList<AppointmentsModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_upcoming_appointments, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AppointmentsModel mModel = mArrayList.get(position);
        holder.txtNameTV.setText(CapitalizedFullName(mModel.getName()));
        holder.txtShopNameTV.setText(mModel.getCity());
        holder.txtTimeTV.setText(mModel.getStart_time() + " " + mModel.getEnd_time()
                + "(" + mModel.getTimezoneAbr() + ")");

        if (mModel.getProfile_pic() != null && mModel.getProfile_pic().contains("http"))
            Picasso.with(mActivity).load(mModel.getProfile_pic())
                    .placeholder(R.drawable.ic_p_pp)
                    .error(R.drawable.ic_p_pp)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.imgProfilePicCIV);

        if (mArrayList.size() - 1 == position)
            holder.mViewV.setVisibility(View.GONE);
        else
            holder.mViewV.setVisibility(View.VISIBLE);

        String[] dateArray = mModel.getDate().split("-");
        String strDay = dateArray[0];
        String strMonthName = gettingMonthNameFromNumber(dateArray[1]);
        holder.txtMonthFormatTV.setText(strMonthName);
        holder.txtDayFormatTV.setText(strDay);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, UpcomingAppointDetailsActivity.class);
                mIntent.putExtra(Constants.MODEL, mModel);
                mActivity.startActivity(mIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imgProfilePicCIV;
        public TextView txtNameTV, txtShopNameTV, txtTimeTV, txtDayFormatTV, txtMonthFormatTV;
        public View mViewV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgProfilePicCIV = itemView.findViewById(R.id.imgProfilePicCIV);
            txtNameTV = itemView.findViewById(R.id.txtNameTV);
            txtShopNameTV = itemView.findViewById(R.id.txtShopNameTV);
            txtTimeTV = itemView.findViewById(R.id.txtTimeTV);
            txtDayFormatTV = itemView.findViewById(R.id.txtDayFormatTV);
            txtMonthFormatTV = itemView.findViewById(R.id.txtMonthFormatTV);

            mViewV = itemView.findViewById(R.id.mViewV);
        }
    }

    private String gettingMonthNameFromNumber(String strNum) {
        if (strNum.equals("01")) {
            return "JAN";
        } else if (strNum.equals("02")) {
            return "FEB";
        } else if (strNum.equals("03")) {
            return "MAR";
        } else if (strNum.equals("04")) {
            return "APR";
        } else if (strNum.equals("05")) {
            return "MAY";
        } else if (strNum.equals("06")) {
            return "JUN";
        } else if (strNum.equals("07")) {
            return "JUL";
        } else if (strNum.equals("08")) {
            return "AUG";
        } else if (strNum.equals("09")) {
            return "SEP";
        } else if (strNum.equals("10")) {
            return "OCT";
        } else if (strNum.equals("11")) {
            return "NOV";
        } else if (strNum.equals("12")) {
            return "DEC";
        } else {
            return "";
        }
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFullName(String fullname) {
        String[] firstLastName = {};
        String FirstName = "", LastName = "";
        if (fullname != null && fullname.contains(" ")) {
            firstLastName = fullname.split(" ");
            FirstName = fullname.substring(0, fullname.lastIndexOf(' '));
            LastName = fullname.substring(fullname.lastIndexOf(" ") + 1);
        } else {
            FirstName = fullname;
        }
        String fname = "";
        String lname = "";
        if (FirstName != null && !FirstName.equals("")) {
            fname = FirstName.substring(0, 1).toUpperCase() + FirstName.substring(1).toLowerCase();
        }
        if (LastName != null && !LastName.equals("")) {
            lname = LastName.substring(0, 1).toUpperCase() + LastName.substring(1).toLowerCase();
        }
        String name = fname + " " + lname;

        return name;
    }
}

