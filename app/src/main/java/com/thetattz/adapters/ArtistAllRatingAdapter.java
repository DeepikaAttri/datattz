package com.thetattz.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.models.ArtistRatingModel;
import com.thetattz.views.RelativeTimeTextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by android-da on 6/7/18.
 */

public class ArtistAllRatingAdapter extends RecyclerView.Adapter<ArtistAllRatingAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<ArtistRatingModel> mArrayList;

    public ArtistAllRatingAdapter(Activity mActivity, ArrayList<ArtistRatingModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_artist_rating_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ArtistRatingModel mModel = mArrayList.get(position);

        holder.txtFirstCapitalTV.setText(mModel.getName());
        holder.txtUserNameTV.setText(CapitalizedFullName(mModel.getName()));
        holder.txtReviewTV.setText(mModel.getReview());
        holder.mRatingBarRB.setRating(Float.parseFloat(mModel.getRating()));

        if (mModel.getProfile_pic() != null && mModel.getProfile_pic().contains("http")) {
            Picasso.with(mActivity).load(mModel.getProfile_pic())
                    .placeholder(R.drawable.ic_p_pp)
                    .error(R.drawable.ic_p_pp)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.imgProfilePicCIV);

            holder.imgProfilePicCIV.setVisibility(View.VISIBLE);
            holder.txtFirstCapitalTV.setVisibility(View.GONE);
        } else {
            holder.imgProfilePicCIV.setVisibility(View.GONE);
            holder.txtFirstCapitalTV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtFirstCapitalTV;
        public CircleImageView imgProfilePicCIV;
        public TextView txtUserNameTV;
        public MaterialRatingBar mRatingBarRB;
        public TextView txtReviewTV;
        public RelativeTimeTextView txtDateTimeTV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtFirstCapitalTV = itemView.findViewById(R.id.txtFirstCapitalTV);
            imgProfilePicCIV = itemView.findViewById(R.id.imgProfilePicCIV);
            txtUserNameTV = itemView.findViewById(R.id.txtUserNameTV);
            mRatingBarRB = itemView.findViewById(R.id.mRatingBarRB);
            txtReviewTV = itemView.findViewById(R.id.txtReviewTV);
            txtDateTimeTV = itemView.findViewById(R.id.txtDateTimeTV);
        }
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFullName(String fullname) {
        String[] firstLastName = {};
        String FirstName = "", LastName = "";
        if (fullname != null && fullname.contains(" ")) {
            firstLastName = fullname.split(" ");
            FirstName = fullname.substring(0, fullname.lastIndexOf(' '));
            LastName = fullname.substring(fullname.lastIndexOf(" ")+1);
        } else {
            FirstName = fullname;
        }
        String fname = "";
        String lname = "";
        if (FirstName != null && !FirstName.equals("")) {
            fname = FirstName.substring(0, 1).toUpperCase() + FirstName.substring(1).toLowerCase();
        }
        if (LastName != null && !LastName.equals("")) {
            lname = LastName.substring(0, 1).toUpperCase() + LastName.substring(1).toLowerCase();
        }
        String name = fname + " " + lname;

        return name;
    }
}

