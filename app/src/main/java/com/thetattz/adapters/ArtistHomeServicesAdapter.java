package com.thetattz.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.thetattz.R;
import com.thetattz.interfaces.DeleteServiceInterface;
import com.thetattz.models.ArtistHomeServiceModel;

import java.util.ArrayList;

/**
 * Created by android-da on 6/7/18.
 */

public class ArtistHomeServicesAdapter extends RecyclerView.Adapter<ArtistHomeServicesAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<ArtistHomeServiceModel> mArrayList;
    DeleteServiceInterface mDeleteServiceInterface;

    public ArtistHomeServicesAdapter(Activity mActivity, ArrayList<ArtistHomeServiceModel> mArrayList, DeleteServiceInterface mDeleteServiceInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mDeleteServiceInterface = mDeleteServiceInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_artist_services_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ArtistHomeServiceModel mModel = mArrayList.get(position);
        holder.txtServiceNameTV.setText(mModel.getService_name());
        holder.txtServiceTimeTV.setText(mModel.getDuration());
        holder.txtServicePriveTV.setText("$" + mModel.getPrice());
        holder.txtServiceDescriptionTV.setText(mModel.getDescription());
        holder.deleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDeleteServiceInterface.mDeleteService(mModel);
            }
        });

        if (position == mArrayList.size() - 1)
            holder.mViewV.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtServiceNameTV, txtServicePriveTV, txtServiceTimeTV,txtServiceDescriptionTV;
        public com.chauthai.swipereveallayout.SwipeRevealLayout swipeRL;
        public ImageView deleteIV;
        public View mViewV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtServiceNameTV = itemView.findViewById(R.id.txtServiceNameTV);
            txtServicePriveTV = itemView.findViewById(R.id.txtServicePriveTV);
            txtServiceTimeTV = itemView.findViewById(R.id.txtServiceTimeTV);
            txtServiceDescriptionTV = itemView.findViewById(R.id.txtServiceDescriptionTV);
            swipeRL = itemView.findViewById(R.id.swipeRL);
            deleteIV = itemView.findViewById(R.id.deleteIV);
            mViewV = itemView.findViewById(R.id.mViewV);
        }
    }
}

