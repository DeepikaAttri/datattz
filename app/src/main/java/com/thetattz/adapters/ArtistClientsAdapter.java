package com.thetattz.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.models.ArtistClientModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by android-da on 6/7/18.
 */


public class ArtistClientsAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private static final String TAG = "ArtistClientsAdapter";


    Activity activity;

    ArrayList<ArtistClientModel> mArrayList = new ArrayList<>();


    private LayoutInflater inflater;


    public ArtistClientsAdapter(FragmentActivity activity, ArrayList<ArtistClientModel> mArrayList) {

        this.activity = activity;
        this.mArrayList = mArrayList;
        inflater = LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ArtistClientModel tempValue = mArrayList.get(position);
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_artist_clients, parent, false);
            holder.imgProfilePicCIV = convertView.findViewById(R.id.imgProfilePicCIV);
            holder.txtClientNameTV = convertView.findViewById(R.id.txtClientNameTV);
            holder.txtFirstCapitalTV = convertView.findViewById(R.id.txtFirstCapitalTV);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        try {

            holder.txtClientNameTV.setText(CapitalizedFullName(tempValue.getName()));
            holder.txtFirstCapitalTV.setText(tempValue.getName());
            if (tempValue.getProfile_pic() != null && tempValue.getProfile_pic().contains("http")) {
                holder.txtFirstCapitalTV.setVisibility(View.GONE);
                holder.imgProfilePicCIV.setVisibility(View.VISIBLE);
//                Picasso.with(activity).load(tempValue.getProfile_pic())
//                        .placeholder(R.drawable.ic_p_pp)
//                        .error(R.drawable.ic_p_pp)
//                        .memoryPolicy(MemoryPolicy.NO_CACHE)
//                        .into(holder.imgProfilePicCIV);

                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.ic_p_pp);
                requestOptions.error(R.drawable.ic_p_pp);

                Glide.with(activity)
                        .setDefaultRequestOptions(requestOptions)
                        .load(tempValue.getProfile_pic())
                        .into(holder.imgProfilePicCIV);

            } else {
                holder.txtFirstCapitalTV.setVisibility(View.VISIBLE);
                holder.imgProfilePicCIV.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }


    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.header_layout, parent, false);
            holder.headerTV = convertView.findViewById(R.id.headerTV);

            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name
//        String headerText = "" + mArrayList.get(position).getName();
        if (mArrayList.get(position).getName() != null && mArrayList.get(position).getName().length() > 0) {
            String headerText = "" + mArrayList.get(position).getName().subSequence(0, 1).charAt(0);
            holder.headerTV.setText(headerText);
        }
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        //return the first character of the country as ID because this is what headers are based upon
        if (mArrayList.size() > 0)
            return mArrayList.get(position).getName().subSequence(0, 1).charAt(0);
        else
            return 0;
    }

    class HeaderViewHolder {
        TextView headerTV;
    }

    class ViewHolder {
        CircleImageView imgProfilePicCIV;
        TextView txtClientNameTV, txtFirstCapitalTV;
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFullName(String fullname) {
        String[] firstLastName = {};
        String FirstName = "", LastName = "";
        if (fullname != null && fullname.contains(" ")) {
            firstLastName = fullname.split(" ");
            FirstName = fullname.substring(0, fullname.lastIndexOf(' '));
            LastName = fullname.substring(fullname.lastIndexOf(" ") + 1);
        } else {
            FirstName = fullname;
        }
        String fname = "";
        String lname = "";
        if (FirstName != null && !FirstName.equals("")) {
            fname = FirstName.substring(0, 1).toUpperCase() + FirstName.substring(1).toLowerCase();
        }
        if (LastName != null && !LastName.equals("")) {
            lname = LastName.substring(0, 1).toUpperCase() + LastName.substring(1).toLowerCase();
        }
        String name = fname + " " + lname;

        return name;
    }
}

//        extends SectioningAdapter {
//    Activity mActivity;
//    ArrayList<ArtistClientModel> mArrayList;
//
//    public ArtistClientsAdapter(Activity mActivity, ArrayList<ArtistClientModel> mArrayList) {
//        this.mActivity = mActivity;
//        this.mArrayList = mArrayList;
//    }
//
//    @NonNull
//    @Override
//    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.item_artist_clients, parent, false);
//        return new MyViewHolder(itemView);
//    }
//
//    public void onBindViewHolder(MyViewHolder holder, int position) {
//        ArtistClientModel mModel = mArrayList.get(position);
//
//        holder.txtClientNameTV.setText(mModel.getName());
//
//        if (mModel.getProfile_pic() != null && mModel.getProfile_pic().contains("http")) {
//            holder.imgProfilePicCIV.setVisibility(View.VISIBLE);
//            holder.txtFirstCapitalTV.setVisibility(View.GONE);
//            Picasso.with(mActivity).load(mModel.getProfile_pic())
//                    .placeholder(R.drawable.ic_p_pp)
//                    .error(R.drawable.ic_p_pp)
//                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                    .into(holder.imgProfilePicCIV);
//        }else{
//            holder.imgProfilePicCIV.setVisibility(View.GONE);
//            holder.txtFirstCapitalTV.setVisibility(View.VISIBLE);
//            holder.txtFirstCapitalTV.setText(mModel.getName());
//        }
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return mArrayList.size();
//    }
//
//    public class MyViewHolder extends SectioningAdapter.ItemViewHolder  {
//        public CircleImageView imgProfilePicCIV;
//        public TextView txtClientNameTV,txtFirstCapitalTV;
//
//
//        public MyViewHolder(@NonNull View itemView) {
//            super(itemView);
//            imgProfilePicCIV = itemView.findViewById(R.id.imgProfilePicCIV);
//            txtClientNameTV = itemView.findViewById(R.id.txtClientNameTV);
//            txtFirstCapitalTV = itemView.findViewById(R.id.txtFirstCapitalTV);
//        }
//    }
//
//
//}
//
