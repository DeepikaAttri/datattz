package com.thetattz.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.interfaces.SeeMoreClickInterface;
import com.thetattz.models.MyTattozModel;

import java.util.ArrayList;


/**
 * Created by android-da on 6/7/18.
 */

public class ArtistShowImagesAdapter extends RecyclerView.Adapter<ArtistShowImagesAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<MyTattozModel> mArrayList;
    SeeMoreClickInterface mSeeMoreClickInterface;

    public ArtistShowImagesAdapter(Activity mActivity, ArrayList<MyTattozModel> mArrayList,SeeMoreClickInterface mSeeMoreClickInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mSeeMoreClickInterface = mSeeMoreClickInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_tattoz, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final MyTattozModel mModel = mArrayList.get(position);

        if (mModel.getImage() != null && mModel.getImage().contains("http")){
            Picasso.with(mActivity).load(mModel.getImage().trim().replaceAll(" ", "%20"))
                    .placeholder(R.drawable.ic_placeholder)
                    .error(R.drawable.ic_placeholder)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.imgTattozIV);
        }else{
            holder.imgTattozIV.setImageResource(R.drawable.ic_placeholder);
        }



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSeeMoreClickInterface.mClick(mModel,position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgTattozIV;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgTattozIV = itemView.findViewById(R.id.imgTattozIV);
        }
    }



}

