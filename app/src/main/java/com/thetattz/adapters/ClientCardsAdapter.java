package com.thetattz.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.thetattz.R;
import com.thetattz.models.CardDetailsModel;

import java.util.ArrayList;

/**
 * Created by android-da on 6/7/18.
 */

public class ClientCardsAdapter extends RecyclerView.Adapter<ClientCardsAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<CardDetailsModel> mArrayList;

    public ClientCardsAdapter(Activity mActivity, ArrayList<CardDetailsModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card_detail, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CardDetailsModel mModel = mArrayList.get(position);

        holder.txtCardNameTV.setText(mModel.getBrand());
        holder.txtCardNumberTV.setText(mActivity.getResources().getString(R.string.ending_with) + " " + mModel.getLast4());

        if (mModel.getBrand().toLowerCase().equals("visa")) {
            holder.imgCardImageIV.setImageResource(R.drawable.ic_visa);
        } else if (mModel.getBrand().toLowerCase().equals("discover")) {
            holder.imgCardImageIV.setImageResource(R.drawable.ic_discover);
        } else if (mModel.getBrand().toLowerCase().equals("american express")) {
            holder.imgCardImageIV.setImageResource(R.drawable.ic_ae);
        } else if (mModel.getBrand().toLowerCase().equals("mastercard")) {
            holder.imgCardImageIV.setImageResource(R.drawable.ic_mc);
        } else {
            holder.imgCardImageIV.setImageResource(R.drawable.ic_other);
        }

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgCardImageIV;
        public TextView txtCardNameTV, txtCardNumberTV;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgCardImageIV = itemView.findViewById(R.id.imgCardImageIV);
            txtCardNameTV = itemView.findViewById(R.id.txtCardNameTV);
            txtCardNumberTV = itemView.findViewById(R.id.txtCardNumberTV);
        }
    }


}

