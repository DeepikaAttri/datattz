package com.thetattz.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thetattz.R;
import com.thetattz.TheTattzApplication;
import com.thetattz.activities.BaseActivity;
import com.thetattz.helper.TouchImageView;
import com.thetattz.models.MyTattozModel;
import com.thetattz.utils.ConnectivityReceiver;
import com.thetattz.utils.Constants;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DetailsSlidingImageAdapter extends PagerAdapter {

    String TAG = DetailsSlidingImageAdapter.this.getClass().getSimpleName();
    Activity mActivity;
    ArrayList<MyTattozModel> mMyTattozArrayList;
    private LayoutInflater inflater;


    public DetailsSlidingImageAdapter(Activity mActivity, Context context, ArrayList<MyTattozModel> mMyTattozArrayList) {
        this.mActivity = mActivity;
        this.mMyTattozArrayList = mMyTattozArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mMyTattozArrayList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.item_sliding_pages, view, false);

        assert imageLayout != null;

        TouchImageView imageView = imageLayout.findViewById(R.id.image);
        RelativeLayout rlBackRL = imageLayout.findViewById(R.id.rlBackRL);
        RelativeLayout rlDeleteRL = imageLayout.findViewById(R.id.rlDeleteRL);


        final MyTattozModel mModel = mMyTattozArrayList.get(position);
        String img = mModel.getImage();
        Log.e("Image", "image  " + img);

        Picasso.with(mActivity).load(mModel.getImage().trim().replaceAll(" ", "%20"))
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(imageView);


        rlBackRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.finish();
            }
        });

        rlDeleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDeleteDialog(mModel);
            }
        });




        view.addView(imageLayout, 0);


        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }



    @Override
    public int getItemPosition(Object object) {
        for(int index = 0 ; index < getCount() ; index++){
            if((MyTattozModel)object == mMyTattozArrayList.get(index)) {
                return index;
            }
        }
        return POSITION_NONE;
    }



    /*
     *
     * Delete Alert Dialog
     * */
    public void showDeleteDialog(final MyTattozModel mMyTattozModel) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_delete_item);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView txtCancelTV = alertDialog.findViewById(R.id.txtCancelTV);
        TextView txtDeleteTV = alertDialog.findViewById(R.id.txtDeleteTV);
        txtMessageTV.setText(mActivity.getString(R.string.do_you));
        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        txtDeleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mDeleteApi(mMyTattozModel);
            }
        });
        alertDialog.show();
    }



    private void mDeleteApi(MyTattozModel mMyTattozModel) {
        if (ConnectivityReceiver.isConnected())
            executeDeleteImage(mMyTattozModel);
        else
            ((BaseActivity)mActivity).showToast(mActivity, mActivity.getString(R.string.internet_connection_error));
    }

    /*
     *
     * Execute delete Image
     * */

    private void executeDeleteImage(final MyTattozModel mMyTattozModel) {
        ((BaseActivity)mActivity).showProgressDialog(mActivity);
        final String mApiUrl = Constants.DELETE_TATTOOS;
        Log.e(TAG, "**Api**" + mApiUrl);
        JSONObject mParams = new JSONObject();
        try {
            mParams.put("id", mMyTattozModel.getId());
            Log.e(TAG, "**Params**" + mParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* prepare the Request */
        JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, mParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ((BaseActivity)mActivity).dismissProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.toString());
                        parseDeleteResponse(response, mMyTattozModel);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ((BaseActivity)mActivity).dismissProgressDialog();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-type", "application/json");
                return headers;
            }

        };

        // add it to the RequestQueue
        TheTattzApplication.getInstance().addToRequestQueue(mJsonObjectRequest);
    }

    private void parseDeleteResponse(JSONObject response, MyTattozModel mMyTattozModel) {
        try {
            if (response.getString("status").equals("1")) {
                ((BaseActivity)mActivity).showToast(mActivity, mActivity.getString(R.string.pic_del_success));
                Intent mIntent = new Intent();
                mIntent.putExtra(Constants.POSITION,getItemPosition(mMyTattozModel));
                mActivity.setResult(Constants.REQUEST_CODE,mIntent);
                mActivity.finish();
            } else {
                ((BaseActivity)mActivity).showToast(mActivity, response.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "**Error**" + e.toString());
        }
    }



}