package com.thetattz.interfaces;

import com.thetattz.models.ServicesModel;

public interface SelectServiceInterface  {
    public void getSelectedService(ServicesModel mServicesModel);
}
