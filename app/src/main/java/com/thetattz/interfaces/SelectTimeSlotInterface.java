package com.thetattz.interfaces;

import com.thetattz.models.FreeTimeSlotsModel;

public interface SelectTimeSlotInterface {
    public void getTimeSlot(FreeTimeSlotsModel mFreeTimeSlotsModel);
}
