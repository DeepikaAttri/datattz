package com.thetattz.interfaces;

import com.thetattz.models.CardDetailsModel;

public interface SelectPaymentInterface {
    void getSelectedPayment(int position, CardDetailsModel mModel);
}
