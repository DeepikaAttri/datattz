package com.thetattz.interfaces;

public interface RadioSelectionCallBack {
    public void  getRadioSelection(boolean isBoolean);
}
