package com.thetattz.interfaces;

import com.thetattz.models.FreeTimeSlotsModel;

public interface ArtistSelectTimeSlotInterface {
    public void getTimeSlot(FreeTimeSlotsModel mTimeSlotsModel);
}
