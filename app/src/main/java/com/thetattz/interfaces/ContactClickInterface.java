package com.thetattz.interfaces;

import com.github.tamir7.contacts.Contact;

public interface ContactClickInterface {
    public void getContactDetails(Contact mContact);
}
