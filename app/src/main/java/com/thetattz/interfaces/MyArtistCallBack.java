package com.thetattz.interfaces;

import com.thetattz.models.MyArtistModel;

public interface MyArtistCallBack {
    public void clickToArtistDetials(MyArtistModel mModel);
    public void clickToBookAppointment(MyArtistModel mModel);
}
