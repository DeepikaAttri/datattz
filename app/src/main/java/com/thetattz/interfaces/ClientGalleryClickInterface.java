package com.thetattz.interfaces;

import com.thetattz.models.MyTattozModel;

public interface ClientGalleryClickInterface {
    public void mClick(MyTattozModel mMyTattozModel, int position);
}
