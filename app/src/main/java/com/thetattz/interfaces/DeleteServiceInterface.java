package com.thetattz.interfaces;

import com.thetattz.models.ArtistHomeServiceModel;

public interface DeleteServiceInterface {
    public void mDeleteService(ArtistHomeServiceModel mArtistHomeServiceModel);
}
